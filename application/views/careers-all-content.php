<section class="main">
    <div class="news-all banner-bg"></div><div class="wave clearfix"></div>
    <div class="news-update-list">
         <div class="heading txtcenter">
                        <h2 class="title-heading">CAREERS</h2>
                        <span>Ka-Chooks opportunity</span>
            </div><br class="clear"/><br/>
           <div class="update-list-content container">
                <div class="search-careers col-md-6 col-sm-12 relative-center txtcenter">
                            <span>Our business is always growing! Interested in working for a company full of opportunity and fulfillment? Have a look at the job openings below to see if we have a vacancy that you could fill.</span><br class="clear"/><br/>
                            <? if(isset($no_result)): ?>
                                <span style="color: red">No career found.</span>
                            <? endif; ?>

                            <div class="select-careers relative-center txtcenter">
                              <form action="<?= site_url('careers/search') ?>" method="GET" id="careers-form">
                                 <select class="col-md-5 col-sm-5 col-xs-10" name="field">
                                    <option>FIELD OF WORK</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Administration' ? 'selected' : '' ?>>Administration</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Animal Health Group' ? 'selected' : '' ?>>Animal Health Group</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Business / Product Development' ? 'selected' : '' ?>>Business / Product Development</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Compliance Assurance' ? 'selected' : '' ?> value="Compliance Assurance">Food Quality and Safety Audit</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Contract Growing' ? 'selected' : '' ?>>Contract Growing</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Sales - Rotisserie/Chooks-to-Go' ? 'selected' : '' ?>>Sales - Rotisserie/Chooks-to-Go</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Customer Service / Delivery' ? 'selected' : '' ?>>Customer Service / Delivery</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Dressing Plant' ? 'selected' : '' ?>>Dressing Plant</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Engineering' ? 'selected' : '' ?>>Engineering</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Feed Mill' ? 'selected' : '' ?>>Feed Mill</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Finance' ? 'selected' : '' ?>>Finance</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Human Resources' ? 'selected' : '' ?>>Human Resources</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Information Systems' ? 'selected' : '' ?>>Information Systems</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Legal' ? 'selected' : '' ?>>Legal</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Live Sales' ? 'selected' : '' ?>>Live Sales</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Logistics' ? 'selected' : '' ?>>Logistics</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Management Control System' ? 'selected' : '' ?> value="Management Control System">Corporate Governance / Planning</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Marketing' ? 'selected' : '' ?>>Marketing</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Materials Management /Purchasing' ? 'selected' : '' ?>>Materials Management /Purchasing</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Motor Pool Services' ? 'selected' : '' ?>>Motor Pool Services</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Sales - Supermarket/Trade Distributor' ? 'selected' : '' ?>>Sales - Supermarket/Trade Distributor</option>
                                    <option <?= isset($_GET['field']) && $_GET['field'] == 'Utility/Warehouse/Liaison' ? 'selected' : '' ?>>Utility/Warehouse/Liaison</option>
                                </select>
                                  <select class="col-md-5 col-sm-5 col-xs-10" name="location">
                                    <option>LOCATION</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Bacolod' ? 'selected' : '' ?>>Bacolod</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Batangas' ? 'selected' : '' ?>>Batangas</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Bicol' ? 'selected' : '' ?>>Bicol</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Cagayan de Oro' ? 'selected' : '' ?>>Cagayan de Oro</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Calbayog' ? 'selected' : '' ?>>Calbayog</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Cavite' ? 'selected' : '' ?>>Cavite</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Cebu' ? 'selected' : '' ?>>Cebu</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Central Luzon' ? 'selected' : '' ?>>Central Luzon</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Davao' ? 'selected' : '' ?>>Davao</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Dumaguete' ? 'selected' : '' ?>>Dumaguete</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'General Santos' ? 'selected' : '' ?>>General Santos</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Head Office - Pasig' ? 'selected' : '' ?>>Head Office - Pasig</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Ilocos' ? 'selected' : '' ?>>Ilocos</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Iloilo' ? 'selected' : '' ?>>Iloilo</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Isabela' ? 'selected' : '' ?>>Isabela</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Laguna' ? 'selected' : '' ?>>Laguna</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Metro Manila' ? 'selected' : '' ?>>Metro Manila</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Ormoc' ? 'selected' : '' ?>>Ormoc</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Pampanga' ? 'selected' : '' ?>>Pampanga</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Pangasinan' ? 'selected' : '' ?>>Pangasinan</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Roxas' ? 'selected' : '' ?>>Roxas</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Southern Tagalog' ? 'selected' : '' ?>>Southern Tagalog</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Tacloban' ? 'selected' : '' ?>>Tacloban</option>
                                    <option <?= isset($_GET['location']) && $_GET['location'] == 'Zamboanga' ? 'selected' : '' ?>>Zamboanga</option>
                                </select>

                            </div><br class="clear"/><br/><br/>
                            <div class="search-btn">
                                     <div class="btn-group find-store">
                                        <a class="btn btn-rounded" onclick="document.getElementById('careers-form').submit()">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </a>
                                        <button class="btn btn-glyph-rounded">SEARCH JOBS</button>
                                      </form>

                                    </div>
                            </div>

                    </div>

           </div><br class="clear"/><br/>
    </div>
    <!--end news-update-list-->
     <div class="wave clearfix"></div><br class="clear"/><br/>
</section>