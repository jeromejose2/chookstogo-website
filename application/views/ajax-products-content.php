<!-- PRODUCTS -->
<div class="cat-product owl-carousel owl-products-theme">
<? foreach($products as $k => $v): ?>
    <div class="item">
        <img class="img-responsive" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>" alt="slider image"><br/>
        <div class="txtcenter">
            <h4><?= $v['title'] ?></h4>
                <div class="btn-group padded txtcenter">
                    <a class="btn product-details-btn btn-glyph-default" href="javascript:void(0)" data-title="<?= $v['title'] ?>" data-description="<?= word_limiter(str_replace('</p>', '', str_replace('<p>', '', $v['description']))) ?>" data-orderurl ="http://www.chookstogodelivery.com.ph" data-fburl = "<?= site_url('products') ?>/<?= $v['id'] ?>" data-thumbnail="<?= str_replace('~path~', base_url(), $v['photo_thumb']) ?>" data-delivery="<?= $v['for_delivery'] ?>">DETAILS</a>
                    <a class="btn btn-glyph product-details-btn" href="#" data-title="<?= $v['title'] ?>" data-description="<?= word_limiter(str_replace('</p>', '', str_replace('<p>', '', $v['description']))) ?>" data-orderurl ="http://www.chookstogodelivery.com.ph" data-fburl = "<?= site_url('products') ?>/<?= $v['id'] ?>" data-thumbnail="<?= str_replace('~path~', base_url(), $v['photo_thumb']) ?>" data-delivery="<?= $v['for_delivery'] ?>">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
        </div>
    </div>
<? endforeach; ?>
</div>
<!-- END PRODUCTS -->

<script type="text/javascript">

        var screenWidth = $(window).width();

        $(".cat-product").owlCarousel({
            navigation: true, // Show next and prev buttons
            navigationText: ["<i class='glyphicon glyphicon-chevron-left'></i>", "<i class='glyphicon glyphicon-chevron-right'></i>"],
            paginationSpeed: 800,
            autoPlay: true,
            items : 4,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,3],
            itemsTablet: [768,3],
            itemsMobile :[480,2]
        });

        $('.product-details-btn').on('click',function(e){
            e.preventDefault();
            var animateHeight;
            if(screenWidth <= 767){
                animateHeight = 500
            }else if(screenWidth >= 768) {
                animateHeight = 300
            }
            var itemDetails = $(this).data();

            if(itemDetails.delivery == 1) {
                $('.product-details-content .order-share-buttons .btn-glyph-default').css('display', 'block').attr('target', '_blank');
                $('.product-details-content .order-share-buttons .btn-glyph').css('display', 'block').attr('target', '_blank');
            } else {
                $('.product-details-content .order-share-buttons .btn-glyph-default').css('display', 'none').attr('target', '');
                $('.product-details-content .order-share-buttons .btn-glyph').css('display', 'none').attr('target', '');
            }

            $('.product-details-content').animate({
                 height:animateHeight,
                 padding:'60px 0px'
            },100,function(){
                    $('.product-details-content .product-thumbnail img').attr('src',itemDetails.thumbnail);
                    $('.product-details-content .description h4').text(itemDetails.title);
                    $('.product-details-content .description > span.description').text(itemDetails.description);
                    $('.product-details-content .order-share-buttons .btn-glyph-default').attr('href',itemDetails.orderurl);
                    $('.product-details-content .order-share-buttons .btn-glyph').attr('href',itemDetails.orderurl);
                    $('.product-details-content .order-share-buttons .share-button').html('<fb:share-button href="'+itemDetails.fburl+'"data-type="box_count"></fb:share-button>');
                    FB.XFBML.parse();
            });
           $('html,body').animate({scrollTop: $("#product-details").offset().top});
    });

</script>
