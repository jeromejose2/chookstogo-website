<section class="main">
    <div class="banner-bg promo-list">
               <div class="container promo-content">
                         <div class="heading txtcenter">
                            <h2 class="title-heading">CHOOKS BLOG</h2>
                               <span>
                                   Dahil gusto mong malaman ang mga kwelang pangyayari sa Chooks-to-Go, babasahin mo ang mga articles dito! Enjoy!
                                </span>
                        </div><br/>
                        <div class="tab-content">
                            <ul class="nav nav-pills promo-tab hidden-xs promo-tab-select">
                                <li><a data-section="Main Section" href="#" class="active">CHOOKS BLOG</a><i class="hidden-xs arrow-down"></i></li>
                                <li><a data-section="Chooks Help" href="#">CHOOKS HELPS</a></li>


                            </ul>
                            <select class="visible-xs promo-select col-xs-10 promo-select-select">
                                    <option value="Main Section">CHOOKS BLOG</option>
                                    <option>CHOOKS HELP</option>


                            </select>
                        </div>
             </div> <br class="clear"/><br/>
            <div class="blog-list container unpadded">

                    <!-- ARTICLES -->
                    <? $i = 0; foreach($articles as $k => $v): ?>
                        <div class="blog-item col-md-6 col-sm-12 col-xs-12">
                            <div class="col-md-4 col-sm-3 thumbnail">
                                <img class="img-responsive" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>" alt="blog image">
                            </div>
                            <div class="col-md-8 col-sm-9 blog-list-post">
                                <h4><?= $v['title'] ?></h4>
                                <p><?= date('F j, Y', strtotime($v['date_of_publish'])) ?></p>
                                <p><?= word_limiter($v['short_description'], 20) ?></p>
                                <div class="btn-group txtcenter">
                                    <a class="btn btn-glyph-default" href="<?= site_url('blogs/article') ?>/<?= $v['id'] . '/' . url_title($v['title']) ?>">READ MORE</a>
                                    <a class="btn btn-glyph" href="<?= site_url('blogs/article') ?>/<?= $v['id'] . '/' . url_title($v['title']) ?>">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                </div>
                                <div class="share-button">
                                    <fb:share-button href="<?= site_url('blogs/article') ?>/<?= $v['id'] ?>" data-type="box_count"></fb:share-button>
                                </div>
                            </div>
                        </div>
                        <? if($i % 2): ?>
                            <br class="clear">
                        <? endif; ?>
                    <? $i++; endforeach; ?>
                    <!-- END ARTICLES -->

           </div>
           <br class="clear"/><br/>
    </div>
<div class="wave clearfix">
</div><br class="clear"/><br/>
</section>

<script>
    FB.XFBML.parse();
</script>