<section class="main">

    <div class="news-all banner-bg"></div><div class="wave clearfix"></div>

    <div class="news-update-list careers-list-results">

         <div class="heading txtcenter">

                        <h2 class="title-heading">CAREERS</h2>

                        <span>Ka-Chooks opportunity</span>

            </div><br class="clear"/><br/>

           <div class="result-list-content container">

                <?php end($careers); $lastElement = key($careers); foreach($careers as $k => $v): ?>

                <?php /* <h3><?= $v['position'] ?></h3> */ ?>
                <h3>
                    <?php if(stripos($v['position'], 'Compliance Assurance') !== FALSE): ?>
                        <?php echo str_replace(strtoupper('Compliance Assurance'), strtoupper('Food Quality and Safety Audit'), $v['position']); ?>
                    <?php elseif(stripos($v['position'], 'Management Control System') !== FALSE): ?>
                        <?php echo str_replace(strtoupper('Management Control System'), strtoupper('Corporate Governance / Planning'), $v['position']); ?>
                    <?php else: ?>
                        <?php echo $v['position']; ?>
                    <?php endif; ?>
                </h3>

                <span><?= $v['location'] ?></span><br class="clear"/><br/>

                <p class="title">Qualifications:</p>



                      <?= $v['qualifications'] ?>



                <p class="title">Responsibilities:</p>



                    <?= $v['description'] ?>





                <br class="clear">



                <? if($k == $lastElement): ?>

                <div class="btn-group txtcenter">

                       <a class="btn btn-glyph btn-glyph-light-gray" href="<?= site_url('careers') ?>">

                            <span class="glyphicon glyphicon-chevron-left"></span>

                        </a>

                        <a class="btn btn-glyph-default btn-glyph-gray" href="<?= site_url('careers') ?>">BACK</a>

                </div>

                <? endif; ?>

                <div class="btn-group txtcenter">



                        <a class="btn btn-glyph-default btn-glyph apply-career" href="javascript:void(0)" data-position="<?=$v['position']?>">APPLY</a>

                        <a class="btn btn-glyph btn-glyph-">

                            <span class="glyphicon glyphicon-chevron-right apply-career" href="javascript:void(0)" data-position="<?=$v['position']?>"></span>

                        </a>

                </div>
                
                <fb:share-button href="<?= $v['share_url'] ?>" data-type="box_count"></fb:share-button>

                <?php endforeach; ?>
                

           </div>



           <br class="clear"/><br/>

    </div>

    <!--end news-update-list-->

     <div class="wave clearfix"></div><br class="clear"/><br/>

</section>

