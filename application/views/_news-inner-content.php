<section class="main">
<div class="news-all banner-bg"></div><div class="wave clearfix"></div>
    <div class="news-update-list people-list">
         <div class="people-list-content container">
                <div class="heading txtcenter">
                        <h2 class="title-heading">BLOG ENTRY</h2>
                </div><br class="clear"/><br/><br/>
                  <div class="row">
                      <div class="thumbnail">
                            <img src="<?= str_replace('~path~', base_url(), $article[0]['photo']) ?>" class="img-responsive" alt="blog entry"/>
                      </div><br class="clear"/><br/>
                      <div class="blog-content">
                            <h4><?= $article[0]['title'] ?></h4>
                            <span><?= date('l, F d, Y', strtotime($article[0]['date_of_publish'])) ?></span>
                            <div class="inner-content">
                                     <p>
                                        <?= $article[0]['description'] ?>

                                    </p>

                            </div>
                            <div class="btn-group padded txtcenter">
                                <a class="btn btn-glyph" href="<?= site_url('news') ?>">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="btn btn-glyph-default" href="<?= site_url('news') ?>">NEWS AND UPDATES</a>

                            </div>

                      </div><br class="clear"/><br/>


                </div>

         </div>
    </div>

    <div class="wave clearfix"></div><br class="clear"/><br/>
</section>