<style type="text/css">
    .error-field {
        color: red !important;
    }

</style>
<script type="text/javascript" src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
<section class="main">
    <div class="banner-bg promo-list">
        <div class="container promo-content">
                    <div class="heading txtcenter">
                        <h2 class="title-heading">LOGIN</h2>
                        <span>
                            May account ka na ba dito? Log-in na, Ka-Chooks, para tuloy ang kwela!
                        </span>
                    </div>
                    <br/>
                    <div class="registration-tab">
                        <ul class="nav nav-pills">
                            <li><a href="#" data-id ="#login" class="active">Login</a><i class="arrow-down"></i>
                            </li>
                            <li><a href="#" data-id ="#registration">Register</a>
                            </li>
                        </ul>
                    </div>
        </div>
        <div class="registration-login-content container">
                     <div class="login active" id="login">
                        <h3 style="text-align:center; float:none !important; color:yellow" class="error-login navbar-nav"></h3>
                            <form class="relative-center txtcenter" method="POST" onsubmit="return chooks.validateLogin()" id="loginForm">
                                    <input type="text" class="col-md-3 login-required" placeholder="USERNAME" name="login_username" alt="Please enter your username.">
                                    <input type="password" class="col-md-3 login-required" placeholder="PASSWORD" name="login_password" alt="Please enter your password.">
                                    <br class="clear"/><br/><br/>
                                    <div class="btn-group txtcenter">
                                        <button class="btn btn-glyph btn-rounded">
                                            <span class="fa fa-lock"></span>
                                        </button>
                                        <button class="btn btn-glyph-rounded" href="#">LOGIN</button>
                                    </div>
                            </form><br class="clear"/><br/>
                            <div class="forgot-pw txtcenter">
                                    <p>Forgot your Login details? Click <a href="javascript:void(0)" onclick="chooks.forgotPassword()" class="yellow">HERE</a></p>
                            </div>
                            <div style="display: none">
                                <span onclick="chooks.triggerNewPassword()" id="new-password"></span>
                            </div>

                      </div>
                      <div class="registration" id="registration">
                            <h3 style="text-align:center; float:none !important; color:yellow" class="error navbar-nav"></h3>
                                <form class="registration" method="POST" onsubmit="return chooks.validateRegistration()" id="regForm">
                                       <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <ul>
                                                            <li>
                                                                <label>FULLNAME</label><br class="clear"/>
                                                                <input type="text" placeholder="FIRST NAME" class="col-md-5 col-sm-5 col-xs-12 registration-required" name="firstname" alt="PLEASE FILL IN REQUIRED FIELDS"></input>
                                                                <input type="text" placeholder="LAST NAME" class="col-md-5 col-sm-5 col-xs-12 registration-required" name="lastname" alt="PLEASE FILL IN REQUIRED FIELDS"></input>
                                                            </li><br class="clear"/><br/><br/>
                                                            <li>
                                                                <label>GENDER</label><br class="clear"/>
                                                                <select class="mm col-md-3 col-xs-3 registration-required" name="gender" alt="PLEASE FILL IN REQUIRED FIELDS">
                                                                    <option value="">SELECT GENDER</option>
                                                                    <option value="1">MALE</option>
                                                                    <option value="2">FEMALE</option>
                                                                </select>
                                                            </li><br class="clear"/><br/><br/>
                                                             <li>
                                                                <label>Birthday</label><br class="clear"/>
                                                                <select class="mm col-md-3 col-xs-3" name="birth_month">
                                                                    <option value="01">JANUARY</option>
                                                                    <option value="02">FEBRUARY</option>
                                                                    <option value="03">MARCH</option>
                                                                    <option value="04">APRIL</option>
                                                                    <option value="05">MAY</option>
                                                                    <option value="06">JUNE</option>
                                                                    <option value="07">JULY</option>
                                                                    <option value="08">AUGUST</option>
                                                                    <option value="09">SEPTEMBER</option>
                                                                    <option value="10">OCTOBER</option>
                                                                    <option value="11">NOVEMBER</option>
                                                                    <option value="12">DECEMBER</option>
                                                                </select>
                                                                 <select class="day col-md-2 col-xs-3" name="birth_day">
                                                                    <? for($d = 1; $d <= 31; $d++): ?>
                                                                        <option value="<?= sprintf("%02s", $d) ?>"><?= sprintf("%02s", $d) ?></option>
                                                                    <? endfor; ?>
                                                                </select>
                                                                 <select class="yy col-md-3 col-xs-3" name="birth_year">
                                                                    <? for($y = 2014; $y >= 1950; $y--): ?>
                                                                        <option value="<?= $y ?>"><?= $y ?></option>
                                                                    <? endfor; ?>
                                                                </select>

                                                            </li><br class="clear"/><br/><br/>
                                                             <li>
                                                                <label>MOBILE NUMBER</label><br class="clear"/>
                                                                <input data-mask="0000-000-0000" type="text" placeholder="09xx - xxx -xxxx" class="col-md-8 col-xs-12 registration-required" name="mobile" alt="PLEASE FILL IN REQUIRED FIELDS" maxlength="11"></input>

                                                            </li><br class="clear"/><br/><br/>
                                                            <li>
                                                                <label>Province & City</label><br class="clear"/>
                                                                <select class="mm col-md-5 col-xs-5 registration-required" name="province" alt="PLEASE FILL IN REQUIRED FIELDS" onchange="fooBar()">
                                                                    <?= $province ?>
                                                                </select>
                                                                <select class="mm col-md-5 col-xs-5 registration-required" name="city" alt="PLEASE FILL IN REQUIRED FIELDS" disabled>
                                                                    <option value="">Select Province First</option>
                                                                </select>
                                                            </li><br class="clear"/><br/><br/>

                                                        </ul>

                                       </div>
                                       <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <ul>
                                                            <li>
                                                                <label>EMAIL ADDRESS</label><br class="clear"/>
                                                                <input type="text" class="col-md-5 col-sm-5 col-xs-12 registration-required reg-email" placeholder="ENTER VALID EMAIL ADDRESS" name="email" alt="PLEASE FILL IN REQUIRED FIELDS"></input>
                                                                <input type="text" class="col-md-5 col-sm-5 col-xs-12 registration-required" placeholder="RE-ENTER EMAIL ADDRESS" name="re_email" alt="PLEASE FILL IN REQUIRED FIELDS"></input>
                                                            </li><br class="clear"/><br/><br/>
                                                           <li>
                                                                <label>CREATE PASSWORD</label><br class="clear"/>
                                                                <input type="password" class="col-md-5 col-sm-5 col-xs-12 registration-required" placeholder="CHOOSE PASSWORD" name="password" alt="PLEASE FILL IN REQUIRED FIELDS" autocomplete="off"></input>
                                                                <input type="password" class="col-md-5 col-sm-5 col-xs-12 registration-required" placeholder="RE-ENTER PASSWORD" name="re_password" alt="PLEASE FILL IN REQUIRED FIELDS" autocomplete="off"></input>
                                                            </li><br class="clear"/><br/><br/>
                                                             <li>
                                                                <!-- RECAPTCHA -->
                                                                <div id="recaptcha_id" style="width: 100% !important"></div>
                                                                <!-- END RECAPTCHA -->


                                                                <!-- <img src="assets/img/captcha.jpg" class="img-responsive" alt="captcha" /> -->
                                                            </li><br class="clear"/><br/><br/>
                                                             <li>
                                                                <input class="registration-required" type="checkbox" name="agree" alt="PLEASE FILL IN REQUIRED FIELDS"/>   I agree to the <a href="javascript:void(0)" class="yellow" onclick="chooks.TermsAndConditionsPopup()">TERMS AND CONDITIONS</a>
                                                            </li><br class="clear"/><br/><br/>

                                                       </ul>
                                       </div><br class="clear"/>
                                      <div class="row txtcenter">
                                                <div class="btn-group">
                                                    <button class="btn btn-glyph btn-rounded">
                                                        <span class="fa fa-lock"></span>
                                                    </button>
                                                    <button class="btn btn-glyph-rounded">REGISTER</button>
                                                </div>

                                      </div>




                                </form>
                      </div>
        </div>
        <br class="clear" />
        <br/>
    </div>
    <div class="wave clearfix"></div><br class="clear"/><br/>
</section>

<style type="text/css">
    #recaptcha_privacy {
        display: none;
    }
</style>

<script>
    function fooBar() {
        var id = $('select[name=province]').val();
        var el = $('select[name=city]');
        var data = { id : id };
        
        el.html('<select><option>Loading</option></select>');
        if(!id) {
            el.html('<select><option value="">Select Province First</option></select>');
        } else {
            $.post("<?=site_url('login/select_city')?>", data, function(response){
                el.attr('disabled', false);
                el.html(response);
            });
        }
    }
</script>