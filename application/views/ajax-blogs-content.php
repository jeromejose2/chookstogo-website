<!-- ARTICLES -->
                    <? $i = 0; foreach($articles as $k => $v): ?>
                        <div class="blog-item col-md-6 col-sm-12 col-xs-12">
                            <div class="col-md-4 col-sm-3 thumbnail">
                                <img class="img-responsive" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>" alt="blog image">
                            </div>
                            <div class="col-md-8 col-sm-9 blog-list-post">
                                <h4><?= $v['title'] ?></h4>
                                <p><?= date('F j, Y', strtotime($v['date_of_publish'])) ?></p>
                                <p><?= word_limiter($v['short_description'], 20) ?></p>
                                <div class="btn-group txtcenter">
                                    <a class="btn btn-glyph-default" href="<?= site_url('blogs/article') ?>/<?= $v['id'] . '/' . url_title($v['title']) ?>">READ MORE</a>
                                    <a class="btn btn-glyph" href="<?= site_url('blogs/article') ?>/<?= $v['id'] . '/' . url_title($v['title']) ?>">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                </div>
                                <div class="share-button">
                                    <fb:share-button href="<?= site_url('blogs/article') ?>/<?= $v['id'] ?>" data-type="box_count"></fb:share-button>
                                </div>
                            </div>
                        </div>
                        <? if($i % 2): ?>
                            <br class="clear">
                        <? endif;?>
                    <? $i++; endforeach; ?>
                    <!-- END ARTICLES -->

<script>
    FB.XFBML.parse();
</script>