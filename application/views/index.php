<?php include 'header.php' ?>

<section class="main">
    <div class="banner-bg " id="banner">
        <div class="container slider-container">
            <div id="home-banner" class="owl-carousel owl-theme">
                <?php for($i=0;$i<5;$i++){ ?>
                <div class="item">
                    <img class="img-responsive" src="assets/img/slides/1.png" alt="slider image">
                </div>
                <?php } ?>


            </div>
            <br class="clear" />

        </div>
        <div class="welcome col-sm-11 col-md-9 col-xs-11">
            <div class="row">
                <div class="col-md-7 col-sm-6 welcome-content">
                    <h2 class="yellow">Welcome Ka-Chooks!</h2>
                    <span>
                        Hello! Super natutuwa kami na bumisita ka dito sa aming website! Ang dami naming
                        <br/>kwelang content dito, at siguradong masaya ang experience mo.
                        <br/>Excited ka na bang mag-explore? Click-click na!
                    </span>
                </div>
                <div class="col-md-5 col-sm-6 delivery-details ">
                    <h3 class="yellow">Chooks Delivery <i class="bike"></i>
                    </h3>
                    <div class="col-md-5">
                        <h1>687.1010</h1>
                    </div>
                    <div class="col-md-7">
                        <dt>Available in <b class="yellow">Metro Manila</b> area only</dt>
                        <dt>Open from 9 AM to 8 PM, Mondays to Sundays</dt>
                    </div>
                    <br class="clear" />
                    <br/>
                    <div class="btn-group padded txtcenter">
                        <a class="btn btn-glyph-default" href="#" type="button">ORDER NA!</a>
                        <a class="btn btn-glyph">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>


                </div>
            </div>

        </div>
        <br class="clear" />
        <!--/welcome-->
    </div>
    <!--/banner-->


    <div class="announcement wave" id="announcement">
        <div class="announcement-content container">
            <div class="heading txtcenter">
                <h2 class="title-heading">CHOOKS ANNOUNCEMENTS</h2>
                <span>'Wag magpahuli! Makibalita sa lahat ng happenings sa Chooks-to-Go!</span>
            </div>
            <br class="clear" />
            <br/>
            <br/>
            <div id="announcement-banner" class="owl-carousel owl-theme">
                <?php for($i=0;$i<5;$i++){ ?>
                <div class="item">
                    <div class="col-md-6 col-sm-6">
                        <img class="img-responsive" src="assets/img/thumbnail-img.jpg" alt="thumbnail" />

                    </div>
                    <div class="col-md-5 description col-sm-6">
                        <h4>KAPITAN CHOOKS 2014</h4>
                        <span class="announcement-msg">
                            Hinahanap ulit namin ang mga taong karapat-dapat na maging Kapitan Chooks! Eto yung mga taong nakaka-inspire at nakakatulong sa kapwa!
                        </span>
                        <br class="clear" />
                        <br/>
                        <div class="btn-group padded txtcenter">
                            <a class="btn btn-glyph-default" href="#" type="button">GO TO PROMO PAGE</a>
                            <a class="btn btn-glyph">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>


                    </div>
                </div>
                <?php } ?>

            </div>
        </div>
    </div>
    <!-- /announcement -->


    <div class="chooks-blog mosaic-bg" id="chooks-blog">
        <div class="chooks-blog-content container">
            <div class="col-md-6 blog-post-container">
                <div class="row heading">
                    <h2>CHOOKS BLOG</h2>
                </div>
                <?php for($i=0;$i<2;$i++){ ?>
                <div class="row post">
                    <div class="blog-thumbnail col-md-4 col-sm-4 col-xs-12 ">
                        <a href="#" class="thumbnail">
                            <img class="img-responsive" src="assets/img/blog-thumbnail.jpg" alt="thumbnail">
                        </a>
                    </div>
                    <div class="blog-post col-md-7 col-sm-6 col-xs-12">
                        <h4>Ngayong Tag-init,Ang Kwelavan 2 Ay Sasapit!</h4>
                        <span class="blog-post-details">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...
                        </span>
                        <br class="clear" />
                        <br/>
                        <br/>
                        <div class="btn-group col-md-8 col-xs-8 col-sm-7 txtcenter">
                            <a class="btn btn-glyph-default" href="#" type="button">READ MORE</a>
                            <a class="btn btn-glyph">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>

                        </div>
                        <div class="share-button col-xs-3">
                            <fb:share-button href="https://developers.facebook.com/docs/plugins/" data-type="box_count"></fb:share-button>
                        </div>
                    </div>
                </div>
                <br class="clear" />
                <br/>
                <?php } ?>
                <div class="view-all">
                    <div class="btn-group padded txtcenter">
                        <a class="btn btn-glyph-default" href="#" type="button">VIEW ALL</a>
                        <a class="btn btn-glyph">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>

                </div>
            </div>
            <div class="komiks-container unpadded txtcenter col-md-6">
                <div class="komiks-content">

                    <div class="heading row col-md-12">
                        <div class="col-md-6 col-sm-6 komiks-title unpadded col-xs-6">
                            <h3>KOMIKS OF THE DAY</h3>
                        </div>
                        <div class="col-md-6 unpadded col-sm-6 enlarge-komiks col-xs-6">
                            <div class="btn-group  pull-right">
                                <a class="btn btn-glyph">
                                    <span class="glyphicon glyphicon-search"></span>
                                </a>
                                <a class="btn btn-glyph-default" href="#" type="button">CLICK TO ENLARGE</a>

                            </div>
                        </div>
                    </div>
                    <br class="clear" /></br>
                    <div class="social-image  unpadded txtcenter">
                        <img class="img-responsive" src="assets/img/social-img-1.jpg" alt="social card" />
                    </div>
                    <br class="clear" />
                    <br/>
                    <div class="col-md-5 pull-right">
                        <div class="btn-group pull-right">
                            <a class="btn btn-glyph-default" href="#" type="button">VIEW ALL</a>
                            <a class="btn btn-glyph">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>

                    </div>
                    <br class="clear" />
                    <br/>








                </div>

            </div>

        </div>
        <br class="clear" />
        <br/>

    </div>
    <!-- /chooks-blog -->
    <div class="banner-bg wave store-locator" id="store-locator">
        <div class="store-locator-content container">
            <div class="heading txtcenter">
                <h2 class="title-heading">STORE LOCATOR</h2>
                <span>Chook-to-Go store ba hanap mo? Search ka lang dito!</span>
            </div>
            <br class="clear" />
            <br/>
            <br/>
            <div class="img-responsive unpadded map col-md-8 txtcenter">
                <img class="img-responsive" src="assets/img/map.jpg" alt="map" />
                <div class="btn-group find-store">
                    <a class="btn btn-rounded">
                        <span class="glyphicon glyphicon-search"></span>
                    </a>
                    <a class="btn btn-glyph-rounded" href="#" type="button">FIND NEARBY STORE</a>

                </div>
            </div>
            <br/>
            <div class="col-md-4 map-details">
                <div class="img-logo">
                    <img class="img-responsive" src="assets/img/chooks-locator-logo.png" alt="chooks locator" />
                </div>
                <br/>
                <div class="dropdowns">
                    <select class="area">
                        <option>area</option>
                    </select>
                    <select class="city">
                        <option>city/municipality</option>
                    </select>
                    <select class="store">
                        <option>store</option>
                    </select>
                </div>
                <br/>
                <a class="btn btn-default col-md-12 col-sm-12 col-xs-12" href="#" type="button">SEARCH STORE</a>
                <br class="clear" />
                <br/>
                <div class="address">
                    <h4>Chooks-To-Go Ortigas-Emerald Branch</h4>
                    <dt>Address:</dt>
                    <dd>Unit 1, Amberland Plaza, Julia Vargas Avenue corner Emerald Avenue, Ortigas Center, Pasig City</dd>
                    <br/>
                    <dt>Phone Number:</dt>
                    <dd>000.000.000</dd>
                    <br/>
                    <dt>Operating hours:</dt>
                    <dd>9AM - 5PM</dd>
                    <br/>
                </div>
            </div>
            <br class="clear" />
            <div class="bottom-img visible-lg">

            </div>
        </div>
        <br class="clear" />
    </div>
    <br/>
    <br/>
    <!--/store-locator-->
</section>



<?php include 'footer.php' ?>
