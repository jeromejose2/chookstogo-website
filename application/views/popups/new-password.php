 <div class="popup">
        <div class="col-md-4 popup-wrapper col-sm-4 relative-center">
                <div class="lytebox-close">X</div>
                <div class="popup-content">
                    <h3>NEW PASSWORD</h3>
                    <h3 style="float:none !important;" class="newpassword-error navbar-nav"></h3>
                    <br/>
                    <form enctype="multipart/form-data" onsubmit="return chooks.validateNewPasswordForm()" id="newpassword-form">

                        <ul class="row">
                            <li class="col-md-12 col-sm-6 col-xs-12" style="list-style: none">
                                    <label>Password*</label><br class="clear"/>
                                    <input type="password" name="password" class="newpassword-required pword" alt="Sorry, you must enter your new password to continue." >
                            </li>

                        </ul><br class="clear"/><br/>
                        <ul class="row">
                            <li class="col-md-12 col-sm-6 col-xs-12" style="list-style: none">
                                    <label>Re-type password*</label><br class="clear"/>
                                    <input type="password" name="re_password" class="newpassword-required" alt="Sorry, you must re-enter your password to continue." >
                            </li>

                        </ul><br class="clear"/><br/>
                               <div class="txtcenter">
                                    <div class="btn-group txtcenter">
                                           <a class="btn btn-glyph-default" href="javascript:void(0)" onclick="$('#newpassword-form').submit()">SUBMIT</a>
                                           <a class="btn btn-glyph">
                                               <span class="glyphicon glyphicon-chevron-right"></span>
                                           </a>
                                   </div>
                              </div>

                    </form>


                </div>

        </div>

</div>
