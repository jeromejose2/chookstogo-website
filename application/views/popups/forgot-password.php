 <div class="popup">
        <div class="col-md-4 popup-wrapper col-sm-4 relative-center">
                <div class="lytebox-close">X</div>
                <div class="popup-content">
                    <h3>FORGOT PASSWORD</h3>
                    <h4 style="text-align:center; float:none !important; color:yellow" class="forgotpassword-error navbar-nav"></h4>
                    <br/>
                    <form enctype="multipart/form-data" onsubmit="return chooks.validateForgotPasswordForm()" id="forgotpassword-form">

                        <ul class="row">
                            <li class="col-md-12 col-sm-6 col-xs-12" style="list-style: none">
                                    <label>Email*</label><br class="clear"/>
                                    <input type="text" name="email" class="forgotpassword-required" alt="Sorry, you must enter your email to retrieve your password." >
                            </li>

                        </ul><br class="clear"/><br/>
                               <div class="txtcenter">
                                    <div class="btn-group txtcenter">
                                           <a class="btn btn-glyph-default btn-forgotpassword-submit-btn" href="javascript:void(0)" onclick="$('#forgotpassword-form').submit()">SUBMIT</a>
                                           <a class="btn btn-glyph btn-forgotpassword-submit-btn">
                                               <span class="glyphicon glyphicon-chevron-right"></span>
                                           </a>
                                   </div>
                              </div>

                    </form>


                </div>

        </div>

</div>
