 <div class="popup">
        <div class="col-md-6 popup-wrapper col-sm-10 relative-center">
                <div class="lytebox-close">X</div>
                <div class="popup-content">
                    <h3>APPLICATION FORM</h3>
                    <div class="error">
                            
                    </div><br/>
                    <form enctype="multipart/form-data" action="<?= site_url('careers/apply') ?>" method="POST" onsubmit="return chooks.validateApplicationForm()" id="application-form">

                        <ul class="row">
                            <li class="col-md-6 col-sm-6 col-xs-12">
                                    <label>Name*</label><br class="clear"/>
                                    <input type="text" name="name" class="application-required" alt="Sorry, you must enter your name to continue." >
                            </li>
                            <li class="col-md-6 col-sm-6 col-xs-12">
                                    <label>Email address*</label><br class="clear"/>
                                    <input type="text" name="email" class="application-required" alt="Sorry, you must enter your email address to continue" >
                            </li><br class="clear"/><br/>
                            <li class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Complete address*</label><br class="clear"/>
                                    <input type="text" name="address" class="application-required" alt="Sorry, you must enter your address to continue." >
                            </li><br class="clear"/><br/>
                            <li class="col-md-6 col-sm-6 col-xs-12">
                                    <label>Position*</label><br class="clear"/>
                                    <input type="text" name="position" value="<?= isset($position) ? $position : '' ?>" class="application-required" alt="Sorry, you must enter your position you are applying." readOnly >
                            </li>
                            <li class="col-md-6 col-sm-6 col-xs-12">
                                    <label>Attach Curriculum Vitae*</label><br class="clear"/>
                                    <input type="file" name="resume" class="application-required" alt="Sorry, you must attach your curriculum vitae to your application form to continue." onchange="validateCVFile(this)" >
                                    <small id="upload-cv-error">File must be in PDF format and not exceeds to 2mb</small>
                            </li><br class="clear"/><br/>
                             <li class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Portfolio: YouTube (Optional)</label><br class="clear"/>
                                    <input type="text" name="portfolio_youtube" class="application-required" >
                            </li><br class="clear"/><br/>
                             <li class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Portfolio: Upload File (Optional)</label><br class="clear"/>
                                    <input type="file" name="portfolio_upload" onchange="validatePortfolioFile(this)" >
                                    <small id="upload-portfolio-error">File must be in PDF format and not exceeds to 2mb</small>
                            </li>

                        </ul><br class="clear"/><br/>
                               <div class="txtcenter">
                                    <div class="btn-group txtcenter">
                                           <a class="btn btn-glyph-default" href="javascript:void(0)" onclick="$('#application-form').submit()">SUBMIT</a>
                                           <a class="btn btn-glyph" onclick="$('#application-form').submit()">
                                               <span class="glyphicon glyphicon-chevron-right"></span>
                                           </a>
                                   </div>
                              </div>

                    </form>


                </div>

        </div>

</div>
