<?php include 'header.php' ?>

<section class="main">
<div class="news-all banner-bg"></div><div class="wave clearfix"></div>
    <div class="news-update-list people-list">
         <div class="people-list-content container">
                <div class="heading txtcenter">
                        <h2 class="title-heading">OUR PEOPLE</h2>
                        <span>Meet the amazing and dedicated people behind Chooks-To-Go. Their hard work and perseverance in constantly improving our products and services is always an inspiration to others in the company.</span>
                </div><br class="clear"/><br/><br/>
                <div class="persons-left row">
                            <div class="pull-left col-md-3 col-md-offset-1 col-sm-offset-1 txtcenter">
                                    <img src="assets/img/person-1.jpg" alt="person"/>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                    <h3>Inna Dominique Villa</h3>
                                    <p>Executive Vice President</p><br/>
                                    <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi

                                    </p>
                                    <div class="btn-group padded">
                                        <a class="btn btn-glyph-default read-profile" href="#" >READ MORE</a>
                                        <a class="btn btn-glyph">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </a>
                                    </div>

                            </div><br class="clear"/>
                            <div class="mosaic-bg person-profile row">
                                <div class="content">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>

                                     <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                     </p>
                                </div>
                            </div>






                </div><br class="clear"><br/>


                <div class="persons-right row">
                    <div class="col-md-3 txtcenter pull-right">
                            <img src="assets/img/person-2.jpg" alt="person"/>
                    </div>
                    <div class="col-md-7 col-sm-7 col-md-offset-2 col-xs-12">
                            <h3>Inna Dominique Villa</h3>
                            <p>Executive Vice President</p><br/>
                            <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi

                            </p>
                            <div class="btn-group padded">
                                <a class="btn btn-glyph-default read-profile" href="#">READ MORE</a>
                                <a class="btn btn-glyph">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                          </div>

                    </div><br class="clear"/>
                    <div class="mosaic-bg person-profile row">
                                <div class="content">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>

                                     <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                     </p>
                                </div>
                            </div>



                </div><br class="clear"><br/>


         </div>
    </div>
    <div class="wave clearfix"></div><br class="clear"/><br/>
</section>



<?php include 'footer.php' ?>
