<section class="main">
<div class="news-all banner-bg"></div><div class="wave clearfix"></div>
    <div class="news-update-list people-list">
         <div class="people-list-content container">
                <div class="heading txtcenter">
                        <h2 class="title-heading">BLOG ENTRY</h2>
                </div><br class="clear"/><br/><br/>
                  <div class="row">
                      <div class="thumbnail">
                            <img src="<?= str_replace('~path~', base_url(), $article[0]['photo']) ?>" class="img-responsive" alt="blog entry"/>
                      </div><br class="clear"/><br/>
                      <!-- SLIDER -->


                      <?php $slider_photos = $article[0]['slider_photos'] ? unserialize($article[0]['slider_photos']) : NULL ?>
                      <?php if( ! is_null($slider_photos)): ?>

                      <div id="blog-slider">

                        <div id="owl-custom-ctg" class="owl-carousel owl-theme">

                          <div class="item">
                            <a href="javascript:void(0);" data-src="<?= str_replace('~path~', base_url(), $article[0]['photo']) ?>"><img src="<?= str_replace('~path~', base_url(), $article[0]['photo']) ?>" alt="blog entry"/></a>
                          </div>

                          <? if($slider_photos): ?>
                            <? foreach($slider_photos as $photo): ?>
                                <div class="item">
                                  <a href="javascript:void(0);" data-src="<?= str_replace('~path~', base_url(), $photo) ?>"><img src="<?= str_replace('~path~', base_url(), $photo) ?>" alt="blog entry"/></a>
                                </div>
                            <? endforeach; ?>
                          <? endif; ?>

                        </div>
                      
                      </div>

                      <?php endif; ?>

                      <!-- SLIDER -->
                      <div class="blog-content">
                            <h4><?= $article[0]['title'] ?></h4>
                            <span><?= date('l, F d, Y', strtotime($article[0]['date_of_publish'])) ?></span>
                            <div class="inner-content">
                                     <p>
                                        <?= $article[0]['description'] ?>

                                    </p>

                            </div>
                            <div class="btn-group padded txtcenter">
                                <a class="btn btn-glyph" href="<?= site_url('news') ?>">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="btn btn-glyph-default" href="<?= site_url('news') ?>">NEWS AND UPDATES</a>

                            </div>

                      </div><br class="clear"/><br/>


                </div>

         </div>
    </div>

    <div class="wave clearfix"></div><br class="clear"/><br/>
</section>