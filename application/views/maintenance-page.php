<!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Chooks To Go</title>
        <link href="<?= base_url() ?>assets/vendors/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/vendors/lytebox/lytebox.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/vendors/owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/vendors/owl-carousel/owl.theme.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/vendors/owl-carousel/owl.transitions.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- build:css assets/css/main.min.css -->
        <link href="<?= base_url() ?>assets/css/main.min.css" rel="stylesheet">
        <!-- /build -->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body id="maintenance">


        <!--[if lt IE 9]>
        <div class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</div>
        <![endif]-->
        <header>
                <img src="<?= base_url() ?>assets/img/logo.png" alt="logo"/>
        </header>
        <section class="main">
            <div class="content banner-bg wave-black">
                    <div class="container content">
                            <h1 class="yellow">Pasensya na, mga Ka-Chooks.</h1>
                            <span>May hinahanda lang kaming kwelang sorpresa.Balik kayo soon!</span><br/><br/>
                            <img src="<?= base_url() ?>assets/img/chikoy-maintenance.png" alt="maintenance"/>

                    </div>
            </div>
        </section>
        <footer>

        </footer>

    </body>

</html>


