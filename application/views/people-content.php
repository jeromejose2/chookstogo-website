<section class="main">
<div class="news-all banner-bg"></div><div class="wave clearfix"></div>
    <div class="news-update-list people-list">
         <div class="people-list-content container">
                <div class="heading txtcenter">
                        <h2 class="title-heading">OUR PEOPLE</h2>
                        <span>Meet the amazing and dedicated people behind Chooks-To-Go. Their hard work and perseverance in constantly improving our products and services is always an inspiration to others in the company.</span>
                </div><br class="clear"/><br/><br/>

                <!-- PEOPLE -->
                <? $i = 1; foreach($people as $k => $v): ?>
                    <div class="about-us-people  <?= $i%2 ? 'persons-left' : 'persons-right' ?> row">
                        <div class="<?= $i%2 ? 'pull-left col-md-3 col-md-offset-1 col-sm-offset-1 txtcenter' : 'col-md-3 txtcenter pull-right' ?>">
                            <img src="<?= str_replace('~path~', base_url(), $v['photo']) ?>">
                        </div>
                        <div class="<?= $i%2 ? 'col-md-6 col-sm-6 col-xs-12' : 'col-md-7 col-sm-7 col-md-offset-2 col-xs-12' ?>">
                            <h3><?= $v['name'] ?></h3>
                            <div id="profile-preview-<?=$v['id']?>">
                            <p>
                                <?
                                    $strip_div1 = str_replace('<div>', '', $v['designation']);
                                    $strip_div2 = str_replace('</div>', '', $strip_div1);
                                    echo $strip_div2;
                                ?>
                            </p>
                            </div>
                            <div id="profile-designation-<?=$v['id']?>" style="display: none; color: black">
                                <em><strong><?= $v['designation_preview'] ?></strong></em>
                            </div>
                            <div class="btn-group padded">
                                <a href="javascript:void(0)" class="btn btn-glyph-default read-profile read-<?=$v['id']?>" data-id="<?=$v['id']?>">READ MORE</a>
                                <a class="btn btn-glyph" onclick="$('.read-<?=$v['id']?>').trigger('click')">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>
                        <br class="clear" />
                        <div class="mosaic-bg person-profile row">
                            <div class="content">
                                <p><?= $v['credentials'] ?></p>
                            </div>
                        </div>
                    </div>
                    <br class="clear"><br/>
                <? $i++; endforeach; ?>
                <!-- END PEOPLE -->
                <br class="clear"><br/>


         </div>
    </div>
    <div class="wave clearfix"></div><br class="clear"/><br/>
</section>
