<section class="main">
    <div class="banner-bg promo-list">
               <div class="container promo-content">
                         <div class="heading txtcenter">
                            <h2 class="title-heading">CHIKOY’S CORNER</h2>
                               <span>
                                    Makisaya sa kwelang samahan ni Chikoy at Tom Crew!<br/>Siguradong mapapaiyak ka sa tawa, kaya basa na!
                                </span>
                        </div><br/>
                        <div class="tab-content">
                            <ul class="nav nav-pills  nav-justified chikoys-tab hidden-xs">
                                <li><a data-type="All" href="javascript:void(0)" class="active" onclick="_ga('all-komiks')">all komiks</a><i class="hidden-xs arrow-down"></i></li>
                                <li><a data-type="Love" href="javascript:void(0)">love</a></li>
                                <li><a data-type="Jokes" href="javascript:void(0)">jokes</a></li>
                                <li><a data-type="Inspirational" href="javascript:void(0)">inspirational</a></li>
                                <li><a data-type="Quotes" href="javascript:void(0)">quotes</a></li>
                                <li><a data-type="Downloadables" href="javascript:void(0)">downloadables</a></li>

                            </ul>
                            <select class="visible-xs promo-select col-xs-10 chikoys-tab-responsive">
                                    <option value="All" onclick="_ga('all-komiks')">all komiks</option>
                                    <option value="Love">love</option>
                                    <option value="Jokes">jokes</option>
                                    <option value="Inspirational">inspirational</option>
                                    <option value="Quotes">quotes</option>
                                    <option value="Downloadables">downloadables</option>

                            </select>
                        </div><br class="clear"/><br/>
                        <div class="chikoys-corner-list row">

                            <!-- COMICS -->
                            <? foreach($comics as $k => $v): ?>
                                <div class="col-md-3 col-sm-4">
                                    <div class="thumbnail komiks-img col-md-11">
                                        <img class="img-responsive" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>">
                                        <div class="hover-enlarge">
                                            <button class="col-md-8 col-xs-8 col-sm-9 click-enlarge" onclick="chooks.loadImage(<?= $v['id'] ?>, 'comics')"><span class="glyphicon glyphicon-search"></span> Click to enlarge</button>
                                        </div>
                                    </div><br class="clear" />
                                    <div class="komiks-shot-details txtcenter">
                                        <h4><?= $v['title'] ?></h4><br/>
                                    </div><br/><br/>
                                </div>
                            <? endforeach; ?>
                            <!-- END COMICS -->

                        </div>


               </div>
    </div>
    <div class="wave clearfix">
   </div><br class="clear"/><br/>

</section>