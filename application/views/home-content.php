<? /*
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAjxtjNyjATUWGLx-1v0zehvr0bkc0ACnE&sensor=true"></script>
<script type="text/javascript">
    var LatLong = [
        <? foreach($stores as $k => $v): ?>
            [<?= $v['latitude'] ?>, <?= $v['longitude'] ?>, "<?= $v['address'] ?>"],
        <? endforeach; ?>
        ]
</script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/gmap.js"></script>
*/ ?>
<section class="main">
    <div class="banner-bg " id="banner">
        <div class="container slider-container">
            <div id="home-banner" class="owl-carousel owl-theme">

                <!-- SLIDERS -->
                <? foreach($sliders as $k => $v): ?>
                    <div class="item">
                        <a href="<?= $v['link'] ?>" target="<?= $v['open_link'] ? '_blank' : '' ?>"><img class="img-responsive" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>" onclick="_ga('slider')"></img></a>
                    </div>
                <? endforeach; ?>
                <!-- END SLIDERS -->

            </div>
            <br class="clear" />

        </div>
        <div class="welcome col-sm-11 col-md-9 col-xs-11">
            <div class="row">
                <div class="col-md-7 col-sm-6 welcome-content">
                    <h2 class="yellow">Welcome Ka-Chooks!</h2>
                    <span>
                        Hello! Super natutuwa kami na bumisita ka dito sa aming website! Ang dami naming
                        kwelang content dito, at siguradong masaya ang experience mo.
                        Excited ka na bang mag-explore? Click-click na!
                    </span>
                </div>
                <div class="col-md-5 col-sm-6 delivery-details ">
                    <h3 class="yellow">Chooks Delivery <i class="bike"></i>
                    </h3>
                    <div class="col-md-5">
                        <h1>687.1010</h1>
                    </div>
                    <div class="col-md-7">
                        <dt>Available in <b class="yellow">Metro Manila</b> area only</dt>
                        <dt>Open from 9 AM to 8 PM, Mondays to Sundays</dt>
                    </div>
                    <br class="clear" />
                    <br/>
                    <div class="btn-group padded txtcenter">
                        <a class="btn btn-glyph-default" href="http://chookstogo.foodpanda.ph/" target="_blank">Order Online</a>
                        <a class="btn btn-glyph" href="http://chookstogo.foodpanda.ph/" target="_blank">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>


                </div>
            </div>

        </div>
        <br class="clear" />
        <!--/welcome-->
    </div>
    <!--/banner-->


    <div class="announcement wave" id="announcement">
        <div class="announcement-content container">
            <div class="heading txtcenter">
                <h2 class="title-heading">CHOOKS ANNOUNCEMENTS</h2>
                <span>'Wag magpahuli! Makibalita sa lahat ng happenings sa Chooks-to-Go!</span>
            </div>
            <br class="clear" />
            <br/>
            <br/>
            <div id="announcement-banner" class="owl-carousel owl-theme">

                <!-- ANNOUNCEMENTS -->
                <? foreach($announcements as $k => $v): ?>
                    <div class="item">
                        <div class="col-md-6 col-sm-6">
                            <img class="img-responsive" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>" alt="thumbnail">
                        </div>

                        <div class="col-md-5 description col-sm-6">
                            <h4><?= $v['title'] ?></h4>
                            <span class="announcement-msg">
                                <?= word_limiter($v['description'], 25) ?>
                            </span>
                            <br class="clear" />
                            <br />
                            <div class="btn-group padded txtcenter">
                                <!-- <a class="btn btn-glyph-default" href="<?= @$v['link'] ?>">GO TO PROMO PAGE</a> -->
                                <? if($v['type'] == 'Promo'): ?>
                                    <a class="btn btn-glyph-default" href="<?= site_url('promos') ?>">GO TO PROMOS PAGE</a>
                                    <a class="btn btn-glyph" href="<?= site_url('promos') ?>">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                <? elseif($v['type'] == 'Article'): ?>
                                    <a class="btn btn-glyph-default" href="<?= site_url('blogs/article') . '/' . $v['id'] ?>">GO TO BLOG PAGE</a>
                                    <a class="btn btn-glyph" href="<?= site_url('blogs/article') . '/' . $v['id'] ?>">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                <? else: ?>
                                    <a class="btn btn-glyph-default" href="<?= site_url('news/article') . '/' . $v['id'] ?>">GO TO NEWS PAGE</a>
                                    <a class="btn btn-glyph" href="<?= site_url('news/article') . '/' . $v['id'] ?>">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
                <!-- END ANNOUNCEMENTS -->

            </div>
        </div>
    </div>
    <!-- /announcement -->


    <div class="chooks-blog mosaic-bg" id="chooks-blog">
        <div class="chooks-blog-content container">
            <div class="col-md-6 blog-post-container">
                <div class="row heading">
                    <h2>CHOOKS BLOG</h2>
                </div>

                <!-- FEATURED BLOG -->
                <? foreach($articles as $k => $v): ?>
                    <div class="row post">
                        <div class="blog-thumbnail col-md-4 col-sm-4 col-xs-12">
                            <a href="javascript:void(0)" class="thumbnail">
                                <img class="img-responsive" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>" alt="thumbnail">
                            </a>
                        </div>
                        <div class="blog-post col-md-7 col-sm-6 col-xs-12">
                            <h4><?= $v['title'] ?></h4>
                            <span class="blog-post-details">
                                <?= word_limiter($v['short_description'], 25) ?>
                            </span>
                            <br class="clear" />
                            <br />
                            <br />
                            <div class="btn-group col-md-9 col-xs-8 col-sm-7 txtcenter">
                                <a class="btn btn-glyph-default" href="<?= site_url('blogs/article') ?>/<?= $v['id'] ?>">READ MORE</a>
                                <a class="btn btn-glyph" href="<?= site_url('blogs/article') ?>/<?= $v['id'] ?>">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                            <div class="share-button col-xs-3">
                                <fb:share-button href="<?= site_url('blogs/article') ?>/<?= $v['id'] ?>" data-type="box_count"></fb:share-button>
                            </div>
                        </div>
                    </div>
                    <br class="clear" />
                    <br />
                <? endforeach; ?>
                <!-- END FEATURED BLOG -->

                <div class="view-all">
                    <div class="btn-group padded txtcenter">
                        <a class="btn btn-glyph-default" href="<?= site_url('blogs') ?>">VIEW ALL</a>
                        <a class="btn btn-glyph" href="<?= site_url('blogs') ?>">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>

                </div>
            </div>
            <div class="komiks-container unpadded txtcenter col-md-6">
                <div class="komiks-content">

                    <div class="heading row col-md-12">
                        <div class="col-md-6 col-sm-6 komiks-title unpadded col-xs-6">
                            <h3>KOMIKS OF THE WEEK</h3>
                        </div>
                        <div class="col-md-6 unpadded col-sm-6 enlarge-komiks col-xs-6">
                            <div class="btn-group  pull-right">
                                <a class="btn btn-glyph">
                                    <span class="glyphicon glyphicon-search"></span>
                                </a>
                                <a class="btn btn-glyph-default" href="javascript:void(0)" onclick="chooks.loadImage(<?= isset($featured_comics[0]['id']) ? $featured_comics[0]['id'] : '' ?>, <?= isset($featured_comics[0]['id']) ? "'comics'" : ''  ?>)">CLICK TO ENLARGE</a>

                            </div>
                        </div>
                    </div>
                    <br class="clear" /></br>
                    <div class="social-image  unpadded txtcenter">
                        <img class="img-responsive" src="<?= @str_replace('~path~', base_url(), $featured_comics[0]['photo']) ?>" alt="social card" />
                    </div>
                    <br class="clear" />
                    <br/>
                    <div class="col-md-5 pull-right">
                        <div class="btn-group pull-right">
                            <a class="btn btn-glyph-default" href="<?= site_url('chikoy_corner') ?>">VIEW ALL</a>
                            <a class="btn btn-glyph" href="<?= site_url('chikoy_corner') ?>">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>

                    </div>
                    <br class="clear" />
                    <br/>


                </div>

            </div>

        </div>
        <br class="clear" />
        <br/>

    </div>
    <?php /*
    <!-- /chooks-blog -->
    <div class="banner-bg wave store-locator" id="store-locator">

        <div class="store-locator-content container">
            <div class="heading txtcenter">
                <h2 class="title-heading">STORE LOCATOR</h2>
                <span>Chooks-to-Go store ba hanap mo? Search ka lang dito!</span>
            </div>
            <br class="clear" />
            <br/>
            <br/>
            <div class="img-responsive map-container col-xs-10 unpadded map col-md-8 txtcenter" id="map-container">
                <!-- <img class="img-responsive" src="assets/img/map.jpg" alt="map" /> -->
                <div id="google_map" style="height: 451px"></div>
                <div class="btn-group find-store">
                    <a class="btn btn-rounded" id="find_nearby_store2">
                        <span class="glyphicon glyphicon-search"></span>
                    </a>
                    <a class="btn btn-glyph-rounded" href="javascript:void(0)" id="find_nearby_store">FIND NEARBY STORE</a>

                </div>
            </div>
            <br/>
            <div class="col-md-4 map-details">
                <div class="img-logo">
                    <img class="img-responsive" src="assets/img/chooks-locator-logo.png" alt="chooks locator" />
                </div>
                <br/>
                <div class="dropdowns">
                    <select name="area" class="areaa" onchange="chooks.googleSelectArea()">
                        <option value="">Province</option>
                        <? foreach($provinces as $k => $v): ?>
                            <option value="<?=$v['province_id']?>"><?=$v['province']?></option>
                        <? endforeach; ?>
                    </select>
                    <select name="city" class="cityy" disabled onchange="chooks.googleSelectCity()">
                        <option value="">City / Municipality</option>
                    </select>
                    <select name="store" class="store" disabled id="store">
                        <option value="11.646338452790836,122.72092948635861,a,a,a,a,a,5">Store</option>
                    </select>
                </div>
                <br/>
                <a class="btn btn-default col-md-12 col-sm-12 col-xs-12" href="#map-container" id="search_store" onclick="chooks.googleSelectStore()">SEARCH STORE</a>
                <br class="clear" />
                <br/>
                <h4 class="gmap-error _hide"></h4>
                <div class="address _hide">
                    <h4 id="store_name"></h4>
                    <dt>Address:</dt>
                    <dd id="store_address"></dd>
                    <br/>
                    <dt class="_hide">Phone Number:</dt>
                    <dd id="store_contact" class="_hide"></dd>
                    <dt class="_hide">Branch Code:</dt>
                    <dd id="store_code" class="_hide"></dd>
                    <dt class="_hide">Operating hours:</dt>
                    <dd id="store_hour" class="_hide"></dd>
                    <br/>
                </div>
            </div>
            <br class="clear" />
            <div class="bottom-img visible-lg">

            </div>
        </div>
        <br class="clear" />
    </div>
    <br/>
    <br/>
    <!--/store-locator-->
    */ ?>
</section>
