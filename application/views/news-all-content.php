<section class="main">
    <div class="news-all banner-bg"></div><div class="wave clearfix"></div>
    <div class="news-update-list">
         <div class="heading txtcenter">
                        <h2 class="title-heading">NEWS AND UPDATES</h2>
                        <span>Alam mo ba ang latest? Well, malalaman mo dito! Basa na para hindi mahuli sa balita!</span>
            </div><br class="clear"/><br/>
           <div class="update-list-content container">
                    <div class="blog-list row unpadded">
                            <? foreach($news as $k => $v): ?>
                                <div class="blog-item  col-md-12 col-sm-12 col-xs-12 ">
                                        <div class="col-md-4 col-sm-3 thumbnail">
                                                <img class="img-responsive" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>" alt="blog image"/>
                                        </div>
                                           <div class="col-md-8 col-sm-9 blog-list-post">
                                                <h4><?= $v['title'] ?></h4>
                                                <p><?= date('F d, Y', strtotime($v['date_of_publish'])) ?></p>
                                                <p class="details">
                                                        <?= character_limiter($v['description'], 300) ?>
                                                </p>
                                                <div class="btn-group txtcenter">
                                                        <a class="btn btn-glyph-default" href="<?= site_url('news/article') ?>/<?= $v['id'] ?>">READ MORE</a>
                                                        <a class="btn btn-glyph" href="<?= site_url('news/article') ?>/<?= $v['id'] ?>">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                        </a>
                                                </div>
                                                <div class="share-button">
                                                    <fb:share-button href="<?= site_url() ?>news/article/<?= $v['id'] ?>" data-type="box_count"></fb:share-button>

                                                </div>
                                        </div>

                                </div>
                            <? endforeach; ?>
                    </div>

           </div>
    </div>
    <!--end news-update-list-->
     <div class="wave clearfix"></div><br class="clear"/><br/>
</section>