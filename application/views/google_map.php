<html>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>E</title>
<head>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAjxtjNyjATUWGLx-1v0zehvr0bkc0ACnE&sensor=false"></script>
<script type="text/javascript">
	function initialize() {
		var area = document.getElementById('area');
		var city = document.getElementById('city');
		var store = document.getElementById('store');	
		var icon = '<?= base_url() ?>assets/admin/images/pin-legend.png';

		var mapProp = {
			center: new google.maps.LatLng(11.646338452790836, 122.72092948635861),
			zoom: 5,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		var map = new google.maps.Map(document.getElementById('googleMap'), mapProp);

		<? if($stores): ?>
			<? foreach($stores as $key => $v): ?>
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(<?= $v['latitude'] ?>, <?= $v['longitude'] ?>),
					map: map,
					icon: icon,
					title: "<?= $v['name'] ?>"
				});
			<? endforeach; ?>
		<? endif; ?>

		function locate(Lat, Long, zoomLevel) {
			var location = new google.maps.LatLng(Lat, Long);
			map.panTo(location);
			map.setZoom(parseInt(zoomLevel));
		}

		google.maps.event.addDomListener(area, 'change', function(){
			var location = area.value.split(',');
			locate(location[0], location[1], location[2]);
		});

		google.maps.event.addDomListener(city, 'change', function(){
			var location = city.value.split(',');
			locate(location[0], location[1], location[2]);
		});

		google.maps.event.addDomListener(store, 'change', function(){
			var location = store.value.split(',');
			locate(location[0], location[1], location[2]);
		});
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>
</head>
<body>

<div id="googleMap" style="width: 500px; height: 380px"></div>

<table>
	<tr>
		<td>Area</td>
		<td>
			<select id="area" class="select">
				<option value="11.646338452790836, 122.72092948635861, 5"></option>
				<? foreach($areas as $key => $v): ?>
					<option value="<?= $v['latitude'] ?>, <?= $v['longitude'] ?>, <?= $v['zoom_level'] ?>"><?= $v['name'] ?></option>
				<? endforeach; ?>
			</select>
		</td>
	</tr>

	<tr>
		<td>City</td>
		<td>
			<select id="city" class="select">
				<option value="11.646338452790836, 122.72092948635861, 5"></option>
				<? foreach($cities as $key => $v): ?>
					<option value="<?= $v['latitude'] ?>, <?= $v['longitude'] ?>, <?= $v['zoom_level'] ?>"><?= $v['name'] ?></option>
				<? endforeach; ?>
			</select>
		</td>
	</tr>

	<tr>
		<td>Stores</td>
		<td>
			<select id="store" class="select">
				<option value="11.646338452790836, 122.72092948635861, 5"></option>
				<? foreach($stores as $key => $v): ?>
					<option value="<?= $v['latitude'] ?>, <?= $v['longitude'] ?>, <?= $v['zoom_level'] ?>"><?= $v['name'] ?></option>
				<? endforeach; ?>
			</select>
		</td>
	</tr>
</table>

</body>
</html>