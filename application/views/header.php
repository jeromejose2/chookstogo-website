<!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="chooks to go, chooks to go delivery, chooks to go delivery number, chooks to go franchise, chooks to go price, chooks to go order online, chooks to go branches, spicy neck chooks, chooks to go delivery price list, chooks to go franchise fee, chooks to go chooksies, chooks to go dressed chicken, chooks to go cut ups, chooks to go spicy necks, chooks to go marinado, chooksies cut ups, chooksies dressed chicken, chooksies spicy necks, chooksies marinado, chooks to go roasted chicken, chooks to go stores, Kapitan Chooks, chooks to go liempo, chooks to go sweet roast, chooks to go pepper roast, chooks to go fried, chooks to go hotdog, chooks to go chicken nuggets, chooks to go pork barrel, chooks to go chikoy, chooks to go tom crew, chooks to go careers, chooks to go news">
    <title>Chooks To Go</title>
    <!--
    <link href="<?=base_url()?>assets/vendors/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/vendors/lytebox/lytebox.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/vendors/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/vendors/owl-carousel/owl.theme.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/vendors/owl-carousel/owl.transitions.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    -->


    <link rel="stylesheet" href="<?=base_url()?>assets/css/output.min.css">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div id="fb-root"></div>
    <script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=1417574621812123&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
    <!--[if lt IE 9]>
            <div class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</div>
    <![endif]-->
    <header>
        <nav class="navbar  " role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img class="img-responsive" src="assets/img/logo.png?v=1.2">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav col-md-8 col-sm-12 col-xs-12 pull-right navbar-nav">
                        <li>
                            <a href="corporate.php#about-us">About Us</a>
                        </li>
                        <li>
                            <a href="corporate.php#careers">Careers</a>
                        </li>
                        <li>
                            <a href="corporate.php#news-updates">News and Updates</a>
                        </li>
                        <li class="yellow-nav">
                            <i class="visible-lg fa fa-caret-left"></i>
                            <a href="corporate.php">Chooks Corporate</a>
                        </li>
                        <li class="yellow-nav">
                            <a href="login.php">Ka-Chooks Login</a>
                        </li>
                    </ul>
                    <ul class="nav second-nav col-sm-12 col-xs-12 pull-left col-md-8  navbar-nav">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li>
                            <a href="products.php">All Products</a>
                        </li>
                        <li>
                            <a href="index.php#store-locator">Store Locator</a>
                        </li>
                        <li>
                            <a href="promos.php">Promos</a>
                        </li>
                        <li>
                            <a href="blog.php">Chooks Blog</a>
                        </li>
                        <li>
                            <a href="chikoys-corner.php">Chikoy’s Corner</a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
    </header>
