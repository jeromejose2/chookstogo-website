<!-- COMICS -->
<? foreach($comics as $k => $v): ?>
    <div class="col-md-3 col-sm-4">
        <div class="thumbnail komiks-img col-md-11">
            <img class="img-responsive" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>">
            <div class="hover-enlarge">
                <button class="col-md-8 col-xs-8 col-sm-9 click-enlarge" onclick="loadImage(<?= $v['id'] ?>, <?= isset($v['extension']) ? "'downloadable'" : "'comics'" ?>)"><span class="glyphicon glyphicon-search"></span> Click to enlarge</button>
            </div>
        </div><br class="clear" />
        <div class="komiks-shot-details txtcenter">
            <h4><?= @$v['title'] ?></h4><br/>
            <? if(isset($v['extension'])): ?>
                <div class="btn-group txtcenter">
                    <a class="btn btn-glyph-default btn-glyph-gray" href="<?= !$is_downloadables ? site_url('chikoy_corner/download') : site_url('chikoy_corner/downloadables') ?>/<?= $v['id'] ?>">DOWNLOAD</a>
                    <a class="btn btn-glyph btn-glyph-light-gray" href="<?= !$is_downloadables ? site_url('chikoy_corner/download') : site_url('chikoy_corner/downloadables') ?>/<?= $v['id'] ?>">
                        <span class="glyphicon glyphicon-arrow-down"></span>
                    </a>
                </div>
            <? endif; ?>
        </div><br/><br/>
    </div>
<? endforeach; ?>
<!-- END COMICS -->

<script type="text/javascript">
    $('.komiks-img').hover( function() {
        $(this).find('.hover-enlarge').animate({top:0});
    }, function() {
        $(this).find('.hover-enlarge').animate({top:300});
    });

    function loadImage(id, type) {
        var data = { id : id, type : type };
        lytebox.load({url:site_url+"popups/large_image", data:data, top: 10});
    }
</script>