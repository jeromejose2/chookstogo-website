<section class="main">
    <div class="banner-bg promo-list">
           <div class="container promo-content">
                     <div class="heading txtcenter">
                        <h2 class="title-heading">PROMOTIONS</h2>
                           <span>
                                Sali ka na sa mga kwelang promotions namin. Baka ikaw pa ang manalo ng astig na premyo!
                            </span>
                    </div><br/>
                    <div class="tab-content">
                        <ul class="nav nav-pills  promo-tab hidden-xs promo-tab-ajax">
                            <li data-value="ongoing"><a href="javascript:void(0)" class="active">Ongoing Promotion</a><i class="hidden-xs arrow-down"></i></li>
                            <li data-value="past"><a href="javascript:void(0)">Past Promotions</a></li>


                        </ul>
                        <select class="visible-xs promo-select col-xs-10 select-promotion" name="promotions">
                                <option value="ongoing">Ongoing Promotion</option>
                                <option value="past">Past Promotions</option>
                        </select>
                    </div>
                    <div class="product-slider-container" id="promo-list">
                        <div class="cat-promos owl-carousel owl-products-theme">

                            <!-- PROMOS -->
                            <? foreach($promos as $k => $v): ?>
                                <div class="item">
                                    <img class="img-responsive" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>" alt="slider image"><br/>
                                    <div class="txtcenter">
                                        <h4><?= $v['title'] ?></h4>
                                            <div class="btn-group padded txtcenter">
                                                <a class="btn promo-details-btn btn-glyph-default" href="javascript:void(0)" data-title="<?= $v['title'] ?>" data-description='<?= character_limiter(str_replace('</p>', '', str_replace('<p>', '', strip_tags($v['description']))), 570) ?>' data-fburl = "https://www.facebook.com/chookstogo" data-gotourl = "<?= $v['link'] ?>" data-readmoreurl = "<?= site_url('promos/select') ?>/<?= $v['id'] ?>" data-thumbnail="<?= str_replace('~path~', base_url(), $v['photo']) ?>" data-badge ="<?= $v['promo_type'] == 'Digital Promo' ? 'Digital' : 'In-Store' ?>" data-date="<?= ( date('Y', strtotime($v['promo_start_date'])) == date('Y', strtotime($v['promo_end_date'])) ) ? date('F', strtotime($v['promo_start_date'])) . ' - ' . date('F Y', strtotime($v['promo_end_date'])) : date('F Y', strtotime($v['promo_start_date'])) . ' - ' . date('F Y', strtotime($v['promo_end_date'])) ?>" data-openlink="<?=$v['open_link']?>">DETAILS</a>
                                                <a class="btn btn-glyph promo-details-btn" data-title="<?= $v['title'] ?>" data-description='<?= character_limiter(str_replace('</p>', '', str_replace('<p>', '', strip_tags($v['description']))), 570) ?>' data-fburl = "https://www.facebook.com/chookstogo" data-gotourl = "<?= $v['link'] ?>" data-readmoreurl = "<?= site_url('promos/select') ?>/<?= $v['id'] ?>" data-thumbnail="<?= str_replace('~path~', base_url(), $v['photo']) ?>" data-badge ="<?= $v['promo_type'] == 'Digital Promo' ? 'Digital' : 'In-Store' ?>" data-date="<?= ( date('Y', strtotime($v['promo_start_date'])) == date('Y', strtotime($v['promo_end_date'])) ) ? date('F', strtotime($v['promo_start_date'])) . ' - ' . date('F Y', strtotime($v['promo_end_date'])) : date('F Y', strtotime($v['promo_start_date'])) . ' - ' . date('F Y', strtotime($v['promo_end_date'])) ?>" data-openlink="<?=$v['open_link']?>">
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                </a>
                                            </div>
                                    </div>
                                </div>
                            <? endforeach; ?>
                            <!-- END PROMOS -->

                        </div>
                    </div>


           </div>
    </div>
    <div class="wave promo-details" id="product-details">
            <div class="container promo-details-content">
                    <div class="col-md-4 promo-thumbnail col-sm-4">
                            <img class="img-responsive"/>
                    </div>
                    <div class="col-md-8 description col-sm-8">
                            <h2></h2>
                            <span class="badge-cat col-md-2 col-sm-4 col-xs-12"></span><span class="col-sm-7 col-md-4 col-xs-12 promo-date"></span><br class="clear"/><br/>
                            <span class="description"></span><br class="clear"/><br/>

                             <div class="order-share-buttons">
                                    <div class="btn-group txtcenter hidden-xs">
                                        <a class="btn btn-read-more btn-glyph-default" href="#">READ MORE</a>
                                        <a class="btn btn-glyph promo-readmore-btn">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </a>
                                    </div>
                                    <div class="btn-group txtcenter">
                                        <a class="btn btn-goto-app btn-glyph-default" href="#">GO TO APP</a>
                                        <a class="btn btn-glyph promo-gotoapp-btn">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </a>
                                    </div>
                                     <div class="share-button">

                                     </div>
                              </div>


                    </div>



            </div>


    </div><br class="clear"/><br/>
</section>
