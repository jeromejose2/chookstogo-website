<!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= isset($title) ? $title : 'Chooks To Go' ?></title>
    <meta property="fb:app_id" content="689783817758453">
    <meta property="og:title" content="<?= @$og_title ?>">
    <meta property="og:image" content="<?= @$og_image ?>">
    <meta property="og:type" contet="website">
    <meta property="og:url" content="<?= @$og_url ?>">
    <meta property="og:description" content="<?= @str_replace('</p>', '', str_replace('&lt;/p&gt;', '', character_limiter($og_description, 100))) ?>">
    <link rel="icon" href="<?= base_url() ?>assets/img/favicon.ico" type="image/x-icon" />
    
    <!--
    <link href="<?= base_url() ?>assets/vendors/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/vendors/lytebox/lytebox.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/vendors/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/vendors/owl-carousel/owl.theme.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/vendors/owl-carousel/owl.transitions.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/main.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/blogslider.css">
    -->

    <link rel="stylesheet" href="<?= base_url() ?>assets/css/output.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    
    <!--[if lt IE 10]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <header>
        <nav class="navbar  " role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= site_url() ?>">
                        <img class="img-responsive" src="<?= base_url() ?>assets/img/logo.png">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav col-md-8 col-sm-12 col-xs-12 pull-right navbar-nav">
                        <li>
                            <a href="<?= site_url('corporate/people') ?>" class="<?= $this->uri->segment(1) == 'corporate' && $this->uri->segment(2) == 'people' ? 'active' : '' ?>">About Us</a>
                        </li>
                        <li>
                            <a href="<?= site_url('careers') ?>" class="<?= $this->uri->segment(1) == 'careers' ? 'active' : '' ?>">Careers</a>
                        </li>
                        <li>
                            <a href="<?= site_url('news') ?>" class="<?= $this->uri->segment(1) == 'news' ? 'active' : '' ?>">News and Updates</a>
                        </li>
                        <li class="yellow-nav">
                            <i class="visible-lg fa fa-caret-left"></i>
                            <a href="<?= site_url('corporate') ?>">Chooks Corporate</a>
                        </li>
                        <li class="yellow-nav">
                            <a href="<?= $this->session->userdata('sess_id') ? site_url('logout') : site_url('login') ?>"><?= $this->session->userdata('sess_id') ? 'Logout' : 'Ka-Chooks Login' ?></a>
                        </li>
                    </ul>
                    <ul class="nav second-nav col-sm-12 col-xs-12 pull-left col-md-8  navbar-nav">
                        <li>
                            <a href="<?= site_url() ?>" class="<?= $this->uri->segment(1) == '' || $this->uri->segment(1) == 'home' ? 'active' : '' ?>">Home</a>
                        </li>
                        <li>
                            <a href="<?= site_url('products') ?>" class="<?= $this->uri->segment(1) == 'products' ? 'active' : '' ?>">All Products</a>
                        </li>
                        <?php /*
                        <li>
                            <a href="<?= site_url('home') ?>#store-locator">Store Locator</a>
                        </li>
                        */ ?>
                        <li>
                            <a href="<?= site_url('promos') ?>" class="<?= $this->uri->segment(1) == 'promos' ? 'active' : '' ?>">Promos</a>
                        </li>
                        <li>
                            <a href="<?= site_url('blogs') ?>" class="<?= $this->uri->segment(1) == 'blogs' ? 'active' : '' ?>">Chooks Blog</a>
                        </li>
                        <li>
                            <a href="<?= site_url('chikoy-corner') ?>" class="<?= $this->uri->segment(1) == 'chikoy-corner' ? 'active' : '' ?>">Chikoy’s Corner</a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
    </header>

<!-- CONTENT -->
<?= $content ?>
<!-- END CONTENT -->

<footer>
    <div class="footer-content">
        <div class="hl visible-lg"></div>
        <div class="container">
            <div class="col-md-6 col-lg-4 footer-feedback col-sm-6">
                <h4>SEND FEEDBACK
                    <span>Say hello. Let us know.</span>
                </h4>
                <section>
                    <!-- typeform html -->
                    <h5 class="feedback-sent yellow" style="dsiplay: none"></h5>
                    <form class="type-form" id="typeform-footer" onsubmit="return false" method="POST">

                        <ul>
                            <li>
                                <label>1
                                    <span class="glyphicon glyphicon-arrow-right"></span> First, Please tell us your name</label>
                                <input type="text" data-error="Oops! Please input your first name to continue" name="name" class="f-required">
                            </li>

                            <li>
                                <label>2
                                    <span class="glyphicon glyphicon-arrow-right"></span> What is your email address</label>
                                <input type="text" data-error="Oops! Please input your email to continue" name="email" class="f-required f-email">
                            </li>

                            <li>
                                <label>3
                                    <span class="glyphicon glyphicon-arrow-right"></span> What is your contact number</label>
                                <input type="text" data-error="Oops! Please input your contact number to continue" name="contact" class="f-required" maxlength="11">
                            </li>

                            <li>
                                <label>4
                                    <span class="glyphicon glyphicon-arrow-right"></span> What is your address</label>
                                <input type="text" name="address" data-error="Oops! Please input your address to continue" class="f-required">
                            </li>

                            <li>
                                <label>5
                                    <span class="glyphicon glyphicon-arrow-right"></span> Subject</label>
                                <input type="text" data-error="Oops! Please enter a subject to continue" name="subject" class="f-required">
                            </li>

                            <li>
                                <label>6
                                    <span class="glyphicon glyphicon-arrow-right"></span> Message</label>
                                <textarea data-error="Oops! Please enter your message to continue" name="message" class="f-required" id="tteesstt"></textarea>
                            </li>

                        </ul>
                    </form>
                    <!-- end typeform -->
                    <div class="progress" style="display:none">
                      <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                        <span class="progress-step">0 / 6</span>
                      </div>
                    </div>
                    <button class="btn btn-default type-form-btn" onclick="tform.next()">CONTINUE</button>
                </section>
            </div>
            <div class="col-md-2  col-sm-6 hidden-xs">
                <h4>QUICK LINKS</h4>

                <ul class="footer-nav">
                    <li><a href="<?= site_url() ?>">Home</a></li>
                    <li><a href="<?= site_url('products') ?>">All Products</a></li>
                    <!-- <li><a href="<?= site_url('home') ?>#store-locator">Store Locator</a></li>-->
                    <li><a href="<?= site_url('promos') ?>">Promos</a></li>
                    <li><a href="<?= site_url('blogs') ?>">Chooks Blog</a></li>
                    <li><a href="<?= site_url('chikoy-corner') ?>">Chikoy's Corner</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 hidden-xs">
                <h4>CHOOKS CORPORATE</h4>

                <ul class="footer-nav">
                    <li><a href="<?= site_url('corporate/people') ?>">About Us</a></li>
                    <li><a href="<?= site_url('careers') ?>">Careers</a></li>
                    <li><a href="<?= site_url('news') ?>">News and Updates</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12  ">
                <h4>SOCIAL NETWORK</h4>
                <ul class="footer-nav visible-sm visible-md visible-lg">
                    <li><a href="https://www.facebook.com/chookstogo" target="_blank"><i class="social-icons-facebook"></i>Facebook</a>
                    </li>
                    <li><a href="https://twitter.com/chookstogoph" target="_blank"><i class="social-icons-twitter"></i>Twitter</a>
                    </li>
                    <li><a href="http://www.youtube.com/channel/UC1mn-pF58NABjDwMoaLeeyQ" target="_blank"><i class="social-icons-youtube"></i>Youtube</a>
                    </li>
                    <li><a href="http://instagram.com/chookstogoph" target="_blank"><i class="social-icons-instagram"></i>Instagram</a>
                    </li>

                </ul>
                <div class="unpadded txtcenter share-big col-xs-12 visible-xs">
                    <a href="#"><i class="social-icons-facebook-big"></i></a>
                    <a href="#"><i class="social-icons-twitter-big"></i></a>
                    <a href="#"><i class="social-icons-youtube-big"></i></a>
                    <a href="#"><i class="social-icons-instagram-big"></i></a>
                </div>
                <br class="clear" />
                <br/>
            </div>
        </div>

        <div class="container">
            <div class="footer-logo">
                <img class="img-responsive" src="<?= base_url() ?>assets/img/footer-logo.png" alt="logo" />
            </div>
            <br class="clear" />
            <div class="footer-address">
                <ul>
                    <li>
                        <span>Unit 1007, The Taipan Place Condominium, F. Ortigas Jr. Ave., Ortigas Center, Pasig City 1605</span>
                    </li>
                    <li>
                        <span>Telephone (02) 687-7230 or (02) 588-1322</span>
                    </li>
                </ul>
            </div>
        </div>

        <div class="copyright ">
            <div class="container">
                <span>
                    Copyright 2014 Bounty Agro Ventures, Inc. All rights reserved.
                </span>
            </div>
        </div>
    </div>
</footer>

<!-- ANALYTICS -->
<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42684485-15', 'auto');
  ga('create', 'UA-16532684-3', 'auto', {'name': 'chooks'});
  ga('send', 'pageview');
  ga('chooks.send', 'pageview');

</script>
-->

<!--
<script src="<?= base_url() ?>assets/vendors/jquery/jquery.js"></script>
<script src="<?= base_url() ?>assets/vendors/lytebox/lytebox.js"></script>
<script src="<?= base_url() ?>assets/js/jTypeform.min.js"></script>
<script src="<?= base_url() ?>assets/vendors/bootstrap/js/bootstrap.js"></script>
<script src="<?= base_url() ?>assets/vendors/owl-carousel/owl.carousel.js"></script>
<script src="<?= base_url() ?>assets/js/tform.min.js"></script>
<script src="<?= base_url() ?>assets/js/chookstogo.js?r=php"></script>
<script src="<?= base_url() ?>assets/js/jMask.js"></script>
-->

<div id="fb-root"></div>

<script>
var site_url = '<?= site_url() ?>';
var base_url = '<?= base_url() ?>';
var is_forgot = <?= isset($is_forgot) ? $is_forgot : 0 ?>;
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=689783817758453&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })
  (window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42684485-15', 'auto');
  ga('create', 'UA-16532684-3', 'auto', {'name': 'chooks'});
  ga('send', 'pageview');
  ga('chooks.send', 'pageview');
</script>

<script>
    (function(d,a,h){var c='fdmobile',b='script',s=a.createElement(b);s.async=true;s.src=['/m.'+h,'splash','js','m.js'].join('/');d[c]=d[c]||{};d[c].h=h;var g=a.getElementsByTagName(b)[0];g.parentNode.insertBefore(s,g);});(window,document,'foodpanda.ph');
</script>

<script src="<?= base_url() ?>assets/js/output.min.js"></script>

<script>
    var lytebox = new Lytebox;
    var chooks = new ChooksToGo;
    var reCaptcha = <?= $this->uri->segment(1) == 'login' ? 1 : 0 ?>;
    var pid = <?= isset($product_id) ? $product_id : 0 ?>;
    var pcat = "<?= isset($product_category) ? $product_category : 0 ?>";
    var confirmed_user = <?= isset($confirmed_user) ? $confirmed_user : 0 ?>;

    $(document).ready(function(){
        <? if(isset($is_confirm) && $is_confirm == TRUE): ?>
            $('.error-login').html('Account has been verified. You may now log-in');
        <? endif; ?>
    });
</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 964571304;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>

<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/964571304/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>

<!-- <script src="<?= base_url() ?>assets/js/app.min.js"></script> -->
<script src="<?= base_url() ?>assets/js/app.js?r=<?= rand() ?>"></script>
<script src="<?= base_url() ?>assets/js/ctg.js?r=<?= rand() ?>"></script>

<!--blogslider-->
<script src="<?= base_url() ?>assets/js/blogslider.js?r=<?= rand() ?>"></script>

</body>
</html>
