<section class="main">
    <div class="corporate-banner">
            <div class="our-company txtcenter col-md-6 col-sm-8 col-xs-11 absolute-center">
                   <div class="our-company-content">
                            <img src="assets/img/our-company.png" class="img-responsive"/><br class="clear">
                            <span>Chooks-to-Go is a chain of stores owned by Bounty Agro Ventures, Inc. (BAVI), a privately owned company operating in the Philippines. It offers roast chicken and processed meats for off-premise consumption, and is the largest roasted chicken retail business in the country.</span>

                   </div>

            </div>

    </div>

    <!-- end corporate-banner -->


    <div class="about-us wave" id="about-us">
        <div class="about-us-content container">
                <div class="heading txtcenter">
                    <h2 class="title-heading">ABOUT CHOOKS-TO-GO</h2>
                    <span>Inside our company</span>
                </div>
                <br class="clear" />
                <div class="about-entry">
                        <div class=" about-img col-md-4 col-sm-6 col-xs-6">
                                <div class="thumbnail">
                                    <img class="img-responsive" src="assets/img/about.jpg" alt="thumbnail" />

                                </div>

                        </div>
                        <div class="col-md-8 description col-sm-6 col-xs-6">
                                <h4>OUR PEOPLE</h4>
                                <span class="about-msg">
                                    Meet the amazing and dedicated people behind Chooks-To-Go. Their hard work and perseverance in constantly improving our products and services is always an inspiration to others in the company.
                                </span>
                                <br class="clear" />
                                <br/>
                                <div class="btn-group padded txtcenter">
                                    <a class="btn btn-glyph-default" href="<?= site_url('corporate/people') ?>">VIEW MORE</a>
                                    <a class="btn btn-glyph" href="<?= site_url('corporate/people') ?>">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                </div>

                        </div>

                </div>


        </div><br class="clear"/><br/>

    </div>
    <!-- end about-us-->


    <div class="banner-bg wave news-updates" id="news-updates">
            <div class="news-updates-content container">
                    <div class="heading txtcenter">
                        <h2 class="title-heading">NEWS AND UPDATES</h2>
                        <span>Be informed and stay updated with all our latest exciting news.</span>
                    </div>
                    <br class="clear" /><br/>
                        <div class="blog-list row unpadded">
                            <? foreach($news as $k => $v): ?>
                                <div class="blog-item  col-md-12 col-sm-12 col-xs-12 ">
                                        <div class="col-md-4 col-sm-3 thumbnail">
                                                <img class="img-responsive" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>" alt="blog image"/>
                                        </div>
                                           <div class="col-md-8 col-sm-9 blog-list-post">
                                                <h4><?= $v['title'] ?></h4>
                                                <p><?= date('F m, Y', strtotime($v['date_of_publish'])) ?></p>
                                                <p>
                                                        <?= character_limiter($v['description'], 300) ?>
                                                </p>
                                                <div class="btn-group txtcenter">
                                                        <a class="btn btn-glyph-default" href="<?= site_url('news/article') ?>/<?= $v['id'] ?>">READ MORE</a>
                                                        <a class="btn btn-glyph" href="<?= site_url('news/article') ?>/<?= $v['id'] ?>">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                        </a>
                                                </div>
                                                <div class="share-button">
                                                    <fb:share-button href="<?= site_url() ?>news/article/<?= $v['id'] ?>" data-type="box_count"></fb:share-button>

                                                </div>
                                        </div>

                                </div>
                            <? endforeach; ?>
                        <a class="btn btn-glyph-default btn-glyph-gray" href="<?= site_url('news') ?>">VIEW ALL</a>
               </div>
           <br class="clear"/><br/>
        </div>
    </div>
    <!-- end new-updates-->
    <div class="careers" id="careers">
            <div class="careers-content container">
                 <div class="heading txtcenter">
                        <h2 class="title-heading">CAREERS</h2>
                        <span>Chooks-To-Go opportunity</span>
                    </div><br class="clear"/>
                    <div class="search-careers col-md-6 col-sm-12 relative-center txtcenter">
                            <span>Our business is always growing! Interested in working for a company full of opportunity and fulfillment? Have a look at the job openings below to see if we have a vacancy that you could fill.</span><br class="clear"/><br/>

                            <div class="select-careers relative-center txtcenter">
                                <form action="<?= site_url('careers/search') ?>" method="GET">
                                 <select class="col-md-5 col-sm-5 col-xs-10" name="field">
                                    <option>FIELD OF WORK</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Administration' ?>>Administration</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Animal Health Group' ?>>Animal Health Group</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Business / Product Development' ?>>Business / Product Development</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Compliance Assurance' ?>>Compliance Assurance</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Contract Growing' ?>>Contract Growing</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Sales - Rotisserie/Chooks-to-Go' ?>>Sales - Rotisserie/Chooks-to-Go</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Customer Service / Delivery' ?>>Customer Service / Delivery</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Dressing Plant' ?>>Dressing Plant</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Engineering' ?>>Engineering</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Feed Mill' ?>>Feed Mill</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Finance' ?>>Finance</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Human Resources' ?>>Human Resources</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Information Systems' ?>>Information Systems</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Legal' ?>>Legal</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Live Sales' ?>>Live Sales</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Logistics' ?>>Logistics</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Management Control System' ?>>Management Control System</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Marketing' ?>>Marketing</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Materials Management /Purchasing' ?>>Materials Management /Purchasing</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Motor Pool Services' ?>>Motor Pool Services</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Sales - Supermarket/Trade Distributor' ?>>Sales - Supermarket/Trade Distributor</option>
                                    <option <?= isset($careers[0]['field_of_work']) && $careers[0]['field_of_work'] == 'Utility/Warehouse/Liaison' ?>>Utility/Warehouse/Liaison</option>
                                </select>
                                  <select class="col-md-5 col-sm-5 col-xs-10" name="location">
                                    <option>LOCATION</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Ilocos' ? 'selected' : '' ?>>Ilocos</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Isabela' ? 'selected' : '' ?>>Isabela</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Pangasinan' ? 'selected' : '' ?>>Pangasinan</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Central Luzon' ? 'selected' : '' ?>>Central Luzon</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Southern Tagalog' ? 'selected' : '' ?>>Southern Tagalog</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Bicol' ? 'selected' : '' ?>>Bicol</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Head Office - Pasig' ? 'selected' : '' ?>>Head Office - Pasig</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Bacolod' ? 'selected' : '' ?>>Bacolod</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Dumaguete' ? 'selected' : '' ?>>Dumaguete</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Cebu' ? 'selected' : '' ?>>Cebu</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Iloilo' ? 'selected' : '' ?>>Iloilo</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Roxas' ? 'selected' : '' ?>>Roxas</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Tacloban' ? 'selected' : '' ?>>Tacloban</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Ormoc' ? 'selected' : '' ?>>Ormoc</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Calbayog' ? 'selected' : '' ?>>Calbayog</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Cagayan de Oro' ? 'selected' : '' ?>>Cagayan de Oro</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Davao' ? 'selected' : '' ?>>Davao</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'General Santos' ? 'selected' : '' ?>>General Santos</option>
                                    <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Zamboanga' ? 'selected' : '' ?>>Zamboanga</option>
                                </select>

                            </div><br class="clear"/><br/><br/>
                            <div class="search-btn">
                                     <div class="btn-group find-store">
                                        <a class="btn btn-rounded">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </a>
                                        <button class="btn btn-glyph-rounded search-jobs-btn">SEARCH JOBS</button>
                                        </form>
                                    </div>
                            </div>

                    </div>
                    <div class="button-apply-back">


                    </div>






            </div><br class="clear"/><br/>


    </div>
    <!--end careers-->
    <div class="wave clearfix"></div><br class="clear"/><br/>


</section>