<section class="main">
    <div class="news-all banner-bg"></div><div class="wave clearfix"></div>
    <div class="news-update-list careers-list-results">
         <div class="heading txtcenter">
                        <h2 class="title-heading">CAREERS</h2>
                        <span>Ka-Chooks opportunity</span>
            </div><br class="clear"/><br/>
           <div class="result-list-content container">
                <?php foreach($careers as $k => $v): ?>
                <h3><?= $v['position'] ?></h3>
                <span><?= $v['location'] ?></span><br class="clear"/><br/>
                <p class="title">Qualifications:</p>

                      <?= $v['qualifications'] ?>

                <p class="title">Responsibilities:</p>

                    <?= $v['description'] ?>


                <br class="clear">
                <?php endforeach; ?>

                <div class="btn-group txtcenter">
                       <a class="btn btn-glyph btn-glyph-light-gray" href="<?= site_url('careers') ?>">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="btn btn-glyph-default btn-glyph-gray" href="<?= site_url('careers') ?>">BACK</a>
                </div>
                <div class="btn-group txtcenter">

                        <a class="btn btn-glyph-default btn-glyph apply-career" href="javascript:void(0)">APPLY</a>
                        <a class="btn btn-glyph btn-glyph-">
                            <span class="glyphicon glyphicon-chevron-right apply-career" href="javascript:void(0)"></span>
                        </a>
                </div>



           </div>

           <br class="clear"/><br/>
    </div>
    <!--end news-update-list-->
     <div class="wave clearfix"></div><br class="clear"/><br/>
</section>
