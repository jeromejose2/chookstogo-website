<section class="main">
    <div class="banner-bg product-list">
           <div class="container product-content">
                     <div class="heading txtcenter">
                        <h2 class="title-heading">ALL PRODUCTS</h2>
                           <span> Mahilig ka ba sa mga masarap na pagkain? Marami kaming ganyan!</span><br/>
                           <span>Dahan-dahan lang sa pagtingin, baka magutom ka ng sobra!</span>
                    </div><br/>
                    <div class="tab-content">
                        <ul class="nav nav-pills nav-justified product-tab hidden-xs">
                            <li><a class="<?= isset($category) && $category == "Roasted Products" ? 'active' : ($this->uri->segment(2) == NULL ? 'active' : '') ?>" data-category="Roasted Products" href="#" id="roasted">Roasted Products</a><i class="hidden-xs arrow-down"></i></li>
                            <li><a class="<?= isset($category) && $category == "Chooksie's Products" ? 'active' : '' ?>" data-category="Chooksie's Products" href="#" id="chooksie">Chooksie's Products</a></li>
                            <li><a class="<?= isset($category) && $category == "Disney Products" ? 'active' : '' ?>" data-category="Disney Products" href="#" id="disney">Disney Products</a></li>
                            <li><a class="<?= isset($category) && $category == "Other Products" ? 'active' : '' ?>" data-category="Other Products" href="#" id="other">Other Products</a></li>
                        </ul>
                        <select class="visible-xs product-select col-xs-10" id="product-select">
                                <option>Roasted Products</option>
                                <option>Chooksie's Products</option>
                                <option value="Disney Products">Disney's Products</option>
                                <option>Other Products</option>
                        </select>
                    </div>
                    <div class="product-slider-container" id="product-list">
                        <div class="cat-product owl-carousel owl-products-theme">

                            <!-- PRODUCTS -->
                            <? foreach($products as $k => $v): ?>
                                <div class="item">
                                    <img class="img-responsive" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>" alt="slider image"><br/>
                                    <div class="txtcenter">
                                        <h4><?= $v['title'] ?></h4>
                                            <div class="btn-group padded txtcenter">
                                                <a class="btn product-details-btn btn-glyph-default product-details-btn-<?= $v['id'] ?>" href="javascript:void(0)" data-title="<?= $v['title'] ?>" data-description="<?= word_limiter(str_replace('</p>', '', str_replace('<p>', '', $v['description']))) ?>" data-orderurl ="http://www.chookstogodelivery.com.ph" data-fburl = "<?= site_url('products') ?>/<?= $v['id'] ?>" data-thumbnail="<?= str_replace('~path~', base_url(), $v['photo_thumb']) ?>" data-delivery="<?= $v['for_delivery'] ?>">DETAILS</a>
                                                <a class="btn btn-glyph product-details-btn product-details-btn-<?= $v['id'] ?>" data-title="<?= $v['title'] ?>" data-description="<?= word_limiter(str_replace('</p>', '', str_replace('<p>', '', $v['description']))) ?>" data-orderurl ="http://www.chookstogodelivery.com.ph" data-fburl = "<?= site_url('products') ?>/<?= $v['id'] ?>" data-thumbnail="<?= str_replace('~path~', base_url(), $v['photo_thumb']) ?>" data-delivery="<?= $v['for_delivery'] ?>">
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                </a>
                                            </div>
                                    </div>
                                </div>
                            <? endforeach; ?>
                            <!-- END PRODUCTS -->

                        </div>
                    </div>
           </div>
    </div>
    <div class="wave product-details"  id="product-details">
            <div class="container product-details-content">
                    <div class="col-md-4 product-thumbnail col-sm-4">
                            <img class="img-responsive"/>
                    </div>
                    <div class="col-md-8 description col-sm-8">
                            <h4></h4>
                            <span class="description">

                            </span><br class="clear"/><br/>
                             <div class="order-share-buttons">
                                    <div class="btn-group txtcenter">
                                        <a class="btn btn-glyph-default" href="#" onclick="_ga('order-na-button')">ORDER NA!</a>
                                        <a class="btn btn-glyph" href="#">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </a>
                                    </div>

                                     <div class="share-button"></div>
                              </div>


                    </div>



            </div>


    </div><br class="clear"/><br/>
</section>
