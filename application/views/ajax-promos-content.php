<div class="cat-promos owl-carousel owl-products-theme" id="promos-container">

    <!-- PROMOS -->
    <? foreach($promos as $k => $v): ?>
        <div class="item">
            <img class="img-responsive" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>" alt="slider image"><br/>
            <div class="txtcenter">
                <h4><?= $v['title'] ?></h4>
                    <div class="btn-group padded txtcenter">
                        <a class="btn promo-details-btn btn-glyph-default" href="javascript:void(0)" data-title="<?= $v['title'] ?>" data-description='<?= character_limiter(str_replace('</p>', '', str_replace('<p>', '', strip_tags($v['description']))), 570) ?>' data-fburl = "https://www.facebook.com/chookstogo" data-gotourl = "<?= $v['link'] ?>" data-readmoreurl = "<?= site_url('promos/select') ?>/<?= $v['id'] ?>" data-thumbnail="<?= str_replace('~path~', base_url(), $v['photo']) ?>" data-badge ="<?= $v['promo_type'] == 'Digital Promo' ? 'Digital' : 'In-Store' ?>" data-date="<?= ( date('Y', strtotime($v['promo_start_date'])) == date('Y', strtotime($v['promo_end_date'])) ) ? date('F', strtotime($v['promo_start_date'])) . ' - ' . date('F Y', strtotime($v['promo_end_date'])) : date('F Y', strtotime($v['promo_start_date'])) . ' - ' . date('F Y', strtotime($v['promo_end_date'])) ?>" data-openlink="<?=$v['open_link']?>">DETAILS</a>
                        <a class="btn btn-glyph promo-details-btn" data-title="<?= $v['title'] ?>" data-description='<?= character_limiter(str_replace('</p>', '', str_replace('<p>', '', strip_tags($v['description']))), 570) ?>' data-fburl = "https://www.facebook.com/chookstogo" data-gotourl = "<?= $v['link'] ?>" data-readmoreurl = "<?= site_url('promos/select') ?>/<?= $v['id'] ?>" data-thumbnail="<?= str_replace('~path~', base_url(), $v['photo']) ?>" data-badge ="<?= $v['promo_type'] == 'Digital Promo' ? 'Digital' : 'In-Store' ?>" data-date="<?= ( date('Y', strtotime($v['promo_start_date'])) == date('Y', strtotime($v['promo_end_date'])) ) ? date('F', strtotime($v['promo_start_date'])) . ' - ' . date('F Y', strtotime($v['promo_end_date'])) : date('F Y', strtotime($v['promo_start_date'])) . ' - ' . date('F Y', strtotime($v['promo_end_date'])) ?>" data-openlink="<?=$v['open_link']?>">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
            </div>
        </div>
    <? endforeach; ?>
    <!-- END PROMOS -->

</div>

<script type="text/javascript">
    $(document).ready(function(){
        var screenWidth = $(window).width();

        $(".cat-promos").owlCarousel({
            navigation: true, // Show next and prev buttons
            navigationText: ["<i class='glyphicon glyphicon-chevron-left'></i>", "<i class='glyphicon glyphicon-chevron-right'></i>"],
            paginationSpeed: 800,
            autoPlay: true,
            items : 4,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,3],
            itemsTablet: [768,3],
            itemsMobile :[520,1]
        });

        $('.promo-details-btn').on('click',function(e){
            e.preventDefault();
            var animateHeight;
            var shareLink;
            if(screenWidth <= 767){
                animateHeight = 630
            }else if(screenWidth >= 768) {
                animateHeight = 400
            }
            var itemDetails = $(this).data();

            if(itemDetails.badge == 'Digital') {
                shareLink = itemDetails.gotourl;
            } else {
                shareLink = itemDetails.readmoreurl;
            }

            if(itemDetails.badge == 'Digital') {
                $('.btn-read-more').parent().css('display', 'none');
                $('.btn-goto-app').parent().css('display', 'block');
                if(itemDetails.openlink) {
                    $('.btn-goto-app').attr('target', '_blank');
                    $('.promo-gotoapp-btn').attr('target', '_blank');
                    $('.promo-readmore-btn').attr('target', '_blank');
                } else {
                    $('.btn-goto-app').attr('target', '');
                    $('.promo-gotoapp-btn').attr('target', '');
                    $('.promo-readmore-btn').attr('target', '');
                }
            } else {
                $('.btn-goto-app').parent().css('display', 'none');
                $('.btn-read-more').parent().css('display', 'block');
                if(itemDetails.openlink) {
                    $('.btn-goto-app').attr('target', '_blank');
                    $('.promo-gotoapp-btn').attr('target', '_blank');
                    $('.promo-readmore-btn').attr('target', '_blank');
                } else {
                    $('.btn-goto-app').attr('target', '');
                    $('.promo-gotoapp-btn').attr('target', '');
                    $('.promo-readmore-btn').attr('target', '');
                }
            }

            $('.promo-details-content').animate({
                 height:animateHeight,
                 padding:'40px 0px'
            },120,function(){
                    $('.promo-details-content .promo-thumbnail img').attr('src',itemDetails.thumbnail);
                    $('.promo-details-content .description h2').text(itemDetails.title);
                    $('.promo-details-content .description > span.description').text(itemDetails.description);
                    $('.promo-details-content .description > span.promo-date').text(itemDetails.date);
                    $('.promo-details-content .description > span.badge-cat').text(itemDetails.badge);
                    $('.promo-details-content .order-share-buttons .btn-read-more').attr('href',itemDetails.readmoreurl);
                    $('.promo-details-content .order-share-buttons .btn-goto-app').attr('href',itemDetails.gotourl);
                    $('.promo-details-content .order-share-buttons .promo-readmore-btn').attr('href',itemDetails.readmoreurl);
                    $('.promo-details-content .order-share-buttons .promo-gotoapp-btn').attr('href',itemDetails.gotourl);
                    $('.promo-details-content .order-share-buttons .share-button').html('<fb:share-button href="'+shareLink+'"data-type="box_count"></fb:share-button>');

                    FB.XFBML.parse();
            });
              $('html,body').animate({scrollTop: $("#product-details").offset().top});

    });
    });


</script>
