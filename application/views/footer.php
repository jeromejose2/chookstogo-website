<footer>
    <div class="footer-content">
        <div class="hl visible-lg"></div>
        <div class="container">
            <div class="col-md-6 col-lg-4 footer-feedback col-sm-6">
                <h4>SEND FEEDBACK
                    <span>Say hello. Let us know.</span>
                </h4>
                <section>
                    <!-- typeform html -->
                    <form class="type-form" id="typeform-footer">
                        <ul>
                            <li>
                                <label>1
                                    <span class="glyphicon glyphicon-arrow-right"></span>First, Please tell us your name</label>
                                <input type="text" data-error="Opps! Please input your first name to continue">
                            </li>

                            <li>
                                <label>2
                                    <span class="glyphicon glyphicon-arrow-right"></span>What is your email address</label>
                                <input type="text" data-error="Opps! Please input your email to continue">
                            </li>

                            <li>
                                <label>3
                                    <span class="glyphicon glyphicon-arrow-right"></span>What is your contact number</label>
                                <input type="text" data-error="Opps! Please input your first name to continue">
                            </li>

                            <li>
                                <label>4
                                    <span class="glyphicon glyphicon-arrow-right"></span>What is your address</label>
                                <input type="text" data-error="Opps! Please input your address to continue">
                            </li>

                            <li>
                                <label>5
                                    <span class="glyphicon glyphicon-arrow-right"></span>Subject</label>
                                <input type="text" data-error="Opps! Please enter a subject to continue">
                            </li>

                            <li>
                                <label>6
                                    <span class="glyphicon glyphicon-arrow-right"></span>Message</label>
                                <textarea data-error="Please enter your message"></textarea>
                            </li>

                        </ul>
                    </form>
                    <!-- end typeform -->
                    <button class="btn btn-default type-form-btn" onclick="tform.next()">CONTINUE</button>
                </section>
            </div>
            <div class="col-md-2 visible-lg ">
                <h4>QUICK LINKS</h4>

                <ul class="footer-nav">
                    <li><a href="#">Home</a>
                    </li>
                    <li><a href="#">All Products</a>
                    </li>
                    <!--<li><a href="#store-locator">Store Locator</a>
                    </li>-->
                    <li><a href="#">Promos</a>
                    </li>
                    <li><a href="#">Chooks Blog</a>
                    </li>
                    <li><a href="#">Chikoy's Corner</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 visible-lg">
                <h4>CHOOKS CORPORATE</h4>
                <ul class="footer-nav">
                    <li><a href="#">About Us</a>
                    </li>
                    <li><a href="#">Careers</a>
                    </li>
                    <li><a href="#">News and Updates</a>
                    </li>

                </ul>

            </div>
            <div class="col-md-3 col-sm-6 col-xs-12  unpadded">
                <h4>SOCIAL NETWORK</h4>
                <ul class="footer-nav visible-sm visible-md visible-lg">
                    <li><a href="#"><i class="social-icons-facebook"></i>Facebook</a>
                    </li>
                    <li><a href="#"><i class="social-icons-twitter"></i>Twitter</a>
                    </li>
                    <li><a href="#"><i class="social-icons-youtube"></i>Youtube</a>
                    </li>
                    <li><a href="#"><i class="social-icons-instagram"></i>Instagram</a>
                    </li>

                </ul>
                <div class="unpadded txtcenter share-big col-xs-12 visible-xs">
                    <a href="#"><i class="social-icons-facebook-big"></i></a>
                    <a href="#"><i class="social-icons-twitter-big"></i></a>
                    <a href="#"><i class="social-icons-youtube-big"></i></a>
                    <a href="#"><i class="social-icons-instagram-big"></i></a>
                </div>
                <br class="clear" />
                <br/>


            </div>

        </div>
        <div class="container">
            <div class="footer-logo">
                <img class="img-responsive" src="assets/img/footer-logo.png?v=1.2" alt="logo" />
            </div>
            <br class="clear" />
            <div class="footer-address">
                <ul>
                    <li>
                        <span>22 & 23 Floor, Taipan Place, Emerald Avenue, Ortigas Center, Pasig City</span>
                    </li>
                    <li>
                        <span>Phone: (02) 810-3550 | 815-6676 | (02) 812-3392 | 810-9207</span>
                    </li>
                </ul>


            </div>
        </div>
        <div class="copyright ">
            <div class="container">
                <span>
                    Copyright 2014, Bounty Agro, Chooks-To-Go. All rights reserved.
                </span>
            </div>
        </div>


    </div>
</footer>
<script src="assets/vendors/jquery/jquery.js"></script>
<script src="assets/vendors/lytebox/lytebox.js"></script>
<script src="assets/js/jtypeform.js"></script>
<script src="assets/vendors/bootstrap/js/bootstrap.js"></script>
<script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
<script src="assets/js/app.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
     tform = $('#typeform-footer').jtypeform();

});
</script>
</body>

</html>
