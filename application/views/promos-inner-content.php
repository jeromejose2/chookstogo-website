<section class="main">
<div class="news-all banner-bg"></div><div class="wave clearfix"></div>
    <div class="news-update-list people-list">
         <div class="people-list-content container">
                <div class="heading txtcenter">
                        <h2 class="title-heading">PROMOS</h2>
                        <span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua.
                    </span>
                </div><br class="clear"/><br/><br/>
                  <div class="row">
                      <div class="thumbnail">
                            <img src="<?= str_replace('~path~', base_url(), $promo[0]['photo']) ?>" class="img-responsive" alt="blog entry"/>
                      </div><br class="clear"/><br/>
                      <div class="blog-content">
                            <h4><?= $promo[0]['title'] ?></h4>
                            <span><?= ( date('Y', strtotime($promo[0]['promo_start_date'])) == date('Y', strtotime($promo[0]['promo_end_date'])) ) ? date('F', strtotime($promo[0]['promo_start_date'])) . ' - ' . date('F Y', strtotime($promo[0]['promo_end_date'])) : date('F Y', strtotime($promo[0]['promo_start_date'])) . ' - ' . date('F Y', strtotime($promo[0]['promo_end_date'])) ?></span>
                            <div class="inner-content">
                                     <p>
                                        <?= $promo[0]['description'] ?>

                                    </p>

                            </div>
                            <div class="btn-group padded txtcenter">
                                <a class="btn btn-glyph">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="btn btn-glyph-default" href="<?= site_url('promos') ?>">BACK</a>

                            </div>

                      </div><br class="clear"/><br/>


                </div>

         </div>
    </div>

    <div class="wave clearfix"></div><br class="clear"/><br/>
</section>