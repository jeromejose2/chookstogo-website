<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blogs extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('text');

	}

	public function index() {

		$params = array(
			'table'=>'tbl_articles',
			'where'=>'section = \'Main Section\'',
			'order'=>'timestamp DESC'
		);
		$this->data['articles'] = $this->mysql_queries->get_data($params);

		$this->template['content'] = $this->load->view('blogs-content', $this->data, TRUE);
		$this->load->view('template', $this->template);

	}

	public function article($id = NULL) {

		$this->data = NULL;

		if($id) {
			if(is_numeric($id)) {
				$params = array(
					'table'=>'tbl_articles',
					'where'=>'id = '.$id
				);
				$article = $this->mysql_queries->get_data($params);
				if($article) {
					$this->data['article'] = $article;
					$this->data['title'] = $article[0]['title'];
					// OG META TAGS //
					$this->data['og_title'] = $article[0]['title'];
					$this->data['og_image'] = str_replace('~path~', base_url(), $article[0]['photo']);
					$this->data['og_url'] = site_url().'blogs/article/'.$article[0]['id'];
					$this->data['og_description'] = $article[0]['short_description'];
					// OG META TAGS//
				} else {
					redirect('blogs', 'location');
				}
			} else {
				redirect('blogs', 'location');
			}
			
		} else {
			redirect('blogs', 'location');
		}

		$this->template['content'] = $this->load->view('blog-inner-content', $this->data, TRUE);
		$this->load->view('template', $this->template);

	}

	public function ajax_process() {

		$params = array(
			'table'=>'tbl_articles',
			'where'=>'section = "'.$_POST['section'].'"',
			'order'=>'timestamp DESC'
		);
		$this->data['articles'] = $this->mysql_queries->get_data($params);

		$this->load->view('ajax-blogs-content', $this->data);

	}

}