<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_Process extends CI_Controller {

	public function index() {

		$this->load->library('email');

		$params = array(
			'table'=>'tbl_feedbacks',
			'post'=>$_POST
		);
		$feedback = $this->mysql_queries->insert_data($params);

		$params = array(
			'table' => 'tbl_feedbacks',
			'where' => 'id = ' . $feedback
		);
		$data = $this->mysql_queries->get_data($params);

		$content = "Name: " . $data[0]['name'] . "<br />";
		$content .= "Email: " . $data[0]['email'] . "<br />";
		$content .= "Address: " . $data[0]['email'] . "<br />";
		$content .= "Contact #: " . $data[0]['contact'] . "<br />";
		$content .= "Message: " . $data[0]['message'] . "<br />";

		$email_data = array(
			"subject" => $data[0]['subject'],
			"from" => $data[0]['email'],
			"name" => $data[0]['name'],
			"content" => $content
		);

		$config['protocol'] = 'sendmail';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            $this->email->from('no-reply@chookstogo.com.ph', "Chooks-to-Go Admin");
            // $this->email->to($data['to']);
            $this->email->to('chookstogo.promo@gmail.com');
            $this->email->subject($email_data['subject']);
            $this->email->message($email_data['content']);
            $this->email->send();

            print_r($this->email->print_debugger());

	}

	private function send_edm($data) {

            $config['protocol'] = 'sendmail';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            $this->email->from('no-reply@nwshare.ph', "Chooks-to-Go Admin");
            $this->email->to($data['to']);
            // $this->email->to('nuworks.client1@gmail.com');
            $this->email->subject($data['subject']);
            $this->email->message($data['content']);
            $this->email->send();
     }

	public function foobar() {

		$this->data = array();

		$params = array(
			'table'=>'tbl_products',
			'where'=>'category = "'.$_POST['category'].'"'
		);
		$this->data['products'] = $this->mysql_queries->get_data($params);

		$this->load->view('ajax-products-content', $this->data);
		
	}

}