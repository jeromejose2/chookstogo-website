<?php include 'header.php' ?>

<section class="main">
    <div class="news-all banner-bg"></div><div class="wave clearfix"></div>
    <div class="news-update-list">
         <div class="heading txtcenter">
                        <h2 class="title-heading">CAREERS</h2>
                        <span>Ka-Chooks opportunity</span>
            </div><br class="clear"/><br/>
           <div class="result-list-content container">
                <h3>STORE MAINTENANCE MAN</h3>
                <span>PAMPANGA, BACOLOD, ORMOC, CAGAYAN DE ORO, GENSAN</span><br class="clear"/><br/><br/>
                <p class="title">Qualifications:</p>
                <p>
                      Candidate must possess Vocational Diploma / Short Course Certificate or Bachelor’s/College Degree in Engineering (Electrical/Electronic) or equivalent.
                      Male not more than 35 years old.
                      With at least 1 year experience as all around technician handling electrical and mechanical work.
                      Must know how to drive and with valid driverâ€™s license.
                      Full-Time positions available.
                </p>
                <p class="title">Responsibilities:</p>
                <p>
                    Responsible in the proper troubleshooting and maintenance of all the food retail/rotisserie equipments covering all outlets in the branch.
                    Responds to emergencies as required during equipments breakdown.
                    Assist in ordering materials and equipments required.
                </p>

                <div class="btn-group txtcenter">
                       <a class="btn btn-glyph btn-glyph-light-gray">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="btn btn-glyph-default btn-glyph-gray" href="#">BACK</a>
                </div>
                <div class="btn-group txtcenter">

                        <a class="btn btn-glyph-default btn-glyph" href="#">APPLY</a>
                        <a class="btn btn-glyph btn-glyph-">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                </div>



           </div><br class="clear"/><br/>
    </div>
    <!--end news-update-list-->
     <div class="wave clearfix"></div><br class="clear"/><br/>
</section>

<?php include 'footer.php' ?>
