<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('text');

	}

	public function index() {


		$params = array(
			'table'=>'tbl_homepage_slider',
			'order'=>'`order` ASC'
		);
		$this->data['sliders'] = $this->mysql_queries->get_data($params);

		$params = array(
			'table'=>'tbl_articles',
			'where'=>'is_featured = 1',
			'order'=>'timestamp DESC',
			'offset'=>0,
			'limit'=>2
		);
		$this->data['articles'] = $this->mysql_queries->get_data($params);

		$params = array(
			'table'=>'tbl_announcements',
			'where'=>'is_featured = 1'
		);
		$this->data['announcements'] = $this->mysql_queries->get_data($params);

		# Announcements
		$params = array(
			'table'=>'tbl_articles',
			'where'=>'is_announcement = 1'
		);
		$article_announcements = $this->mysql_queries->get_data($params);

		$params = array(
			'table'=>'tbl_promos',
			'where'=>'is_announcement = 1'
		);
		$promo_announcements = $this->mysql_queries->get_data($params);

		$params = array(
			'table'=>'tbl_about',
			'where'=>'is_featured = 1'
		);
		$news_and_updates_announcements = $this->mysql_queries->get_data($params);

		$announcements_article = array();
		$announcements_promo = array();
		$announcements_news_and_updaes = array();

		foreach($article_announcements as $k => $v) {
			$announcements_article[$k]['id'] = $v['id'];
			$announcements_article[$k]['title'] = $v['title'];
			$announcements_article[$k]['photo'] = $v['photo'];
			$announcements_article[$k]['description'] = $v['short_description'];
			$announcements_article[$k]['type'] = 'Article';
		}

		foreach($promo_announcements as $k => $v) {
			$announcements_promo[$k]['id'] = $v['id'];
			$announcements_promo[$k]['title'] = $v['title'];
			$announcements_promo[$k]['photo'] = $v['photo'];
			$announcements_promo[$k]['description'] = $v['description'];
			$announcements_promo[$k]['type'] = 'Promo';
		}

		foreach($news_and_updates_announcements as $k => $v) {
			$announcements_news_and_updaes[$k]['id'] = $v['id'];
			$announcements_news_and_updaes[$k]['title'] = $v['title'];
			$announcements_news_and_updaes[$k]['photo'] = $v['photo'];
			$announcements_news_and_updaes[$k]['description'] = $v['description'];
			$announcements_news_and_updaes[$k]['type'] = 'News_And_Updates';
		}

		$this->data['announcements'] = array_merge($announcements_promo, $announcements_news_and_updaes, $announcements_article);
		# End Announcements

		$params = array(
			'table'=>'tbl_chikoys_corner',
			'where'=>'is_featured = 1'
		);
		$this->data['featured_comics'] = $this->mysql_queries->get_data($params);

		$params = array(
			'table'=>'tbl_stores',
		);
		$this->data['stores'] = $this->mysql_queries->get_data($params);

		$this->load->model('mysql_queries_registrants');

		$params = array(
			'table'=>'tbl_provinces'
		);
		$this->data['provinces'] = $this->mysql_queries_registrants->get_data($params);

		$this->template['content'] = $this->load->view('home-content', $this->data, TRUE);
		$this->load->view('template', $this->template);

	}
}