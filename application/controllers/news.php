<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('text');

	}

	public function index() {

		$params = array(
			'table'=>'tbl_about',
			'order'=>'date(date_of_publish) DESC'
		);
		$this->data['news'] = $this->mysql_queries->get_data($params);

		$this->template['content'] = $this->load->view('news-all-content', $this->data, TRUE);
		$this->load->view('template', $this->template);
		
	}

	public function article($id = NULL) {

		$this->data = NULL;

		if($id) {
			if(is_numeric($id)) {
				$params = array(
					'table'=>'tbl_about',
					'where'=>'id = '.$id
				);
				$article = $this->mysql_queries->get_data($params);
				if($article) {
					$this->data['article'] = $article;
					$this->data['title'] = $article[0]['title'];
					$this->data['og_title'] = $article[0]['title'];
					$this->data['og_image'] = str_replace('~path~', base_url(), $article[0]['photo']);
					$this->data['og_url'] = site_url().'news/article/'.$article[0]['id'];
					$this->data['og_description'] = $article[0]['description'];
				} else {
					redirect('news', 'location');
				}
			} else {
				redirect('news', 'location');
			}
		} else {
			redirect('news', 'location');
		}

		$this->template['content'] = $this->load->view('news-inner-content', $this->data, TRUE);
		$this->load->view('template', $this->template);

	}

}