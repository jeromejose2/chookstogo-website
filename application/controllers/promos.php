<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promos extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('text');

	}

	public function index() {

		$params = array(
			'table'=>'tbl_promos',
			'where'=>'NOW() < date(promo_end_date)'
		);
		$this->data['promos'] = $this->mysql_queries->get_data($params);

		$this->template['content'] = $this->load->view('promos-content', $this->data, TRUE);
		$this->load->view('template', $this->template);

	}

	public function select($id) {

		if($id) {
			if(is_numeric($id)) {
				$params = array(
					'table'=>'tbl_promos',
					'where'=>'id = '.$id
				);
				$selected = $this->mysql_queries->get_data($params);
				if($selected) {
					$this->data['promo'] = $selected;
					$this->data['og_title'] = $selected[0]['title'];
					$this->data['og_image'] = str_replace('~path~', base_url(), $selected[0]['photo']);
					$this->data['og_url'] = site_url().'promos/select/'.$selected[0]['id'];
					$this->data['og_description'] = $selected[0]['description'];

					$this->template['content'] = $this->load->view('promo-inner-content', $this->data, TRUE);
					$this->load->view('template', $this->template);
				} else {
					redirect('promos', 'location');
				}
			} else {
				redirect('promos', 'location');
			}
		} else {
			redirect('promos', 'location');
		}

	}

	public function select_promo() {

		$params = array(
			'table'=>'tbl_promos'
		);
		$promos = $this->mysql_queries->get_data($params);

		if($_POST['value'] == 'past') {
			$params = array(
				'table'=>'tbl_promos',
				'where'=>'NOW() > promo_end_date'
			);
			$this->data['promos'] = $this->mysql_queries->get_data($params);
		} else {
			$params = array(
				'table'=>'tbl_promos',
				'where'=>'NOW() < promo_end_date'
			);
			$this->data['promos'] = $this->mysql_queries->get_data($params);
		}

		$this->load->view('ajax-promos-content', $this->data);

	}

}