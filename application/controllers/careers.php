<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Careers extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('text');

	}

	public function index() {

		
		$this->template['content'] = $this->load->view('careers-all-content', NULL, TRUE);
		$this->load->view('template', $this->template);

	}

	public function search() {

		$this->data['full_url'] = $this->_bitly(urlencode("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"));

		$field_of_work = $_GET['field'];
		$location = $_GET['location'];
		$position = isset($_GET['position']) ? $_GET['position'] : FALSE;

		if($position) {
			$params = array(
				'table' => 'tbl_careers',
				'where' => 'field_of_work = "'.$field_of_work.'" AND location = "'.$location.'" AND position LIKE "%'.$position.'%"'
			);
			$this->data['careers'] = $this->mysql_queries->get_data($params);
			foreach($this->data['careers'] as $key => $value) {
				$location = urlencode($value['location']);
				$field = urlencode($value['field_of_work']);
				$pos = strtolower(urlencode($this->_delete_between_tags("(", ")", $value['position'])));

				$this->data['careers'][$key] = $value;
				$this->data['careers'][$key]['share_url'] = $this->_bitly(urlencode(site_url('careers/search') . "?field=$field&location=$location&position=$pos"));
			}
			$this->data['og_title'] = @$this->data['careers'][0]['position'];
			$this->data['og_description'] = @strip_tags($this->data['careers'][0]['description']);
			$this->data['og_url'] = @$this->data['full_url'];
			$this->data['og_image'] = base_url() . 'assets/img/logo.png';
			if(!$this->data['careers']) {
				$this->data['no_result'] = TRUE;
				$this->template['content'] = $this->load->view('careers-all-content', $this->data, TRUE);
			} else {
				$this->template['content'] = $this->load->view('careers-all-results-content', $this->data, TRUE);
			}
		} else {
			if($field_of_work != 'FIELD OF WORK' && $location == 'LOCATION') {
				$params = array(
					'table'=>'tbl_careers',
					'where'=>'field_of_work = "'.$field_of_work.'"'
				);
				$this->data['careers'] = $this->mysql_queries->get_data($params);
				foreach($this->data['careers'] as $key => $value) {
					$location = urlencode($value['location']);
					$field = urlencode($value['field_of_work']);
					$pos = strtolower(urlencode($this->_delete_between_tags("(", ")", $value['position'])));

					$this->data['careers'][$key] = $value;
					$this->data['careers'][$key]['share_url'] = $this->_bitly(urlencode(site_url('careers/search') . "?field=$field&location=$location&position=$pos"));
				}
				if(!$this->data['careers']) {
					$this->data['no_result'] = TRUE;
					$this->template['content'] = $this->load->view('careers-all-content', $this->data, TRUE);
				} else {
					$this->template['content'] = $this->load->view('careers-all-results-content', $this->data, TRUE);
				}
			} elseif($field_of_work == 'FIELD OF WORK' && $location != 'LOCATION') {
				$params = array(
					'table'=>'tbl_careers',
					'where'=>'location = "'.$location.'"'
				);
				$this->data['careers'] = $this->mysql_queries->get_data($params);
				foreach($this->data['careers'] as $key => $value) {
					$location = urlencode($value['location']);
					$field = urlencode($value['field_of_work']);
					$pos = strtolower(urlencode($this->_delete_between_tags("(", ")", $value['position'])));

					$this->data['careers'][$key] = $value;
					$this->data['careers'][$key]['share_url'] = $this->_bitly(urlencode(site_url('careers/search') . "?field=$field&location=$location&position=$pos"));
				}
				if(!$this->data['careers']) {
					$this->data['no_result'] = TRUE;
					$this->template['content'] = $this->load->view('careers-all-content', $this->data, TRUE);
				} else {
					$this->template['content'] = $this->load->view('careers-all-results-content', $this->data, TRUE);
				}
			} else {
				$params = array(
					'table'=>'tbl_careers',
					'where'=>'field_of_work = "'.$field_of_work.'" AND location = "'.$location.'"'
				);
				$this->data['careers'] = $this->mysql_queries->get_data($params);
				foreach($this->data['careers'] as $key => $value) {
					$location = urlencode($value['location']);
					$field = urlencode($value['field_of_work']);
					$pos = strtolower(urlencode($this->_delete_between_tags("(", ")", $value['position'])));

					$this->data['careers'][$key] = $value;
					$this->data['careers'][$key]['share_url'] = $this->_bitly(urlencode(site_url('careers/search') . "?field=$field&location=$location&position=$pos"));
				}
				$this->data['og_title'] = @$this->data['careers'][0]['position'];
				$this->data['og_description'] = @strip_tags($this->data['careers'][0]['description']);
				$this->data['og_url'] = @$this->data['full_url'];
				$this->data['og_image'] = base_url() . 'assets/img/logo.png';
				if(!$this->data['careers']) {
					$this->data['no_result'] = TRUE;
					$this->template['content'] = $this->load->view('careers-all-content', $this->data, TRUE);
				} else {
					$this->template['content'] = $this->load->view('careers-all-results-content', $this->data, TRUE);
				}
			}
		}

		

		// $params = array(
		// 	'table'=>'tbl_careers',
		// 	'where'=>'field_of_work = "'.$field_of_work.'"'
		// );
		// $this->data['careers'] = $this->mysql_queries->get_data($params);

		// if(!$this->data['careers']) {
		// 	$this->data['no_result'] = TRUE;

		// 	$this->template['content'] = $this->load->view('careers-all-content', $this->data, TRUE);
		// 	// $this->load->view('template', $this->template);
		// } else {
		// 	$this->template['content'] = $this->load->view('careers-all-results-content', $this->data, TRUE);
		// }

		// $this->template['content'] = $this->load->view('careers-all-results-content', $this->data, TRUE);

		$this->load->view('template', $this->template);

	}

	public function apply() {

		$this->load->library('email');

		$config['upload_path'] = 'assets/admin/uploads/applicants/';
		$config['allowed_types'] = 'pdf';
		$config['max_size'] = '2048';
		$this->load->library('upload', $config);
		$resume = 'resume';

		if($this->upload->do_upload($resume)) {
			$file = $this->upload->data();
			$_POST['curriculum_vitae_path'] = '~path~/assets/admin/uploads/applicants/'.$file['file_name'];
		}

		$portfolio_upload = 'portfolio_upload';

		if($this->upload->do_upload($portfolio_upload)) {
			$file = $this->upload->data();
			$_POST['portfolio_upload_path'] = '~path~/assets/admin/uploads/applicants/'.$file['file_name'];
		}

		$params = array(
			'table'=>'tbl_applicants',
			'post'=>$_POST
		);
		$data_id = $this->mysql_queries->insert_data($params);

		$params = array(
			'table'=>'tbl_applicants',
			'where'=>'id = '.$data_id
		);
		$email_data = $this->mysql_queries->get_data($params);

		$message = 'Name : '.$email_data[0]['name'];
		$message .= '<br>Email : '.$email_data[0]['email'];
		$message .= '<br>Address : '.$email_data[0]['address'];
		$message .= '<br>Position : '.$email_data[0]['position'];

		$curriculum_vitae = end(explode('/', $email_data[0]['curriculum_vitae_path']));

		if($email_data[0]['portfolio_upload_path']) {
			$portfolio = end(explode('/', $email_data[0]['portfolio_upload_path']));
		}

		$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from('no-reply@nwshare.ph', 'Chooks-to-Go Admin');		
		$this->email->to('nuworks.client1@gmail.com');
		$this->email->subject('Job Application');
		$this->email->message($message);
		$this->email->attach('assets/admin/uploads/applicants/'.$curriculum_vitae);
		if($email_data[0]['portfolio_upload_path']) {
			$this->email->attach('assets/admin/uploads/applicants/'.$portfolio);
		}
		$this->email->send();

		redirect('careers', 'location');

	}

	public function _bitly($url) {

		$ch = curl_init('http://api.bitly.com/v3/shorten?login=o_fbl5e4nms&apiKey=R_2a49be08f36b3b621c4c45752e2f011e&longUrl=' . $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		 
		$result = curl_exec($ch);
		$result_json = json_decode($result);

		return @$result_json->data->url;

	}

	public function _delete_between_tags($beginning, $end, $string) {

		$beginningPos = strpos($string, $beginning);
		$endPos = strpos($string, $end);
		if ($beginningPos === false || $endPos === false) {
		return $string;
		}

		$textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);

		return str_replace($textToDelete, '', $string);

	}

}