<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {

		parent::__construct();
		
		$this->load->model('mysql_queries_registrants');
		$this->load->helper('view');
		$this->load->helper('inflector');
	}

	public function index() {

		$params = array(
			'table'=>'tbl_registrants',
		);
		$data = $this->mysql_queries_registrants->get_data($params);

		$emails = array();
		foreach($data as $k => $v) {
			$emails[$k] = $v['email'];
		}

		$test = 'jeffrey.mabazza@nuworks.ph';

		if(in_array($test, $emails)) {
			// echo true;
		} else {
			// echo false;
		}

		$params = array(
			'table'=>'tbl_provinces'
		);
		$provinces = $this->mysql_queries->get_data($params);

		$this->data['province'] = '<option value="">Select Province</option>';
		foreach($provinces as $k => $v) {
			$this->data['province'] .= '<option value="'.$v['province_id'].'">'.humanize(strtolower($v['province'])).'</option>';
		}

		$this->template['content'] = $this->load->view('login-content', $this->data, TRUE);
		$this->load->view('template', $this->template);

	}

	public function add_token() {

		$params = array(
			'table'=>'tbl_registrants'
		);
		$data = $this->mysql_queries_registrants->get_data($params);

		foreach($data as $k => $v) {
			$params = array(
				'table'=>'tbl_registrants',
				'where'=>'id = '.$v['id'],
				'post'=>array(
					'reset_token'=>uniqid('', TRUE)
				)
			);
			$this->mysql_queries_registrants->update_data($params);
		}

	}

	public function reset($id = NULL, $token = NULL) {

		if($id) {
			if($token) {
				$params = array(
					'table'=>'tbl_registrants',
					'where'=>'id = '.$id.' AND reset_token = "'.$token.'"'
				);
				$user = $this->mysql_queries_registrants->get_data($params);
				if($user) {
					$params = array(
						'table'=>'tbl_provinces'
					);
					$provinces = $this->mysql_queries->get_data($params);

					$this->data['province'] = '<option value="">Select Province</option>';
					foreach($provinces as $k => $v) {
						$this->data['province'] .= '<option value="'.$v['province_id'].'">'.humanize(strtolower($v['province'])).'</option>';
					}
					$this->data['is_forgot'] = $user[0]['id'];

					$this->template['content'] = $this->load->view('login-content', $this->data, TRUE);
					$this->load->view('template', $this->template);
				} else {
					redirect('login', 'location');
				}
			} else {
				redirect('login', 'location');
			}
		} else {
			redirect('login', 'location');
		}

	}

	public function validate() {

		$error = NULL;

		if($this->input->post('login_username') == '') {
			$error = 'Please enter your username.';
			echo $error;
			exit;
		} else if($this->input->post('login_password') == '') {
			$error = 'Please enter your username.';
			echo $error;
			exit;
		} else {
			$params = array(
				'table'=>'tbl_registrants',
				'where'=>'login = "'.$this->input->post('login_username').'" AND password = "'.md5($this->input->post('login_password')).'"'
			);
			$user = $this->mysql_queries_registrants->get_data($params);
			if($user) {
				if($user[0]['confirmed_user'] == 1) {
					$data = array(
						'sess_id'=>$user[0]['id'],
						'sess_username'=>$user[0]['login'],
						'sess_firstname'=>$user[0]['firstname'],
						'sess_lastname'=>$user[0]['lastname'],
						'sess_email'=>$user[0]['email']
					);
					$this->session->set_userdata($data);

					$params = array(
						"table" => "tbl_audit_logs",
						"post" => array(
							"category" => "Corporate",
							"firstname" => $user[0]["firstname"],
							"lastname" => $user[0]["lastname"],
							"email" => $user[0]["email"]
						)
					);
					$this->mysql_queries->insert_data($params);
				} else {
					$error = 'You need to verify your account first to continue.';
					echo $error;
					exit;
				}
			} else {
				$error = 'Invalid username or password.';
				echo $error;
				exit;
			}
		}

	}

	public function select_city() {

		$params = array(
			'table'=>'tbl_cities',
			'where'=>'province_id = '.$_POST['id']
		);
		$cities = $this->mysql_queries->get_data($params);

		$this->data['cities'] = '<option value="">Select City</option>';
		foreach($cities as $k => $v) {
			$this->data['cities'] .= '<option>'.humanize(strtolower($v['city'])).'</option>';
		}

		echo $this->data['cities'];

	}

	public function verify($id = NULL, $param = NULL) {

		if($id) {
			if($param) {
				$params = array(
					'table'=>'tbl_registrants',
					'where'=>'id = '.$id
				);
				$user = $this->mysql_queries_registrants->get_data($params);
				if($user) {
					$parameters = explode('-', $param);
					$params = array(
						'table'=>'tbl_registrants',
						'where'=>'id = '.$id.' AND reset_token = "'.$parameters[1].'"'
					);
					$valid = $this->mysql_queries_registrants->get_data($params);
					if($valid) {
						if(!$valid[0]['confirmed_user']) {
							$params = array(
								'table'=>'tbl_provinces'
							);
							$provinces = $this->mysql_queries->get_data($params);

							$this->data['province'] = '<option value="">Select Province</option>';
							foreach($provinces as $k => $v) {
								$this->data['province'] .= '<option value="'.$v['province_id'].'">'.humanize(strtolower($v['province'])).'</option>';
							}
							$this->data['is_confirm'] = TRUE;

							$params = array(
								'table'=>'tbl_registrants',
								'where'=>'id = '.$id,
								'post'=>array('confirmed_user'=>'1')
							);
							$this->mysql_queries_registrants->update_data($params);

							$this->data['confirmed_user'] = TRUE;

							$this->template['content'] = $this->load->view('login-content', $this->data, TRUE);
							$this->load->view('template', $this->template);
						} else {
							redirect('login');
						}						
					} else {
						redirect('login', 'location');
					}
				} else {
					redirect('login', 'location');
				}
			} else {
				redirect('login', 'location');
			}
		} else {
			redirect('login', 'location');
		}
 
	}

}