<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gmap_Query extends CI_Controller {

	public function select_google_area() {

		$this->load->model('mysql_queries_registrants');

		$params = array(
			'table'=>'tbl_cities',
			'where'=>'province_id = "'. $_POST['area'] .'"'
		);
		$result = $this->mysql_queries_registrants->get_data($params);

		$data = '<option value="">City / Municipality</option>';
		foreach($result as $k => $v) {
			$data .= '<option value="'.$v['city_id'].'">'. $v['city'] .'</option>';
		}

		echo $data;

	}

	public function select_google_city() {

		$params = array(
			'table'=>'tbl_stores',
			'where'=>'area = "'. $_POST['area'] .'" AND city = "'. $_POST['city'] .'"'
		);
		$result = $this->mysql_queries->get_data($params);

		$data = '<option value="11.646338452790836~122.72092948635861~a~a~a~a~a~5">Stores</option>';
		foreach($result as $k => $v) {
			$data .= '<option value="'. $v['latitude'] .'~'. $v['longitude'] .'~'. $v['name'] .'~'. $v['address'] .'~'. $v['contact'] .'~'. $v['branch_code'] .'~'. $v['branch_hour'] .'~'. $v['zoom_level'] .'">'. $v['name'] .'</option>';
		}

		echo $data;

	}

}