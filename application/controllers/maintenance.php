<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maintenance extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$this->load->view('maintenance-page');

	}

	public function _remap() {

		$this->index();

	}

}