<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forgot_Password extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->library('email');

		$this->load->model('mysql_queries_registrants');

	}

	public function index() {



	}

	public function send() {

		$params = array(
			'table'=>'tbl_registrants',
			'where'=>'email = "'.$_POST['email'].'"'
		);
		$valid = $this->mysql_queries_registrants->get_data($params);

		if($valid) {
			$edm_data = array(
				"username" => $valid[0]["firstname"],
				"link" => site_url().'login/reset/'.$valid[0]['id'].'/'.$valid[0]['reset_token']
			);
			$edm_content = $this->load->view('edm/edm', $edm_data, TRUE);
			$data = array(
				'to'=>$_POST['email'],
				'subject'=>'Chooks-to-Go Ka-Chooks Club: Password Reset',
				'content'=>$edm_content
			);
			// $data = array(
			// 	'to'=>$_POST['email'],
			// 	'subject'=>'Chooks-to-Go Ka-Chooks Club: Password Reset',
			// 	'content'=>'Hello, '.$valid[0]['firstname'].'.<br><br>Balita namin, nakalimutan mo ang password mo sa Ka-Chooks Club. Di bale, i-reset na lang natin para masaya! Mag-click ka lang dito para gumawa ng bago!<br><br><a href="'.site_url().'login/reset/'.$valid[0]['id'].'/'.$valid[0]['reset_token'].'">'.site_url().'login/reset/'.$valid[0]['id'].'/'.$valid[0]['reset_token'].'</a><br><br>Kita-kits sa pinakamasaya at pinakakwelang website, <a href="http://www.chookstogo.com.ph">http://www.chookstogo.com.ph</a>!<br><br>Salamat,<br>Chooks-to-Go Admin'
			// );
			$this->send_edm($data);
		} else {
			echo 'No user found.';
			exit;
		}		

	}

	public function update_password() {

		$params = array(
			'table'=>'tbl_registrants',
			'where'=>'id = '.$_POST['id'],
			'post'=>array(
				'reset_token'=>uniqid('', TRUE),
				'password'=>md5($_POST['password'])
			)
		);
		$this->mysql_queries_registrants->update_data($params);

	}

	private function send_edm($data) {

          $config['protocol'] = 'sendmail';
          $config['mailtype'] = 'html';
          $this->email->initialize($config);
          $this->email->from('no-reply@nwshare.ph', "Chooks-to-Go Admin");
          $this->email->to($data['to']);
          $this->email->subject($data['subject']);
          $this->email->message($data['content']);
          $this->email->send();
     }

}