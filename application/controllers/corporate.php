<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Corporate extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('text');

	}

	public function index() {

		$params = array(
			'table'=>'tbl_about',
			'order'=>'id DESC',
			'offset'=>0,
			'limit'=>4
		);
		$this->data['news'] = $this->mysql_queries->get_data($params);
		
		$this->template['content'] = $this->load->view('corporate-content', $this->data, TRUE);
		$this->load->view('template', $this->template);

	}

	public function people() {

		$params = array(
			'table'=>'tbl_people'
		);
		$people = $this->mysql_queries->get_data($params);

		function extractString($string, $start, $end) {
		    $string = " ".$string;
		    $ini = strpos($string, $start);
		    if ($ini == 0) return "";
		    $ini += strlen($start);
		    $len = strpos($string, $end, $ini) - $ini;
		    return substr($string, $ini, $len);
		}

		$this->data['people'] = array();
		foreach($people as $k => $v) {
			$this->data['people'][$k] = $v;
			$this->data['people'][$k]['designation_preview'] = extractString($v['designation'], '<strong>', '</strong>');
		}

		$this->template['content'] = $this->load->view('people-content', $this->data, TRUE);
		$this->load->view('template', $this->template);

	}

}