<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('inflector');
		$this->load->library('email');		

	}

	public function index() {}

	public function validate() {

		$this->load->model('mysql_queries_registrants');

		require_once('./recaptchalib.php');
		$private_key = '6Lf8ifMSAAAAAFauMcpMn26pORLnqkhgCTR5Tnw_';
		$address = $_SERVER['REMOTE_ADDR'];
		$challenge_field = $this->input->post('recaptcha_challenge_field');
		$response_field = $this->input->post('recaptcha_response_field');
		$response = recaptcha_check_answer($private_key, $address, $challenge_field, $response_field);

		$error = NULL;

		if($response->is_valid) {
			$emails = array();
			$params = array(
				'table'=>'tbl_registrants'
			);
			foreach( $this->mysql_queries_registrants->get_data($params) as $k => $v ) {
				$emails[$k] = $v['email'];
			}
			if($this->input->post('mobile')) {
				$_POST['mobile'] = str_replace('-', '', $this->input->post('mobile'));
			}
			if($this->input->post('firstname') == '') {
				$error = 'Sorry, you need to enter your first name to register!';
				echo $error;
				exit;
			} else if($this->input->post('lastname') == '') {
				$error = 'Sorry, you need to enter your last name to register.';
				echo $error;
				exit;
			} else if($this->input->post('province') == '') {
				$error = 'Please select your province.';
				echo $error;
				exit;
			} else if($this->input->post('city') == '') {
				$error = 'Please select your city.';
				echo $error;
				exit;
			} else if($this->input->post('mobile') == '') {
				$error = 'Sorry, you need to enter your mobile number to register.';
				echo $error;
				exit;
			} else if(!is_numeric($this->input->post('mobile'))) {
				$error = 'Sorry, you have entered an invalid mobile number. Please try again.';
				echo $error;
				exit;
			} else if(strlen($this->input->post('mobile')) < 11) {
				$error = 'Sorry, you have entered an invalid mobile number. Please try again.';
				echo $error;
				exit;
			} else if($this->input->post('email') == '') {
				$error = 'Sorry, you need to enter your email to register.';
				echo $error;
				exit;
			} else if($this->input->post('email') != $this->input->post('re_email') || $this->input->post('re_email') != $this->input->post('email')) {
				$error = 'Sorry, the email do not match. Please retype them and try again.';
				echo $error;
				exit;
			} else if(in_array($this->input->post('email'), $emails)) {
				$error = 'Sorry, the email you provided is already in use.';
				echo $error;
				exit;
			} else if($this->input->post('password') == '') {
				$error = 'Sorry, you need to create your password for your account.';
				echo $error;
				exit;
			} else if($this->input->post('password') != $this->input->post('re_password') || $this->input->post('re_password') != $this->input->post('password')) {
				$error = 'Sorry, the passwords do not match. Please retype them and try again.';
				echo $error;
				exit;
			} else if(!isset($_POST['agree'])) {
				$error = 'Sorry, you need to agree to our terms and conditions.';
				echo $error;
				exit;
			} else {
				$params = array(
					'table'=>'tbl_provinces',
					'where'=>'province_id = '.$this->input->post('province')
				);
				$province = $this->mysql_queries->get_data($params);
				unset($_POST['province']);
				$_POST['province'] = humanize(strtolower($province[0]['province']));
				$_POST['city'] = humanize(strtolower($_POST['city']));
				$_POST['bday'] = $this->input->post('birth_year') .'-'. $this->input->post('birth_month') .'-'. $this->input->post('birth_day');
				$_POST['password'] = md5($this->input->post('password'));
				$_POST['login'] = $this->input->post('email');
				$_POST['reset_token'] = uniqid('', true);
				unset($_POST['birth_month']);
				unset($_POST['birth_day']);
				unset($_POST['birth_year']);
				unset($_POST['re_email']);
				unset($_POST['re_password']);
				unset($_POST['recaptcha_challenge_field']);
				unset($_POST['recaptcha_response_field']);
				unset($_POST['agree']);

				$params = array(
					'table'=>'tbl_registrants',
					'post'=>$_POST
				);
				$registrant = $this->mysql_queries_registrants->insert_data($params);

				$this->send_edm($registrant);
			}
		} else {
			echo 'INCORRECT reCAPTCHA CODE';
			exit;
		}

	}

	private function send_edm($id) {

		$this->load->model('mysql_queries_registrants');

		$params = array(
			'table'=>'tbl_registrants',
			'where'=>'id = '.$id
		);
		$user = $this->mysql_queries_registrants->get_data($params);

		$link = site_url('login/verify').'/'.$user[0]['id'].'/'.md5($user[0]['email']).'-'.$user[0]['reset_token'];

		$this->edm = array(
			'username'=>$user[0]['firstname'],
			'link'=>site_url('login/verify').'/'.$user[0]['id'].'/'.md5($user[0]['email']).'-'.$user[0]['reset_token']
		);
		$content = $this->load->view('edm/edm', $this->edm, TRUE);

		$data = array(
			'to'=>$user[0]['email'],
			'subject'=>'Chooks-to-Go : Pa-confirm and registration',
			// 'content'=>'Hello, '.$user[0]['firstname'].'!,<br><br>Salamat sa pag-register sa Ka-Chooks Club at sa aming website! Pa-click nalang sa link sa baba para maconfirm namin ang registration mo. Last na \'to, promise!<br><br><a href="'.$link.'">'.$link.'</a><br><br>Salamat,<br>Chooks-to-Go Admin'
			'content'=>$content
		);

            $config['protocol'] = 'sendmail';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            $this->email->from('no-reply@chookstogo.com.ph', "Chooks-to-Go Admin");
            $this->email->to($data['to']);
            // $this->email->to('nuworks.client1@gmail.com');
            $this->email->subject($data['subject']);
            $this->email->message($data['content']);
            $this->email->send();
     }
 
}