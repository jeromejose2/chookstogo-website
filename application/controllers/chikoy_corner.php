<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chikoy_Corner extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$params = array(
			'table'=>'tbl_chikoys_corner'
		);
		$this->data['comics'] = $this->mysql_queries->get_data($params);

		$this->template['content'] = $this->load->view('chikoys-corner-content', $this->data, TRUE);
		$this->load->view('template', $this->template);

	}

	public function ajax_process() {

		$this->data = array();
		$this->data['is_downloadables'] = FALSE;

		if($_POST['type'] != 'All' || $_POST['type'] != 'Downloadables') {
			$params = array(
				'table'=>'tbl_chikoys_corner',
				'where'=>'type = "'.$_POST['type'].'"'
			);
			$this->data['comics'] = $this->mysql_queries->get_data($params);
		} 

		if($_POST['type'] == 'All') {
			$params = array(
				'table'=>'tbl_chikoys_corner'
			);
			$this->data['comics'] = $this->mysql_queries->get_data($params);
		}

		if($_POST['type'] == 'Downloadables') {
			$params = array(
				'table'=>'tbl_downloadables'
			);
			$this->data['comics'] = $this->mysql_queries->get_data($params);
			$this->data['is_downloadables'] = TRUE;
		}

		$this->load->view('ajax-chikoys-corner-content', $this->data);

	}

	public function download($id) {

		$this->load->helper('download');

		$params = array(
			'table'=>'tbl_chikoys_corner',
			'where'=>'id = '.$id
		);
		$item = $this->mysql_queries->get_data($params);

		$data = file_get_contents('./assets/admin/uploads/chikoys_corner/'.$item[0]['filename']);
		$filename = $item[0]['filename'];

		force_download($filename, $data);

	}

	public function downloadables($id) {

		$this->load->helper('download');

		$params = array(
			'table'=>'tbl_downloadables',
			'where'=>'id = '.$id
		);
		$item = $this->mysql_queries->get_data($params);

		$data = file_get_contents('./assets/admin/uploads/downloadables/'.$item[0]['item_filename']);
		$filename = $item[0]['item_filename'];

		force_download($filename, $data);

	}

}