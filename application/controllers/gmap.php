<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gmap extends CI_Controller {

	public function index() {

		$this->params = array(
			'table'=>'tbl_areas'
		);
		$this->data['areas'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>'tbl_cities'
		);
		$this->data['cities'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>'tbl_stores'
		);
		$this->data['stores'] = $this->mysql_queries->get_data($this->params);

		$this->load->view('google_map', $this->data);

	}
}