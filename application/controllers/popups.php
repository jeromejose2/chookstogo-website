<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Popups extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function application_form() {

		$this->data['position'] = $_GET['position'];

		$this->load->view('popups/application-form', $this->data);

	}

	public function large_image() {

		$table = $_GET['type'] == 'comics' ? 'tbl_chikoys_corner' : 'tbl_downloadables';

		$params = array(
			'table'=>$table,
			'where'=>'id = '.$_GET['id']
		);
		$this->data['image'] = $this->mysql_queries->get_data($params);

		$this->load->view('popups/large-image', $this->data);

	}

	public function forgot_password() {

		$this->load->view('popups/forgot-password');

	}

	public function new_password() {

		$this->load->view('popups/new-password');

	}

	public function terms_and_conditions() {

		$params = array(
			'table'=>'tbl_settings',
			'where'=>'type = \'terms\''
		);
		$this->data['content'] = $this->mysql_queries->get_data($params);

		$this->load->view('popups/terms-and-conditions', $this->data);

	}

	public function edm() {

		$this->load->view('edm/edm');

	}

	public function thank_you() {
		$this->data['content'] = $_GET['content'];
		$this->load->view('popups/thank-you', $this->data);
	}

}