<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->helper('text');

	}

	public function index($id = NULL) {
		$this->data = array();

		if($id) {
			if(is_numeric($id)) {
				$params = array(
					'table'=>'tbl_products',
					'where'=>'id = '.$id
				);
				$product = $this->mysql_queries->get_data($params);
				if($product) {
					$this->data['og_title'] = $product[0]['title'];
					$this->data['og_description'] = $product[0]['description'];
					$this->data['og_url'] = site_url().'products/'.$product[0]['id'];
					$this->data['og_image'] = str_replace('~path~', base_url(), $product[0]['photo']);

					$this->data['product_id'] = $product[0]['id'];
					$this->data['category'] = $product[0]['category'];

					$params = array(
						'table'=>'tbl_products',
						'where'=>'category = "'.$product[0]['category'].'"'
					);
					$this->data['products'] = $this->mysql_queries->get_data($params);
				} else {
					$params = array(
						'table'=>'tbl_products',
						'where'=>'category = \'Roasted Products\''
					);
					$this->data['products'] = $this->mysql_queries->get_data($params);
				}				
			} else {
				$categories = array(
					'roasted',
					'chooksie',
					'disney',
					'other'
				);
				if(in_array($id, $categories)) {
					$this->data['product_category'] = $id;
					$params = array(
						'table'=>'tbl_products',
						'where'=>'category LIKE "%'. $id .'%"'
					);
					$this->data['products'] = $this->mysql_queries->get_data($params);
				} else {
					$params = array(
						'table'=>'tbl_products',
						'where'=>'category = \'Roasted Products\''
					);
					$this->data['products'] = $this->mysql_queries->get_data($params);
				}				
			}
		} else {
			$params = array(
				'table'=>'tbl_products',
				'where'=>'category = \'Roasted Products\''
			);
			$this->data['products'] = $this->mysql_queries->get_data($params);
		}		

		$this->template['content'] = $this->load->view('products-content', $this->data, TRUE);
		$this->load->view('template', $this->template);

	}

	public function ajax_process() {

		$this->data = array();

		$params = array(
			'table'=>'tbl_products',
			'where'=>'category = "'.$_POST['category'].'"'
		);
		$this->data['products'] = $this->mysql_queries->get_data($params);

		$this->load->view('ajax-products-content', $this->data);

	}

	public function _remap($id) {

		$this->index($id);

	}

}