<div class="container main-content">
     <div class="page-header">
          <h3>Terms and Conditions</h3>
     </div>

     <form class="form-horizontal" role="form" method="POST" >
          <textarea id="content" style="height: 200px" name="content"><?= $content[0]['content'] ?></textarea>
          <div class="form-group">
            <br>
                    <button type="submit" class="btn btn-primary" style="margin-left: 2%">Submit</button>
          </div>
     </form>


</div>

<script src="<?=base_url()?>assets/admin/js/texteditor/nicEdit.js?r=<?=rand()?>"></script>
<script type="text/javascript">
bkLib.onDomLoaded(function(){
  new nicEditor({iconsPath : '<?=base_url()?>assets/admin/js/texteditor/nicEditorIcons.gif'}).panelInstance('content');
});
</script>
<script type="text/javascript">
         $('input[name=promo_start_date], input[name=promo_end_date], input[name=publish_start_date], input[name=publish_end_date]').datepicker({
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat:'yy-mm-dd'
         });

     function uploadPhoto(input) {
        var name = input.files[0].name;
        var size = convertSize(input.files[0].size);
        $('.photo-filename').html(name + '<small> - '+ size +' </small>');
     }

     function convertSize(bytes) {

           if(bytes == 0) return '0 Byte';
           var k = 1000;
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
           var i = Math.floor(Math.log(bytes) / Math.log(k));
           var raw = (bytes / Math.pow(k, i)).toPrecision(3);
           var result;

           return raw >= 2097152 ? 'file limit exceeded' : raw + ' ' + sizes[i];
           
    }
</script>