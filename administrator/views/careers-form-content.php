<div class="container main-content">
     <div class="page-header">
          <? if(isset($career[0]['id'])): ?>
               <h3>Update Career</h3>
          <? else: ?>
               <h3>Create Career</h3>
          <? endif; ?>
     </div>

     <form class="form-horizontal" role="form" action="<?= site_url('careers/form') ?>" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="id" value="<?= isset($career[0]['id']) ? $career[0]['id'] : '' ?>">
          <div class="form-group">
               <label class="col-sm-2 control-label">Position</label>
               <div class="col-sm-4">
                    <input type="text" class="form-control" name="position" value="<?= isset($career[0]['position']) ? $career[0]['position'] : '' ?>">
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Location</label>
               <div class="col-sm-4">
                    <select name="location" class="form-control">
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Ilocos' ? 'selected' : '' ?>>Ilocos</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Isabela' ? 'selected' : '' ?>>Isabela</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Pangasinan' ? 'selected' : '' ?>>Pangasinan</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Central Luzon' ? 'selected' : '' ?>>Central Luzon</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Southern Tagalog' ? 'selected' : '' ?>>Southern Tagalog</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Bicol' ? 'selected' : '' ?>>Bicol</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Head Office - Pasig' ? 'selected' : '' ?>>Head Office - Pasig</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Bacolod' ? 'selected' : '' ?>>Bacolod</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Dumaguete' ? 'selected' : '' ?>>Dumaguete</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Cebu' ? 'selected' : '' ?>>Cebu</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Iloilo' ? 'selected' : '' ?>>Iloilo</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Roxas' ? 'selected' : '' ?>>Roxas</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Tacloban' ? 'selected' : '' ?>>Tacloban</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Ormoc' ? 'selected' : '' ?>>Ormoc</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Calbayog' ? 'selected' : '' ?>>Calbayog</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Cagayan de Oro' ? 'selected' : '' ?>>Cagayan de Oro</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Davao' ? 'selected' : '' ?>>Davao</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'General Santos' ? 'selected' : '' ?>>General Santos</option>
                        <option <?= isset($career[0]['location']) && $career[0]['location'] == 'Zamboanga' ? 'selected' : '' ?>>Zamboanga</option>
                    </select>
               </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Field of Work</label>
              <div class="col-sm-4">
                  <select class="form-control" name="field_of_work">
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Administration' ? 'selected' : '' ?>>Administration</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Animal Health Group' ? 'selected' : '' ?>>Animal Health Group</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Business / Product Development' ? 'selected' : '' ?>>Business / Product Development</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Compliance Assurance' ? 'selected' : '' ?> value='Compliance Assurance'>Food Quality and Safety Audit</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Contract Growing' ? 'selected' : '' ?>>Contract Growing</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Sales - Rotisserie/Chooks-to-Go' ? 'selected' : '' ?>>Sales - Rotisserie/Chooks-to-Go</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Customer Service / Delivery' ? 'selected' : '' ?>>Customer Service / Delivery</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Dressing Plant' ? 'selected' : '' ?>>Dressing Plant</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Engineering' ? 'selected' : '' ?>>Engineering</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Feed Mill' ? 'selected' : '' ?>>Feed Mill</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Finance' ? 'selected' : '' ?>>Finance</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Human Resources' ? 'selected' : '' ?>>Human Resources</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Information Systems' ? 'selected' : '' ?>>Information Systems</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Legal' ? 'selected' : '' ?>>Legal</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Live Sales' ? 'selected' : '' ?>>Live Sales</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Logistics' ? 'selected' : '' ?>>Logistics</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Management Control System' ? 'selected' : '' ?> value='Management Control System'>Corporate Governance / Planning</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Marketing' ? 'selected' : '' ?>>Marketing</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Materials Management /Purchasing' ? 'selected' : '' ?>>Materials Management /Purchasing</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Motor Pool Services' ? 'selected' : '' ?>>Motor Pool Services</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Sales - Supermarket/Trade Distributor' ? 'selected' : '' ?>>Sales - Supermarket/Trade Distributor</option>
                      <option <?= isset($career[0]['field_of_work']) && $career[0]['field_of_work'] == 'Utility/Warehouse/Liaison' ? 'selected' : '' ?>>Utility/Warehouse/Liaison</option>
                  </select>
              </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Description</label>
               <div class="col-sm-4">
                    <textarea name="description" id="content" id="a"><?= isset($career[0]['description']) ? $career[0]['description'] : '' ?></textarea>
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Qualifications</label>
               <div class="col-sm-4">
                    <textarea name="qualifications" id="content2" id="b" style="width: 100%"><?= isset($career[0]['qualifications']) ? $career[0]['qualifications'] : '' ?></textarea>
               </div>
          </div>
          <div class="form-group">
               <div class="col-sm-offset-2 col-sm-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
               </div>
          </div>
     </form>


</div>
<script type="text/javascript">
    tinymce.init({
        selector : "textarea",
        menubar : false,
        plugins : ["code preview"],
        toolbar : [
            "undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright | removeformat | code | bullist | fontsizeselect"
        ]
    });
</script>
<script type="text/javascript">
         $('input[name=promo_start_date], input[name=promo_end_date], input[name=publish_start_date], input[name=publish_end_date]').datepicker({
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat:'yy-mm-dd'
         });

     function uploadPhoto(input) {
        var name = input.files[0].name;
        var size = convertSize(input.files[0].size);
        $('.photo-filename').html(name + '<small> - '+ size +' </small>');
     }

     function convertSize(bytes) {

           if(bytes == 0) return '0 Byte';
           var k = 1000;
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
           var i = Math.floor(Math.log(bytes) / Math.log(k));
           var raw = (bytes / Math.pow(k, i)).toPrecision(3);
           var result;

           return raw >= 2097152 ? 'file limit exceeded' : raw + ' ' + sizes[i];
           
    }
</script>