<div class="container main-content">
     <div class="page-header">
          <? if(isset($updates[0]['id'])): ?>
               <h3>Update News & Updates</h3>
          <? else: ?>
               <h3>Create News & Updates</h3>
          <? endif; ?>
     </div>


     <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('about_us/db_query')?>" enctype="multipart/form-data">
                                          <div class="form-group">
                                            <input type="hidden" name="id" value="<?= isset($updates[0]['id']) ? $updates[0]['id'] : '' ?>">
                                                <label class="control-label">Photo</label>
                                                <input type="file" id="photo-new" class="photo" name="photo" style="display: none" onchange="uploadPhoto(this)">
                                                <div>
                                                     <button class="btn btn-primary" onclick="$('#photo-new').trigger('click'); return false">Browse</button>
                                                     <span class="photo-filename"><?php echo isset($updates[0]['filename']) ? $updates[0]['filename'] : '' ?></span>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Photo Slider</label>
                                                <input type="hidden" value="0" name="slider_photos_changed">
                                                <input type="file" id="slider-photo" class="slider-photo" name="slider_photos[]" style="display: none" class="form-control" onchange="sliderPhoto()" multiple>
                                                <div>
                                                    <?php if(isset($updates[0]['slider_photos'])): ?>
                                                      <?php if($updates[0]['slider_photos']): ?>
                                                         <button class="btn btn-primary disabled browse-slider-photo" onclick="$('#slider-photo').trigger('click'); return false">Browse</button>
                                                      <?php else: ?>
                                                         <button class="btn btn-primary browse-slider-photo" onclick="$('#slider-photo').trigger('click'); return false">Browse</button>
                                                      <?php endif; ?>
                                                    <?php else: ?>
                                                         <button class="btn btn-primary browse-slider-photo" onclick="$('#slider-photo').trigger('click'); return false">Browse</button>
                                                    <?php endif; ?>
                                                     <!-- <button class="btn btn-primary" onclick="$('#slider-photo').trigger('click'); return false">Browse</button> -->
                                                     <span class="slider-photo-filename">
                                                        <? if(isset($updates[0]['slider_photos'])): ?>
                                                          <? if($updates[0]['slider_photos']): ?>
                                                            <? $slider_photos = unserialize($updates[0]['slider_photos']); ?>
                                                            <? foreach($slider_photos as $v): ?>
                                                              <?= end(explode('/', $v)) . ', ' ?>
                                                            <? endforeach; ?>
                                                            <a href="javascript:;" onclick="removeSliderPhoto()"><i class="glyphicon glyphicon-remove-circle" title="Remove all photos" style="color: red"></i></a>
                                                          <? endif; ?>
                                                        <? endif; ?>
                                                     </span>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Title</label>
                                                <div>
                                                     <input type="text" class="form-control" name="title" value="<?= isset($updates[0]['title']) ? $updates[0]['title'] : '' ?>">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Description</label>
                                                <div>
                                                     <textarea type="text" class="form-control" name="description"><?= isset($updates[0]['description']) ? $updates[0]['description'] : '' ?></textarea>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Publish Date</label>
                                                <div>
                                                     <input type="text" class="form-control" name="date_of_publish" value="<?= isset($updates[0]['date_of_publish']) ? date('Y-m-d', strtotime($updates[0]['date_of_publish'])) : '' ?>">
                                                </div>
                                           </div>

                            <!-- END MODAL CONTENT -->
                            <div class="form-group">
                            <label class="control-label">
               <!-- <div class="col-sm-offset-2 col-sm-4"> -->
                            </label>
                    <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                          

               <!-- </div> -->
          </div>
                              </form>


</div>

<script type="text/javascript">
    tinymce.init({
        selector : "textarea",
        menubar : false,
        plugins : ["code preview"],
        toolbar : [
            "undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright | removeformat | code | bullist | fontsizeselect"
        ]
    });
</script>
<script type="text/javascript">
         $('input[name=date_of_publish]').datepicker({
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat:'yy-mm-dd'
         });

     function uploadPhoto(input) {
        var name = input.files[0].name;
        var size = convertSize(input.files[0].size);
        $('.photo-filename').html(name + '<small> - '+ size +' </small>');
     }

     function sliderPhoto() {
        var files = document.getElementById('slider-photo').files;
        var fname_container = $('.slider-photo-filename');

        for(var i = 0; i < files.length; i++) {
            fname_container.append(files[i].name + ', ');
        }

        $('input[name=slider_photos_changed]').val('1');
        console.log(files);
     }

     function convertSize(bytes) {

           if(bytes == 0) return '0 Byte';
           var k = 1000;
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
           var i = Math.floor(Math.log(bytes) / Math.log(k));
           var raw = (bytes / Math.pow(k, i)).toPrecision(3);
           var result;

           return raw >= 2097152 ? 'file limit exceeded' : raw + ' ' + sizes[i];
           
    }

    function removeSliderPhoto() {

        $("#slider-photo").val("");
        $(".slider-photo-filename").html('');
        $(".browse-slider-photo").removeClass('disabled');
        $('input[name=slider_photos_changed]').val('1');

    }
</script>