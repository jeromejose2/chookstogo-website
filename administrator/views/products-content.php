<div class="container main-content">
     <div class="page-header">
          <h3>Products <span class="badge"><?= $total ?></span></h3>

          <div class="actions">
               <a class="btn btn-primary advanced"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Advance Search</span></a>
          </div>

          <div class="actions" style="margin-right: 1%">
               <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#popup-new" onclick="$('.foo > div').css('width', '100%'); $('.nicEdit-main').css('width', '100%').css('height', '80px')">New Item</a>
          </div>

          <div class="actions" style="margin-right: 1%">
              <a href="<?=site_url('export/products?'.http_build_query($_GET, '', "&"))?>" class="btn btn-primary hidden-phone"><i class="icon-download-alt"></i> Export</a>
          </div>

          <div class="actions" style="margin-right: 1%">
              <a href="<?=site_url('export_image/products')?>" class="btn btn-primary hidden-phone"><i class="icon-download-alt"></i> Export Images</a>
          </div>

          <div class=" advance-search">
          <div class="form-content">
               <form class="form-search">
                    <div class="input-prepend">
                         <span class="add-on">ID</span>
                         <input type="text" class="input-medium" name="id" value="<?=isset($_GET['id']) ? $_GET['id'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">NAME</span>
                         <input type="text" class="input-medium" name="name" value="<?=isset($_GET['name']) ? $_GET['name'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">CATEGORY</span>
                         <select class="input-small" name="category">
                            <option value=""></option>
                            <option <?= isset($_GET['category']) && $_GET['category'] == 'Roasted Products' ? 'selected' : '' ?>>Roasted Products</option>
                            <option <?= isset($_GET['category']) && $_GET['category'] == 'Chooksie\'s Products' ? 'selected' : '' ?>>Chooksie's Products</option>
                            <option <?= isset($_GET['category']) && $_GET['category'] == 'Disney Products' ? 'selected' : '' ?>>Disney Products</option>
                            <option <?= isset($_GET['category']) && $_GET['category'] == 'Other Products' ? 'selected' : '' ?>>Other Products</option>
                         </select>
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">DESCRIPTION</span>
                         <input type="text" class="input-medium" name="description" value="<?=isset($_GET['description']) ? $_GET['description'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">ITEMS/PAGE</span>
                         <select name="psize" class="input-small">
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>
                         </select>
                    </div> 

                    <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>
               </form>
          </div>
     </div>
     </div>


     <table class="table table-bordered">
          <thead>
               <tr>
                    <th style="width:1%">Image</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Description</th>
                    <th>Action</th>
               </tr>
          </thead>

          <tbody>
               <? if($items): ?>
                    <? foreach($items as $key => $v): ?>
                         <tr id="row-<?= $v['id'] ?>">
                              <td><a href="javascript://" data-toggle="modal" data-target="#popup-image-<?= $v['id'] ?>"><img style="height: 25px" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>"></a></td>
                              <td><?= $v['title'] ?></td>
                              <td><?= $v['category'] ?></td>
                              <td><?= $v['description'] ?></td>
                              <td style="width: 1%">
                                   <a href="javascript:void(0)" style="margin-right: 10px" title="Edit" data-toggle="modal" data-target="#popup-<?php echo $v['id'];?>"><i class="glyphicon glyphicon-edit"></i></a>
                                   <a href="javascript:void(0)" title="Delete" class="delete" data-id="<?php echo $v['id'];?>"><i class="glyphicon glyphicon-trash"></i></a>
                              </td>
                         </tr>

                         <!-- MODALS -->
                              <div class="modal fade" id="popup-<?php echo $v['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                      <h4 class="modal-title" id="myModalLabel">Update Product</h4>
                                    </div>
                                    <div class="modal-body">
                                      <!-- MODAL CONTENT -->

                                        <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('products/update_product')?>" enctype="multipart/form-data">
                                          <div class="form-group">
                                            <input type="hidden" name="id" value="<?= $v['id'] ?>">
                                                <label class="control-label">Photo</label>
                                                <input type="file" id="photo-<?= $v['id'] ?>" class="photo" name="photo" style="display: none" onchange="uploadPhoto(this)">
                                                <div>
                                                     <button class="btn btn-primary" onclick="$('#photo-<?= $v['id'] ?>').trigger('click'); return false">Browse</button>
                                                     <span class="photo-filename"><?= $v['filename'] ?></span>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Thumbnail</label>
                                                <input type="file" id="photo-thumb-<?= $v['id'] ?>" class="photo_thumb" name="photo_thumb" style="display: none" onchange="uploadPhotoThumb(this)">
                                                <div>
                                                     <button class="btn btn-primary" onclick="$('#photo-thumb-<?= $v['id'] ?>').trigger('click'); return false">Browse</button>
                                                     <span class="photo-filename-thumb"><?= $v['filename_thumb'] ?></span>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Name</label>
                                                <div>
                                                     <input type="text" class="form-control" name="title" value="<?= $v['title'] ?>">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Category</label>
                                                <div>
                                                     <select class="form-control" name="category">
                                                        <option <?= $v['category'] == 'Roasted Products' ? 'selected' : '' ?>>Roasted Products</option>
                                                        <option <?= $v['category'] == 'Chooksie\'s Products' ? 'selected' : '' ?>>Chooksie's Products</option>
                                                        <option <?= $v['category'] == 'Disney Products' ? 'selected' : '' ?>>Disney Products</option>
                                                        <option <?= $v['category'] == 'Other Products' ? 'selected' : '' ?>>Other Products</option>
                                                     </select>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">For Delivery</label>
                                                <div>
                                                     <input align="left" type="checkbox" class="form-control" name="for_delivery" <?= isset($v['for_delivery']) && $v['for_delivery'] == 1 ? 'checked' : '' ?>>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Description</label>
                                                <div class="foo">
                                                    <textarea name="description"><?= $v['description'] ?></textarea>
                                                </div>
                                           </div>

                                      <!-- END MODAL CONTENT -->
                                      </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Save changes</button>
                                        </form>

                                    </div>
                                  </div>
                                </div>
                              </div>
                         <!-- END MODALS -->

                         <!-- IMAGE MODAL -->
                         <!-- MODALS -->
                              <div class="modal fade" id="popup-image-<?php echo $v['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    </div>
                                    <div class="modal-body">
                                      <!-- MODAL CONTENT -->

                                        <img style="width: 100%" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>">

                                      <!-- END MODAL CONTENT -->
                                      </div>
                                  </div>
                                </div>
                              </div>
                         <!-- END MODALS -->
                         <!-- END IMAGE MODAL -->

                    <? endforeach; ?>
               <? else: ?>
                         <tr>
                              <td colspan="10"><center style="color: red">No data found.</center></td>
                         </tr>
               <? endif; ?>

               <!-- NEW ITEM -->
                    <!-- MODALS -->
                    <div class="modal fade" id="popup-new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">New Product</h4>
                          </div>
                          <div class="modal-body">
                            <!-- MODAL CONTENT -->

                              <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('products/save_product')?>" enctype="multipart/form-data">
                                  <div class="form-group">
                                        <label class="control-label">Photo</label>
                                        <input type="file" id="photo-new" class="photo" name="photo" style="display: none" onchange="uploadPhoto(this)">
                                        <div>
                                             <button class="btn btn-primary" onclick="$('#photo-new').trigger('click'); return false">Browse</button>
                                             <span class="photo-filename"></span>
                                        </div>
                                   </div>

                                   <div class="form-group">
                                                <label class="control-label">Thumbnail</label>
                                                <input type="file" id="photo-thumb-new" class="photo_thumb" name="photo_thumb" style="display: none" onchange="uploadPhotoThumb(this)">
                                                <div>
                                                     <button class="btn btn-primary" onclick="$('#photo-thumb-new').trigger('click'); return false">Browse</button>
                                                     <span class="photo-filename-thumb"></span>
                                                </div>
                                           </div>

                                   <div class="form-group">
                                        <label class="control-label">Name</label>
                                        <div>
                                             <input type="text" class="form-control" name="title" value="">
                                        </div>
                                   </div>

                                   <div class="form-group">
                                        <label class="control-label">Category</label>
                                        <div>
                                             <select class="form-control" name="category">
                                                <option>Roasted Products</option>
                                                <option>Chooksie's Products</option>
                                                <option>Disney Products</option>
                                                <option>Other Products</option>
                                             </select>
                                        </div>
                                   </div>

                                   <div class="form-group">
                                                <label class="control-label">For Delivery</label>
                                                <div>
                                                     <input align="left" type="checkbox" class="form-control" name="for_delivery">
                                                </div>
                                           </div>

                                   <div class="form-group">
                                        <label class="control-label">Description</label>
                                        <div class="foo">
                                            <textarea name="description" id="content"></textarea>
                                        </div>
                                   </div>

                            <!-- END MODAL CONTENT -->
                            </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                              </form>

                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END MODALS -->
                    <!-- END NEW ITEM -->

          </tbody>
     </table>

     <?=$pagination?>


</div>

<script type="text/javascript">
    tinymce.init({
        selector : "textarea",
        menubar : false,
        plugins : ["code preview"],
        toolbar : [
            "undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright | removeformat | code | bullist | fontsizeselect"
        ]
    });
</script>

<script type="text/javascript">
     $(function(){
          $('.advanced').click(function(){
               $('.advance-search').slideToggle();
          });

          var lytebox = new Lytebox;
          $('.delete').on('click', function(){
               var id = $(this).data('id');
               lytebox.dialog({
                    message: 'Are you sure you want to delete this?', 
                    title : 'Confirm<hr style="margin: 5px 0; border-color: #ccc">',
                    type : 'confirm',
                    onConfirm : function(){
                         $.post('<?php echo site_url("products/delete")?>', {id:id}, function(){
                              $('#row-'+id).hide();
                         });
                    },
                    top: 100
               });
          });
     });

     function uploadPhoto(input) {
        var name = input.files[0].name;
        var size = convertSize(input.files[0].size);
        $('.photo-filename').html(name + '<small> - '+ size +' </small>');
     }

     function uploadPhotoThumb(input) {
        var name = input.files[0].name;
        var size = convertSize(input.files[0].size);
        $('.photo-filename-thumb').html(name + '<small> - '+ size +' </small>');
     }

     function convertSize(bytes) {

           if(bytes == 0) return '0 Byte';
           var k = 1000;
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
           var i = Math.floor(Math.log(bytes) / Math.log(k));
           var raw = (bytes / Math.pow(k, i)).toPrecision(3);
           var result;

           return raw >= 2097152 ? 'file limit exceeded' : raw + ' ' + sizes[i];
           
    }
</script>