<!--start main content -->
<div class="container main-content">
     <div class="page-header">
          <h3>Gallery <span class="badge"><?php echo $total;?></span></h3>

          <div class="actions">
               <a class="btn btn-primary advanced"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Advance Search</span></a>
          </div>

          <div class=" advance-search">
               <div class="form-content">
                    <form class="form-search">
                         <div class="input-prepend">
                              <span class="add-on">ID</span>
                              <input type="text" class="input-medium" name="id" value="<?=isset($_GET['id']) ? $_GET['id'] : '' ?>" >
                         </div>
                         <div class="input-prepend">
                              <span class="add-on">ITEMS/PAGE</span>
                              <select name="psize" class="input-small">
                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>
                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>
                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>
                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>
                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>
                              </select>
                         </div>
                         <div class="input-prepend">
                              <span class="add-on">SORTING</span>
                              <select name="sort" class="input-small">
                                   <option></option>
                                   <option <?=isset($_GET['sort']) && $_GET['sort']=='DESC' ? 'selected="selected"' : ''?>>DESC</option>
                                   <option <?=isset($_GET['sort']) && $_GET['sort']=='ASC' ? 'selected="selected"' : ''?>>ASC</option>
                              </select>
                         </div>

                         <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>
                    </form>
               </div>
          </div>
     </div>

     <div class="row" id="sortable-gallery">
          <? if($items): ?>
               <? foreach($items as $key => $v): ?>
                    <div id="row-<?= $v['id'] ?>" class="col-sm-6 col-md-3">
                         <div class="thumbnail">
                              <img src="<?= $v['url_standard'] ?>">
                              <div class="caption">
                                   <h4>ID: <?= $v['id'] ?></h4>
                                   <p style="min-height: 70px"><?= character_limiter($v['caption'], 80) ?></p>
                                   <!-- <p style="text-align: center"><a href="#" class="btn btn-primary" role="button" data-toggle="modal" data-target="#popup-large-<?= $v['id'] ?>">Preview</a> <a href="#" class="btn btn-success" role="button">Edit</a> <a href="#" class="btn btn-danger" role="button">Delete</a></p> -->
                                   <p>
                                        <fieldset>
                                             <legend><small></small></legend>
                                             <a href="javascript:void(0)" class="delete pull-right" style="margin-right: 10px" title="Delete" data-id="<?php echo $v['id'];?>"><i class="glyphicon glyphicon-trash"></i></a>
                                             <a href="javascript:void(0)" class="pull-right" style="margin-right: 10px" title="Edit" data-toggle="modal" data-target="#popup-<?php echo $v['user_id'];?>"><i class="glyphicon glyphicon-edit"></i></a>
                                             <a href="javascript:void(0)" class="pull-right" style="margin-right: 10px" title="Preview" data-toggle="modal" data-target="#popup-large-<?= $v['id'] ?>"><i class="glyphicon glyphicon-eye-open"></i></a>
                                        </fieldset>
                                   </p>
                              </div>
                         </div>
                    </div>

                    <!-- POPUP -->
                    <div class="modal fade" id="popup-large-<?= $v['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">ID: <?= $v['id'] ?></h4>
                          </div>
                          <div class="modal-body">
                            <center><img style="width:50%" src="<?= $v['url_standard'] ?>"></center>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END POPUP -->
               <? endforeach; ?>
          <? endif; ?>
     </div>

     <?= $pagination ?>

</div>

<script>
     var lytebox = new Lytebox;

     $('#sortable-gallery').sortable({});
     $('#sortable-gallery').disableSelection();

     $('.advanced').click(function(){
          $('.advance-search').slideToggle();
     });

     $('.delete').on('click', function(){
          var data = {id:$(this).data('id'), table:'tbl_entries'};
          lytebox.dialog({
               message: 'Are you sure you want to delete this?', 
               title : 'Confirm<hr style="margin: 5px 0; border-color: #ccc">',
               type : 'confirm',
               onConfirm : function(){
                    $.post('<?= site_url("ajax/delete") ?>', data, function(){
                         $('#row-'+data.id).hide('slow');
                    });
               },
               top: 100
          });
     });
</script>

<style>
     .thumbnail img {
          height: 180px;
          background-image: url('<?php echo base_url()?>assets/admin/images/spinner.gif');
          background-repeat: no-repeat;
          background-position: center;
     }
</style>