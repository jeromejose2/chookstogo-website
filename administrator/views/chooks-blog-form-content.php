<div class="container main-content">
     <div class="page-header">
          <? if(isset($article[0]['id'])): ?>
               <h3>Update Article</h3>
          <? else: ?>
               <h3>Create Article</h3>
          <? endif; ?>
     </div>

     <form class="form-horizontal" role="form" action="<?= site_url('chooks_blog/form') ?>" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="id" value="<?= isset($article[0]['id']) ? $article[0]['id'] : '' ?>">
          <div class="form-group">
               <label class="col-sm-2 control-label">Title</label>
               <div class="col-sm-8">
                    <input type="text" class="form-control" name="title" value="<?= isset($article[0]['title']) ? $article[0]['title'] : '' ?>">
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Section</label>
               <div class="col-sm-8">
                    <select name="section" class="form-control">
                         <option <?= isset($article[0]['section']) && $article[0]['section'] == 'Main Section' ? 'selected' : '' ?>>Main Section</option>
                         <option <?= isset($article[0]['section']) && $article[0]['section'] == 'Chooks Help' ? 'selected' : '' ?>>Chooks Help</option>
                    </select>
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Photo</label>
               <div class="col-sm-8">
                    <input type="file" id="photo-new" class="photo" name="photo" style="display: none" onchange="uploadPhoto(this)" class="form-control">
                    <button class="btn btn-primary" onclick="$('#photo-new').trigger('click'); return false">Browse</button>
                    <span class="photo-filename"><?= isset($article[0]['filename']) ? $article[0]['filename'] : '' ?></span>
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Slider Photo</label>
               <div class="col-sm-8">
                    <input type="hidden" value="0" name="slider_photos_changed">
                    <input type="file" id="slider-photo" class="slider-photo" name="slider_photos[]" style="display: none" class="form-control" onchange="sliderPhoto()" multiple>
                    <? if(isset($article[0]['slider_photos'])): ?>
                      <? if($article[0]['slider_photos']): ?>
                        <button class="btn btn-primary disabled browse-slider-photo" onclick="$('#slider-photo').trigger('click'); return false">Browse</button>
                      <? else: ?>
                        <button class="btn btn-primary browse-slider-photo" onclick="$('#slider-photo').trigger('click'); return false">Browse</button>
                      <? endif; ?>
                    <? else: ?>
                        <button class="btn btn-primary browse-slider-photo" onclick="$('#slider-photo').trigger('click'); return false">Browse</button>
                    <? endif; ?>
                    <span class="slider-photo-filename">
                        <? if(isset($article[0]['slider_photos'])): ?>
                          <? if($article[0]['slider_photos']): ?>
                            <? $slider_photos = unserialize($article[0]['slider_photos']); ?>
                            <? foreach($slider_photos as $v): ?>
                              <?= end(explode('/', $v)) . ', ' ?>
                            <? endforeach; ?>
                            <a href="javascript:;" onclick="removeSliderPhoto()"><i class="glyphicon glyphicon-remove-circle" title="Remove all photos" style="color: red"></i></a>
                          <? endif; ?>
                        <? endif; ?>
                    </span>
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Description</label>
               <div class="col-sm-8">
                    <textarea name="short_description" id="content"><?= isset($article[0]['short_description']) ? $article[0]['short_description'] : '' ?></textarea>
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Date of Publish</label>
               <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Date of Publish" name="date_of_publish" value="<?= isset($article[0]['date_of_publish']) ? date('Y-m-d', strtotime($article[0]['date_of_publish'])) : '' ?>">
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Featured</label>
               <div class="col-sm-8">
                    <input type="checkbox" name="is_featured" <?= isset($article[0]['is_featured']) && $article[0]['is_featured'] == '1' ? 'checked' : '' ?>>
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Announcement</label>
               <div class="col-sm-8">
                    <input type="checkbox" name="is_announcement" <?= isset($article[0]['is_announcement']) && $article[0]['is_announcement'] == '1' ? 'checked' : '' ?>>
               </div>
          </div>
          <div class="form-group">
               <div class="col-sm-offset-2 col-sm-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
               </div>
          </div>
     </form>


</div>

<script type="text/javascript">
    tinymce.init({
        selector : "textarea",
        menubar : false,
        plugins : ["code preview"],
        toolbar : [
            "undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright | removeformat | code | bullist | fontsizeselect"
        ]
    });
</script>
<script type="text/javascript">
         $('input[name=date_of_publish]').datepicker({
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat:'yy-mm-dd'
         });

     function uploadPhoto(input) {
        var name = input.files[0].name;
        var size = convertSize(input.files[0].size);
        $('.photo-filename').html(name + '<small> - '+ size +' </small>');
     }

     function sliderPhoto() {
        var files = document.getElementById('slider-photo').files;
        var fname_container = $('.slider-photo-filename');

        for(var i = 0; i < files.length; i++) {
            fname_container.append(files[i].name + ', ');
        }

        $('input[name=slider_photos_changed]').val('1');
        console.log(files);
     }

     function convertSize(bytes) {

           if(bytes == 0) return '0 Byte';
           var k = 1000;
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
           var i = Math.floor(Math.log(bytes) / Math.log(k));
           var raw = (bytes / Math.pow(k, i)).toPrecision(3);
           var result;

           return raw >= 2097152 ? 'file limit exceeded' : raw + ' ' + sizes[i];
           
    }

    function removeSliderPhoto() {

        $("#slider-photo").val("");
        $(".slider-photo-filename").html('');
        $(".browse-slider-photo").removeClass('disabled');
        $('input[name=slider_photos_changed]').val('1');

    }
</script>