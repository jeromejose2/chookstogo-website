<!DOCTYPE html>
<html>
<head>
     <title>CMS</title>
     <!-- Bootstrap -->
     <link rel="stylesheet" href="<?= base_url() ?>assets/admin/css/bootstrap.min.css" >
     <link rel="stylesheet" href="<?= base_url() ?>assets/admin/css/style.css" >
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="<?= base_url() ?>assets/admin/js/html5shiv.js"></script>
     <script src="<?= base_url() ?>assets/admin/js/respond.min.js"></script>
     <![endif]-->
     <link rel="stylesheet" href="<?= base_url() ?>assets/admin/css/custom.css">
     <link rel="stylesheet" href="<?= base_url() ?>assets/admin/css/lytebox.css">
     <link rel="stylesheet" href="<?= base_url() ?>assets/admin/css/jquery-ui.min.css">
     <script type="text/javascript" src="<?= base_url() ?>assets/admin/js/lytebox.2.3.js"></script>
     <script src="<?= base_url() ?>assets/admin/js/jquery.js"></script>
     <script src="<?= base_url() ?>assets/admin/js/jquery-ui.js"></script>
     <script src="<?= base_url() ?>assets/admin/js/tinymce/tinymce.min.js"></script>

</head>
<body>
     <!-- HEADER -->
     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?= base_url() ?>admin/home">NuWorks</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav">
                    <? if($this->session->userdata('user_email') != 'ept@bountyagro.com.ph'): ?>
                         <li class="<?= $this->uri->segment(1) == 'home' ? 'active' : '' ?>"><a href="<?= site_url('home') ?>"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>

                         <!-- HOMEPAGE -->
                         <!-- <li class="<?= $this->uri->segment(1) == 'slider' || $this->uri->segment(1) == 'announcements' || $this->uri->segment(1) == 'stores' ? 'active' : '' ?>">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home Page <b class="caret"></b></a>
                              <ul class="dropdown-menu"> -->
                                   <!-- <li class="<?= $this->uri->segment(1) == 'slider' ? 'active' : '' ?>"><a href="<?= site_url('slider') ?>">Slider</a></li> -->
                                   <!-- <li class="<?= $this->uri->segment(1) == 'announcements' ? 'active' : '' ?>"><a href="<?= site_url('announcements') ?>">Announcements</a></li> -->
                                   <!-- <li class="<?= $this->uri->segment(1) == 'stores' ? 'active' : '' ?>"><a href="<?= site_url('stores') ?>">Stores</a></li> -->
                              <!-- </ul>
                         </li> -->
                         <!-- END HOMEPAGE -->

                         <!-- SLIDER -->
                         <li class="<?= $this->uri->segment(1) == 'slider' ? 'active' : '' ?>"><a href="<?= site_url('slider') ?>">Slider</a></li>
                         <!-- END SLIDER -->

                         <!-- PRODUCTS -->
                         <li class="<?= $this->uri->segment(1) == 'products' ? 'active' : '' ?>"><a href="<?= site_url('products') ?>">Products</a></li>
                         <!-- END PRODUCTS -->

                         <!-- STORES -->
                         <li class="<?= $this->uri->segment(1) == 'stores' ? 'active' : '' ?>"><a href="<?= site_url('stores') ?>">Stores</a></li>
                         <!-- END STORES -->

                         <!-- PROMOS -->
                         <li class="<?= $this->uri->segment(1) == 'promos' ? 'active' : '' ?>"><a href="<?= site_url('promos') ?>">Promos</a></li>
                         <!-- END PROMOS -->

                         <!-- BLOG -->
                         <li class="<?= $this->uri->segment(1) == 'chooks_blog' ? 'active' : '' ?>"><a href="<?= site_url('chooks_blog') ?>">Chooks Blog</a></li>
                         <!-- END BLOG -->

                         <!-- CHIKOYS CORNER -->
                         <li class="<?= $this->uri->segment(1) == 'chikoys_corner' || $this->uri->segment(1) == 'downloadables' ? 'active' : '' ?>">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Chikoy's Corner <b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                   <li class="<?= $this->uri->segment(1) == 'comics' ? 'active' : '' ?>"><a href="<?= site_url('chikoys_corner') ?>">Comics</a></li>
                                   <li class="<?= $this->uri->segment(1) == 'downloadables' ? 'active' : '' ?>"><a href="<?= site_url('downloadables') ?>">Downloadables</a></li>
                              </ul>
                         </li>
                         <!-- END CHIKOYS CORNER -->

                         <!-- USER LISTS -->
                         <li class="<?= $this->uri->segment(1) == 'registrants' || $this->uri->segment(1) == 'feedbacks' ? 'active' : '' ?>">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Users Page <b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                   <li class="<?= $this->uri->segment(1) == 'registrants' ? 'active' : '' ?>"><a href="<?= site_url('registrants') ?>">Registrants</a></li>
                                   <li class="<?= $this->uri->segment(1) == 'feedbacks' ? 'active' : '' ?>"><a href="<?= site_url('feedbacks') ?>">Feedbacks</a></li>
                              </ul>
                         </li>
                         <!-- END USER LISTS -->

                         <!-- ABOUT US -->
                         <li class="<?= $this->uri->segment(1) == 'about_us' ? 'active' : '' ?>">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                   <li class="<?= $this->uri->segment(1) == 'about_us' && $this->uri->segment(2) == NULL ? 'active' : '' ?>"><a href="<?= site_url('about_us') ?>">News and Updates</a></li>
                                   <li class="<?= $this->uri->segment(1) == 'about_us' && $this->uri->segment(2) == 'personalities' ? 'active' : '' ?>"><a href="<?= site_url('about_us/personalities') ?>">Personalities</a></li>
                              </ul>
                         </li>
                         <!-- END ABOUT US -->

                         <li class="<?= $this->uri->segment(1) == 'careers' ? 'active' : '' ?>"><a href="<?= site_url('careers') ?>">Careers</a></li>

                         <li class="<?= $this->uri->segment(1) == 'terms' ? 'active' : '' ?>"><a href="<?= site_url('terms') ?>">Terms</a></li>

                         <li class="<?= $this->uri->segment(1) == 'logs' ? 'active' : '' ?>"><a href="<?= site_url('logs') ?>">Audit Logs</a></li>
                         
                         <!-- <li class="<?= $this->uri->segment(1) == 'form' ? 'active' : '' ?>"><a href="<?= site_url('form') ?>"><span class="glyphicon glyphicon-list-alt"></span> Form</a></li> -->
                         <!-- <li class="<?= $this->uri->segment(1) == 'lists' ? 'active' : '' ?>"><a href="<?= site_url('lists') ?>"><span class="glyphicon glyphicon-list"></span> List</a></li> -->
                         <!-- <li class="<?= $this->uri->segment(1) == 'gallery' ? 'active' : '' ?>"><a href="<?= site_url('gallery') ?>"><span class="glyphicon glyphicon-picture"></span> Gallery</a></li> -->
                         <!-- <li class="<?= $this->uri->segment(1) == 'article' ? 'active' : '' ?>"><a href="<?= site_url('article') ?>">Article</a></li> -->
                         
                         
                         <!-- <li>
                              <a href="#" class="dropdown-toggle <?= $this->uri->segment(1) == 'stores' ? 'active' : '' ?>" data-toggle="dropdown">Stores <b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                   <li><a href="<?= site_url('stores/area') ?>">Area</a></li>
                                   <li><a href="<?= site_url('stores/city') ?>">City</a></li>
                                   <li><a href="<?= site_url('stores/branch') ?>">Branch</a></li>
                              </ul>
                         </li> -->
                    <? else: ?>
                         <li class="<?= $this->uri->segment(1) == 'careers' ? 'active' : '' ?>"><a href="<?= site_url('careers') ?>">Careers</a></li>
                    <? endif; ?>

               </ul>

               <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown <?= $this->uri->segment(1) == 'accounts' ? 'active' : '' ?>">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> Settings <b class="caret"></b></a>
                         <ul class="dropdown-menu">
                              <? if($this->session->userdata('user_email') != 'ept@bountyagro.com.ph'): ?>
                                   <li><a href="<?= site_url('accounts') ?>"><span class="glyphicon glyphicon-user"></span> Accounts</a></li>
                                   <li class="divider"></li>
                              <? endif; ?>
                              <li><a href="<?= site_url('logout') ?>"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                         </ul>
                    </li>
               </ul>
          </div>
          <!-- END NAVBAR-COLLAPSE -->
     </nav>
     <!-- END HEADER -->


     <!-- MAIN CONTENT -->
     <?php echo $content;?>
     <!-- END MAIN CONTENT -->

     <!-- START FOOTER -->
     <footer class="main">
          <div class="container">
               <small>Nuworks Interactive Labs &copy; 2014</small>
          </div>
     </footer>
     <!-- END FOOTER -->

     <!-- LOAD JAVASCRIPT -->
     <script src="<?php echo base_url()?>assets/admin/js/bootstrap.min.js"></script>
     <?php if($this->uri->segment(1) != 'slider'): ?>
          <script src="<?php echo base_url()?>assets/admin/js/holder.js"></script>
     <?php endif; ?>
     <!-- END LOAD JAVASCRIPT -->
</body>
</html>