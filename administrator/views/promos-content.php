<div class="container main-content">
     <div class="page-header">
          <h3>Promos <span class="badge"><?= $total ?></span></h3>

          <div class="actions">
               <a class="btn btn-primary advanced"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Advance Search</span></a>
          </div>

          <div class="actions" style="margin-right: 1%">
               <a href="<?= site_url('promos/form') ?>" class="btn btn-primary">New Item</a>
          </div>

          <div class="actions" style="margin-right: 1%">
              <a href="<?=site_url('export/promos?'.http_build_query($_GET, '', "&"))?>" class="btn btn-primary hidden-phone"><i class="icon-download-alt"></i> Export</a>
          </div>

          <div class="actions" style="margin-right: 1%">
              <a href="<?=site_url('export_image/promos')?>" class="btn btn-primary hidden-phone"><i class="icon-download-alt"></i> Export Images</a>
          </div>

          <div class=" advance-search">
          <div class="form-content">
               <form class="form-search">
                    <div class="input-prepend">
                         <span class="add-on">ID</span>
                         <input type="text" class="input-medium" name="id" value="<?=isset($_GET['id']) ? $_GET['id'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">TITLE</span>
                         <input type="text" class="input-medium" name="title" value="<?=isset($_GET['title']) ? $_GET['title'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">TYPE</span>
                         <select name="promo_type">
                            <option value=""></option>
                           <option <?= isset($_GET['promo_type']) && $_GET['promo_type'] == 'In-store Promo' ? 'selected' : '' ?>>In-store Promo</option>
                           <option <?= isset($_GET['promo_type']) && $_GET['promo_type'] == 'Digital Promo' ? 'selected' : '' ?>>Digital Promo</option>
                      </select>
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">DESCRIPTION</span>
                         <input type="text" class="input-medium" name="description" value="<?=isset($_GET['description']) ? $_GET['description'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">PROMO DATE</span>
                         <input type="text" class="input-medium" name="promo_start_date" value="<?=isset($_GET['promo_start_date']) ? $_GET['promo_start_date'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">PUBLISH DATE</span>
                         <input type="text" class="input-medium" name="publish_start_date" value="<?=isset($_GET['publish_start_date']) ? $_GET['publish_start_date'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">TYPE OF PROMO</span>
                         <select name="type_of_promo">
                            <option value=""></option>
                             <option <?= isset($_GET['type_of_promo']) && $_GET['type_of_promo'] == 'Article' ? 'selected' : '' ?>>Article</option>
                             <option <?= isset($_GET['type_of_promo']) && $_GET['type_of_promo'] == 'App/Microsite' ? 'selected' : '' ?>>App/Microsite</option>
                        </select>
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">LINK</span>
                         <input type="text" class="input-medium" name="link" value="<?=isset($_GET['link']) ? $_GET['link'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">FEATURED</span>
                         <select name="is_featured">
                              <option value=""></option>
                             <option <?= isset($_GET['is_featured']) && $_GET['is_featured'] == 0 ? 'selected' : '' ?> value="0">No</option>
                             <option <?= isset($_GET['is_featured']) && $_GET['is_featured'] == 1 ? 'selected' : '' ?> value="1">Yes</option>
                        </select>
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">ITEMS/PAGE</span>
                         <select name="psize" class="input-small">
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>
                         </select>
                    </div> 

                    <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>
               </form>
          </div>
     </div>
     </div>


     <table class="table table-bordered">
          <thead>
               <tr>
                    <th style="width:1%">Image</th>
                    <th>Title</th>
                    <th>Promo Type</th>
                    <th>Description</th>
                    <th>Promo Date</th>
                    <th>Publish Date</th>
                    <th>Type of Promo</th>
                    <th>Link</th>
                    <th>Featured</th>
                    <th>Action</th>
               </tr>
          </thead>

          <tbody>
               <? if($items): ?>
                    <? foreach($items as $key => $v): ?>
                         <tr id="row-<?= $v['id'] ?>">
                              <td><a href="javascript://" data-toggle="modal" data-target="#popup-image-<?= $v['id'] ?>"><img style="height: 25px" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>"></a></td>
                              <td><?= $v['title'] ?></td>
                              <td><?= $v['promo_type'] ?></td>
                              <td><?= $v['description'] ?></td>
                              <td><?= date('F j, Y', strtotime($v['promo_start_date'])) . ' - ' . date('F j, Y', strtotime($v['promo_end_date'])) ?></td>
                              <td><?= date('F j, Y', strtotime($v['publish_start_date'])) . ' - ' . date('F j, Y', strtotime($v['publish_end_date'])) ?></td>
                              <td><?= $v['type_of_promo'] ?></td>
                              <td><?= $v['link'] ?></td>
                              <td style="text-align: center">
                                  <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle action-callback-<?= $v['id'] ?> <?= $v['is_featured'] ? 'btn-success' : '' ?>" data-toggle="dropdown">
                                      <span class="glyphicon glyphicon-star"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                      <li class="featured-action" data-action="1" data-id="<?= $v['id'] ?>"><a href="javascript://">Yes</a></li>
                                      <li class="featured-action" data-action="0" data-id="<?= $v['id'] ?>"><a href="javascript://">No</a></li>
                                    </ul>
                                  </div>
                                </td>
                              <td style="width: 1%">
                                   <a href="<?= site_url('promos/form') ?>/<?= $v['id'] ?>" style="margin-right: 10px" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
                                   <a href="javascript:void(0)" title="Delete" class="delete" data-id="<?php echo $v['id'];?>"><i class="glyphicon glyphicon-trash"></i></a>
                              </td>
                         </tr>

                         <!-- IMAGE MODAL -->
                         <!-- MODALS -->
                              <div class="modal fade" id="popup-image-<?php echo $v['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    </div>
                                    <div class="modal-body">
                                      <!-- MODAL CONTENT -->

                                        <img style="width: 100%" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>">

                                      <!-- END MODAL CONTENT -->
                                      </div>
                                  </div>
                                </div>
                              </div>
                         <!-- END MODALS -->
                         <!-- END IMAGE MODAL -->
                    <? endforeach; ?>
               <? else: ?>
                         <tr>
                              <td colspan="10"><center style="color: red">No data found.</center></td>
                         </tr>
               <? endif; ?>
          </tbody>
     </table>

     <?=$pagination?>

</div>

<script src="<?=base_url()?>assets/admin/js/texteditor/nicEdit.js?r=<?=rand()?>"></script>
<script type="text/javascript">
bkLib.onDomLoaded(function(){
  new nicEditor({iconsPath : '<?=base_url()?>assets/admin/js/texteditor/nicEditorIcons.gif'}).panelInstance('');
});
</script>

<script type="text/javascript">
     $(function(){
          $('.advanced').click(function(){
               $('.advance-search').slideToggle();
          });

          var lytebox = new Lytebox;
          $('.delete').on('click', function(){
               var id = $(this).data('id');
               lytebox.dialog({
                    message: 'Are you sure you want to delete this?', 
                    title : 'Confirm<hr style="margin: 5px 0; border-color: #ccc">',
                    type : 'confirm',
                    onConfirm : function(){
                         $.post('<?php echo site_url("promos/delete")?>', {id:id}, function(){
                              $('#row-'+id).hide();
                         });
                    },
                    top: 100
               });
          });
     });

     $('input[name=publish_start_date], input[name=promo_start_date]').datepicker({
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat:'yy-mm-dd'
     });

     function uploadPhoto(input) {
        var name = input.files[0].name;
        var size = convertSize(input.files[0].size);
        $('.photo-filename').html(name + '<small> - '+ size +' </small>');
     }

     function convertSize(bytes) {

           if(bytes == 0) return '0 Byte';
           var k = 1000;
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
           var i = Math.floor(Math.log(bytes) / Math.log(k));
           var raw = (bytes / Math.pow(k, i)).toPrecision(3);
           var result;

           return raw >= 2097152 ? 'file limit exceeded' : raw + ' ' + sizes[i];
           
    }

    $('.featured-action').on('click', function(){
        var id = $(this).data('id');
        var action = $(this).data('action');

        data = {id:id, is_featured:action};
        $.post("<?= site_url('promos/ajax') ?>", data, function(){
            $('.action-callback-'+id).removeClass('btn-success');
            if(action == '1') {
                $('.action-callback-'+id).addClass('btn-success');
            }
        });
    });
</script>