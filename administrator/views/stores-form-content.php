<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjxtjNyjATUWGLx-1v0zehvr0bkc0ACnE&sensor=false">></script>
<div class="container main-content">
     <div class="page-header">
          <? if(isset($store[0]['id'])): ?>
               <h3>Update Store</h3>
          <? else: ?>
               <h3>Create Store</h3>
          <? endif; ?>
     </div>

     <form id="promos-form" class="form-horizontal" role="form" action="<?= site_url('stores/form') ?>" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="id" value="<?= isset($store[0]['id']) ? $store[0]['id'] : '' ?>">
          <div class="form-group">
                                                <label class="control-label">Name</label>
                                                <div>
                                                     <input type="text" class="form-control" name="name" value="<?= isset($store[0]['name']) ? $store[0]['name'] : '' ?>">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Area</label>
                                                <div>
                                                     <select class="form-control update-area" name="area" data-id="<?= $store[0]['id'] ?>">
                                                          <option <?= isset($store[0]['area']) && $store[0]['area'] == 'Metro Manila' ? 'selected' : '' ?>>Metro Manila</option>
                                                          <option <?= isset($store[0]['area']) && $store[0]['area'] == 'North Luzon' ? 'selected' : '' ?>>North Luzon</option>
                                                          <option <?= isset($store[0]['area']) && $store[0]['area'] == 'South Luzon' ? 'selected' : '' ?>>South Luzon</option>
                                                          <option <?= isset($store[0]['area']) && $store[0]['area'] == 'Visayas' ? 'selected' : '' ?>>Visayas</option>
                                                          <option <?= isset($store[0]['area']) && $store[0]['area'] == 'Mindanao' ? 'selected' : '' ?>>Mindanao</option>
                                                     </select>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                              <label class="control-label">Municipal / City <img id="spinner" style="display: none" src="<?=base_url()?>assets/admin/images/spinner.gif"></label>
                                              <div>
                                                <select class="form-control" name="city" id="new-item-city">
                                                  <? if(isset($store[0]['area']) && $store[0]['area'] == 'Metro Manila'): ?>
                                                    <? foreach($metro_manila as $k => $val): ?>
                                                      <option <?= $store[0]['city'] == $val['name'] ? 'selected' : '' ?>><?= $val['name'] ?></option>
                                                    <? endforeach; ?>
                                                  <? elseif(isset($store[0]['area']) && $store[0]['area'] == 'North Luzon'): ?>
                                                    <? foreach($north_luzon as $k => $val): ?>
                                                      <option <?= $store[0]['city'] == $val['name'] ? 'selected' : '' ?>><?= $val['name'] ?></option>
                                                    <? endforeach;?>
                                                  <? elseif(isset($store[0]['area']) && $store[0]['area'] == 'South Luzon'): ?>
                                                    <? foreach($south_luzon as $k => $val): ?>
                                                      <option <?= $store[0]['city'] == $val['name'] ? 'selected' : '' ?>><?= $val['name'] ?></option>
                                                    <? endforeach;?>
                                                  <? elseif(isset($store[0]['area']) && $store[0]['area'] == 'Visayas'): ?>
                                                    <? foreach($visayas as $k => $val): ?>
                                                      <option <?= $store[0]['city'] == $val['name'] ? 'selected' : '' ?>><?= $val['name'] ?></option>
                                                    <? endforeach;?>
                                                  <? elseif(isset($store[0]['area']) && $store[0]['area'] == 'Mindanao'): ?>
                                                    <? foreach($mindanao as $k => $val): ?>
                                                      <option <?= $store[0]['city'] == $val['name'] ? 'selected' : '' ?>><?= $val['name'] ?></option>
                                                    <? endforeach;?>
                                                  <? endif; ?>
                                                </select>   
                                              </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Address</label>
                                                <div>
                                                     <input type="text" class="form-control" name="address" value="<?= isset($store[0]['address']) ? $store[0]['address'] : '' ?>">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Branch Code</label>
                                                <div>
                                                     <input type="text" class="form-control" name="branch_code" value="<?= isset($store[0]['branch_code']) ? $store[0]['branch_code'] : '' ?>">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Branch Hours</label>
                                                <div>
                                                     <input type="text" class="form-control" name="branch_hour" value="<?= isset($store[0]['branch_hour']) ? $store[0]['branch_hour'] : '' ?>">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Contact Details</label>
                                                <div>
                                                     <input type="text" class="form-control" name="contact" value="<?= isset($store[0]['contact']) ? $store[0]['contact'] : '' ?>">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Zoom Level (Recommended: 17)</label>
                                                <div>
                                                     <select class="form-control" name="zoom_level">
                                                        <? for($i = 1; $i <= 20; $i++): ?>
                                                          <option <?= isset($store[0]['zoom_level']) && $store[0]['zoom_level'] == $i ? 'selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                                                        <? endfor; ?>
                                                     </select>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Google Location</label>
                                                <div>
                                                     <div id="map_canvas" style="width: 500px; height: 500px"></div>
                                                     <input type="text" name="longitude" value="<?= isset($store[0]['longitude']) ? $store[0]['longitude'] : '' ?>" placeholder="Longitude" id="longitude">
                                                     <input type="text" name="latitude" value="<?= isset($store[0]['latitude']) ? $store[0]['latitude'] : '' ?>" placeholder="Latitude" id="latitude">
                                                </div>
                                           </div>
          <div class="form-group">
               <div class="col-sm-offset-2 col-sm-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="javascript:void(0)" onclick="console.log(tinymce.get('textarea').getContent())">asdasd</a>
               </div>
          </div>
     </form>


</div>

<script type="text/javascript">
  $(function(){
        
        <? if( isset($store[0]['longitude']) || isset($store[0]['latitude']) ) : ?>
            var myLatlng = new google.maps.LatLng(<?=$store[0]['latitude']?>,<?=$store[0]['longitude']?>);
        <? else : ?>
            var myLatlng = new google.maps.LatLng(14.59885354948353, 120.98423133572385);
        <? endif; ?>

        var mapOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

        var infowindow = new google.maps.InfoWindow();

        var bounds = new google.maps.LatLngBounds();

        var marker = new google.maps.Marker({
              draggable: true,
              position: myLatlng, 
              map: map,
              // icon: '<?=base_url()?>images/admin/marker.png',
              title: "Store Location"
          });

        google.maps.event.addListener(marker, 'drag', function(event){
           document.getElementById("latitude").value = event.latLng.lat();
           document.getElementById("longitude").value = event.latLng.lng();
    });

        var listener = google.maps.event.addListener(map, "idle", function () {
            map.setZoom(15);
            google.maps.event.removeListener(listener);
        });

          // google.maps.event.addDomListener(window, 'load', initialize);
    });
</script>

<script type="text/javascript">
    $('select[name=area]').on('change', function(){
        var area = $(this).val();
        console.log(area);
        $('#spinner').show();
        $.post('<?= site_url("stores/load_cities") ?>', {area:area}, function(response){
          $('#spinner').hide();
          $('#new-item-city').html(response);
        });
     });
</script>