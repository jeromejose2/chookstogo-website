<div class="container main-content">
     <div class="page-header">
          <h3>Stores - Branch <span class="badge"><?=$total?></span></h3>

          <div class="actions">
               <a class="btn btn-primary advanced"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Advance Search</span></a>
          </div>

          <div class="actions" style="margin-right: 1%">
               <a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#popup-new">New Item</a>
          </div>

          <div class=" advance-search">
          <div class="form-content">
               <form class="form-search">
                    <div class="input-prepend">
                         <span class="add-on">ID</span>
                         <input type="text" class="input-medium" name="id" value="<?=isset($_GET['id']) ? $_GET['id'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">ITEMS/PAGE</span>
                         <select name="psize" class="input-small">
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>
                         </select>
                    </div> 

                    <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>
               </form>
          </div>
     </div>
     </div>


     <table class="table table-bordered">
          <thead>
               <tr>
                    <th>Name</th>
                    <th>City</th>
                    <th>Longitude</th>
                    <th>Latitude</th>
                    <th>Zoom Level</th>
                    <th>Timestamp</th>
                    <th>Action</th>
               </tr>
          </thead>

          <tbody>
               <? if($items): ?>
                    <? $a=0; foreach($items as $key => $v): $a++; ?>
                         <tr id="row-<?= $v['id'] ?>">
                              <td><?= $v['name'] ?></td>
                              <td><?= $v['city_name'] ?></td>
                              <td><?= $v['longitude'] ?></td>
                              <td><?= $v['latitude'] ?></td>
                              <td><?= $v['zoom_level'] ?></td>
                              <td><?= $v['timestamp'] ?></td>
                              <td style="width: 1%">
                                   <a href="javascript:void(0)" style="margin-right: 10px" title="Edit" data-toggle="modal" data-target="#popup-<?php echo $v['id'];?>"><i class="glyphicon glyphicon-edit"></i></a>
                                   <a href="javascript:void(0)" title="Delete" class="delete" data-id="<?php echo $v['id'];?>"><i class="glyphicon glyphicon-trash"></i></a>
                              </td>
                         </tr>

                         <!-- MODALS -->
                              <div class="modal fade" id="popup-<?php echo $v['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                      <h4 class="modal-title" id="myModalLabel">Update City</h4>
                                    </div>
                                    <div class="modal-body">
                                      <!-- MODAL CONTENT -->

                                        <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('stores/update_branch')?>">
                                             <input type="hidden" name="id" value="<?php echo $v['id'];?>">
                                             <div class="form-group">
                                                  <label class="control-label">Name</label>
                                                  <div>
                                                       <input type="text" class="form-control" name="name" value="<?php echo $v['name'];?>">
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label">City</label>
                                                  <div>
                                                       <select name="city_id" class="form-control">
                                                            <? foreach($cities as $key => $value): ?>
                                                                <option value="<?= $value['id'] ?>" <?= $v['city_name'] == $value['name'] ? 'selected' : '' ?>><?= $value['name'] ?></option>
                                                            <? endforeach; ?>
                                                       </select>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label">Longitude</label>
                                                  <div>
                                                       <input type="text" class="form-control" name="longitude" value="<?php echo $v['longitude'];?>">
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label">Latitude</label>
                                                  <div>
                                                       <input type="text" class="form-control" name="latitude" value="<?php echo $v['latitude'];?>">
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label">Zoom Level</label>
                                                  <div>
                                                       <select name="zoom_level" class="form-control">
                                                            <? for($i=1; $i < 20; $i++): ?>
                                                                 <option value="<?= $i ?>" <?= $v['zoom_level'] == $i ? 'selected' : '' ?>><?= $i ?></option>
                                                            <? endfor; ?>
                                                       </select>
                                                  </div>
                                             </div>

                                      <!-- END MODAL CONTENT -->
                                      </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Save changes</button>
                                        </form>

                                    </div>
                                  </div>
                                </div>
                              </div>
                         <!-- END MODALS -->

                    <? endforeach; ?>
               <? else: ?>
                         <tr>
                              <td colspan="10"><center style="color: red">No data found.</center></td>
                         </tr>
               <? endif; ?>

               <!-- NEW ITEM -->
                    <!-- MODALS -->
                    <div class="modal fade" id="popup-new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Update Area</h4>
                          </div>
                          <div class="modal-body">
                            <!-- MODAL CONTENT -->

                                        <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('stores/save_branch')?>">
                                             <div class="form-group">
                                                  <label class="control-label">Name</label>
                                                  <div>
                                                       <input type="text" class="form-control" name="name" value="">
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label">City</label>
                                                  <div>
                                                       <select name="city_id" class="form-control">
                                                            <? foreach($cities as $key => $value): ?>
                                                                <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                                                            <? endforeach; ?>
                                                       </select>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label">Longitude</label>
                                                  <div>
                                                       <input type="text" class="form-control" name="longitude" value="">
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label">Latitude</label>
                                                  <div>
                                                       <input type="text" class="form-control" name="latitude" value="">
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label">Zoom Level</label>
                                                  <div>
                                                       <select name="zoom_level" class="form-control">
                                                            <? for($i=1; $i < 20; $i++): ?>
                                                                 <option value="<?= $i ?>"><?= $i ?></option>
                                                            <? endfor; ?>
                                                       </select>
                                                  </div>
                                             </div>

                                      <!-- END MODAL CONTENT -->
                            </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                              </form>

                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END MODALS -->
                    <!-- END NEW ITEM -->

          </tbody>
     </table>

     <?=$pagination?>


</div>



<script type="text/javascript">
     $(function(){
          $('.advanced').click(function(){
               $('.advance-search').slideToggle();
          });

          var lytebox = new Lytebox;
          $('.delete').on('click', function(){
               var id = $(this).data('id');
               lytebox.dialog({
                    message: 'Are you sure you want to delete this?', 
                    title : 'Confirm<hr style="margin: 5px 0; border-color: #ccc">',
                    type : 'confirm',
                    onConfirm : function(){
                         $.post('<?php echo site_url("stores/delete_branch")?>', {id:id}, function(){
                              $('#row-'+id).hide();
                         });
                    },
                    top: 100
               });
          });
     });
</script>