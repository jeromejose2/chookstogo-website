<div class="container main-content">
     <div class="page-header">
          <h3>List <span class="badge"><?=$total?></span></h3>

          <div class="actions">
               <a class="btn btn-primary advanced"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Advance Search</span></a>
          </div>

          <div class="actions" style="margin-right: 1%">
               <a href="#" class="btn btn-primary">New Item</a>
          </div>

          <div class=" advance-search">
          <div class="form-content">
               <form class="form-search">
                    <div class="input-prepend">
                         <span class="add-on">ID</span>
                         <input type="text" class="input-medium" name="id" value="<?=isset($_GET['id']) ? $_GET['id'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">ITEMS/PAGE</span>
                         <select name="psize" class="input-small">
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>
                         </select>
                    </div> 

                    <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>
               </form>
          </div>
     </div>
     </div>


     <table class="table table-bordered">
          <thead>
               <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Caption</th>
                    <th>Source</th>
                    <th>Timestamp</th>
               </tr>
          </thead>

          <tbody>
               <? if($items): ?>
                    <? foreach($items as $key => $v): ?>
                         <tr>
                              <td><?= $v['id'] ?></td>
                              <td><?= $v['author'] ?></td>
                              <td><?= $v['caption'] ?></td>
                              <td><?= $v['source'] ?></td>
                              <td><?= date('F j, Y', strtotime($v['timestamp'])) ?></td>
                         </tr>
                    <? endforeach; ?>
               <? else: ?>
                         <tr>
                              <td colspan="10"><center style="color: red">No data found.</center></td>
                         </tr>
               <? endif; ?>
          </tbody>
     </table>

     <?=$pagination?>

</div>

<script type="text/javascript">
     $(function(){
          $('.advanced').click(function(){
               $('.advance-search').slideToggle();
          });
     });
</script>