<!DOCTYPE html>
<html>
<head>
     <title>CMS</title>
     <!-- Bootstrap -->
     <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/bootstrap.min.css" >
     <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/style.css" >
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
     <![endif]-->
</head>
<body>

     <!--start main content -->
     <div class="container login-content">
          <div class="page-header">
               <h3>Login</h3>
          </div>

          <?php if( !empty($error_message) ): ?>
               <div class="alert alert-danger"><?php echo $error_message;?></div>
          <?php endif; ?>

          <form class="form-horizontal" role="form" method="POST">
               <div class="form-group">
                    <label class="col-sm-3 control-label">Username</label>
                    <div class="col-sm-9">
                         <input type="text" class="form-control" name="username">
                    </div>
               </div>
               <div class="form-group">
                    <label class="col-sm-3 control-label">Password</label>
                    <div class="col-sm-9">
                         <input type="password" class="form-control" name="password">
                    </div>
               </div>
               
               <!-- <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                         <div class="checkbox">
                              <label>
                                   <input type="checkbox"> Remember me
                              </label>
                         </div>
                    </div>
               </div> -->

               <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                         <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-log-in"></span> Login</button>
                    </div>
               </div>
          </form>


     </div>
     <!--end main content -->



     <script src="<?php echo base_url()?>assets/admin/js/jquery.js"></script>
     <script src="<?php echo base_url()?>assets/admin/js/bootstrap.min.js"></script>
</body>
</html>