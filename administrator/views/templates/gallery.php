<!DOCTYPE html>
<html>
<head>
     <title>CMS</title>
     <!-- Bootstrap -->
     <link rel="stylesheet" href="css/bootstrap.min.css" >
     <link rel="stylesheet" href="css/style.css" >
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
     <![endif]-->
</head>
<body>

     <!--start header-->
     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">NuWorks</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="form.html">Form</a></li>
                    <li><a href="list.html">List</a></li>
                    <li class="active"><a href="gallery.html">Gallery</a></li>
                    <li><a href="article.html">Article</a></li>
                    <li>
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dopdown <b class="caret"></b></a>
                         <ul class="dropdown-menu">
                              <li><a href="#">Item</a></li>
                              <li><a href="#">Item</a></li>
                              <li><a href="#">Item</a></li>
                              <li class="divider"></li>
                              <li><a href="#">Item</a></li>
                         </ul>
                    </li>

               </ul>

               <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings <b class="caret"></b></a>
                         <ul class="dropdown-menu">
                              <li><a href="#">My Account</a></li>
                              <li><a href="#">Logout</a></li>
                         </ul>
                    </li>
               </ul>
          </div><!-- /.navbar-collapse -->
     </nav>
     <!--end header-->


     <!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h4>Gallery</h4>
          </div>


          <div class="row">
               <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                         <img data-src="holder.js/230x230" alt="...">
                         <div class="caption">
                              <h4>Thumbnail label</h4>
                              <p>Lorem ipsum dolor sit emet the quick brown fox jumps over the lazy dog</p>
                              <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                         </div>
                    </div>
               </div>
               <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                         <img data-src="holder.js/230x230" alt="...">
                         <div class="caption">
                              <h4>Thumbnail label</h4>
                              <p>Lorem ipsum dolor sit emet the quick brown fox jumps over the lazy dog</p>
                              <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                         </div>
                    </div>
               </div>
               <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                         <img data-src="holder.js/230x230" alt="...">
                         <div class="caption">
                              <h4>Thumbnail label</h4>
                              <p>Lorem ipsum dolor sit emet the quick brown fox jumps over the lazy dog</p>
                              <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                         </div>
                    </div>
               </div>
               <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                         <img data-src="holder.js/230x230" alt="...">
                         <div class="caption">
                              <h4>Thumbnail label</h4>
                              <p>Lorem ipsum dolor sit emet the quick brown fox jumps over the lazy dog</p>
                              <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                         </div>
                    </div>
               </div>
               <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                         <img data-src="holder.js/230x230" alt="...">
                         <div class="caption">
                              <h4>Thumbnail label</h4>
                              <p>Lorem ipsum dolor sit emet the quick brown fox jumps over the lazy dog</p>
                              <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                         </div>
                    </div>
               </div>
               <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                         <img data-src="holder.js/230x230" alt="...">
                         <div class="caption">
                              <h4>Thumbnail label</h4>
                              <p>Lorem ipsum dolor sit emet the quick brown fox jumps over the lazy dog</p>
                              <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                         </div>
                    </div>
               </div>
               <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                         <img data-src="holder.js/230x230" alt="...">
                         <div class="caption">
                              <h4>Thumbnail label</h4>
                              <p>Lorem ipsum dolor sit emet the quick brown fox jumps over the lazy dog</p>
                              <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                         </div>
                    </div>
               </div>
               <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                         <img data-src="holder.js/230x230" alt="...">
                         <div class="caption">
                              <h4>Thumbnail label</h4>
                              <p>Lorem ipsum dolor sit emet the quick brown fox jumps over the lazy dog</p>
                              <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                         </div>
                    </div>
               </div>
               <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                         <img data-src="holder.js/230x230" alt="...">
                         <div class="caption">
                              <h4>Thumbnail label</h4>
                              <p>Lorem ipsum dolor sit emet the quick brown fox jumps over the lazy dog</p>
                              <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                         </div>
                    </div>
               </div>
               <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                         <img data-src="holder.js/230x230" alt="...">
                         <div class="caption">
                              <h4>Thumbnail label</h4>
                              <p>Lorem ipsum dolor sit emet the quick brown fox jumps over the lazy dog</p>
                              <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                         </div>
                    </div>
               </div>
          </div>


          <ul class="pagination pagination-sm pull-right">
               <li><a href="#">Prev</a></li>
               <li><a href="#">1</a></li>
               <li><a href="#">2</a></li>
               <li><a href="#">3</a></li>
               <li><a href="#">4</a></li>
               <li><a href="#">5</a></li>
               <li><a href="#">Next</a></li>
          </ul>

     </div>
     <!--end main content -->

     <!--start footer -->
     <footer class="main">
          <div class="container">
               <small>Nuworks Interactive Labs &copy; 2013</small>
          </div>
     </footer>
     <!--end footer -->



     <script src="js/jquery.js"></script>
     <script src="js/holder.js"></script>
     <script src="js/bootstrap.min.js"></script>
</body>
</html>