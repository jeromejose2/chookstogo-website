<div class="container main-content">
     <div class="page-header">
          <h3>Registrants <span class="badge"><?=$total?></span></h3>

          <div class="actions">
               <a class="btn btn-primary advanced"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Advance Search</span></a>
          </div>

          <?php /* <div class="actions" style="margin-right: 1%">
               <a href="#" class="btn btn-primary">New Item</a>
          </div> */ ?>

          <div class="actions" style="margin-right: 1%">
              <a href="<?=site_url('export/registrants?'.http_build_query($_GET, '', "&"))?>" class="btn btn-primary hidden-phone"><i class="icon-download-alt"></i> Export</a>
          </div>

          <div class=" advance-search">
          <div class="form-content">
               <form class="form-search">
                    <div class="input-prepend">
                         <span class="add-on">ID</span>
                         <input type="text" class="input-medium" name="id" value="<?=isset($_GET['id']) ? $_GET['id'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">NAME</span>
                         <input type="text" class="input-medium" name="name" value="<?=isset($_GET['name']) ? $_GET['name'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">BDAY</span>
                         <input type="text" class="input-medium" name="bday" value="<?=isset($_GET['bday']) ? $_GET['bday'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">MOBILE</span>
                         <input type="text" class="input-medium" name="mobile" value="<?=isset($_GET['mobile']) ? $_GET['mobile'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">EMAIL</span>
                         <input type="text" class="input-medium" name="email" value="<?=isset($_GET['email']) ? $_GET['email'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">FROM</span>
                         <input type="text" class="input-medium" name="from" value="<?=isset($_GET['from']) ? $_GET['from'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">TO</span>
                         <input type="text" class="input-medium" name="to" value="<?=isset($_GET['to']) ? $_GET['to'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">GENDER</span>
                         <select name="gender">
                              <option value=""></option>
                              <option <?= isset($_GET['gender']) && $_GET['gender'] == 1 ? 'selected' : '' ?> value="1">Male</option>
                              <option <?= isset($_GET['gender']) && $_GET['gender'] == 2 ? 'selected' : '' ?> value="2">Female</option>
                         </select>
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">ITEMS/PAGE</span>
                         <select name="psize" class="input-small">
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>
                         </select>
                    </div>

                    <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>
               </form>
          </div>
     </div>
     </div>


     <table class="table table-bordered">
          <thead>
               <tr>
                    <th>Name</th>
                    <th>Birthday</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Gender</th>
                    <th>Confirmed User</th>
                    <th>Timestamp</th>
               </tr>
          </thead>

          <tbody>
               <? if($items): ?>
                    <? foreach($items as $key => $v): ?>
                         <tr id="row-<?= $v['id'] ?>">
                              <td><?= $v['name'] ?></td>
                              <td><?= date('F j, Y', strtotime($v['bday'])) ?></td>
                              <td><?= $v['mobile'] ?></td>
                              <td><?= $v['email'] ?></td>
                              <td><?= $v['gender'] == 1 ? 'Male' : 'Female' ?></td>
                              <td><?= $v['confirmed_user'] ? 'Yes' : 'No' ?></td>
                              <td><?= $v['timestamp'] ?></td>
                         </tr>
                    <? endforeach; ?>
               <? else: ?>
                         <tr>
                              <td colspan="10"><center style="color: red">No data found.</center></td>
                         </tr>
               <? endif; ?>
          </tbody>
     </table>

     <?=$pagination?>

</div>

<script type="text/javascript">
     $(function(){
          $('.advanced').click(function(){
               $('.advance-search').slideToggle();
          });
     });

     $('input[name=bday]').datepicker({
          dateFormat:'yy-mm-dd'
     });

     $('input[name=from]').datepicker({
          dateFormat:'yy-mm-dd'
     });

     $('input[name=to]').datepicker({
          dateFormat:'yy-mm-dd'
     });
</script>