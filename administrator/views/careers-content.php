<div class="container main-content">
     <div class="page-header">
          <h3>Careers <span class="badge"><?=$total?></span></h3>

          <div class="actions">
               <a class="btn btn-primary advanced"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Advance Search</span></a>
          </div>

          <div class="actions" style="margin-right: 1%">
               <a href="<?= site_url('careers/form') ?>" class="btn btn-primary">New Item</a>
          </div>

          <div class="actions" style="margin-right: 1%">
              <a href="<?=site_url('export/careers?'.http_build_query($_GET, '', "&"))?>" class="btn btn-primary hidden-phone"><i class="icon-download-alt"></i> Export</a>
          </div>

          <div class=" advance-search">
          <div class="form-content">
               <form class="form-search">
                    <div class="input-prepend">
                         <span class="add-on">ID</span>
                         <input type="text" class="input-medium" name="id" value="<?=isset($_GET['id']) ? $_GET['id'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">POSITION</span>
                         <input type="text" class="input-medium" name="position" value="<?=isset($_GET['position']) ? $_GET['position'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">LOCATION</span>
                         <input type="text" class="input-medium" name="location" value="<?=isset($_GET['location']) ? $_GET['location'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">FIELD OF WORK</span>
                         <input type="text" class="input-medium" name="field_of_work" value="<?=isset($_GET['field_of_work']) ? $_GET['field_of_work'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">DESCRIPTION</span>
                         <input type="text" class="input-medium" name="description" value="<?=isset($_GET['description']) ? $_GET['description'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">QUALIFICATION</span>
                         <input type="text" class="input-medium" name="qualifications" value="<?=isset($_GET['qualifications']) ? $_GET['qualifications'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">ITEMS/PAGE</span>
                         <select name="psize" class="input-small">
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>
                         </select>
                    </div> 

                    <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>
               </form>
          </div>
     </div>
     </div>


     <table class="table table-bordered">
          <thead>
               <tr>
                    <th>Position</th>
                    <th>Location</th>
                    <th>Field of Work</th>
                    <th>Description</th>
                    <th>Qualifications</th>
                    <th>Action</th>
               </tr>
          </thead>

          <tbody>
               <? if($items): ?>
                    <? foreach($items as $key => $v): ?>
                         <tr id="row-<?= $v['id'] ?>">
                              <td><?= $v['position'] ?></td>
                              <td><?= $v['location'] ?></td>
                              <?php /* <td><?= $v['field_of_work'] ?></td> */ ?>
                              <td>
                                   <?php switch ($v['field_of_work']) {
                                        case 'Compliance Assurance':
                                             echo 'Food Quality and Safety Audit';
                                             break;

                                        case 'Management Control System':
                                             echo 'Corporate Governance / Planning';
                                             break;
                                        
                                        default:
                                             echo $v['field_of_work'];
                                             break;
                                   } ?>
                              </td>
                              <td><?= $v['description'] ?></td>
                              <td><?= $v['qualifications'] ?></td>
                              <td style="width: 1%">
                                   <a href="<?= site_url('careers/form') ?>/<?= $v['id'] ?>" style="margin-right: 10px" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
                                   <a href="javascript:void(0)" title="Delete" class="delete" data-id="<?php echo $v['id'];?>"><i class="glyphicon glyphicon-trash"></i></a>
                              </td>
                         </tr>
                    <? endforeach; ?>
               <? else: ?>
                         <tr>
                              <td colspan="10"><center style="color: red">No data found.</center></td>
                         </tr>
               <? endif; ?>
          </tbody>
     </table>

     <?=$pagination?>

</div>

<script type="text/javascript">
     $(function(){
          $('.advanced').click(function(){
               $('.advance-search').slideToggle();
          });
     });

     var lytebox = new Lytebox;
          $('.delete').on('click', function(){
               var id = $(this).data('id');
               lytebox.dialog({
                    message: 'Are you sure you want to delete this?', 
                    title : 'Confirm<hr style="margin: 5px 0; border-color: #ccc">',
                    type : 'confirm',
                    onConfirm : function(){
                         $.post('<?php echo site_url("careers/delete")?>', {id:id}, function(){
                              $('#row-'+id).hide();
                         });
                    },
                    top: 100
               });
          });
</script>