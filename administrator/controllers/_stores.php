<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stores extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {}

	public function area() {

		$this->table = 'tbl_areas';

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(4, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 4;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';
		$sort = 'timestamp DESC';
		if(isset($_GET['sort']) && (@$_GET['sort'] == 'ASC') || @$_GET['sort'] == 'DESC') {
			$sort = 'timestamp '.$_GET['sort'];
		}

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('stores/area/index/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? "AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>$sort
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('stores/area/index'), $curpage, $limit);

		$this->template['content'] = $this->load->view('stores-area-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function city() {

		$this->table = 'tbl_areas as a JOIN tbl_cities as c ON a.id = c.area_id';

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(4, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 4;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';
		$sort = 'c.timestamp DESC';
		if(isset($_GET['sort']) && (@$_GET['sort'] == 'ASC') || @$_GET['sort'] == 'DESC') {
			$sort = 'c.timestamp '.$_GET['sort'];
		}

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('stores/city/index/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? "AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'fields'=>'*, a.name as area_name, c.timestamp as timestamp',
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>$sort
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('stores/city/index'), $curpage, $limit);

		// additional data
		$this->params = array(
			'table'=>'tbl_areas'
		);
		$this->data['areas'] = $this->mysql_queries->get_data($this->params);
		// end additional data

		$this->template['content'] = $this->load->view('stores-city-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function branch() {

		$this->table = 'tbl_cities as c JOIN tbl_stores as s ON c.id = s.city_id';

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(4, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 4;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';
		$sort = 's.timestamp DESC';
		if(isset($_GET['sort']) && (@$_GET['sort'] == 'ASC') || @$_GET['sort'] == 'DESC') {
			$sort = 's.timestamp '.$_GET['sort'];
		}

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('branch/city/index/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? "AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'fields'=>'*, c.name as city_name, c.timestamp as timestamp',
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>$sort
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('branch/city/index'), $curpage, $limit);

		// additional data
		$this->params = array(
			'table'=>'tbl_cities'
		);
		$this->data['cities'] = $this->mysql_queries->get_data($this->params);
		// end additional data

		$this->template['content'] = $this->load->view('stores-branch-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function update_branch() {

		$this->params = array(
			'table'=>'tbl_stores',
			'where'=>'id = '.$_POST['id'],
			'post'=>$_POST
		);
		$this->mysql_queries->update_data($this->params);

		redirect('stores/branch');

	}

	public function save_branch() {

		$this->params = array(
			'table'=>'tbl_stores',
			'post'=>$_POST
		);
		$this->mysql_queries->insert_data($this->params);

		redirect('stores/branch');

	}

	public function delete_branch() {

		$this->params = array(
			'table'=>'tbl_stores',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

	public function update_city() {

		$this->params = array(
			'table'=>'tbl_cities',
			'where'=>'id = '.$_POST['id'],
			'post'=>$_POST
		);
		$this->mysql_queries->update_data($this->params);

		redirect('stores/city');

	}

	public function save_city() {

		$this->params = array(
			'table'=>'tbl_cities',
			'post'=>$_POST
		);
		$this->mysql_queries->insert_data($this->params);

		redirect('stores/city');

	}

	public function delete_city() {

		$this->params = array(
			'table'=>'tbl_cities',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

	public function update_areas() {

		$this->params = array(
			'table'=>'tbl_areas',
			'where'=>'id = '.$_POST['id'],
			'post'=>$_POST
		);
		$this->mysql_queries->update_data($this->params);

		redirect('stores/area');

	}

	public function save_areas() {

		$this->params = array(
			'table'=>'tbl_areas',
			'post'=>$_POST
		);
		$this->mysql_queries->insert_data($this->params);

		redirect('stores/area');

	}

	public function delete_areas() {

		$this->params = array(
			'table'=>'tbl_areas',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

}