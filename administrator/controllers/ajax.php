<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function delete() {

		$this->params = array(
			'table'=>$_POST['table'],
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

}