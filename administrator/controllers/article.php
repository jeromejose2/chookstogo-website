<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Article extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$this->data = NULL;

		$this->template['content'] = $this->load->view('article-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template);

	}
	
}