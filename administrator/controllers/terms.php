<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Terms extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		if($_POST) {
			$params = array(
				'table'=>'tbl_settings',
				'where'=>'type = \'terms\'',
				'post'=>$_POST
			);
			$this->mysql_queries->update_data($params);
		}

		$params = array(
			'table'=>'tbl_settings',
			'where'=>'type = \'terms\''
		);
		$this->data['content'] = $this->mysql_queries->get_data($params);

		$this->template['content'] = $this->load->view('terms-and-conditions-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

}