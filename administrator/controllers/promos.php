<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promos extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$this->table = 'tbl_promos';

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 3;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';
		$sort = 'timestamp DESC';
		if(isset($_GET['sort']) && (@$_GET['sort'] == 'ASC') || @$_GET['sort'] == 'DESC') {
			$sort = 'timestamp '.$_GET['sort'];
		}

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('promos/index/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['title']) ? " AND title LIKE '%" . $filter['title'] . "%'" : FALSE;
				$search_filters .= isset($filter['promo_type']) ? " AND promo_type LIKE '%" . $filter['promo_type'] . "%'" : FALSE;
				$search_filters .= isset($filter['description']) ? " AND description LIKE '%" . $filter['description'] . "%'" : FALSE;
				$search_filters .= isset($filter['start_promo_date']) ? " AND date(start_promo_date) = '" . $filter['start_promo_date'] . "'" : FALSE;
				$search_filters .= isset($filter['start_publish_date']) ? " AND date(start_publish_date) = '" . $filter['start_publish_date'] . "'" : FALSE;
				$search_filters .= isset($filter['type_of_promo']) ? " AND type_of_promo LIKE '%" . $filter['type_of_promo'] . "%'" : FALSE;
				$search_filters .= isset($filter['link']) ? " AND link LIKE '%" . $filter['link'] . "%'" : FALSE;
				$search_filters .= isset($filter['is_featured']) ? " AND is_featured LIKE '%" . $filter['is_featured'] . "%'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>'timestamp DESC'
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('promos/index'), $curpage, $limit);

		$this->template['content'] = $this->load->view('promos-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function form($id = NULL) {

		$this->data = array();
		
		if($_POST) {
			
			if($_POST['id']) {
				$config['upload_path'] = 'assets/admin/uploads/promos/';
				$config['allowed_types'] = 'jpg|jpeg|png';
				$this->load->library('upload', $config);
				$image = 'photo';

				if($this->upload->do_upload($image)) {
					$file = $this->upload->data();
					unset($_POST['photo']);
					$_POST['photo'] = '~path~/assets/admin/uploads/promos/'.$file['file_name'];
					$_POST['filename'] = $file['file_name'];
				}

				$_POST['is_featured'] = isset($_POST['is_featured']) ? 1 : 0;
				$_POST['is_announcement'] = isset($_POST['is_announcement']) ? 1 : 0;
				$_POST['open_link'] = isset($_POST['open_link']) ? 1 : 0;
				$this->params = array(
					'table'=>'tbl_promos',
					'where'=>'id = '.$_POST['id'],
					'post'=>$_POST
				);
				$this->mysql_queries->update_data($this->params);
			} else {
				$config['upload_path'] = 'assets/admin/uploads/promos/';
				$config['allowed_types'] = 'jpg|jpeg|png';
				$this->load->library('upload', $config);
				$image = 'photo';

				if($this->upload->do_upload($image)) {
					$file = $this->upload->data();
					unset($_POST['photo']);
					$_POST['photo'] = '~path~/assets/admin/uploads/promos/'.$file['file_name'];
					$_POST['filename'] = $file['file_name'];
				}

				$_POST['is_featured'] = isset($_POST['is_featured']) ? 1 : 0;
				$_POST['is_announcement'] = isset($_POST['is_announcement']) ? 1 : 0;
				$_POST['open_link'] = isset($_POST['open_link']) ? 1 : 0;
				$this->params = array(
					'table'=>'tbl_promos',
					'post'=>$_POST
				);
				$this->mysql_queries->insert_data($this->params);
			}

			redirect('promos');
		}

		if($id) {
			$this->params = array(
				'table'=>'tbl_promos',
				'where'=>'id = '.$id
			);
			$this->data['promo'] = $this->mysql_queries->get_data($this->params);
		}

		$this->template['content'] = $this->load->view('promos-form-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template);

	}

	public function ajax() {

		$this->params = array(
			'table'=>'tbl_promos',
			'where'=>'id = '.$_POST['id'],
			'post'=>$_POST
		);
		$this->mysql_queries->update_data($this->params);

	}

	public function delete() {

		$this->params = array(
			'table'=>'tbl_promos',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

}