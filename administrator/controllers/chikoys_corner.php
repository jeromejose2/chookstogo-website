<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chikoys_Corner extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$this->table = 'tbl_chikoys_corner';

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 3;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';
		$sort = 'timestamp DESC';
		if(isset($_GET['sort']) && (@$_GET['sort'] == 'ASC') || @$_GET['sort'] == 'DESC') {
			$sort = 'timestamp '.$_GET['sort'];
		}

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('chikoys_corner/index/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['title']) ? " AND title LIKE '%" . $filter['title'] . "%'" : FALSE;
				$search_filters .= isset($filter['type']) ? " AND type LIKE '%" . $filter['type'] . "%'" : FALSE;
				$search_filters .= isset($filter['is_featured']) ? " AND is_featured = '" . $filter['is_featured'] . "'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>$sort
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('chikoys_corner/index'), $curpage, $limit);

		$this->template['content'] = $this->load->view('chikoys-corner-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function update_comics() {

		$config['upload_path'] = 'assets/admin/uploads/chikoys_corner/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$this->load->library('upload', $config);
		$image = 'photo';

		if($this->upload->do_upload($image)) {
			$file = $this->upload->data();
			unset($_POST['photo']);
			$_POST['photo'] = '~path~/assets/admin/uploads/chikoys_corner/'.$file['file_name'];
			$_POST['filename'] = $file['file_name'];
		}

		$_POST['is_featured'] = isset($_POST['is_featured']) ? 1 : 0;
		$this->params = array(
			'table'=>'tbl_chikoys_corner',
			'where'=>'id = '.$_POST['id'],
			'post'=>$_POST
		);
		$this->mysql_queries->update_data($this->params);

		redirect('chikoys_corner');

	}

	public function create_comics() {

		$config['upload_path'] = 'assets/admin/uploads/chikoys_corner/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$this->load->library('upload', $config);
		$image = 'photo';

		if($this->upload->do_upload($image)) {
			$file = $this->upload->data();
			unset($_POST['photo']);
			$_POST['photo'] = '~path~/assets/admin/uploads/chikoys_corner/'.$file['file_name'];
			$_POST['filename'] = $file['file_name'];
		}

		$_POST['is_featured'] = isset($_POST['is_featured']) ? 1 : 0;
		$this->params = array(
			'table'=>'tbl_chikoys_corner',
			'post'=>$_POST
		);
		$this->mysql_queries->insert_data($this->params);

		redirect('chikoys_corner');

	}

	public function ajax() {

		$this->params = array(
			'table'=>'tbl_chikoys_corner',
		);
		$items = $this->mysql_queries->get_data($this->params);
		foreach($items as $k => $v) {
			$this->params = array(
				'table'=>'tbl_chikoys_corner',
				'where'=>'id = '.$v['id'],
				'post'=>array('is_featured'=>'0')
			);
			$this->mysql_queries->update_data($this->params);
		}

		$this->params = array(
			'table'=>'tbl_chikoys_corner',
			'where'=>'id = '.$_POST['id'],
			'post'=>$_POST
		);
		$this->mysql_queries->update_data($this->params);

	}

	public function delete_chikoy() {

		$this->params = array(
			'table'=>'tbl_chikoys_corner',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

}