<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$this->table = 'tbl_products';

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 3;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';
		$sort = 'timestamp DESC';
		if(isset($_GET['sort']) && (@$_GET['sort'] == 'ASC') || @$_GET['sort'] == 'DESC') {
			$sort = 'timestamp '.$_GET['sort'];
		}

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('products/index/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['name']) ? " AND title LIKE '%" . $filter['name'] . "%'" : FALSE;
				$search_filters .= isset($filter['category']) ? ' AND category = "' . $filter['category'] . '"' : FALSE;
				$search_filters .= isset($filter['description']) ? " AND description LIKE '%" . $filter['description'] . "%'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>$sort
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('products/index'), $curpage, $limit);

		$this->template['content'] = $this->load->view('products-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function save_product() {

		$config['upload_path'] = 'assets/admin/uploads/products/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$this->load->library('upload', $config);
		$image = 'photo';
		$image2 = 'photo_thumb';

		if($this->upload->do_upload($image)) {
			$file = $this->upload->data();
			unset($_POST['photo']);
			$_POST['photo'] = '~path~/assets/admin/uploads/products/'.$file['file_name'];
			$_POST['filename'] = $file['file_name'];
		}
		if($this->upload->do_upload($image2)) {
			$file = $this->upload->data();
			unset($_POST['photo_thumb']);
			$_POST['photo_thumb'] = '~path~/assets/admin/uploads/products/'.$file['file_name'];
			$_POST['filename_thumb'] = $file['file_name'];
		}
		$_POST['for_delivery'] = isset($_POST['for_delivery']) && $_POST['for_delivery'] == 'on' ? 1 : 0;
		
		$this->params = array(
			'table'=>'tbl_products',
			'post'=>$_POST
		);
		$this->mysql_queries->insert_data($this->params);
		redirect('products');

	}

	public function update_product() {

		$config['upload_path'] = 'assets/admin/uploads/products/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$this->load->library('upload', $config);
		$image = 'photo';
		$image2 = 'photo_thumb';

		if($this->upload->do_upload($image)) {

			$file = $this->upload->data();
			unset($_POST['photo']);
			$_POST['photo'] = '~path~/assets/admin/uploads/products/'.$file['file_name'];
			$_POST['filename'] = $file['file_name'];

		}
		if($this->upload->do_upload($image2)) {
			$file = $this->upload->data();
			unset($_POST['photo_thumb']);
			$_POST['photo_thumb'] = '~path~/assets/admin/uploads/products/'.$file['file_name'];
			$_POST['filename_thumb'] = $file['file_name'];
		}
		$_POST['for_delivery'] = isset($_POST['for_delivery']) && $_POST['for_delivery'] == 'on' ? 1 : 0;
		$this->params = array(
			'table'=>'tbl_products',
			'where'=>'id = '.$_POST['id'],
			'post'=>$_POST
		);
		$this->mysql_queries->update_data($this->params);

		redirect('products');

	}

	public function delete() {

		$this->params = array(
			'table'=>'tbl_products',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

}