<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slider extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$this->table = 'tbl_homepage_slider';

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 3;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';
		$sort = '`order` ASC';
		if(isset($_GET['sort']) && (@$_GET['sort'] == 'ASC') || @$_GET['sort'] == 'DESC') {
			$sort = '`order` '.$_GET['sort'];
		}

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('slider/index/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['link']) ? " AND link LIKE '%" . $filter['link'] . "%'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>$sort
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('slider/index'), $curpage, $limit);

		$this->template['content'] = $this->load->view('slider-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function db_query() {

		if($_POST['id']) {
			$config['upload_path'] = 'assets/admin/uploads/sliders/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$this->load->library('upload', $config);
			$image = 'photo';

			if($this->upload->do_upload($image)) {
				$file = $this->upload->data();
				unset($_POST['photo']);
				$_POST['photo'] = '~path~/assets/admin/uploads/sliders/'.$file['file_name'];
				$_POST['filename'] = $file['file_name'];
			}

			$_POST['open_link'] = isset($_POST['open_link']) ? 1 : 0;

			$this->params = array(
				'table'=>'tbl_homepage_slider',
				'where'=>'id = '.$_POST['id'],
				'post'=>$_POST
			);
			$this->mysql_queries->update_data($this->params);
		} else {
			$config['upload_path'] = 'assets/admin/uploads/sliders/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$this->load->library('upload', $config);
			$image = 'photo';

			if($this->upload->do_upload($image)) {
				$file = $this->upload->data();
				unset($_POST['photo']);
				$_POST['photo'] = '~path~/assets/admin/uploads/sliders/'.$file['file_name'];
				$_POST['filename'] = $file['file_name'];
			}

			$_POST['open_link'] = isset($_POST['open_link']) ? 1 : 0;

			$this->params = array(
				'table'=>'tbl_homepage_slider',
				'post'=>$_POST
			);
			$this->mysql_queries->insert_data($this->params);
		}
		redirect('slider');

	}

	public function delete() {

		$this->params = array(
			'table'=>'tbl_homepage_slider',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

	public function sort_items()
	{
		$items = $this->input->post('row');
		$total_items = count($this->input->post('row'));

		echo '<h3>Debugging:</h3>';
		echo "<p>Total items sent: ".$total_items."</p>";
			for($item = 0; $item < $total_items; $item++)
			{
				$data = array('id' => $items[$item],
							  'order' => $item + 1);
				$this->db->where('id', $data['id']);
				$this->db->update('tbl_homepage_slider', $data);				

				echo '<br>'.$this->db->last_query();
			}
		exit;
	}

}