<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Careers extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$this->table = 'tbl_careers';

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 3;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';
		$sort = 'timestamp DESC';
		if(isset($_GET['sort']) && (@$_GET['sort'] == 'ASC') || @$_GET['sort'] == 'DESC') {
			$sort = 'timestamp '.$_GET['sort'];
		}

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('careers/index/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['position']) ? " AND position LIKE '%" . $filter['position'] . "%'" : FALSE;
				$search_filters .= isset($filter['location']) ? " AND location LIKE '%" . $filter['location'] . "%'" : FALSE;
				$search_filters .= isset($filter['field_of_work']) ? " AND field_of_work LIKE '%" . $filter['field_of_work'] . "%'" : FALSE;
				$search_filters .= isset($filter['description']) ? " AND description LIKE '%" . $filter['description'] . "%'" : FALSE;
				$search_filters .= isset($filter['qualifications']) ? " AND qualifications LIKE '%" . $filter['qualifications'] . "%'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>$sort
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('careers/index'), $curpage, $limit);

		$this->template['content'] = $this->load->view('careers-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function form($id = NULL) {

		$this->data = array();

		if($_POST) {
			if($_POST['id']) {
				$this->params = array(
					'table'=>'tbl_careers',
					'where'=>'id = '.$_POST['id'],
					'post'=>$_POST
				);
				$this->mysql_queries->update_data($this->params);
			} else {
				$this->params = array(
					'table'=>'tbl_careers',
					'post'=>$_POST
				);
				$this->mysql_queries->insert_data($this->params);
			}

			redirect('careers');
		}

		if($id) {
			$this->params = array(
				'table'=>'tbl_careers',
				'where'=>'id = '.$id
			);
			$this->data['career'] = $this->mysql_queries->get_data($this->params);
		}

		$this->template['content'] = $this->load->view('careers-form-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template);

	}

	public function delete() {

		$this->params = array(
			'table'=>'tbl_careers',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

}