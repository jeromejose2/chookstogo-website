<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Announcements extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$this->table = 'tbl_announcements';

		$limit = isset($_GET['psize']) && is_numeric($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 3;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('announcements/index/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>'timestamp DESC'
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('announcements/index'), $curpage, $limit);

		$this->params = array(
			'table'=>'tbl_announcements'
		);
		$this->data['municipals'] = $this->mysql_queries->get_data($this->params);

		$this->template['content'] = $this->load->view('announcements-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function db_query() {

		if($_POST['id']) {
			$config['upload_path'] = 'assets/admin/uploads/announcements/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$this->load->library('upload', $config);
			$image = 'photo';

			if($this->upload->do_upload($image)) {
				$file = $this->upload->data();
				unset($_POST['photo']);
				$_POST['photo'] = '~path~/assets/admin/uploads/announcements/'.$file['file_name'];
				$_POST['filename'] = $file['file_name'];
			}
			$_POST['is_featured'] = isset($_POST['is_featured']) ? 1 : 0;
			$this->params = array(
				'table'=>'tbl_announcements',
				'where'=>'id = '.$_POST['id'],
				'post'=>$_POST
			);
			$this->mysql_queries->update_data($this->params);
		} else {
			$config['upload_path'] = 'assets/admin/uploads/announcements/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$this->load->library('upload', $config);
			$image = 'photo';

			if($this->upload->do_upload($image)) {
				$file = $this->upload->data();
				unset($_POST['photo']);
				$_POST['photo'] = '~path~/assets/admin/uploads/announcements/'.$file['file_name'];
				$_POST['filename'] = $file['file_name'];
			}
			$_POST['is_featured'] = isset($_POST['is_featured']) ? 1 : 0;
			$this->params = array(
				'table'=>'tbl_announcements',
				'post'=>$_POST
			);
			$this->mysql_queries->insert_data($this->params);
		}

		redirect('announcements');

	}

	public function ajax() {

		$this->params = array(
			'table'=>'tbl_announcements',
			'where'=>'id = '.$_POST['id'],
			'post'=>$_POST
		);
		$this->mysql_queries->update_data($this->params);

	}

	public function delete() {

		$this->params = array(
			'table'=>'tbl_announcements',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

}