<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About_Us extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$this->table = 'tbl_about';

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 3;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';
		$sort = 'timestamp DESC';
		if(isset($_GET['sort']) && (@$_GET['sort'] == 'ASC') || @$_GET['sort'] == 'DESC') {
			$sort = 'timestamp '.$_GET['sort'];
		}

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('about_us/index/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['title']) ? " AND title LIKE '%" . $filter['title'] . "%'" : FALSE;
				$search_filters .= isset($filter['description']) ? " AND description LIKE '%" . $filter['description'] . "%'" : FALSE;
				$search_filters .= isset($filter['date_of_publish']) ? " AND date(date_of_publish) = '" . $filter['date_of_publish'] . "'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>$sort
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('about_us/index'), $curpage, $limit);

		$this->template['content'] = $this->load->view('about-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function is_featured() {

		$this->params = array(
			'table'=>'tbl_about',
			'where'=>'id = '.$_POST['id'],
			'post'=>$_POST
		);
		$this->mysql_queries->update_data($this->params);

	}

	public function personalities() {


		$this->table = 'tbl_people';

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 3;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';
		$sort = 'timestamp DESC';
		if(isset($_GET['sort']) && (@$_GET['sort'] == 'ASC') || @$_GET['sort'] == 'DESC') {
			$sort = 'timestamp '.$_GET['sort'];
		}

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('about_us/personalities/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['name']) ? " AND name LIKE '%" . $filter['name'] . "%'" : FALSE;
				$search_filters .= isset($filter['designation']) ? " AND designation LIKE '%" . $filter['designation'] . "%'" : FALSE;
				$search_filters .= isset($filter['credentials']) ? " AND credentials LIKE '%" . $filter['credentials'] . "%'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>$sort
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('about_us/personalities'), $curpage, $limit);

		$this->template['content'] = $this->load->view('personalities-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);
	
	}

	public function form($id = NULL) {

		$this->data = array();

		if($id) {
			$params = array(
				'table'=>'tbl_people',
				'where'=>'id = '.$id
			);
			$this->data['personalities'] = $this->mysql_queries->get_data($params);
		}

		$this->template['content'] = $this->load->view('personalities-form-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function db_query_personalities() {

		if($_POST['id']) {
			$config['upload_path'] = 'assets/admin/uploads/personalities/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$this->load->library('upload', $config);
			$image = 'photo';

			if($this->upload->do_upload($image)) {
				$file = $this->upload->data();
				unset($_POST['photo']);
				$_POST['photo'] = '~path~/assets/admin/uploads/personalities/'.$file['file_name'];
				$_POST['filename'] = $file['file_name'];
			}
			$this->params = array(
				'table'=>'tbl_people',
				'where'=>'id = '.$_POST['id'],
				'post'=>$_POST
			);
			$this->mysql_queries->update_data($this->params);
		} else {
			$config['upload_path'] = 'assets/admin/uploads/personalities/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$this->load->library('upload', $config);
			$image = 'photo';

			if($this->upload->do_upload($image)) {
				$file = $this->upload->data();
				unset($_POST['photo']);
				$_POST['photo'] = '~path~/assets/admin/uploads/personalities/'.$file['file_name'];
				$_POST['filename'] = $file['file_name'];
			}
			$this->params = array(
				'table'=>'tbl_people',
				'post'=>$_POST
			);
			$this->mysql_queries->insert_data($this->params);
		}
		redirect('about_us/personalities');

	}

	public function db_query() {

		if($_POST['id']) {
			$config['upload_path'] = 'assets/admin/uploads/about_us/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$this->load->library('upload', $config);
			$image = 'photo';

			if($this->upload->do_upload($image)) {
				$file = $this->upload->data();
				unset($_POST['photo']);
				$_POST['photo'] = '~path~/assets/admin/uploads/about_us/'.$file['file_name'];
				$_POST['filename'] = $file['file_name'];
			}
			$this->params = array(
				'table'=>'tbl_about',
				'where'=>'id = '.$_POST['id'],
				'post'=>$_POST
			);
			$this->mysql_queries->update_data($this->params);
		} else {
			$config['upload_path'] = 'assets/admin/uploads/about_us/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$this->load->library('upload', $config);
			$image = 'photo';

			if($this->upload->do_upload($image)) {
				$file = $this->upload->data();
				unset($_POST['photo']);
				$_POST['photo'] = '~path~/assets/admin/uploads/about_us/'.$file['file_name'];
				$_POST['filename'] = $file['file_name'];
			}
			$this->params = array(
				'table'=>'tbl_about',
				'post'=>$_POST
			);
			$this->mysql_queries->insert_data($this->params);
		}
		redirect('about_us');

	}

	public function delete_about_us() {

		$this->params = array(
			'table'=>'tbl_about',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

	public function delete_personalities() {

		$this->params = array(
			'table'=>'tbl_people',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

}