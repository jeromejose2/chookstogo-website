<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Import extends CI_Controller {
	 
 	public function __construct() {

		parent::__construct(); 
		
	}
	
	public function csv() {

		$file = $_FILES['csv']['tmp_name'];
		$result = fopen($file, 'r');

		while( !feof($result) ) {
			$data = fgetcsv($result);
			$params = array(
				'table'=>'tbl_stores',
				'post'=>array(
					'name'=>$data[0],
					'area'=>$data[1],
					'city'=>$data[2],
					'address'=>$data[3],
					'branch_code'=>$data[4],
					'branch_hour'=>$data[5],
					'contact'=>$data[6],
					'longitude'=>$data[7],
					'latitude'=>$data[8],
				)
			);
			$this->mysql_queries->insert_data($params);
		}
		redirect('stores');

	}

}