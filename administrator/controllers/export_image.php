<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export_Image extends CI_Controller {
	 
 	public function __construct() {

		parent::__construct(); 

		$this->load->library('zip');
		
	}

	public function slider() {

		$path = 'assets/admin/uploads/sliders/';
		$this->zip->read_dir($path, FALSE);
		$this->zip->download('Slider_Images_'.date('m-d-y').'.zip');

	}

	public function products() {

		$path = 'assets/admin/uploads/products/';
		$this->zip->read_dir($path, FALSE);
		$this->zip->download('Product_Images_'.date('m-d-y').'.zip');

	}

	public function promos() {

		$path = 'assets/admin/uploads/promos/';
		$this->zip->read_dir($path, FALSE);
		$this->zip->download('Promo_Images_'.date('m-d-y').'.zip');

	}

	public function blogs() {

		$path = 'assets/admin/uploads/articles/';
		$this->zip->read_dir($path, FALSE);
		$this->zip->download('Blog_Images_'.date('m-d-y').'.zip');

	}

	public function comics() {

		$path = 'assets/admin/uploads/chikoys_corner/';
		$this->zip->read_dir($path, FALSE);
		$this->zip->download('Comics_Images_'.date('m-d-y').'.zip');

	}

	public function downloadables() {

		$path = 'assets/admin/uploads/downloadables/';
		$this->zip->read_dir($path, FALSE);
		$this->zip->download('Downloadable_Images_'.date('m-d-y').'.zip');

	}

	public function news() {

		$path = 'assets/admin/uploads/about_us/';
		$this->zip->read_dir($path, FALSE);
		$this->zip->download('News_And_Updates_Images_'.date('m-d-y').'.zip');

	}

	public function people() {

		$path = 'assets/admin/uploads/personalities/';
		$this->zip->read_dir($path, FALSE);
		$this->zip->download('Personalities_Images_'.date('m-d-y').'.zip');

	}

}