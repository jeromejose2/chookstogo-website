<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Downloadables extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$this->table = 'tbl_downloadables';

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 3;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';
		$sort = 'timestamp DESC';
		if(isset($_GET['sort']) && (@$_GET['sort'] == 'ASC') || @$_GET['sort'] == 'DESC') {
			$sort = 'timestamp '.$_GET['sort'];
		}

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('downloadables/index/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['name']) ? " AND name LIKE '%" . $filter['name'] . "%'" : FALSE;
				$search_filters .= isset($filter['type']) ? " AND type LIKE '%" . $filter['type'] . "%'" : FALSE;
				$search_filters .= isset($filter['timestamp']) ? " AND date(timestamp) = '" . $filter['timestamp'] . "'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>$sort
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('downloadables/index'), $curpage, $limit);

		$this->template['content'] = $this->load->view('downloadables-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function db_query() {

		if(!$_POST['id']) {
			$config['upload_path'] = 'assets/admin/uploads/downloadables/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$config['file_name'] = $_POST['filename'];
			$this->load->library('upload', $config);
			$item = 'photo';

			if($this->upload->do_upload($item)) {
				$file = $this->upload->data();
				unset($_POST['photo']);
				$_POST['path'] = '~path~/assets/admin/uploads/downloadables/'.$file['file_name'];
				$_POST['download_path'] = './assets/admin/uploads/downloadables/'.$file['file_name'];
				$_POST['photo'] = '~path~/assets/admin/uploads/downloadables/'.$file['file_name'];
				$_POST['item_filename'] = $file['file_name'];
				$_POST['extension'] = $file['file_ext'];
			}
			$this->params = array(
				'table'=>'tbl_downloadables',
				'post'=>$_POST
			);
			$this->mysql_queries->insert_data($this->params);
		} else {
			$config['upload_path'] = 'assets/admin/uploads/downloadables/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$config['file_name'] = $_POST['filename'];
			$this->load->library('upload', $config);
			$item = 'photo';

			if($this->upload->do_upload($item)) {
				$file = $this->upload->data();
				unset($_POST['photo']);
				$_POST['path'] = '~path~/assets/admin/uploads/downloadables/'.$file['file_name'];
				$_POST['download_path'] = './assets/admin/uploads/downloadables/'.$file['file_name'];
				$_POST['photo'] = '~path~/assets/admin/uploads/downloadables/'.$file['file_name'];
				$_POST['item_filename'] = $file['file_name'];
				$_POST['extension'] = $file['file_ext'];
			}
			$this->params = array(
				'table'=>'tbl_downloadables',
				'where'=>'id = '.$_POST['id'],
				'post'=>$_POST
			);
			$this->mysql_queries->update_data($this->params);
		}
		redirect('downloadables');

	}

	public function download($id) {

		$this->load->helper('download');

		$this->params = array(
			'table'=>'tbl_downloadables',
			'where'=>'id = '.$id
		);
		$item = $this->mysql_queries->get_data($this->params);

		$data = file_get_contents($item[0]['download_path']);
		$filename = $item[0]['item_filename'];

		force_download($filename, $data);

	}

	public function delete() {

		$this->params = array(
			'table'=>'tbl_downloadables',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

}