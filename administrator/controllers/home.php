<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {

		parent::__construct();

		if($this->session->userdata('user_email') == 'ept@bountyagro.com.ph') {
			redirect('careers', 'location');
		}

	}

	public function index() {

		$this->load->model("mysql_queries_registrants");

		/* daily per month */
		$start_date = "2011-04-16";
		$end_date = "2014-07-11";

		$dates_count = $this->globals->get_days($start_date);

		for ($i=0; $i < sizeof($dates_count); $i++) { 
			$params = array(
				"table" => "tbl_registrants",
				"where" => "timestamp != '0000-00-00 00:00:00'",
				"fields" => "COUNT(*) as total",
				"where" => "date(timestamp) = '" . $dates_count[$i][0] . "'"
			);
			$items = $this->mysql_queries_registrants->get_data($params);
			if($items) $registrants_daily["daily"][] = array($dates_count[$i][0], $items[0]["total"]);
			else $registrants_daily["daily"][] = array($dates_count[$i][0], 0);
		}

		$data["registrants_daily"] = $registrants_daily;
		/* end daily per month */

		/* monthly */
		$start_month = '2011-04-16';
		$end_month = date('Y-m-t');

		$months_count = $this->globals->get_months($start_month, $end_month);

		for($i = 0; $i < sizeof($months_count); $i++) {
			$params = array(
				"table" => "tbl_registrants",
				"where" => "timestamp != '0000-00-00 00:00:00'",
				"fields" => "COUNT(*) as total",
				"where" => "date(timestamp) between '" . $months_count[$i][0] . "' AND '" . $months_count[$i][1] . "'"
			);
			$items = $this->mysql_queries_registrants->get_data($params);
			if($items) $registrants_monthly['monthly'][] = array($months_count[$i][0], $items[0]['total'], $months_count[$i][1]);
			else $registrants_monthly['monthly'][] = array($months_count[$i][0], 0, $months_count[$i][1]);
		}

		$data['registrants_monthly'] = $registrants_monthly;
		/* end monthly */

		$this->template['content'] = $this->load->view('home-content', $data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function _remap() {

		$this->index();

	}

}