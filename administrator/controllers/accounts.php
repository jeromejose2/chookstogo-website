<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts extends CI_Controller {

	public function __construct() {

		parent::__construct();

		if($this->session->userdata("user_email") == "ept@bountyagro.com.ph") {
			redirect("careers", "location");
		}

		$this->table = 'tbl_cms_users';

	}

	public function index() {

		$this->params = array(
			'table'=>$this->table
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);
		$this->data['total'] = sizeof($this->data['items']);

		$this->template['content'] = $this->load->view('accounts', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function create() {

		$_POST['password'] = md5($_POST['password']);

		$this->params = array(
			'table'=>$this->table,
			'post'=>$_POST
		);
		$this->mysql_queries->insert_data($this->params);

		redirect('accounts');

	}

	public function update() {

		if( $_POST['password'] ) {
			$_POST['password'] = md5($_POST['password']);
		} else {
			unset($_POST['password']);
		}

		$this->params = array(
			'table'=>$this->table,
			'where'=>'user_id = '.$_POST['user_id'],
			'post'=>$_POST
		);
		$this->mysql_queries->update_data($this->params);

		if( $_POST['password'] ) {
			redirect('logout');
		} else {
			redirect('accounts', 'location');
		}

	}

	public function delete() {

		$this->params = array(
			'table'=>$this->table,
			'field'=>'user_id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}
}