<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export extends CI_Controller {

 	public function __construct() {

		parent::__construct();

	}

	public function about_us() {
		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['title']) ? " AND title LIKE '%" . $filter['title'] . "%'" : FALSE;
				$search_filters .= isset($filter['description']) ? " AND description LIKE '%" . $filter['description'] . "%'" : FALSE;
				$search_filters .= isset($filter['date_of_publish']) ? " AND date(date_of_publish) = '" . $filter['date_of_publish'] . "'" : FALSE;
		}
		/*end search function*/

		$params = array('table' => 'tbl_about','where' => $search_filters);

		$data = $this->mysql_queries->get_data($params);
		$row  = array();

		if($data){
				$row[] = array( 'ID',
								'TITLE',
								'DESCRIPTION',
								'PUBLISH DATE',
								'TIMESTAMP'
						);
               foreach($data as $k => $v)
			   {
					extract($v);
			   		$row[] =  array($id,
									$title,
									$description,
									date('y-m-d', strtotime($date_of_publish)),
									date('M d, Y', strtotime($timestamp))
							);
            	 }
         }

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'about_us_'.date("M-d-Y"));
	}

	public function careers() {
		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['position']) ? " AND position LIKE '%" . $filter['position'] . "%'" : FALSE;
				$search_filters .= isset($filter['location']) ? " AND location LIKE '%" . $filter['location'] . "%'" : FALSE;
				$search_filters .= isset($filter['field_of_work']) ? " AND field_of_work LIKE '%" . $filter['field_of_work'] . "%'" : FALSE;
				$search_filters .= isset($filter['description']) ? " AND description LIKE '%" . $filter['description'] . "%'" : FALSE;
				$search_filters .= isset($filter['qualifications']) ? " AND qualifications LIKE '%" . $filter['qualifications'] . "%'" : FALSE;
		}
		/*end search function*/

		$params = array('table' => 'tbl_careers','where' => $search_filters);

		$data = $this->mysql_queries->get_data($params);
		$row  = array();

		if($data){
				$row[] = array( 'ID',
								'POSITION',
								'LOCATION',
								'FIELD OF WORK',
								'DESCRIPTION',
								'QUALIFICATION',
								'TIMESTAMP'
						);
               foreach($data as $k => $v)
			   {
					extract($v);
					switch ($field_of_work) {
						case 'Compliance Assurance':
							$fow = 'Food Quality and Safety Audit';
							break;

						case 'Management Control System':
							$fow = 'Corporate Governance / Planning';
							break;

						default:
							$fow = $field_of_work;
							break;
					}
			   		$row[] =  array($id,
									$position,
									$location,
									$fow,
									$description,
									$qualifications,
									date('M d, Y', strtotime($timestamp))
							);
            	 }
         }

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'careers_'.date("M-d-Y"));
	}

	public function chikoys_corner() {
		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['title']) ? " AND title LIKE '%" . $filter['title'] . "%'" : FALSE;
				$search_filters .= isset($filter['type']) ? " AND type LIKE '%" . $filter['type'] . "%'" : FALSE;
				$search_filters .= isset($filter['is_featured']) ? " AND is_featured = '" . $filter['is_featured'] . "'" : FALSE;
		}
		/*end search function*/

		$params = array('table' => 'tbl_chikoys_corner','where' => $search_filters);

		$data = $this->mysql_queries->get_data($params);
		$row  = array();

		if($data){
				$row[] = array( 'ID',
								'TITLE',
								'TYPE',
								'TIMESTAMP'
						);
               foreach($data as $k => $v)
			   {
					extract($v);
			   		$row[] =  array($id,
									$title,
									$type,
									date('M d, Y', strtotime($timestamp))
							);
            	 }
         }

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'chikoys_corner_'.date("M-d-Y"));
	}

	public function chooks_blog() {
		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['title']) ? " AND title LIKE '%" . $filter['title'] . "%'" : FALSE;
				$search_filters .= isset($filter['section']) ? " AND section LIKE '%" . $filter['section'] . "%'" : FALSE;
				$search_filters .= isset($filter['short_description']) ? " AND short_description LIKE '%" . $filter['short_description'] . "%'" : FALSE;
				$search_filters .= isset($filter['is_featured']) ? " AND is_featured = '" . $filter['is_featured'] . "'" : FALSE;
				$search_filters .= isset($filter['is_announcement']) ? " AND is_announcement = '" . $filter['is_announcement'] . "'" : FALSE;
		}
		/*end search function*/

		$params = array('table' => 'tbl_articles','where' => $search_filters);

		$data = $this->mysql_queries->get_data($params);
		$row  = array();

		if($data){
				$row[] = array( 'ID',
								'TITLE',
								'SECTION',
								'DESCRIPTION',
								'PUBLISH DATE',
								'TIMESTAMP'
						);
               foreach($data as $k => $v)
			   {
					extract($v);
			   		$row[] =  array($id,
									$title,
									$section,
									$short_description,
									date('m-d-y', strtotime($date_of_publish)),
									date('M d, Y', strtotime($timestamp))
							);
            	 }
         }

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'chikoys_corner_'.date("M-d-Y"));
	}

	public function downloadables() {
		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['name']) ? " AND name LIKE '%" . $filter['name'] . "%'" : FALSE;
				$search_filters .= isset($filter['type']) ? " AND type LIKE '%" . $filter['type'] . "%'" : FALSE;
				$search_filters .= isset($filter['timestamp']) ? " AND date(timestamp) = '" . $filter['timestamp'] . "'" : FALSE;
		}
		/*end search function*/

		$params = array('table' => 'tbl_downloadables','where' => $search_filters);

		$data = $this->mysql_queries->get_data($params);
		$row  = array();

		if($data){
				$row[] = array( 'ID',
								'FILENAME',
								'TYPE',
								'TIMESTAMP'
						);
               foreach($data as $k => $v)
			   {
					extract($v);
			   		$row[] =  array($id,
									$filename,
									$type,
									date('M d, Y', strtotime($timestamp))
							);
            	 }
         }

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'downloadables_'.date("M-d-Y"));
	}

	public function feedbacks() {
		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['name']) ? " AND name LIKE '%" . $filter['name'] . "%'" : FALSE;
				$search_filters .= isset($filter['email']) ? " AND email LIKE '%" . $filter['email'] . "%'" : FALSE;
				$search_filters .= isset($filter['message']) ? " AND message LIKE '%" . $filter['message'] . "%'" : FALSE;
		}
		/*end search function*/

		$params = array('table' => 'tbl_feedbacks','where' => $search_filters);

		$data = $this->mysql_queries->get_data($params);
		$row  = array();

		if($data){
				$row[] = array( 'ID',
								'NAME',
								'EMAIL',
								'ADDRESS',
								'CONTACT #',
								'SUBJECT',
								'MESSAGE',
								'TIMESTAMP'
						);
               foreach($data as $k => $v)
			   {
					extract($v);
			   		$row[] =  array($id,
			   						$name,
			   						$email,
			   						$address,
			   						$contact,
			   						$subject,
			   						$message,
									date('M d, Y', strtotime($timestamp))
							);
            	 }
         }

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'feedbacks'.date("M-d-Y"));
	}

	public function products() {
		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['name']) ? " AND title LIKE '%" . $filter['name'] . "%'" : FALSE;
				$search_filters .= isset($filter['category']) ? ' AND category = "' . $filter['category'] . '"' : FALSE;
				$search_filters .= isset($filter['description']) ? " AND description LIKE '%" . $filter['description'] . "%'" : FALSE;
		}
		/*end search function*/

		$params = array('table' => 'tbl_products','where' => $search_filters);

		$data = $this->mysql_queries->get_data($params);
		$row  = array();

		if($data){
				$row[] = array( 'ID',
								'TITLE',
								'DESCRIPTION',
								'CATEGORY',
								'TIMESTAMP'
						);
               foreach($data as $k => $v)
			   {
					extract($v);
			   		$row[] =  array($id,
			   						$title,
			   						$description,
			   						$category,
									date('M d, Y', strtotime($timestamp))
							);
            	 }
         }

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'products_'.date("M-d-Y"));
	}

	public function promos() {
		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['title']) ? " AND title LIKE '%" . $filter['title'] . "%'" : FALSE;
				$search_filters .= isset($filter['promo_type']) ? " AND promo_type LIKE '%" . $filter['promo_type'] . "%'" : FALSE;
				$search_filters .= isset($filter['description']) ? " AND description LIKE '%" . $filter['description'] . "%'" : FALSE;
				$search_filters .= isset($filter['start_promo_date']) ? " AND date(start_promo_date) = '" . $filter['start_promo_date'] . "'" : FALSE;
				$search_filters .= isset($filter['start_publish_date']) ? " AND date(start_publish_date) = '" . $filter['start_publish_date'] . "'" : FALSE;
				$search_filters .= isset($filter['type_of_promo']) ? " AND type_of_promo LIKE '%" . $filter['type_of_promo'] . "%'" : FALSE;
				$search_filters .= isset($filter['link']) ? " AND link LIKE '%" . $filter['link'] . "%'" : FALSE;
				$search_filters .= isset($filter['is_featured']) ? " AND is_featured LIKE '%" . $filter['is_featured'] . "%'" : FALSE;
		}
		/*end search function*/

		$params = array('table' => 'tbl_promos','where' => $search_filters);

		$data = $this->mysql_queries->get_data($params);
		$row  = array();

		if($data){
				$row[] = array( 'ID',
								'TITLE',
								'PROMO TYPE',
								'DESCRIPTION',
								'PROMO DATE START',
								'PROMO DATE END',
								'PUBLISH DATE START',
								'PUBLISH DATE END',
								'TYPE OF PROMO',
								'LINK',
								'TIMESTAMP'
						);
               foreach($data as $k => $v)
			   {
					extract($v);
			   		$row[] =  array($id,
			   						$title,
			   						$promo_type,
			   						$description,
			   						date('y-m-d', strtotime($promo_start_date)),
			   						date('y-m-d', strtotime($promo_end_date)),
			   						date('y-m-d', strtotime($publish_start_date)),
			   						date('y-m-d', strtotime($publish_end_date)),
			   						$type_of_promo,
			   						$link,
									date('M d, Y', strtotime($timestamp))
							);
            	 }
         }

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'promos_'.date("M-d-Y"));
	}

	public function slider() {
		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['link']) ? " AND link LIKE '%" . $filter['link'] . "%'" : FALSE;
		}
		/*end search function*/

		$params = array('table' => 'tbl_homepage_slider','where' => $search_filters);

		$data = $this->mysql_queries->get_data($params);
		$row  = array();

		if($data){
				$row[] = array( 'ID',
								'PATH',
								'LINK',
								'TIMESTAMP'
						);
               foreach($data as $k => $v)
			   {
					extract($v);
			   		$row[] =  array($id,
			   						$photo,
			   						$link,
									date('M d, Y', strtotime($timestamp))
							);
            	 }
         }

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'slider_'.date("M-d-Y"));
	}

	public function people() {
		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['name']) ? " AND name LIKE '%" . $filter['name'] . "%'" : FALSE;
				$search_filters .= isset($filter['designation']) ? " AND designation LIKE '%" . $filter['designation'] . "%'" : FALSE;
				$search_filters .= isset($filter['credentials']) ? " AND credentials LIKE '%" . $filter['credentials'] . "%'" : FALSE;
		}
		/*end search function*/

		$params = array('table' => 'tbl_people','where' => $search_filters);

		$data = $this->mysql_queries->get_data($params);
		$row  = array();

		if($data){
				$row[] = array( 'ID',
								'NAME',
								'DESIGNATION',
								'CREDENTIALS',
								'TIMESTAMP'
						);
               foreach($data as $k => $v)
			   {
					extract($v);
			   		$row[] =  array($id,
			   						$name,
			   						$designation,
			   						$credentials,
									date('M d, Y', strtotime($timestamp))
							);
            	 }
         }

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'people_'.date("M-d-Y"));
	}

	public function stores() {
		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['name']) ? " AND name LIKE '%" . $filter['name'] . "%'" : FALSE;
				$search_filters .= isset($filter['area']) ? " AND area LIKE '%" . $filter['area'] . "%'" : FALSE;
				$search_filters .= isset($filter['city']) ? " AND city LIKE '%" . $filter['city'] . "%'" : FALSE;
				$search_filters .= isset($filter['branch_code']) ? " AND branch_code LIKE '%" . $filter['branch_code'] . "%'" : FALSE;
				$search_filters .= isset($filter['contact']) ? " AND contact LIKE '%" . $filter['contact'] . "%'" : FALSE;
		}
		/*end search function*/

		$params = array('table' => 'tbl_stores','where' => $search_filters);

		$data = $this->mysql_queries->get_data($params);

		$this->load->model('mysql_queries_registrants');
		foreach($data as $k => $v) {
			$v['area'] = $v['area'] ? $v['area'] : 1;
			$params = array(
				'table'=>'tbl_provinces',
				'where'=>'province_id = '.$v['area']
			);
			$province = $this->mysql_queries_registrants->get_data($params);
			$data[$k]['province_name'] = $province[0]['province'];

			$v['city'] = $v['city'] ? $v['city'] : 1;
			$params = array(
				'table'=>'tbl_cities',
				'where'=>'city_id = '.$v['city']
			);
			$city = $this->mysql_queries_registrants->get_data($params);
			$data[$k]['city_name'] = $city[0]['city'];
		}

		$row  = array();

		if($data){
				$row[] = array( 'ID',
								'NAME',
								'AREA',
								'CITY',
								'ADDRESS',
								'BRANCH CODE',
								'CONTACT #',
								'LONGITUDE',
								'LATITUDE',
								'TIMESTAMP'
						);
               foreach($data as $k => $v)
			   {
					extract($v);
			   		$row[] =  array($id,
			   						$name,
			   						$province_name,
			   						$city_name,
			   						$address,
			   						$branch_code,
			   						$contact,
			   						$longitude,
			   						$latitude,
									date('M d, Y', strtotime($timestamp))
							);
            	 }
         }

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'stores_'.date("M-d-Y"));
	}

	public function registrants() {

		ini_set('memory_limit', '-1');

		$this->load->model('mysql_queries_registrants');

		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
				// $search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				// $search_filters .= isset($filter['name']) ? " AND concat(lastname,', ',firstname) LIKE '%" . $filter['name'] . "%'" : FALSE;
				// $search_filters .= isset($filter['bday']) ? " AND date(bday) = '" . $filter['bday'] . "'" : FALSE;
				// $search_filters .= isset($filter['mobile']) ? " AND mobile LIKE '%" . $filter['mobile'] . "%'" : FALSE;
				// $search_filters .= isset($filter['email']) ? " AND email LIKE '%" . $filter['email'] . "%'" : FALSE;
				// $search_filters .= isset($filter['gender']) ? " AND gender = '" . $filter['gender'] . "'" : FALSE;

				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['name']) ? " AND concat(lastname,', ',firstname) LIKE '%" . $filter['name'] . "%'" : FALSE;
				$search_filters .= isset($filter['bday']) ? " AND date(bday) = '" . $filter['bday'] . "'" : FALSE;
				$search_filters .= isset($filter['mobile']) ? " AND mobile LIKE '%" . $filter['mobile'] . "%'" : FALSE;
				$search_filters .= isset($filter['email']) ? " AND email LIKE '%" . $filter['email'] . "%'" : FALSE;
				$search_filters .= isset($filter['gender']) ? " AND gender = '" . $filter['gender'] . "'" : FALSE;
				$search_filters .= isset($filter['from']) ? " AND date(timestamp) >= '" . $filter['from'] . "'" : FALSE;
				$search_filters .= isset($filter['to']) ? " AND date(timestamp) <= '" . $filter['to'] . "'" : FALSE;
		}
		/*end search function*/

		$params = array('table' => 'tbl_registrants','where' => $search_filters);

		$data = $this->mysql_queries_registrants->get_data($params);
		$row  = array();

		if($data){
				$row[] = array( 'ID',
								'NAME',
								'EMAIL',
								'MOBILE',
								'GENDER',
								'CITY',
								'PROVINCE',
								'BIRTHDAY',
								'CONFIRMED',
								'TIMESTAMP'
						);
               foreach($data as $k => $v)
			   {
					extract($v);
			   		$row[] =  array($id,
			   						$lastname.', '.$firstname,
			   						$email,
			   						$mobile,
			   						$gender == 1 ? 'Male' : 'Female',
			   						$city,
			   						$province,
			   						date('F j, Y', strtotime($bday)),
			   						$confirmed_user ? 'Yes' : 'No',
									date('M d, Y', strtotime($timestamp))
							);
            	 }
         }

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'registrants_'.date("M-d-Y"));
	}

	public function logs() {

		$this->load->model('mysql_queries_registrants');

		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
			$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
			$search_filters .= isset($filter['name']) ? " AND concat(lastname, ', ', firstname) LIKE '%" . $filter['name'] . "%'" : FALSE;
			$search_filters .= isset($filter['email']) ? " AND email LIKE '%" . $filter['email'] . "%'" : FALSE;
		}
		/*end search function*/

		$params = array('table' => 'tbl_audit_logs','where' => $search_filters);

		$data = $this->mysql_queries_registrants->get_data($params);
		$row  = array();

		if($data){
				$row[] = array( 'ID',
								'NAME',
								'EMAIL',
								'TIMESTAMP'
						);
               foreach($data as $k => $v)
			   {
					extract($v);
			   		$row[] =  array($log_id,
			   						$lastname.', '.$firstname,
			   						$email,
									date('M j, Y g:i:s A', strtotime($v['timestamp']))
							);
            	 }
         }

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'registrants_'.date("M-d-Y"));
	}

}