<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About_Us extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$this->table = 'tbl_about';

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 3;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';
		$sort = 'timestamp DESC';
		if(isset($_GET['sort']) && (@$_GET['sort'] == 'ASC') || @$_GET['sort'] == 'DESC') {
			$sort = 'timestamp '.$_GET['sort'];
		}

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('about_us/index/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['title']) ? " AND title LIKE '%" . $filter['title'] . "%'" : FALSE;
				$search_filters .= isset($filter['description']) ? " AND description LIKE '%" . $filter['description'] . "%'" : FALSE;
				$search_filters .= isset($filter['date_of_publish']) ? " AND date(date_of_publish) = '" . $filter['date_of_publish'] . "'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>$sort
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('about_us/index'), $curpage, $limit);

		$this->template['content'] = $this->load->view('about-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function form_update($id = NULL)
	{
		$data = array();

		if($_POST) {
			if($_POST['id']) {

			} else {

			}
		}

		if($id) {
			$data['updates'] = $this->mysql_queries->get_data(array(
				'table' => 'tbl_about',
				'where' => 'id = '.$id
			));
		}

		$template['content'] = $this->load->view('updates-form-content', $data, TRUE);
		$this->load->view('main_template', $template);
	}

	public function is_featured() {

		$this->params = array(
			'table'=>'tbl_about',
			'where'=>'id = '.$_POST['id'],
			'post'=>$_POST
		);
		$this->mysql_queries->update_data($this->params);

	}

	public function personalities() {


		$this->table = 'tbl_people';

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 3;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';
		$sort = 'timestamp DESC';
		if(isset($_GET['sort']) && (@$_GET['sort'] == 'ASC') || @$_GET['sort'] == 'DESC') {
			$sort = 'timestamp '.$_GET['sort'];
		}

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('about_us/personalities/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['name']) ? " AND name LIKE '%" . $filter['name'] . "%'" : FALSE;
				$search_filters .= isset($filter['designation']) ? " AND designation LIKE '%" . $filter['designation'] . "%'" : FALSE;
				$search_filters .= isset($filter['credentials']) ? " AND credentials LIKE '%" . $filter['credentials'] . "%'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>$sort
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('about_us/personalities'), $curpage, $limit);

		$this->template['content'] = $this->load->view('personalities-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);
	
	}

	public function form($id = NULL) {

		$this->data = array();

		if($id) {
			$params = array(
				'table'=>'tbl_people',
				'where'=>'id = '.$id
			);
			$this->data['personalities'] = $this->mysql_queries->get_data($params);
		}

		$this->template['content'] = $this->load->view('personalities-form-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function db_query_personalities() {

		if($_POST['id']) {
			$config['upload_path'] = 'assets/admin/uploads/personalities/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$this->load->library('upload', $config);
			$image = 'photo';

			if($this->upload->do_upload($image)) {
				$file = $this->upload->data();
				unset($_POST['photo']);
				$_POST['photo'] = '~path~/assets/admin/uploads/personalities/'.$file['file_name'];
				$_POST['filename'] = $file['file_name'];
			}
			$this->params = array(
				'table'=>'tbl_people',
				'where'=>'id = '.$_POST['id'],
				'post'=>$_POST
			);
			$this->mysql_queries->update_data($this->params);
		} else {
			$config['upload_path'] = 'assets/admin/uploads/personalities/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$this->load->library('upload', $config);
			$image = 'photo';

			if($this->upload->do_upload($image)) {
				$file = $this->upload->data();
				unset($_POST['photo']);
				$_POST['photo'] = '~path~/assets/admin/uploads/personalities/'.$file['file_name'];
				$_POST['filename'] = $file['file_name'];
			}
			$this->params = array(
				'table'=>'tbl_people',
				'post'=>$_POST
			);
			$this->mysql_queries->insert_data($this->params);
		}
		redirect('about_us/personalities');

	}

	public function db_query() {

		if($_POST['id']) {
			$config['upload_path'] = 'assets/admin/uploads/about_us/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$this->load->library('upload', $config);
			$image = 'photo';

			if($this->upload->do_upload($image)) {
				$file = $this->upload->data();
				unset($_POST['photo']);
				$_POST['photo'] = '~path~/assets/admin/uploads/about_us/'.$file['file_name'];
				$_POST['filename'] = $file['file_name'];
			}

			// slider photos
			$files = $_FILES;
			$file_count = count($_FILES['slider_photos']['name']);
			$file_names = array();
			$slider_photos_changed = $_POST['slider_photos_changed'];
			unset($_POST['slider_photos_changed']);

			if($slider_photos_changed) {
				if($files['slider_photos']['name'][0]) {
					for($i = 0; $i < $file_count; $i++) {
						$_FILES['slider_photos']['name'] = $files['slider_photos']['name'][$i];
						$_FILES['slider_photos']['type'] = $files['slider_photos']['type'][$i];
						$_FILES['slider_photos']['tmp_name'] = $files['slider_photos']['tmp_name'][$i];
						$_FILES['slider_photos']['error'] = $files['slider_photos']['error'][$i];
						$_FILES['slider_photos']['size'] = $files['slider_photos']['size'][$i];

						if($this->upload->do_upload('slider_photos')) {
							$file_names[] = '~path~/assets/admin/uploads/about_us/' . $files['slider_photos']['name'][$i];
						} else {
							print_r($this->upload->display_errors());
						}
					}

					$_POST['slider_photos'] = serialize($file_names);
				} else {
					$_POST['slider_photos'] = '';
				}
			} else {
				unset($_POST['slider_photos']);
			}
			
			$this->params = array(
				'table'=>'tbl_about',
				'where'=>'id = '.$_POST['id'],
				'post'=>$_POST
			);
			$this->mysql_queries->update_data($this->params);
		} else {
			$config['upload_path'] = 'assets/admin/uploads/about_us/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$this->load->library('upload', $config);
			$image = 'photo';

			if($this->upload->do_upload($image)) {
				$file = $this->upload->data();
				unset($_POST['photo']);
				$_POST['photo'] = '~path~/assets/admin/uploads/about_us/'.$file['file_name'];
				$_POST['filename'] = $file['file_name'];
			}

			$files = $_FILES;
			$file_count = count($_FILES['slider_photos']['name']);
			$file_names = array();
			unset($_POST['slider_photos_changed']);

			if($files['slider_photos']['name'][0]) {
				for($i = 0; $i < $file_count; $i++) {
					$_FILES['slider_photos']['name'] = $files['slider_photos']['name'][$i];
					$_FILES['slider_photos']['type'] = $files['slider_photos']['type'][$i];
					$_FILES['slider_photos']['tmp_name'] = $files['slider_photos']['tmp_name'][$i];
					$_FILES['slider_photos']['error'] = $files['slider_photos']['error'][$i];
					$_FILES['slider_photos']['size'] = $files['slider_photos']['size'][$i];

					if($this->upload->do_upload('slider_photos')) {
						$file_names[] = '~path~/assets/admin/uploads/about_us/' . $files['slider_photos']['name'][$i];
					} else {
						print_r($this->upload->display_errors());
					}
				}

				$_POST['slider_photos'] = serialize($file_names);
			} else {
				$_POST['slider_photos'] = '';
			}

			// echo '<pre>';
			// print_r($_POST);
			// exit;

			$this->params = array(
				'table'=>'tbl_about',
				'post'=>$_POST
			);
			$this->mysql_queries->insert_data($this->params);
		}
		redirect('about_us');

	}

	public function delete_about_us() {

		$this->params = array(
			'table'=>'tbl_about',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

	public function delete_personalities() {

		$this->params = array(
			'table'=>'tbl_people',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

}