<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logs extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$this->load->model('mysql_queries_registrants');

		$this->table = 'tbl_audit_logs';

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 3;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';
		$sort = 'timestamp DESC';
		if(isset($_GET['sort']) && (@$_GET['sort'] == 'ASC') || @$_GET['sort'] == 'DESC') {
			$sort = 'timestamp '.$_GET['sort'];
		}

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('logs/index/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['name']) ? " AND concat(lastname, ', ', firstname) LIKE '%" . $filter['name'] . "%'" : FALSE;
				$search_filters .= isset($filter['email']) ? " AND email LIKE '%" . $filter['email'] . "%'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'fields'=>'*, concat(lastname, ", ", firstname) as name',
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>'timestamp DESC'
		);
		$this->data['items'] = $this->mysql_queries_registrants->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries_registrants->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('logs/index'), $curpage, $limit);

		$this->template['content'] = $this->load->view('audit-logs-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

}