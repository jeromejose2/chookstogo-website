<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Form extends CI_Controller {

	public function index() {

		$this->template['content'] = $this->load->view('form-content', NULL, TRUE);
		$this->load->view('main_template', $this->template);

	}

	public function _remap() {
		$this->index();
	}
}