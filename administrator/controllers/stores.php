<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stores extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$this->table = 'tbl_stores';

		$limit = isset($_GET['psize']) && is_numeric($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset = ($curpage - 1) * $limit;
		$paging = 3;

		/* search function */
		$filter = FALSE;
		$search_filters = '1';

		if(isset($_GET['search']) || isset($_GET['filter'])) {
			foreach($_GET as $k => $v) {
				if($v != '') {
					$filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if(isset($filter['search'])) {
				unset($filter['search']);
				$filter['filter'] = 1;
				/* here goes the reset */
				redirect('stores/index/1' . '?' . http_build_query($filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$search_filters .= isset($filter['id']) ? " AND id LIKE '%" . $filter['id'] . "%'" : FALSE;
				$search_filters .= isset($filter['name']) ? " AND name LIKE '%" . $filter['name'] . "%'" : FALSE;
				$search_filters .= isset($filter['area']) ? " AND area = '" . $filter['area'] . "'" : FALSE;
				$search_filters .= isset($filter['city']) ? " AND city = '" . $filter['city'] . "'" : FALSE;
				$search_filters .= isset($filter['branch_code']) ? " AND branch_code LIKE '%" . $filter['branch_code'] . "%'" : FALSE;
				$search_filters .= isset($filter['contact']) ? " AND contact LIKE '%" . $filter['contact'] . "%'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>'timestamp DESC'
		);
		$this->data['items'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>$this->table,
			'where'=>$search_filters
		);
		$this->data['total'] = sizeof($this->mysql_queries->get_data($this->params));
		$this->data['pagination'] = $this->globals->pagination($this->data['total'], $curpage, site_url('stores/index'), $curpage, $limit);

		$this->params = array(
			'table'=>'tbl_municipalities'
		);
		$this->data['municipals'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>'tbl_municipalities_and_cities',
			'where'=>'area = "Metro Manila"'
		);
		$this->data['metro_manila'] = $this->mysql_queries->get_data($this->params);
		$this->params = array(
			'table'=>'tbl_municipalities_and_cities',
			'where'=>'area = "North Luzon"'
		);
		$this->data['north_luzon'] = $this->mysql_queries->get_data($this->params);
		$this->params = array(
			'table'=>'tbl_municipalities_and_cities',
			'where'=>'area = "South Luzon"'
		);
		$this->data['south_luzon'] = $this->mysql_queries->get_data($this->params);
		$this->params = array(
			'table'=>'tbl_municipalities_and_cities',
			'where'=>'area = "Visayas"'
		);
		$this->data['visayas'] = $this->mysql_queries->get_data($this->params);
		$this->params = array(
			'table'=>'tbl_municipalities_and_cities',
			'where'=>'area = "Mindanao"'
		);

		$this->load->model('mysql_queries_registrants');
		foreach($this->data['items'] as $k => $v) {
			$params = array(
				'table'=>'tbl_provinces',
				'where'=>'province_id = '.$v['area']
			);
			$province = $this->mysql_queries_registrants->get_data($params);
			$this->data['items'][$k]['province_name'] = $province[0]['province'];

			$params = array(
				'table'=>'tbl_cities',
				'where'=>'city_id = '.$v['city']
			);
			$city = $this->mysql_queries_registrants->get_data($params);
			$this->data['items'][$k]['city_name'] = $city[0]['city'];
		}

		$params = array(
			'table'=>'tbl_provinces'
		);
		$this->data['_provinces'] = $this->mysql_queries_registrants->get_data($params);

		$params = array(
			'table'=>'tbl_cities'
		);
		$this->data['_cities'] = $this->mysql_queries_registrants->get_data($params);

		$this->data['mindanao'] = $this->mysql_queries->get_data($this->params);
		$this->template['content'] = $this->load->view('stores-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function form($id = NULL) {

		if($_POST) {
			if($_POST['id']) {
				$params = array(
					'table'=>'tbl_stores',
					'where'=>'id = '.$_POST['id'],
					'post'=>$_POST
				);
				$this->mysql_queries->update_data($params);
				redirect('stores');
			} else {
				unset($_POST['id']);
				$params = array(
					'table'=>'tbl_stores',
					'post'=>$_POST
				);
				$this->mysql_queries->insert_data($params);
				redirect('stores');
			}
		}

		$this->params = array(
			'table'=>'tbl_municipalities'
		);
		$this->data['municipals'] = $this->mysql_queries->get_data($this->params);

		$this->params = array(
			'table'=>'tbl_municipalities_and_cities',
			'where'=>'area = "Metro Manila"'
		);
		$this->data['metro_manila'] = $this->mysql_queries->get_data($this->params);
		$this->params = array(
			'table'=>'tbl_municipalities_and_cities',
			'where'=>'area = "North Luzon"'
		);
		$this->data['north_luzon'] = $this->mysql_queries->get_data($this->params);
		$this->params = array(
			'table'=>'tbl_municipalities_and_cities',
			'where'=>'area = "South Luzon"'
		);
		$this->data['south_luzon'] = $this->mysql_queries->get_data($this->params);
		$this->params = array(
			'table'=>'tbl_municipalities_and_cities',
			'where'=>'area = "Visayas"'
		);
		$this->data['visayas'] = $this->mysql_queries->get_data($this->params);
		$this->params = array(
			'table'=>'tbl_municipalities_and_cities',
			'where'=>'area = "Mindanao"'
		);
		$this->data['mindanao'] = $this->mysql_queries->get_data($this->params);

		if($id) {
			$params = array(
				'table'=>'tbl_stores',
				'where'=>'id = '.$id
			);
			$this->data['store'] = $this->mysql_queries->get_data($params);
		}

		$this->template['content'] = $this->load->view('stores-form-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

	public function test() {

		$result = array();

		if(isset($_POST['name'])) {
			$values = array($_POST['id'], $_POST['name']);
				foreach($_POST['id'] as $index => $key) {
					$t = array();
					foreach($values as $value) {
						$t[] = $value[$index];
					}
					$result[$index] = $t;
				}

			foreach($result as $k => $v) {
				$params = array(
					'table'=>'tbl_municipalities',
					'where'=>'id = '.$v[0],
					'post'=>array('name'=>$v[1])
				);
				$this->mysql_queries->update_data($params);
			}
		}

		if($_POST['new_name']) {
			$this->params = array(
				'table'=>'tbl_municipalities',
				'post'=>array('name'=>$_POST['new_name'])
			);
			$this->mysql_queries->insert_data($this->params);
		}
		redirect('stores');
	}

	public function db_query() {

		$this->data = array();

		if($_POST['id']) {
			$this->params = array(
				'table'=>'tbl_stores',
				'where'=>'id = '.$_POST['id'],
				'post'=>$_POST
			);
			$this->mysql_queries->update_data($this->params);
		} else {
			$this->params = array(
				'table'=>'tbl_stores',
				'post'=>$_POST
			);
			$this->mysql_queries->insert_data($this->params);
		}

		redirect('stores');

	}

	public function delete() {

		$this->params = array(
			'table'=>'tbl_stores',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

	public function delete_municipal() {

		$this->params = array(
			'table'=>'tbl_municipalities',
			'field'=>'id',
			'value'=>$_POST['id']
		);
		$this->mysql_queries->delete_data($this->params);

	}

	public function load_cities() {

		$this->params = array(
			'table'=>'tbl_municipalities_and_cities',
			'where'=>'area = "'.$_POST['area'].'"'
		);
		$this->results = $this->mysql_queries->get_data($this->params);

		$return = '<option>Select City</option>';
		foreach($this->results as $k => $v) {
			$return .= '<option>'.$v['name'].'</option>';
		}

		echo $return;
	}

}