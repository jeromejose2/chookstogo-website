<div class="container main-content">
     <div class="page-header">
          <h3>Personalities <span class="badge"><?= $total ?></span></h3>

          <div class="actions">
               <a class="btn btn-primary advanced"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Advance Search</span></a>
          </div>

          <div class="actions" style="margin-right: 1%">
               <a href="<?= site_url('about_us/form') ?>" class="btn btn-primary">New Item</a>
               <!-- <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#popup-new" onclick="$('.foo > div').css('width', '100%'); $('.nicEdit-main').css('width', '100%').css('height', '80px')">New Item</a> -->
          </div>

          <div class="actions" style="margin-right: 1%">
              <a href="<?=site_url('export/people?'.http_build_query($_GET, '', "&"))?>" class="btn btn-primary hidden-phone"><i class="icon-download-alt"></i> Export</a>
          </div>

          <div class="actions" style="margin-right: 1%">
              <a href="<?=site_url('export_image/people')?>" class="btn btn-primary hidden-phone"><i class="icon-download-alt"></i> Export Images</a>
          </div>

          <div class=" advance-search">
          <div class="form-content">
               <form class="form-search">
                    <div class="input-prepend">
                         <span class="add-on">ID</span>
                         <input type="text" class="input-medium" name="id" value="<?=isset($_GET['id']) ? $_GET['id'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">NAME</span>
                         <input type="text" class="input-medium" name="name" value="<?=isset($_GET['name']) ? $_GET['name'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">DESIGNATION</span>
                         <input type="text" class="input-medium" name="designation" value="<?=isset($_GET['designation']) ? $_GET['designation'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">CREDENTIALS</span>
                         <input type="text" class="input-medium" name="credentials" value="<?=isset($_GET['credentials']) ? $_GET['credentials'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">ITEMS/PAGE</span>
                         <select name="psize" class="input-small">
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>
                         </select>
                    </div> 

                    <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>
               </form>
          </div>
     </div>
     </div>


     <table class="table table-bordered">
          <thead>
               <tr>
                    <th style="width:1%">Image</th>
                    <th>Name</th>
                    <th>Designation</th>
                    <th>Credentials</th>
                    <th>Action</th>
               </tr>
          </thead>

          <tbody>
               <? if($items): ?>
                    <? foreach($items as $key => $v): ?>
                         <tr id="row-<?= $v['id'] ?>">
                              <td><a href="javascript://" data-toggle="modal" data-target="#popup-image-<?= $v['id'] ?>"><img style="height: 25px" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>"></a></td>
                              <td><?= $v['name'] ?></td>
                              <td><?= $v['designation'] ?></td>
                              <td><?= $v['credentials'] ?></td>
                              <td style="width: 1%">
                                   <a href="<?= site_url('about_us/form') ?>/<?= $v['id'] ?>" style="margin-right: 10px" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
                                   <!-- <a href="javascript:void(0)" style="margin-right: 10px" title="Edit" data-toggle="modal" data-target="#popup-<?php echo $v['id'];?>"><i class="glyphicon glyphicon-edit"></i></a> -->
                                   <a href="javascript:void(0)" title="Delete" class="delete" data-id="<?php echo $v['id'];?>"><i class="glyphicon glyphicon-trash"></i></a>
                              </td>
                         </tr>

                         <!-- MODALS -->
                              <div class="modal fade" id="popup-<?php echo $v['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                      <h4 class="modal-title" id="myModalLabel">Update Personnel</h4>
                                    </div>
                                    <div class="modal-body">
                                      <!-- MODAL CONTENT -->

                                        <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('about_us/db_query_personalities')?>" enctype="multipart/form-data">
                                          <div class="form-group">
                                            <input type="hidden" name="id" value="<?= $v['id'] ?>">
                                                <label class="control-label">Photo</label>
                                                <input type="file" id="photo-<?= $v['id'] ?>" class="photo" name="photo" style="display: none" onchange="uploadPhoto(this)">
                                                <div>
                                                     <button class="btn btn-primary" onclick="$('#photo-<?= $v['id'] ?>').trigger('click'); return false">Browse</button>
                                                     <span class="photo-filename"><?= $v['filename'] ?></span>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Name</label>
                                                <div>
                                                     <input type="text" class="form-control" name="name" value="<?= $v['name'] ?>">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Designation</label>
                                                <div>
                                                     <textarea type="text" class="form-control" name="designation"><?= $v['designation'] ?></textarea>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Credentials</label>
                                                <div>
                                                     <textarea type="text" class="form-control" name="credentials"><?= $v['credentials'] ?></textarea>
                                                </div>
                                           </div>

                                      <!-- END MODAL CONTENT -->
                                      </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Save changes</button>
                                        </form>

                                    </div>
                                  </div>
                                </div>
                              </div>
                         <!-- END MODALS -->

                         <!-- IMAGE MODAL -->
                         <!-- MODALS -->
                              <div class="modal fade" id="popup-image-<?php echo $v['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    </div>
                                    <div class="modal-body">
                                      <!-- MODAL CONTENT -->

                                        <img style="width: 100%" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>">

                                      <!-- END MODAL CONTENT -->
                                      </div>
                                  </div>
                                </div>
                              </div>
                         <!-- END MODALS -->
                         <!-- END IMAGE MODAL -->

                    <? endforeach; ?>
               <? else: ?>
                         <tr>
                              <td colspan="10"><center style="color: red">No data found.</center></td>
                         </tr>
               <? endif; ?>

               <!-- NEW ITEM -->
                    <!-- MODALS -->
                    <div class="modal fade" id="popup-new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">New Product</h4>
                          </div>
                          <div class="modal-body">
                            <!-- MODAL CONTENT -->

                              <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('about_us/db_query_personalities')?>" enctype="multipart/form-data">
                                          <div class="form-group">
                                            <input type="hidden" name="id" value="">
                                                <label class="control-label">Photo</label>
                                                <input type="file" id="photo-new" class="photo" name="photo" style="display: none" onchange="uploadPhoto(this)">
                                                <div>
                                                     <button class="btn btn-primary" onclick="$('#photo-new').trigger('click'); return false">Browse</button>
                                                     <span class="photo-filename"></span>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Name</label>
                                                <div>
                                                     <input type="text" class="form-control" name="name" value="">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Designation</label>
                                                <div>
                                                     <textarea type="text" class="form-control" name="designation"></textarea>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Credentials</label>
                                                <div>
                                                     <textarea type="text" class="form-control" name="credentials"></textarea>
                                                </div>
                                           </div>

                            <!-- END MODAL CONTENT -->
                            </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                              </form>

                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END MODALS -->
                    <!-- END NEW ITEM -->

          </tbody>
     </table>

     <?=$pagination?>


</div>

<script src="<?=base_url()?>assets/admin/js/texteditor/nicEdit.js?r=<?=rand()?>"></script>
<script type="text/javascript">
bkLib.onDomLoaded(function(){
  new nicEditor({iconsPath : '<?=base_url()?>assets/admin/js/texteditor/nicEditorIcons.gif'}).panelInstance('');
});
</script>

<script type="text/javascript">
     $(function(){
          $('.advanced').click(function(){
               $('.advance-search').slideToggle();
          });

          var lytebox = new Lytebox;
          $('.delete').on('click', function(){
               var id = $(this).data('id');
               lytebox.dialog({
                    message: 'Are you sure you want to delete this?', 
                    title : 'Confirm<hr style="margin: 5px 0; border-color: #ccc">',
                    type : 'confirm',
                    onConfirm : function(){
                         $.post('<?php echo site_url("about_us/delete_personalities")?>', {id:id}, function(){
                              $('#row-'+id).hide();
                         });
                    },
                    top: 100
               });
          });
     });

     $('input[name=date_of_publish]').datepicker({
          changeMonth: true,
          numberOfMonths: 1,
          dateFormat:'yy-mm-dd'
      });

     function uploadPhoto(input) {
        var name = input.files[0].name;
        var size = convertSize(input.files[0].size);
        $('.photo-filename').html(name + '<small> - '+ size +' </small>');
     }

     function convertSize(bytes) {

           if(bytes == 0) return '0 Byte';
           var k = 1000;
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
           var i = Math.floor(Math.log(bytes) / Math.log(k));
           var raw = (bytes / Math.pow(k, i)).toPrecision(3);
           var result;

           return raw >= 2097152 ? 'file limit exceeded' : raw + ' ' + sizes[i];
           
    }
</script>