<div class="container main-content">
     <div class="page-header">
          <h3>About Us <span class="badge"><?= $total ?></span></h3>

          <div class="actions">
               <a class="btn btn-primary advanced"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Advance Search</span></a>
          </div>

          <div class="actions" style="margin-right: 1%">
               <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#popup-new" onclick="$('.foo > div').css('width', '100%'); $('.nicEdit-main').css('width', '100%').css('height', '80px')">New Item</a>
          </div>

          <div class="actions" style="margin-right:1%">
              <a href="<?=site_url('export/about_us?'.http_build_query($_GET, '', "&"))?>" class="btn btn-primary hidden-phone"><i class="icon-download-alt"></i> Export</a>
          </div>

          <div class="actions" style="margin-right: 1%">
              <a href="<?=site_url('export_image/news')?>" class="btn btn-primary hidden-phone"><i class="icon-download-alt"></i> Export Images</a>
          </div>

          <div class=" advance-search">
          <div class="form-content">
               <form class="form-search">
                    <div class="input-prepend">
                         <span class="add-on">ID</span>
                         <input type="text" class="input-medium" name="id" value="<?=isset($_GET['id']) ? $_GET['id'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">TITLE</span>
                         <input type="text" class="input-medium" name="title" value="<?=isset($_GET['title']) ? $_GET['title'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">DESCRIPTION</span>
                         <input type="text" class="input-medium" name="description" value="<?=isset($_GET['description']) ? $_GET['description'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">PUBLISH DATE</span>
                         <input type="text" class="input-medium" name="date_of_publish" value="<?=isset($_GET['date_of_publish']) ? $_GET['date_of_publish'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">ITEMS/PAGE</span>
                         <select name="psize" class="input-small">
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>
                         </select>
                    </div> 

                    <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>
               </form>
          </div>
     </div>
     </div>


     <table class="table table-bordered">
          <thead>
               <tr>
                    <th style="width: 1%">Image</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Announcement</th>
                    <th>Publish Date</th>
                    <th>Action</th>
               </tr>
          </thead>

          <tbody>
               <? if($items): ?>
                    <? foreach($items as $key => $v): ?>
                         <tr id="row-<?= $v['id'] ?>">
                              <td><a href="javascript://" data-toggle="modal" data-target="#popup-image-<?= $v['id'] ?>"><img style="height: 25px" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>"></a></td>
                              <td><?= $v['title'] ?></td>
                              <td><?= $v['description'] ?></td>
                              <td style="text-align:center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle action-callback-<?= $v['id'] ?> <?= $v['is_featured'] ? 'btn-success' : '' ?>" data-toggle="dropdown">
                                      <span class="glyphicon glyphicon-star"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                      <li class="featured-action" data-action="1" data-id="<?= $v['id'] ?>"><a href="javascript://">Yes</a></li>
                                      <li class="featured-action" data-action="0" data-id="<?= $v['id'] ?>"><a href="javascript://">No</a></li>
                                    </ul>
                                  </div>
                              </td>
                              <td><?= date('F j, Y', strtotime($v['date_of_publish'])) ?></td>
                              <td style="width: 1%">
                                   <a href="javascript:void(0)" style="margin-right: 10px" title="Edit" data-toggle="modal" data-target="#popup-<?php echo $v['id'];?>"><i class="glyphicon glyphicon-edit"></i></a>
                                   <a href="javascript:void(0)" title="Delete" class="delete" data-id="<?php echo $v['id'];?>"><i class="glyphicon glyphicon-trash"></i></a>
                              </td>
                         </tr>

                         <!-- MODALS -->
                              <div class="modal fade" id="popup-<?php echo $v['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                      <h4 class="modal-title" id="myModalLabel">Update Product</h4>
                                    </div>
                                    <div class="modal-body">
                                      <!-- MODAL CONTENT -->

                                        <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('about_us/db_query')?>" enctype="multipart/form-data">
                                          <div class="form-group">
                                            <input type="hidden" name="id" value="<?= $v['id'] ?>">
                                                <label class="control-label">Photo</label>
                                                <input type="file" id="photo-<?= $v['id'] ?>" class="photo" name="photo" style="display: none" onchange="uploadPhoto(this)">
                                                <div>
                                                     <button class="btn btn-primary" onclick="$('#photo-<?= $v['id'] ?>').trigger('click'); return false">Browse</button>
                                                     <span class="photo-filename"><?= $v['filename'] ?></span>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Title</label>
                                                <div>
                                                     <input type="text" class="form-control" name="title" value="<?= $v['title'] ?>">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Description</label>
                                                <div>
                                                     <textarea type="text" class="form-control" name="description"><?= $v['description'] ?></textarea>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Publish Date</label>
                                                <div>
                                                     <input type="text" class="form-control" name="date_of_publish" value="<?= date('Y-m-d', strtotime($v['date_of_publish'])) ?>">
                                                </div>
                                           </div>

                                      <!-- END MODAL CONTENT -->
                                      </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Save changes</button>
                                        </form>

                                    </div>
                                  </div>
                                </div>
                              </div>
                         <!-- END MODALS -->

                         <!-- IMAGE MODAL -->
                         <!-- MODALS -->
                              <div class="modal fade" id="popup-image-<?php echo $v['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    </div>
                                    <div class="modal-body">
                                      <!-- MODAL CONTENT -->

                                        <img style="width: 100%" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>">

                                      <!-- END MODAL CONTENT -->
                                      </div>
                                  </div>
                                </div>
                              </div>
                         <!-- END MODALS -->
                         <!-- END IMAGE MODAL -->

                    <? endforeach; ?>
               <? else: ?>
                         <tr>
                              <td colspan="10"><center style="color: red">No data found.</center></td>
                         </tr>
               <? endif; ?>

               <!-- NEW ITEM -->
                    <!-- MODALS -->
                    <div class="modal fade" id="popup-new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">New Product</h4>
                          </div>
                          <div class="modal-body">
                            <!-- MODAL CONTENT -->

                              <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('about_us/db_query')?>" enctype="multipart/form-data">
                                          <div class="form-group">
                                            <input type="hidden" name="id" value="">
                                                <label class="control-label">Photo</label>
                                                <input type="file" id="photo-new" class="photo" name="photo" style="display: none" onchange="uploadPhoto(this)">
                                                <div>
                                                     <button class="btn btn-primary" onclick="$('#photo-new').trigger('click'); return false">Browse</button>
                                                     <span class="photo-filename"></span>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Title</label>
                                                <div>
                                                     <input type="text" class="form-control" name="title" value="">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Description</label>
                                                <div>
                                                     <textarea type="text" class="form-control" name="description"></textarea>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Publish Date</label>
                                                <div>
                                                     <input type="text" class="form-control" name="date_of_publish" value="">
                                                </div>
                                           </div>

                            <!-- END MODAL CONTENT -->
                            </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                              </form>

                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END MODALS -->
                    <!-- END NEW ITEM -->

          </tbody>
     </table>

     <?=$pagination?>


</div>

<script type="text/javascript">
    tinymce.init({
        selector : "textarea",
        menubar : false,
        plugins : ["code"],
        toolbar : [
            "undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright | removeformat | code"
        ]
    });
</script>

<script type="text/javascript">
     $(function(){
          $('.advanced').click(function(){
               $('.advance-search').slideToggle();
          });

          var lytebox = new Lytebox;
          $('.delete').on('click', function(){
               var id = $(this).data('id');
               lytebox.dialog({
                    message: 'Are you sure you want to delete this?', 
                    title : 'Confirm<hr style="margin: 5px 0; border-color: #ccc">',
                    type : 'confirm',
                    onConfirm : function(){
                         $.post('<?php echo site_url("about_us/delete_about_us")?>', {id:id}, function(){
                              $('#row-'+id).hide();
                         });
                    },
                    top: 100
               });
          });
     });

     $('input[name=date_of_publish]').datepicker({
          changeMonth: true,
          numberOfMonths: 1,
          dateFormat:'yy-mm-dd'
      });

     function uploadPhoto(input) {
        var name = input.files[0].name;
        var size = convertSize(input.files[0].size);
        $('.photo-filename').html(name + '<small> - '+ size +' </small>');
     }

     function convertSize(bytes) {

           if(bytes == 0) return '0 Byte';
           var k = 1000;
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
           var i = Math.floor(Math.log(bytes) / Math.log(k));
           var raw = (bytes / Math.pow(k, i)).toPrecision(3);
           var result;

           return raw >= 2097152 ? 'file limit exceeded' : raw + ' ' + sizes[i];
           
    }

    $('.featured-action').on('click', function(){
             var id = $(this).data('id');
             var action = $(this).data('action');

             data = {id:id, is_featured:action};
             $.post("<?= site_url('about_us/is_featured') ?>", data, function(){
                 $('.action-callback-'+id).removeClass('btn-success');
                 if(action == '1') {
                     $('.action-callback-'+id).addClass('btn-success');
                 }
             });
         });
</script>