<script type="text/javascript" src="<?= base_url() ?>assets/admin/js/jquery.collapse.js"></script>
<div class="container main-content">

	<div class="row-fluid">
		<div class="span9 visible-desktop">
			<h4>Registrants for <?= date("F, Y") ?></h4>
			<div id="registrantsChart"></div>
		</div>
		<div class="span9 visible-desktop">
			<h4>Registrants Monthly</h4>
			<div id="registrantsMonthlyChart"></div>
		</div>
			<br>
		<div class="span3">
			<h4 class="collapse-summary">Summary</h4>
			<table class="table table-hover table-bordered table-heading table-striped table-float">
				<tr><td colspan="10"><b>Daily</b></td></tr>
				<? if($registrants_daily['daily'] ): $i=0; foreach($registrants_daily['daily'] as $v ) : $i++; ?>
					<tr><td><?= date("M j, Y", strtotime($v[0])) ?></td><td><?=$v[1]?></tr>
				<? endforeach; endif; ?>
			</table>
			<table class="table table-hover table-bordered table-heading table-striped table-float">
				<tr><td colspan="10"><b>Monthly</b> <button class="monthly-stats-collapse"></button></td></tr>
				<? if($registrants_monthly['monthly'] ): $i=0; foreach($registrants_monthly['monthly'] as $v ) : $i++; ?>
					<tr><td><?= date("M, Y", strtotime($v[0])) ?></td><td><?=$v[1]?></tr>
				<? endforeach; endif; ?>	
			</table>
		</div>
	</div>

</div>


<script type="text/javascript" src="<?= base_url() ?>assets/admin/js/jsapi.min.js"></script>
<script type="text/javascript">

	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
  
  	function drawChart() {
		var data = google.visualization.arrayToDataTable([
			['Date', 'Registrants'],
			<? if($registrants_daily['daily'] ): $i=0; foreach($registrants_daily['daily'] as $v ) : $i++; ?>
				['<?= date("M j, Y", strtotime($v[0])) ?>',  <?=$v[1]?>],
			<? endforeach; endif; ?>
		]);

		var options = {
		  fontSize:11,
		  chartArea:{width:'90%'},
		  legend:{position:'none'},
		  vAxis:{minValue:0,format:'#'}
		};

		var data2 = google.visualization.arrayToDataTable([
			['Month', 'Registrants'],
			<? if($registrants_monthly['monthly'] ): $i=0; foreach($registrants_monthly['monthly'] as $v ) : $i++; ?>
				['<?= date("M, Y", strtotime($v[0])) ?>',  <?=$v[1]?>],
			<? endforeach; endif; ?>
		]);

		var options2 = {
		  fontSize:11,
		  chartArea:{width:'90%'},
		  legend:{position:'none'},
		  vAxis:{minValue:0,format:'#'}
		};

		var chart = new google.visualization.AreaChart(document.getElementById('registrantsChart'));
		chart.draw(data, options);

		var chart2 = new google.visualization.AreaChart(document.getElementById('registrantsMonthlyChart'));
		chart2.draw(data2, options2);
  	}

	$(function(){
		$(window).resize(function(){
			drawChart();			
		})
	})
</script>

<style type="text/css">
	svg {
		border-radius: 5px;
	}

	.table-float {
		float: left;
		width: 50%;
	}
</style>