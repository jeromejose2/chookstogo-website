     <div class="container main-content">
          <div class="page-header">
               <h3>List</h3>

               <div class="actions">
                    <a href="#" class="btn btn-primary">New Item</a>
               </div>
          </div>


          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>First Name</th>
                         <th>Last Name</th>
                         <th>Username</th>
                    </tr>
               </thead>
               <tbody>
                    <tr>
                         <td>1</td>
                         <td>Mark</td>
                         <td>Otto</td>
                         <td>@mdo</td>
                    </tr>
                    <tr>
                         <td>2</td>
                         <td>Mark</td>
                         <td>Otto</td>
                         <td>@TwBootstrap</td>
                    </tr>
                    <tr>
                         <td>3</td>
                         <td>Jacob</td>
                         <td>Thornton</td>
                         <td>@fat</td>
                    </tr>
                    <tr>
                         <td>4</td>
                         <td>Jay</td>
                         <td>Ramirez</td>
                         <td>@twitter</td>
                    </tr>
                    <tr>
                         <td>1</td>
                         <td>Mark</td>
                         <td>Otto</td>
                         <td>@mdo</td>
                    </tr>
                    <tr>
                         <td>2</td>
                         <td>Mark</td>
                         <td>Otto</td>
                         <td>@TwBootstrap</td>
                    </tr>
                    <tr>
                         <td>3</td>
                         <td>Jacob</td>
                         <td>Thornton</td>
                         <td>@fat</td>
                    </tr>
                    <tr>
                         <td>4</td>
                         <td>Jay</td>
                         <td>Ramirez</td>
                         <td>@twitter</td>
                    </tr>
                    <tr>
                         <td>1</td>
                         <td>Mark</td>
                         <td>Otto</td>
                         <td>@mdo</td>
                    </tr>
                    <tr>
                         <td>2</td>
                         <td>Mark</td>
                         <td>Otto</td>
                         <td>@TwBootstrap</td>
                    </tr>
                    <tr>
                         <td>3</td>
                         <td>Jacob</td>
                         <td>Thornton</td>
                         <td>@fat</td>
                    </tr>
                    <tr>
                         <td>4</td>
                         <td>Jay</td>
                         <td>Ramirez</td>
                         <td>@twitter</td>
                    </tr>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <li><a href="#">Prev</a></li>
               <li><a href="#">1</a></li>
               <li><a href="#">2</a></li>
               <li><a href="#">3</a></li>
               <li><a href="#">4</a></li>
               <li><a href="#">5</a></li>
               <li><a href="#">Next</a></li>
          </ul>

     </div>