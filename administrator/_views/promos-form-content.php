<div class="container main-content">
     <div class="page-header">
          <? if(isset($promo[0]['id'])): ?>
               <h3>Update Promo</h3>
          <? else: ?>
               <h3>Create Promo</h3>
          <? endif; ?>
     </div>

     <form id="promos-form" class="form-horizontal" role="form" action="<?= site_url('promos/form') ?>" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="id" value="<?= isset($promo[0]['id']) ? $promo[0]['id'] : '' ?>">
          <div class="form-group">
               <label class="col-sm-2 control-label">Title</label>
               <div class="col-sm-8">
                    <input type="text" class="form-control" name="title" value="<?= isset($promo[0]['title']) ? $promo[0]['title'] : '' ?>">
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Photo</label>
               <div class="col-sm-8">
                    <input type="file" id="photo" class="photo" name="photo" style="display: none" onchange="uploadPhoto(this)" class="form-control">
                    <button class="btn btn-primary" onclick="$('#photo').trigger('click'); return false">Browse</button>
                    <span class="photo-filename"><?= isset($promo[0]['filename']) ? $promo[0]['filename'] : '' ?></span>
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Description</label>
               <div class="col-sm-8">
                    <textarea name="description" id="content"><?= isset($promo[0]['description']) ? $promo[0]['description'] : '' ?></textarea>
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Promo Type</label>
               <div class="col-sm-8">
                    <select name="promo_type" class="form-control">
                         <option <?= isset($promo[0]['promo_type']) && $promo[0]['promo_type'] == 'In-store Promo' ? 'selected' : '' ?>>In-store Promo</option>
                         <option <?= isset($promo[0]['promo_type']) && $promo[0]['promo_type'] == 'Digital Promo' ? 'selected' : '' ?>>Digital Promo</option>
                    </select>
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Promo Duration</label>
               <div class="col-sm-8">
                    <input type="text" placeholder="Start Date" name="promo_start_date" value="<?= isset($promo[0]['promo_start_date']) ? date('Y-m-d', strtotime($promo[0]['promo_start_date'])) : '' ?>">
                    <input type="text" placeholder="End Date" name="promo_end_date" value="<?= isset($promo[0]['promo_end_date']) ? date('Y-m-d', strtotime($promo[0]['promo_end_date'])) : '' ?>">
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Publish Duration</label>
               <div class="col-sm-8">
                    <input type="text" placeholder="Start Date" name="publish_start_date" value="<?= isset($promo[0]['publish_end_date']) ? date('Y-m-d', strtotime($promo[0]['publish_end_date'])) : '' ?>">
                    <input type="text" placeholder="End Date" name="publish_end_date" value="<?= isset($promo[0]['publish_end_date']) ? date('Y-m-d', strtotime($promo[0]['publish_end_date'])) : '' ?>">
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Type of Promo</label>
               <div class="col-sm-8">
                    <select class="form-control" name="type_of_promo">
                         <option <?= isset($promo[0]['type_of_promo']) && $promo[0]['type_of_promo'] == 'Article' ? 'selected' : '' ?>>Article</option>
                         <option <?= isset($promo[0]['type_of_promo']) && $promo[0]['type_of_promo'] == 'App/Microsite' ? 'selected' : '' ?>>App/Microsite</option>
                    </select>
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Link</label>
               <div class="col-sm-8">
                    <input type="text" name="link" class="form-control" value="<?= isset($promo[0]['link']) ? $promo[0]['link'] : '' ?>">
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Featured</label>
               <div class="col-sm-8">
                    <input type="checkbox" name="is_featured" <?= isset($promo[0]['is_featured']) && $promo[0]['is_featured'] == '1' ? 'checked' : '' ?>>
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Announcement</label>
               <div class="col-sm-8">
                    <input type="checkbox" name="is_announcement" <?= isset($promo[0]['is_announcement']) && $promo[0]['is_announcement'] == '1' ? 'checked' : '' ?>>
               </div>
          </div>
          <div class="form-group">
               <label class="col-sm-2 control-label">Open Link in New Tab</label>
               <div class="col-sm-8">
                    <input type="checkbox" name="open_link" value="1" <?= isset($promo[0]['open_link']) && $promo[0]['open_link'] == 1 ? 'checked' : '' ?>>
               </div>
          </div>
          <div class="form-group">
               <div class="col-sm-offset-2 col-sm-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="javascript:void(0)" onclick="console.log(tinymce.get('textarea').getContent())">asdasd</a>
               </div>
          </div>
     </form>


</div>

<script>
    tinymce.init({
        selector : "textarea",
        menubar : false,
        plugins : ["code preview"],
        toolbar : [
            "undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright | removeformat | code"
        ]
    });
</script>
<script type="text/javascript">
         $('input[name=promo_start_date], input[name=promo_end_date], input[name=publish_start_date], input[name=publish_end_date]').datepicker({
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat:'yy-mm-dd'
         });

     function uploadPhoto(input) {
        var name = input.files[0].name;
        var size = convertSize(input.files[0].size);
        $('.photo-filename').html(name + '<small> - '+ size +' </small>');
     }

     function convertSize(bytes) {

           if(bytes == 0) return '0 Byte';
           var k = 1000;
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
           var i = Math.floor(Math.log(bytes) / Math.log(k));
           var raw = (bytes / Math.pow(k, i)).toPrecision(3);
           var result;

           return raw >= 2097152 ? 'file limit exceeded' : raw + ' ' + sizes[i];
           
    }
</script>