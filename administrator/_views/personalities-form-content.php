<div class="container main-content">
     <div class="page-header">
          <? if(isset($personalities[0]['id'])): ?>
               <h3>Update People</h3>
          <? else: ?>
               <h3>Create People</h3>
          <? endif; ?>
     </div>

     <form class="form-horizontal" role="form" action="<?php echo site_url('about_us/db_query_personalities')?>" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="id" value="<?= isset($personalities[0]['id']) ? $personalities[0]['id'] : '' ?>">
          <div class="form-group">
                  <label class="control-label">Photo</label>
                  <input type="file" id="photo-new" class="photo" name="photo" style="display: none" onchange="uploadPhoto(this)">
                  <div>
                       <button class="btn btn-primary" onclick="$('#photo-new').trigger('click'); return false">Browse</button>
                       <span class="photo-filename"><?= isset($personalities[0]['filename']) ? $personalities[0]['filename'] : '' ?></span>
                  </div>
             </div>

             <div class="form-group">
                  <label class="control-label">Name</label>
                  <div>
                       <input type="text" class="form-control" name="name" value="<?= isset($personalities[0]['name']) ? $personalities[0]['name'] : '' ?>">
                  </div>
             </div>

             <div class="form-group">
                  <label class="control-label">Designation</label>
                  <div>
                      <!-- <input type="text" class="form-control" name="designation" value="<?= isset($personalities[0]['designation']) ? $personalities[0]['designation'] : '' ?>"> -->
                       <textarea type="text" class="form-control" name="designation" id="content"><?= isset($personalities[0]['designation']) ? $personalities[0]['designation'] : '' ?></textarea>
                  </div>
             </div>

             <div class="form-group">
                  <label class="control-label">Credentials</label>
                  <div>
                       <textarea type="text" class="form-control" name="credentials" id="content2"><?= isset($personalities[0]['credentials']) ? $personalities[0]['credentials'] : '' ?></textarea>
                  </div>
             </div>
          <div class="form-group">
               <div class="col-sm-offset-2 col-sm-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
               </div>
          </div>
     </form>


</div>

<script type="text/javascript">
    tinymce.init({
        selector : "textarea",
        menubar : false,
        plugins : ["code"],
        toolbar : [
            "undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright | removeformat | code"
        ]
    });
</script>
<script type="text/javascript">
     function uploadPhoto(input) {
        var name = input.files[0].name;
        var size = convertSize(input.files[0].size);
        $('.photo-filename').html(name + '<small> - '+ size +' </small>');
     }

     function convertSize(bytes) {

           if(bytes == 0) return '0 Byte';
           var k = 1000;
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
           var i = Math.floor(Math.log(bytes) / Math.log(k));
           var raw = (bytes / Math.pow(k, i)).toPrecision(3);
           var result;

           return raw >= 2097152 ? 'file limit exceeded' : raw + ' ' + sizes[i];
           
    }
</script>