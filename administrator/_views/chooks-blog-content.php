<div class="container main-content">
     <div class="page-header">
          <h3>Chooks Blog <span class="badge"><?=$total?></span></h3>

          <div class="actions">
               <a class="btn btn-primary advanced"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Advance Search</span></a>
          </div>

          <div class="actions" style="margin-right: 1%">
               <a href="<?= site_url('chooks_blog/form') ?>" class="btn btn-primary">New Item</a>
          </div>

          <div class="actions" style="margin-right: 1%">
              <a href="<?=site_url('export/chooks_blog?'.http_build_query($_GET, '', "&"))?>" class="btn btn-primary hidden-phone"><i class="icon-download-alt"></i> Export</a>
          </div>

          <div class="actions" style="margin-right: 1%">
              <a href="<?=site_url('export_image/blogs')?>" class="btn btn-primary hidden-phone"><i class="icon-download-alt"></i> Export Images</a>
          </div>

          <div class=" advance-search">
          <div class="form-content">
               <form class="form-search">
                    <div class="input-prepend">
                         <span class="add-on">ID</span>
                         <input type="text" class="input-medium" name="id" value="<?=isset($_GET['id']) ? $_GET['id'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">TITLE</span>
                         <input type="text" class="input-medium" name="title" value="<?=isset($_GET['title']) ? $_GET['title'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">SECTION</span>
                         <select name="section">
                              <option value=""></option>
                             <option <?= isset($_GET['section']) && $_GET['section'] == 'Main Section' ? 'selected' : '' ?>>Main Section</option>
                             <option <?= isset($_GET['section']) && $_GET['section'] == 'Chooks Help' ? 'selected' : '' ?>>Chooks Help</option>
                        </select>
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">DESCRIPTION</span>
                         <input type="text" class="input-medium" name="short_description" value="<?=isset($_GET['short_description']) ? $_GET['short_description'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">FEATURED</span>
                         <select name="is_featured">
                            <option value=""></option>             
                            <option <?= isset($_GET['is_featured']) && $_GET['is_featured'] == 1 ? 'selected' : '' ?> value="1">Yes</option>
                            <option <?= isset($_GET['is_featured']) && $_GET['is_featured'] == 0 ? 'selected' : '' ?> value="0">No</option>
                         </select>
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">ANNOUNCEMENT</span>
                         <select name="is_announcement">
                          <option value=""></option>
                            <option <?= isset($_GET['is_announcement']) && $_GET['is_announcement'] == 1 ? 'selected' : '' ?> value="1">Yes</option>
                            <option <?= isset($_GET['is_announcement']) && $_GET['is_announcement'] == 0 ? 'selected' : '' ?> value="0">No</option>
                         </select>
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">ITEMS/PAGE</span>
                         <select name="psize" class="input-small">
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>
                         </select>
                    </div> 

                    <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>
               </form>
          </div>
     </div>
     </div>


     <table class="table table-bordered">
          <thead>
               <tr>
                    <th style="width:1%">Image</th>
                    <th>Title</th>
                    <th>Section</th>
                    <th>Short Description</th>
                    <th>Featured</th>
                    <th>Announcement</th>
                    <th>Action</th>
               </tr>
          </thead>

          <tbody>
               <? if($items): ?>
                    <? foreach($items as $key => $v): ?>
                         <tr id="row-<?= $v['id'] ?>">
                              <td><a href="javascript://" data-toggle="modal" data-target="#popup-image-<?= $v['id'] ?>"><img style="height: 25px" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>"></a></td>
                              <td><?= $v['title'] ?></td>
                              <td><?= $v['section'] ?></td>
                              <td><?= $v['short_description'] ?></td>
                              <td style="width: 1%; text-align: center">
                                   <div class="btn-group">
                                         <button type="button" class="btn btn-default dropdown-toggle action-callback-<?= $v['id'] ?> <?= $v['is_featured'] ? 'btn-success' : '' ?>" data-toggle="dropdown">
                                           <span class="glyphicon glyphicon-star"></span>
                                         </button>
                                         <ul class="dropdown-menu" role="menu">
                                           <li class="featured-action" data-action="1" data-id="<?= $v['id'] ?>"><a href="javascript://">Yes</a></li>
                                           <li class="featured-action" data-action="0" data-id="<?= $v['id'] ?>"><a href="javascript://">No</a></li>
                                         </ul>
                                       </div>
                              </td>
                              <td style="width: 1%; text-align: center">
                                   <div class="btn-group">
                                         <button type="button" class="btn btn-default dropdown-toggle action-callback-announcement-<?= $v['id'] ?> <?= $v['is_announcement'] ? 'btn-success' : '' ?>" data-toggle="dropdown">
                                           <span class="glyphicon glyphicon-star"></span>
                                         </button>
                                         <ul class="dropdown-menu" role="menu">
                                           <li class="featured-action-announcement" data-action="1" data-id="<?= $v['id'] ?>"><a href="javascript://">Yes</a></li>
                                           <li class="featured-action-announcement" data-action="0" data-id="<?= $v['id'] ?>"><a href="javascript://">No</a></li>
                                         </ul>
                                       </div>
                              </td>
                              <td style="width: 1%">
                                   <a href="<?= site_url('chooks_blog/form') ?>/<?= $v['id'] ?>" style="margin-right: 10px" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
                                   <a href="javascript:void(0)" title="Delete" class="delete" data-id="<?php echo $v['id'];?>"><i class="glyphicon glyphicon-trash"></i></a>
                              </td>
                         </tr>

                         <!-- IMAGE MODAL -->
                         <!-- MODALS -->
                              <div class="modal fade" id="popup-image-<?php echo $v['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    </div>
                                    <div class="modal-body">
                                      <!-- MODAL CONTENT -->

                                        <img style="width: 100%" src="<?= str_replace('~path~', base_url(), $v['photo']) ?>">

                                      <!-- END MODAL CONTENT -->
                                      </div>
                                  </div>
                                </div>
                              </div>
                         <!-- END MODALS -->
                         <!-- END IMAGE MODAL -->
                    <? endforeach; ?>
               <? else: ?>
                         <tr>
                              <td colspan="10"><center style="color: red">No data found.</center></td>
                         </tr>
               <? endif; ?>
          </tbody>
     </table>

     <?=$pagination?>

</div>

<script type="text/javascript">
     $(function(){
          $('.advanced').click(function(){
               $('.advance-search').slideToggle();
          });
     });

     var lytebox = new Lytebox;
     $('.delete').on('click', function(){
          var id = $(this).data('id');
          lytebox.dialog({
               message: 'Are you sure you want to delete this?', 
               title : 'Confirm<hr style="margin: 5px 0; border-color: #ccc">',
               type : 'confirm',
               onConfirm : function(){
                    $.post('<?php echo site_url("chooks_blog/delete")?>', {id:id}, function(){
                         $('#row-'+id).hide();
                    });
               },
               top: 100
          });
     });

     $('.featured-action').on('click', function(){
             var id = $(this).data('id');
             var action = $(this).data('action');

             data = {id:id, is_featured:action};
             $.post("<?= site_url('chooks_blog/ajax') ?>", data, function(){
                 $('.action-callback-'+id).removeClass('btn-success');
                 if(action == '1') {
                     $('.action-callback-'+id).addClass('btn-success');
                 }
             });
         });

      $('.featured-action-announcement').on('click', function(){
             var id = $(this).data('id');
             var action = $(this).data('action');

             data = {id:id, is_announcement:action};
             $.post("<?= site_url('chooks_blog/ajax_announcement') ?>", data, function(){
                 $('.action-callback-'+id).removeClass('btn-success');
                 if(action == '1') {
                     $('.action-callback-announcement-'+id).addClass('btn-success');
                 }
             });
         });
</script>