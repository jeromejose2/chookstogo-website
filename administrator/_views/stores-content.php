<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjxtjNyjATUWGLx-1v0zehvr0bkc0ACnE&sensor=false">></script>
<div class="container main-content">
     <div class="page-header">
          <h3>Stores <span class="badge"><?= $total ?></span></h3>

          <div class="actions">
               <a class="btn btn-primary advanced"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Advance Search</span></a>
          </div>

          <!-- <div class="actions" style="margin-right: 1%">
               <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#popup-municipal">Manage Municipals</a>
          </div> -->

          <div class="actions" style="margin-right: 1%">
               <a href="<?= site_url('stores/form') ?>" class="btn btn-primary">New Item</a>
          </div>

          <div class="actions" style="margin-right: 1%">
              <a href="<?=site_url('export/stores?'.http_build_query($_GET, '', "&"))?>" class="btn btn-primary hidden-phone"><i class="icon-download-alt"></i> Export</a>
          </div>

          <div class="actions" style="margin-right: 1%">
               <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#import-data">Import Data</a>
          </div>

          <div class=" advance-search">
          <div class="form-content">
               <form class="form-search">
                    <div class="input-prepend">
                         <span class="add-on">ID</span>
                         <input type="text" class="input-medium" name="id" value="<?=isset($_GET['id']) ? $_GET['id'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">NAME</span>
                         <input type="text" class="input-medium" name="name" value="<?=isset($_GET['name']) ? $_GET['name'] : '' ?>" >
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">AREA</span>
                         <select class="input-small" name="area">
                              <option value=""></option>
                              <? foreach($_provinces as $k => $v): ?>
                                <option <?= isset($_GET['area']) && $_GET['area'] == $v['province_id'] ? 'selected' : '' ?> value="<?= $v['province_id'] ?>"><?= $v['province'] ?></option>
                              <? endforeach; ?>
                              <!-- <option <?= isset($_GET['area']) && $_GET['area'] == 'Metro Manila' ? 'selected' : '' ?>>Metro Manila</option>
                              <option <?= isset($_GET['area']) && $_GET['area'] == 'North Luzon' ? 'selected' : '' ?>>North Luzon</option>
                              <option <?= isset($_GET['area']) && $_GET['area'] == 'South Luzon' ? 'selected' : '' ?>>South Luzon</option>
                              <option <?= isset($_GET['area']) && $_GET['area'] == 'Visayas' ? 'selected' : '' ?>>Visayas</option>
                              <option <?= isset($_GET['area']) && $_GET['area'] == 'Mindanao' ? 'selected' : '' ?>>Mindanao</option> -->
                         </select>
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">CITY</span>
                         <!-- <input type="text" class="input-medium" name="city" value="<?=isset($_GET['city']) ? $_GET['city'] : '' ?>" > -->
                         <select class="input-small" name="city">
                            <option value=""></option>
                            <? foreach($_cities as $k => $v): ?>
                              <option <?= isset($_GET['city']) && $_GET['city'] == $v['city_id'] ? 'selected' : '' ?> value="<?= $v['city_id'] ?>"><?= $v['city'] ?></option>
                            <? endforeach; ?>
                         </select>
                    </div>
                    <div class="input-prepend">
                         <span class="add-on">BRANCH CODE</span>
                         <input type="text" class="input-medium" name="branch_code" value="<?=isset($_GET['branch_code']) ? $_GET['branch_code'] : '' ?>" >
                    </div>
                    <!-- <div class="input-prepend">
                         <span class="add-on">CONTACT #</span>
                         <input type="text" class="input-medium" name="contact" value="<?=isset($_GET['contact']) ? $_GET['contact'] : '' ?>" >
                    </div> -->
                    <div class="input-prepend">
                         <span class="add-on">ITEMS/PAGE</span>
                         <select name="psize" class="input-small">
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>
                              <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>
                         </select>
                    </div> 

                    <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>
               </form>
          </div>
     </div>
     </div>


     <table class="table table-bordered">
          <thead>
               <tr>
                    <th>Name</th>
                    <th>Area</th>
                    <th>City</th>
                    <th>Address</th>
                    <th>Branch Code</th>
                    <!-- <th>Contact</th> -->
                    <th>Action</th>
               </tr>
          </thead>

          <tbody>
               <? if($items): ?>
                    <? foreach($items as $key => $v): ?>
                         <tr id="row-<?= $v['id'] ?>">
                              <td><?= $v['name'] ?></td>
                              <td><?= $v['province_name'] ?></td>
                              <td><?= $v['city_name'] ?></td>
                              <td><?= $v['address'] ?></td>
                              <td><?= $v['branch_code'] ?></td>
                              <!-- <td><?= $v['contact'] ?></td> -->
                              <td style="width: 1%">
                                   <!-- <a href="javascript:void(0)" style="margin-right: 10px" title="Edit" data-toggle="modal" data-target="#popup-<?php echo $v['id'];?>"><i class="glyphicon glyphicon-edit"></i></a> -->
                                   <a href="<?= site_url('stores/form') ?>/<?= $v['id'] ?>" style="margin-right: 10px" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
                                   <a href="javascript:void(0)" title="Delete" class="delete" data-id="<?php echo $v['id'];?>"><i class="glyphicon glyphicon-trash"></i></a>
                              </td>
                         </tr>

                         <!-- MODALS -->
                              <div class="modal fade" id="popup-<?php echo $v['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                      <h4 class="modal-title" id="myModalLabel">Update Store</h4>
                                    </div>
                                    <div class="modal-body">
                                      <!-- MODAL CONTENT -->

                                        <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('stores/db_query')?>" enctype="multipart/form-data">
                                          <input type="hidden" name="id" value="<?= $v['id'] ?>">

                                           <div class="form-group">
                                                <label class="control-label">Name</label>
                                                <div>
                                                     <input type="text" class="form-control" name="name" value="<?= $v['name'] ?>">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Area</label>
                                                <div>
                                                     <select class="form-control update-area" name="area" data-id="<?= $v['id'] ?>">
                                                          <option <?= $v['area'] == 'Metro Manila' ? 'selected' : '' ?>>Metro Manila</option>
                                                          <option <?= $v['area'] == 'North Luzon' ? 'selected' : '' ?>>North Luzon</option>
                                                          <option <?= $v['area'] == 'South Luzon' ? 'selected' : '' ?>>South Luzon</option>
                                                          <option <?= $v['area'] == 'Visayas' ? 'selected' : '' ?>>Visayas</option>
                                                          <option <?= $v['area'] == 'Mindanao' ? 'selected' : '' ?>>Mindanao</option>
                                                     </select>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                              <label class="control-label">Municipal / City <img id="spinner-<?= $v['id'] ?>" style="display: none" src="<?=base_url()?>assets/admin/images/spinner.gif"></label>
                                              <div>
                                                <select class="form-control" name="city" id="city-<?= $v['id'] ?>">
                                                  <? if($v['area'] == 'Metro Manila'): ?>
                                                    <? foreach($metro_manila as $k => $val): ?>
                                                      <option <?= $v['city'] == $val['name'] ? 'selected' : '' ?>><?= $val['name'] ?></option>
                                                    <? endforeach; ?>
                                                  <? elseif($v['area'] == 'North Luzon'): ?>
                                                    <? foreach($north_luzon as $k => $val): ?>
                                                      <option <?= $v['city'] == $val['name'] ? 'selected' : '' ?>><?= $val['name'] ?></option>
                                                    <? endforeach;?>
                                                  <? elseif($v['area'] == 'South Luzon'): ?>
                                                    <? foreach($south_luzon as $k => $val): ?>
                                                      <option <?= $v['city'] == $val['name'] ? 'selected' : '' ?>><?= $val['name'] ?></option>
                                                    <? endforeach;?>
                                                  <? elseif($v['area'] == 'Visayas'): ?>
                                                    <? foreach($visayas as $k => $val): ?>
                                                      <option <?= $v['city'] == $val['name'] ? 'selected' : '' ?>><?= $val['name'] ?></option>
                                                    <? endforeach;?>
                                                  <? elseif($v['area'] == 'Mindanao'): ?>
                                                    <? foreach($mindanao as $k => $val): ?>
                                                      <option <?= $v['city'] == $val['name'] ? 'selected' : '' ?>><?= $val['name'] ?></option>
                                                    <? endforeach;?>
                                                  <? endif; ?>
                                                </select>   
                                              </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Address</label>
                                                <div>
                                                     <input type="text" class="form-control" name="address" value="<?= $v['address'] ?>">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Branch Code</label>
                                                <div>
                                                     <input type="text" class="form-control" name="branch_code" value="<?= $v['branch_code'] ?>">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Contact Details</label>
                                                <div>
                                                     <input type="text" class="form-control" name="contact" value="<?= $v['contact'] ?>">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Zoom Level (Recommended: 17)</label>
                                                <div>
                                                     <select class="form-control" name="zoom_level">
                                                        <? for($i = 1; $i <= 20; $i++): ?>
                                                          <option <?= $v['zoom_level'] == $i ? 'selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                                                        <? endfor; ?>
                                                     </select>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Google Location</label>
                                                <div>
                                                     <div id="map_canvas-<?= $v['id'] ?>" style="width: 500px; height: 500px"></div>
                                                     <input type="text" name="longitude" value="<?= $v['longitude'] ?>" placeholder="Longitude" id="longitude-<?= $v['id'] ?>">
                                                     <input type="text" name="latitude" value="<?= $v['latitude'] ?>" placeholder="Latitude" id="latitude-<?= $v['id'] ?>">
                                                </div>
                                           </div>

                                      <!-- END MODAL CONTENT -->
                                      </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Save changes</button>
                                        </form>

                                    </div>
                                  </div>
                                </div>
                              </div>
                         <!-- END MODALS -->

                    <? endforeach; ?>
               <? else: ?>
                         <tr>
                              <td colspan="10"><center style="color: red">No data found.</center></td>
                         </tr>
               <? endif; ?>

               <!-- NEW ITEM -->
                    <!-- MODALS -->
                    <div class="modal fade" id="popup-new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">New Store</h4>
                          </div>
                          <div class="modal-body">
                            <!-- MODAL CONTENT -->

                              <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('stores/db_query')?>" enctype="multipart/form-data">
                                          <input type="hidden" name="id" value="">

                                           <div class="form-group">
                                                <label class="control-label">Name</label>
                                                <div>
                                                     <input type="text" class="form-control" name="name" value="">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Area</label>
                                                <div>
                                                     <select class="form-control" name="area" id="new-item-area">
                                                          <option>Metro Manila</option>
                                                          <option>North Luzon</option>
                                                          <option>South Luzon</option>
                                                          <option>Visayas</option>
                                                          <option>Mindanao</option>
                                                     </select>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                              <label class="control-label">Municipal / City <img id="spinner" style="display: none" src="<?=base_url()?>assets/admin/images/spinner.gif"></label>
                                              <div>
                                                  <select class="form-control" name="city" id="new-item-city">
                                                      <? foreach($metro_manila as $k => $v): ?>
                                                        <option><?= $v['name'] ?></option>
                                                      <? endforeach; ?>
                                                  </select>
                                              </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Address</label>
                                                <div>
                                                     <input type="text" class="form-control" name="address" value="">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Branch Code</label>
                                                <div>
                                                     <input type="text" class="form-control" name="branch_code" value="">
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Contact Details</label>
                                                <div>
                                                     <input type="text" class="form-control" name="contact" value="">
                                                </div>
                                           </div>
                                           <div class="form-group">
                                                <label class="control-label">Zoom Level (Recommended: 17)</label>
                                                <div>
                                                     <select class="form-control" name="zoom_level">
                                                        <? for($i = 1; $i <= 20; $i++): ?>
                                                          <option <?= $i == 17 ? 'selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                                                        <? endfor; ?>
                                                     </select>
                                                </div>
                                           </div>

                                           <div class="form-group">
                                                <label class="control-label">Google Location</label>
                                                <div>
                                                      <div id="map_canvas" style="width: 500px; height: 500px"></div>
                                                     <input type="text" name="longitude" value="" placeholder="Longitude">
                                                     <input type="text" name="latitude" value="" placeholder="Latitude">
                                                </div>
                                           </div>

                            <!-- END MODAL CONTENT -->
                            </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                              </form>

                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END MODALS -->
                    <!-- END NEW ITEM -->

                    <!-- NEW MUNICIPAL -->
                    <!-- MODALS -->
                    <div class="modal fade" id="popup-municipal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Manage Municipals</h4>
                          </div>
                          <div class="modal-body">
                            <!-- MODAL CONTENT -->

                              <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('stores/test')?>" enctype="multipart/form-data">

                                   <div class="form-group">
                                        <label class="control-label">Name</label>
                                        <div>
                                             <input type="text" class="form-control" name="new_name">
                                        </div>
                                   </div>

                                    <label class="control-label">Municipals <span class="badge"><?= sizeof($municipals) ?></span></label>
                                    <? foreach($municipals as $k => $v): ?>
                                    <div style="margin-bottom: 1px" id="municipal-row-<?= $v['id'] ?>">
                                      <input style="width: 90%" name="name[]" value="<?= $v['name'] ?>" id="input-<?= $v['id'] ?>" class="read-only" readOnly>
                                      <a href="javascript://" class="edit-municipal" data-id="<?= $v['id'] ?>"><i class="glyphicon glyphicon-edit"></i></a>
                                      <a href="javascript://" class="delete-municipal" data-id="<?= $v['id'] ?>"><i class="glyphicon glyphicon-trash"></i></a>
                                      <input type="hidden" name="id[]" value="<?= $v['id'] ?>">
                                    </div>

                                    <? endforeach; ?>

                            <!-- END MODAL CONTENT -->
                            </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                              </form>

                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END MODALS -->
                    <!-- END NEW MUNICIPAL -->

                    <!-- IMPORT DATA -->
                    <!-- MODALS -->
                    <div class="modal fade" id="import-data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Import Data (CSV)</h4>
                          </div>
                          <div class="modal-body">
                            <!-- MODAL CONTENT -->

                              <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('import/csv')?>" enctype="multipart/form-data">

                                   <div class="form-group">
                                        <label class="control-label"></label>
                                        <div>
                                             <input type="file" class="form-control" name="csv">
                                        </div>
                                   </div>

                            <!-- END MODAL CONTENT -->
                            </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                              </form>

                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END MODALS -->
                    <!-- END IMPORT DATA -->

          </tbody>
     </table>

     <?=$pagination?>


</div>

<script type="text/javascript">
  $(function(){
        
            var myLatlng = new google.maps.LatLng(14.59885354948353, 120.98423133572385);

        var mapOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

        var infowindow = new google.maps.InfoWindow();

        var bounds = new google.maps.LatLngBounds();

        var marker = new google.maps.Marker({
              draggable: true,
              position: myLatlng, 
              map: map,
              // icon: '<?=base_url()?>images/admin/marker.png',
              title: "Store Location"
          });

        google.maps.event.addListener(marker, 'drag', function(event){
           document.getElementById("latitude").value = event.latLng.lat();
           document.getElementById("longitude").value = event.latLng.lng();
    });

        var listener = google.maps.event.addListener(map, "idle", function () {
            map.setZoom(15);
            google.maps.event.removeListener(listener);
        });

          // google.maps.event.addDomListener(window, 'load', initialize);
    });
</script>

<script type="text/javascript">
     $(function(){
          $('.advanced').click(function(){
               $('.advance-search').slideToggle();
          });

          var lytebox = new Lytebox;
          $('.delete').on('click', function(){
               var id = $(this).data('id');
               lytebox.dialog({
                    message: 'Are you sure you want to delete this?', 
                    title : 'Confirm<hr style="margin: 5px 0; border-color: #ccc">',
                    type : 'confirm',
                    onConfirm : function(){
                         $.post('<?php echo site_url("stores/delete")?>', {id:id}, function(){
                              $('#row-'+id).hide();
                         });
                    },
                    top: 100
               });
          });
     });

     $('.delete-municipal').on('click', function(){
        var id = $(this).data('id');
        $.post("<?= site_url('stores/delete_municipal') ?>", {id:id}, function(){
            $('#municipal-row-'+id).remove();
        });
     });

     $('.edit-municipal').on('click', function(){
        var id = $(this).data('id');
        $('#input-'+id).attr('readOnly', false).removeClass('read-only');
     });

     $('select[name=area]').on('change', function(){
        var area = $(this).val();
        console.log(area);
        $('#spinner').show();
        $.post('<?= site_url("stores/load_cities") ?>', {area:area}, function(response){
          $('#spinner').hide();
          $('#new-item-city').html(response);
        });
     });

     $('.update-area').on('change', function(){
        var area = $(this).val();
        var id = $(this).data('id');
        $('#spinner-'+id).show();
        $.post('<?= site_url("stores/load_cities") ?>', {area:area}, function(response){
            $('#spinner-'+id).hide();
            $('#city-'+id).html(response);
        });
     });
</script>

<style type="text/css">
  .read-only {
    background-color: #eaeaea;
  }
</style>