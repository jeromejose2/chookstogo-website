module.exports = function(grunt){
	
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		uglify: {
			my_target: {
				options: {
					sourceMap: true,
					sourceMapName: 'js/sourcemap.map'
				},
				files: {
					'js/output.min.js' : [
					'assets/vendors/jquery/jquery.js',
					'assets/vendors/lytebox/lytebox.js',
					'assets/js/jTypeform.min.js',
					'assets/vendors/bootstrap/js/bootstrap.js',
					'assets/vendors/owl-carousel/owl.carousel.js',
					'assets/js/tform.min.js',
					'assets/js/chookstogo.js',
					'assets/js/jMask.js'
					]
				}
			}
		},
		cssmin : {
			combine : {
				files : {
					'css/output.min.css' : ['css/main.min.css',
											'assets/vendors/bootstrap/css/bootstrap.css',
											'assets/vendors/lytebox/lytebox.css',
											'assets/vendors/owl-carousel/owl.carousel.css',
											'assets/vendors/owl-carousel/owl.theme.css',
											'assets/vendors/owl-carousel/owl.transitions.css',
											'assets/vendors/font-awesome/css/font-awesome.min.css',
											'assets/css/main.min.css',
											'assets/css/blogslider.css'
					]
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.registerTask('default', ['uglify','cssmin']);
	//grunt.registerTask('default', ['cssmin']);
};