tform = $('#typeform-footer').jtypeform();

function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;

    if (event.keyCode == 8 || event.keyCode == 46
     || event.keyCode == 37 || event.keyCode == 39) {
        return true;
    }
    else if ( key < 48 || key > 57 ) {
        return false;
    }
    else return true;
}

$('input[name=contact]').keypress(validateNumber);

$('textarea[name=message]').on('focus', function(){
    $('.type-form-btn').html('SUBMIT');
});

$('.type-form-btn').click(function(){
    $('#typeform-footer').submit();
});

$('#typeform-footer').on('submit', function(){
    var empty = false;
    var buttonFeedback = $('.type-form-btn');
    var loop = $('.f-required');

    loop.each(function(){
        var elem = $(this);
        var email = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

        if(elem.attr('name') != 'name' && elem.attr('name') != 'address' && !elem.val()) {
            empty = true;
            return false;
        }
    });

    if(empty) {
        return false;
    } else {
        var data = $('#typeform-footer').serialize();
        buttonFeedback.addClass('disabled');
        $.post(site_url+'ajax_process', data, function() {
            buttonFeedback.html('CONTINUE').removeClass('disabled');
            $('.feedback-sent').fadeIn().html('Feedback sent');
            setTimeout(function(){
                $('.feedback-sent').fadeOut('slow')
            }, 10000);
            tform.clearForm();
        });
        return false;
    }
});