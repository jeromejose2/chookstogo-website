function ChooksToGo() {

    var Me = this;
    var spinner = '<img src="'+base_url+'assets/admin/images/spinner-content.GIF">';

    Me.chikoyTabChange = function() {
        var data = { type : $(this).data('type') };
        $('.chikoys-corner-list').html('<div style="margin-left: 48%; margin-bottom: 5%">'+ spinner +'</div>');
        $.post(site_url+'chikoy_corner/ajax_process', data, function (response){
            $('.chikoys-corner-list').html(response);
        })
    }

    Me.chikoySelectChange = function() {
        var data = { type : $(this).val() };
        $('.chikoys-corner-list').html('<div style="margin-left: 48%; margin-bottom: 5%">'+ spinner +'</div>');
        $.post(site_url+'chikoy_corner/ajax_process', data, function (response){
            $('.chikoys-corner-list').html(response);
        });
    }

    Me.productTabChange = function() {
        var data = { category : $(this).data('category') };
        if(!$(this).hasClass('active')) {
            $('#product-list').html('<div style="margin-left: 48%">'+ spinner +'</div>');
            $('.product-details-content').animate({'height':0,'padding':0});
            $.post(site_url+'ajax_process/foobar', data, function (response){
                $('#product-list').html(response);
            });
        }
    }

    Me.productSelectChange = function() {
        var data = { category : $(this).val() };
        $('#product-list').html('<div style="margin-left: 48%">'+ spinner +'</div>');
        $('.product-details-content').animate({'height':0,'padding':0});
        $.post(site_url+'ajax_process/foobar', data, function (response){
            $('#product-list').html(response);
        });
    }

    Me.promoTabChange = function() {
        var data = { value : $(this).data('value') };
        $('#promo-list').html('<div style="margin-left: 48%">'+ spinner +'</div>');
        $('.promo-details-content').animate({'height':0,'padding':0});
        $.post(site_url+'promos/select_promo', data, function (response){
            $('#promo-list').html(response);
        });
    }

    Me.promoSelectChange = function() {
        var data = { value : $(this).val() };
        $('#promo-list').html('<div style="margin-left: 48%">'+ spinner +'</div>');
        $('.promo-details-content').animate({'height':0,'padding':0});
        $.post(site_url+'promos/select_promo', data, function (response){
            $('#promo-list').html(response);
        });
    }

    Me.blogTabChange = function() {
        var data = { section : $(this).data('section') };
        if(!$(this).hasClass('active')) {
            $('.blog-list').html('<div style="margin-left: 48%">'+ spinner +'</div>');
            $.post(site_url+'blogs/ajax_process', data, function (response){
                $('.blog-list').html(response);
            });
        }
    }

    Me.blogSelectChange = function() {
        var data = { section : $(this).val() };
        $('.blog-list').html('<div style="margin-left: 48%">'+ spinner +'</div>');
        $.post(site_url+'blogs/ajax_process', data, function (response){
            $('.blog-list').html(response);
        });
    }

    Me.TermsAndConditionsPopup = function() {
        lytebox.load({url:site_url+"popups/terms_and_conditions", top:50});
    }

    Me.applicationFormPopup = function() {
        var data = { position : $(this).data('position') };
        lytebox.load({url:site_url+"popups/application_form", data:data, top:150});
    }

    Me.forgotPassword = function() {
        lytebox.load({url:site_url+"popups/forgot_password", top:150});
    }

    Me.loadImage = function(id, type) {
        var data = { id : id, type : type };
        lytebox.load({url:site_url+"popups/large_image", data:data, top: 10});
    }

    Me.triggerNewPassword = function() {
        lytebox.load({url:site_url+"popups/new_password", top:150});
    }

    Me.validateNewPasswordForm = function() {
        var error = false;
        var loop = $('.newpassword-required');
        var password = $('.pword').val();

        loop.each(function(){
            loop.removeClass('error-field');
            var elem = $(this);
            var error_msg = elem.attr('alt');
            var value = $.trim(elem.val());

            if(elem.val() == '') {
                elem.addClass('error-field');
                error = error_msg;
                elem.focus();
                return false;
            } else if(elem.attr('name') == 're_password') {
                if(elem.val() != password) {
                    elem.addClass('error-field');
                    elem.focus();
                    error = 'Sorry, passwords do not match. Please try again.';
                    return false;
                }
            }
        });

        if(error) {
            $('.newpassword-error').html(error);
            return false;
        } else {
            $('.newpassword-required').removeClass('error-field');
            var data = { password : $('.pword').val(), id : is_forgot };
            $('.btn-forgotpassword-submit-btn').addClass('disabled');
            $.post(site_url+"forgot_password/update_password", data, function (){
                lytebox.close();
                Me.thankYouPopUp('Uy, na-reset na ang password mo! Pwede mo nang gamitin ang bagong password mo. Enjoy!');
            });
            return false;
        }
        return false;
    }

    Me.validateForgotPasswordForm = function() {
        var error = false;
        var email = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        var loop = $('.forgotpassword-required');

        loop.each(function(){
            loop.addClass('error-field');
            var elem = $(this);
            var error_msg = elem.attr('alt');
            var value = $.trim(elem.val());

            if(elem.val() == '') {
                elem.addClass('error-field');
                error = error_msg;
                elem.focus();
                return false;
            } else if(!email.test(value)) {
                elem.addClass('error-field');
                error = 'Sorry, you have entered an invalid email address.';
                elem.focus();
                return false;
            }
        });

        if(error) {
            $('.forgotpassword-error').html(error);
            return false;
        } else {
            $('.forgotpassword-error').removeClass('error-field');
            var data = $('#forgotpassword-form').serialize();
            $.post(site_url+'forgot_password/send', data, function (response){
                if(response) {
                    $('.forgotpassword-error').html(response);
                    $('.forgotpassword-required').addClass('error-field');
                    $('.forgotpassword-required').focus();
                } else {
                    lytebox.close();
                    Me.thankYouPopUp('Salamat sa pag-type! Antayin mo na lang yung email namin para matapos ang password reset.');
                }
            });
            return false;
        }
        return false;
    }

    Me.validateRegistration = function() {
        var error = false;
        var email = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        var numbers = /^\d+$/;
        var letters = /^[a-zA-Z-'\s]+$/;
        var re_password = $('input[name=password]').val();
        var re_email = $('input[name=email]').val();
        var loop = $('.registration-required');

        loop.each(function(){
            loop.removeClass('error-field');
            var elem = $(this);
            var error_msg = elem.attr('alt');
            var value = $.trim(elem.val());

           if(elem.val() == '') {
                error = error_msg;
                elem.focus();
                elem.addClass('error-field');
                return false;
            } else if(elem.attr('name') == 'email') {
                if(!email.test(value)) {
                    elem.addClass('error-field');
                    elem.focus();
                    error = 'Sorry, you have entered an invalid email address.';
                    return false;
                }
            } else if(elem.attr('name') == 're_email') {
                if(value != re_email) {
                    elem.addClass('error-field');
                    elem.focus();
                    error = 'Sorry, the email do not match. Please retype them and try again.';
                    return false;
                }
            } else if(elem.attr('name') == 're_password') {
                if(value != re_password) {
                    elem.addClass('error-field');
                    elem.focus();
                    error = 'Sorry, the passwords do not match. Please retype them and try again.';
                    return false;
                }
            } else if(elem.attr('name') == 'agree') {
                if(!$(this).is(':checked')) {
                    error = error_msg;
                    elem.focus();
                    elem.addClass('error-field');
                    return false;
                }
            }
        });

        if(error) {
            if($('input[name=firstname]').val() == '') {
                $('input[name=firstname]').addClass('error-field');
                $('.error').html('PLEASE FILL IN REQUIRED FIELDS');
            }
            if($('input[name=lastname]').val() == '') {
                $('input[name=lastname]').addClass('error-field');
                $('.error').html('PLEASE FILL IN REQUIRED FIELDS');
            }
            if($('select[name=gender]').val() == '') {
                $('select[name=gender]').addClass('error-field');
                $('.error').html('PLEASE FILL UP REQUIRED FIELDS');
            }
            if($('input[name=mobile]').val() == '') {
                $('input[name=mobile]').addClass('error-field');
                $('.error').html('PLEASE FILL IN REQUIRED FIELDS');
            }
            if($('select[name=province]').val() == '') {
                $('select[name=province]').addClass('error-field');
                $('.error').html('PLEASE FILL IN REQUIRED FIELDS');
            }
            if($('select[name=city]').val() == '') {
                $('select[name=city]').addClass('error-field');
                $('.error').html('PLEASE FILL IN REQUIRED FIELDS');
            }
            if($('input[name=email]').val() == '') {
                $('input[name=email]').addClass('error-field');
                $('.error').html('PLEASE FILL IN REQUIRED FIELDS');
            }
            if($('input[name=re_email]').val() == '') {
                $('input[name=re_email]').addClass('error-field');
                $('.error').html('PLEASE FILL IN REQUIRED FIELDS');
            }
            if($('input[name=email]').val() != $('input[name=re_email]').val() || $('input[name=re_email]').val() != $('input[name=email]').val()) {
                $('input[name=email]').addClass('error-field');
                $('input[name=re_email]').addClass('error-field');
            }
            if($('input[name=password]').val() == '') {
                $('input[name=password]').addClass('error-field');
                $('.error').html('PLEASE FILL IN REQUIRED FIELDS');
            }
            if($('input[name=re_password]').val() == '') {
                $('input[name=re_password]').addClass('error-field');
                $('.error').html('PLEASE FILL IN REQUIRED FIELDS');
            }
            if($('input[name=password]').val() != $('input[name=re_password]').val() || $('input[name=re_password]').val() != $('input[name=password]').val()) {
                $('input[name=password]').addClass('error-field');
                $('input[name=re_password]').addClass('error-field');
            }
            if($('input[name=recaptcha_response_field]').val() == '') {
                $('input[name=recaptcha_response_field]').addClass('error-field');
                $('.error').html(error);
            }
            if(!$('input[name=agree]').is(':checked')) {
                $('input[name=agree]').addClass('error-field');
                $('.error').html('PLEASE FILL IN REQUIRED FIELDS');
            }
            // TEST


        } else {
            $('.btn-rounded, .btn-glyph-rounded').addClass('disabled');
            var data = $('#regForm').serialize();
            $.post(site_url+'registration/validate', data, function (hasError){
                $('.btn-rounded, .btn-glyph-rounded').removeClass('disabled');
                console.log(hasError);
                if(hasError) {
                    $('#recaptcha_response_field').removeClass('error-field');
                    $('.reg-email').removeClass('error-field');
                    if(hasError == 'Incorrect reCaptcha code. Please try again.') {
                        $('#recaptcha_response_field').addClass('error-field');
                        $('#recaptcha_reload').trigger('click');
                    } else {
                        $('.reg-email').focus();
                        $('.reg-email').addClass('error-field');
                        $('#recaptcha_reload').trigger('click');
                    }
                    $('#recaptcha_response_field').val('');
                    $('.error').html(hasError);
                } else {
                    $('#recaptcha_response_field').removeClass('error-field');
                    $('.error').html('');
                    lytebox.load({url:site_url+"popups/thank_you", top:150, data:{content:'Salamat sa pag-register! Check mo lang yung email mo para ma-confirm namin ang registration mo!'}, onClose: function(){
                        window.location.reload();
                    }});
                }
            });
        }
        return false;
    }

    Me.validateApplicationForm = function() {

        var error = false;
        var errcon = $('.error');
        var email = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        var loop = $('.application-required');

        loop.each(function(){
            var elem = $(this);
            var error_msg = elem.attr('alt');
            var val = $.trim(elem.val());

            if(val == '' && elem.attr('name') != 'portfolio_youtube') {
                error = error_msg;
                elem.focus();
                return false;
            } else if(elem.attr('name') == 'email') {
                if(!email.test(val)) {
                    error = 'Please provide a valid email address.';
                    elem.focus();
                    return false;
                }
            } else if(elem.attr('name') == 'portfolio_youtube') {
                if(val != '') {
                    if(val.indexOf('youtube') < 0) {
                        error = 'Please provide a valid youtube url video.';
                        elem.focus();
                        return false;
                    }
                }              
            }
        });

        if(error) {
            errcon.html(error);
            return false;
        } else {
            return true;
        }
    }

    Me.validateLogin = function() {
        var error = false;
        var errcon = $('.error-login');
        var email = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        var loop = $('.login-required');

        loop.each(function(){
            loop.removeClass('error-field');
            var elem = $(this);
            var msg = elem.attr('alt');
            var val = $.trim(elem.val());

            if(val == '') {
                error = msg;
                elem.focus();
                elem.addClass('error-field');
                return false;
            } else if(elem.attr('name') == 'login_username') {
                if(!email.test(val)) {
                    elem.focus();
                    elem.addClass('error-field');
                    error = 'You have entered an invalid username format.';
                    return false;
                }
            }
        });

        if(error) {
            errcon.html(error);
            return false;
        } else {
            $('.login-required').removeClass('error-field');
            var data = $('#loginForm').serialize();
            $('.btn-rounded, .btn-glyph-rounded').addClass('disabled');
            $.post(site_url+'login/validate', data, function (hasError){
                if(hasError) {
                    $('.btn-rounded, .btn-glyph-rounded').removeClass('disabled');
                    errcon.html(hasError);
                    $('input[name=login_password]').val('');
                } else {
                    errcon.html('');
                    window.location.href = site_url;
                }
            });
        }
        return false;
    }

    Me.googleSelectArea = function() {
        var area = $('select[name=area]').val();
        
        $('select[name=city] > option').text('LOADING');
        $('select[name=store] > option').text('STORE');
        $('select[name=city]').attr('disabled', true);
        $('select[name=store]').attr('disabled', true);
        $.post(site_url+'gmap_query/select_google_area', { area : area }, function (response){
            $('select[name=city]').html(response).attr('disabled', false);
            $('select[name=store] option:first-child').attr('selected', 'selected');
        });
    }

    Me.googleSelectCity = function() {
        var area = $('select[name=area]').val();
        var city = $('select[name=city]').val();

        $('select[name=store] > option').text('LOADING');
        $('select[name=store]').attr('disabled', true);
        $.post(site_url+'gmap_query/select_google_city', { city : city, area : area }, function (response){
            $('select[name=store]').html(response).attr('disabled', false);
        });
    }

    Me.googleSelectStore = function() {
        var province = $('.areaa').val();
        var validate_city = $('.cityy').val();
        var validate_store = $('.store').val();

        if(!province) {
            $('.gmap-error').html('*Please select province.').show();
        } else {
            if(!validate_city) {
                $('.address').hide();
                $('.gmap-error').html('*Please select city/municipality.').show();
            } else {
                $('.gmap-error').html('').hide();
                if(validate_store == '11.646338452790836,122.72092948635861,a,a,a,a,a,5') {
                    $('.gmap-error').html('*Please select store.').show();
                } else {
                    $('.gmap-error').html('').hide();
                }
            }
        }

        var store = $('#store').val();
        var storeData = store.split('~');

        if(storeData[2] == 'a') {
            $('.address').hide();
        } else {
            $('#store_name').html(storeData[2]);
            $('#store_address').html(storeData[3]);
            $('#store_contact').html(storeData[4]);
            $('#store_code').html(storeData[5]);
            $('#store_hour').html(storeData[6]);
            $('.address').show();
        }
    }

    Me.thankYouPopUp = function(copy) {
        lytebox.load({url:site_url+"popups/thank_you", top:150, data:{content:copy}});
    }

}

function validateCVFile(oInput) {

    var _validFileExtensions = [".pdf"];
    var _element = $('#upload-cv-error');  

    var file_name = '';
    if (oInput.type == "file") {
        the_file = oInput.files[0];
        the_file_size = the_file.size;
        file_name = the_file.name;
        var sFileName = oInput.value;
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
             
            if (!blnValid) {
                _element.css('color', 'red');
                oInput.value = "";
                return false;
            }

            if(the_file_size > 2000000) {
                _element.css('color', 'red');
                oInput.value = "";
                return false;
            }

            _element.css('color', 'black');
        }
    }
}

function validatePortfolioFile(oInput) {

    var _validFileExtensions = [".pdf"];
    var _element = $('#upload-portfolio-error');  

    var file_name = '';
    if (oInput.type == "file") {
        the_file = oInput.files[0];
        the_file_size = the_file.size;
        file_name = the_file.name;
        var sFileName = oInput.value;
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
             
            if (!blnValid) {
                _element.css('color', 'red');
                oInput.value = "";
                return false;
            }

            if(the_file_size > 2000000) {
                _element.css('color', 'red');
                oInput.value = "";
                return false;
            }

            _element.css('color', 'black');
        }
    }
}