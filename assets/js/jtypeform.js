$.fn.jtypeform = function(options) {
    
    var Self = this;
    var typeForm = {};
    var settings;
    var steps = 0;

    settings = $.extend({
                onComplete : function(){},
                duration : 300,
                typeForm : typeForm
            }, options );

    Self.next = function(){
        typeForm.active = typeForm.list.children('.active');
        var next = typeForm.active.next('.item');
        var height = typeForm.children.not('.active').outerHeight();
        var index   = next.index();
        var ptop = (index*height)*-1 ;
        var isComplete = false;

        
        $('.error',typeForm.children).remove();

        
        var input = $('input, textarea, select',typeForm.active);

        if(input.attr('data-error') && $.trim(input.val())=='' ){
            input.focus();
            typeForm.active.append('<div class="error">'+input.data('error')+'</div>');
            return;
        }

        if(typeForm.active.is(':last-child')){
            settings.onComplete();
            // Self.clearForm();
            return;
        }

        var email = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

        if(input.attr('name') == 'email') {
            if(!email.test(input.val())) {
                input.focus();
                typeForm.active.append('<div class="error">Opps! You have entered an invalid email address</div>');
                return false;
            }
        }

        typeForm.children.removeClass('active');
        next.addClass('active');
        typeForm.list.css({top: ptop});

        var progressBar = $('.progress-bar').attr('aria-valuenow');
        var progressBarNow = parseInt(progressBar) + 17;
        steps = steps + 1;

        $('.progress').slideDown();

        $('.progress-bar').attr('aria-valuenow', progressBarNow).css('width', progressBarNow+'%');
        $('.progress-step').html(steps+' / 6');

        setTimeout(function(){
             $('input, textarea, select',next).focus();  
         },settings.duration)
    }

    Self.clearForm = function(){
        typeForm.inputs.val('');
        typeForm.list.css({ top: 0});
        typeForm.children.removeClass('active');
        typeForm.children.eq(0).addClass('active');
        $('.progress-bar').attr('aria-valuenow', '0').css('width', '0%');
        $('.progress-step').html('0 / 6');
        steps = 0;
        $('.progress').slideUp();
    }

    return this.each(function() {

        typeForm.elem = $(this);
        typeForm.list = typeForm.elem.children('ul');
        typeForm.children = typeForm.list.children();
        typeForm.inputs = $('input, textarea, select',typeForm.children);

        initialize(this);
    });

    function initialize(){

        if(typeForm.elem.hasClass('initialized')) return;
        typeForm.elem.addClass('initialized');
            
        typeForm.children.addClass('item')
        typeForm.children.eq(0).addClass('active');

        typeForm.list.css({
            top: 0,
            '-webkit-transition-duration': settings.duration+'ms',
            'transition-duration': settings.duration+'ms',
        });

        typeForm.inputs.each(function(){
            $(this)
            .attr('tabindex',-1)
            .keypress(function(e){
                if(getKeyCode(e)==13 && !e.shiftKey){
                    Self.next();
                }
            })
        })

        typeForm.elem.keypress(function(e){
                if(getKeyCode(e)==9){
                    e.preventDefault();
                    Self.next();
                }
            })

        Self.clearForm();

    }

    function getKeyCode(evt){
        return (evt.which) ? evt.which : evt.keyCode;
    }
 
};

function log(str){
    console.log(str);
}

(function () {
  "use strict";
  
  $ = this.jQuery || require('jquery');
  
  function offsetRelative(selector) {
    var $el = this;
    var $parent = $el.parent();
    if (selector) {
      $parent = $parent.closest(selector);
    }
 
    var elOffset     = $el.offset();
    var parentOffset = $parent.offset();
 
    return {
      left: elOffset.left - parentOffset.left,
      top: elOffset.top - parentOffset.top
    };
  }
  
  // Exports
  $.fn.offsetRelative || ($.fn.offsetRelative = offsetRelative);
  if (typeof module !== "undefined" && module !== null) {
    module.exports = $.fn.offsetRelative;
  }
}).call(this);