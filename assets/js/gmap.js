var my_current_address = 'Please wait.';
var myMarkers = [];
function init() {
    var Properties = {
        center: new google.maps.LatLng(11.646338452790836, 122.72092948635861),
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    geocoder = new google.maps.Geocoder();

    navigator.geolocation.getCurrentPosition(function(position){
        var geolocate = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        geocoder.geocode({'latLng':geolocate}, function(results, status){
            if(status == google.maps.GeocoderStatus.OK) {
                if(results[1]) {
                    console.log(results);
                    my_current_address = results[1].formatted_address;
                } else {
                    my_current_address = 'Sorry, we cannot find your location.';
                }
            } else {
                my_current_address = 'Sorry, an error occured. Please reload your browser.';
            }
        });
    });    

    var map = new google.maps.Map(document.getElementById('google_map'), Properties);

    for(i = 0; i < LatLong.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(LatLong[i][0], LatLong[i][1]),
            map: map,
            visible: false
        });

        var infowindow = new google.maps.InfoWindow({
                content:
                    '<div style="line-height:1.35; overflow:hidden; white-space:nowrap"><h5>' + LatLong[i][2] + '</h5></div>'
        });

        makeInfoWindowEvent(map, infowindow, marker);

        myMarkers.push(marker);
    }

    function makeInfoWindowEvent(map, infowindow, marker) {
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
      });
    }


    function locateStore(lat, long, zoom) {
        var location = new google.maps.LatLng(lat, long);
        map.panTo(location);
        map.setZoom(parseInt(zoom));
    }

    function myCurrentLocation() {

        navigator.geolocation.getCurrentPosition(function(position){
            var geolocate = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setCenter(geolocate);
            map.setZoom(16);
            var infowindow = new google.maps.InfoWindow({
                    map: map,
                    position: geolocate,
                    content:
                        '<div style="line-height:1.35; overflow:hidden; white-space:nowrap"><h5>' + my_current_address + '</h5></div>'
            });
            marker = new google.maps.Marker({
                    position: new google.maps.LatLng(geolocate.k, geolocate.B),
                    map: map,
                    title: 'My Current Location'
            });
            makeInfoWindowEvent(map, infowindow, marker);
        });
    }

    google.maps.event.addDomListener(find_nearby_store, 'click', function(){
        myCurrentLocation();
    });

    google.maps.event.addDomListener(find_nearby_store2, 'click', function(){
        myCurrentLocation();
    });

    google.maps.event.addDomListener(search_store, 'click', function(){
        console.log(location);
        var location = store.value.split('~');
        locateStore(location[0], location[1], location[7]);
    });

    google.maps.event.addDomListener(map, 'zoom_changed', function(){
        var zoom = map.getZoom();
        if(zoom >= 8) {
            for(i = 0; i < LatLong.length; i++) {
                myMarkers[i].setVisible(true);
            }
        } else {
            for(i = 0; i < LatLong.length; i++) {
                myMarkers[i].setVisible(false);
            }
        }
        
    });
}

google.maps.event.addDomListener(window, 'load', init);