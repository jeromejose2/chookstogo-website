
$(document).ready(function() {
      var screenWidth = $(window).width();

      /** Smooth Scrolling Functionality **/
                  function filterPath(string) {
                        return string
                          .replace(/^\//,'')
                          .replace(/(index|default).[a-zA-Z]{3,4}$/,'')
                          .replace(/\/$/,'');
                  }
                  var locationPath = filterPath(location.pathname);
                  var scrollElem = scrollableElement('html', 'body');
                  var urlHash = '#' + window.location.href.split("#")[1];

                  $('a[href*=#]').each(function() {
                        $(this).click(function(event) {

                        var thisPath = filterPath(this.pathname) || locationPath;
                        if (  locationPath == thisPath
                        && (location.hostname == this.hostname || !this.hostname)
                        && this.hash.replace(/#/,'') ) {
                          var $target = $(this.hash), target = this.hash;
                          if (target) {
                            var targetOffset = $target.offset().top;
                              event.preventDefault();
                              $(scrollElem).animate({scrollTop: targetOffset}, 400, function() {
                                location.hash = target;
                              });
                          }
                        }
                       });
                  });

                  // use the first element that is "scrollable"
                  function scrollableElement(els) {
                        for (var i = 0, argLength = arguments.length; i <argLength; i++) {
                          var el = arguments[i],
                              $scrollElement = $(el);
                          if ($scrollElement.scrollTop()> 0) {
                            return el;
                          } else {
                            $scrollElement.scrollTop(1);
                            var isScrollable = $scrollElement.scrollTop()> 0;
                            $scrollElement.scrollTop(0);
                            if (isScrollable) {
                              return el;
                            }
                          }
                        }
                        return [];
                  }
      /** END SMOOTH SCROLLING FUNCTIONALITY **/

      /** CLICK EVENTS **/
      $('.tab-content ul li a').on('click',function(e){
            e.preventDefault();
           $('.arrow-down').remove();
           $(this).addClass('active').parent().siblings().find('a').removeClass('active');
           $(this).parent().append('<i class="arrow-down"></i>');

      });
      $('.registration-tab ul li a').on('click',function(e){
            e.preventDefault();
            var showDiv = $(this).data('id');
            if(showDiv == '#login') {
                $('.heading h2').text('LOGIN');
                $('.heading span').text('May account ka na ba dito? Log-in na, Ka-Chooks, para tuloy ang kwela!');
            } else if(showDiv == '#registration') {
                $('.heading h2').text('REGISTRATION');
                $('.heading span').text('Gumawa ka na ng account para maging Ka-Chooks! Laging makwela at laging masaya dito!');
            }
           $('.arrow-down').remove();
           $(this).addClass('active').parent().siblings().find('a').removeClass('active');
           $(this).parent().append('<i class="arrow-down"></i>');
           $(showDiv).addClass('active').siblings().removeClass('active');

      });

    $('.product-details-btn').on('click',function(e){

            var animateHeight;
            if(screenWidth <= 767){
                animateHeight = 500
            }else if(screenWidth >= 768) {
                animateHeight = 300
            }
            var itemDetails = $(this).data();

            if(itemDetails.delivery == 1) {
                $('.product-details-content .order-share-buttons .btn-glyph-default').css('display', 'block').attr('target', '_blank');
                $('.product-details-content .order-share-buttons .btn-glyph').css('display', 'block').attr('target', '_blank');
            } else {
                $('.product-details-content .order-share-buttons .btn-glyph-default').css('display', 'none').attr('target', '');
                $('.product-details-content .order-share-buttons .btn-glyph').css('display', 'none').attr('target', '');
            }

            $('.product-details-content').animate({
                 height:animateHeight,
                 padding:'60px 0px'
            },100,function(){
                    $('.product-details-content .product-thumbnail img').attr('src',itemDetails.thumbnail);
                    $('.product-details-content .description h4').text(itemDetails.title);
                    $('.product-details-content .description > span.description').text(itemDetails.description);
                    $('.product-details-content .order-share-buttons .btn-glyph-default').attr('href',itemDetails.orderurl);
                    $('.product-details-content .order-share-buttons .btn-glyph').attr('href',itemDetails.orderurl);
                    $('.product-details-content .order-share-buttons .share-button').html('<fb:share-button href="'+itemDetails.fburl+'"data-type="box_count"></fb:share-button>');
                    FB.XFBML.parse();
            });
            $('html,body').animate({scrollTop: $("#product-details").offset().top});
    });

    $('.promo-details-btn').on('click',function(e){
            var shareLink;
            var animateHeight;
            if(screenWidth <= 767){
                animateHeight = 630
            }else if(screenWidth >= 768) {
                animateHeight = 400
            }
            var itemDetails = $(this).data();

            if(itemDetails.badge == 'Digital') {
                shareLink = itemDetails.gotourl;
            } else {
                shareLink = itemDetails.readmoreurl;
            }

            if(itemDetails.badge == 'Digital') {
                $('.btn-read-more').parent().css('display', 'none');
                $('.btn-goto-app').parent().css('display', 'block');
                if(itemDetails.openlink) {
                    $('.btn-goto-app').attr('target', '_blank');
                    $('.promo-gotoapp-btn').attr('target', '_blank');
                    $('.promo-readmore-btn').attr('target', '_blank');
                } else {
                    $('.btn-goto-app').attr('target', '');
                    $('.promo-gotoapp-btn').attr('target', '');
                    $('.promo-readmore-btn').attr('target', '');
                }
            } else {
                $('.btn-goto-app').parent().css('display', 'none');
                $('.btn-read-more').parent().css('display', 'block');
                if(itemDetails.openlink) {
                    $('.btn-goto-app').attr('target', '_blank');
                    $('.promo-gotoapp-btn').attr('target', '_blank');
                    $('.promo-readmore-btn').attr('target', '_blank');
                } else {
                    $('.btn-goto-app').attr('target', '');
                    $('.promo-gotoapp-btn').attr('target', '');
                    $('.promo-readmore-btn').attr('target', '');
                }
            }

            $('.promo-details-content').animate({
                 height:animateHeight,
                 padding:'40px 0px'
            },120,function(){
                    $('.promo-details-content .promo-thumbnail img').attr('src',itemDetails.thumbnail);
                    $('.promo-details-content .description h2').text(itemDetails.title);
                    $('.promo-details-content .description > span.description').text(itemDetails.description);
                    $('.promo-details-content .description > span.promo-date').text(itemDetails.date);
                    $('.promo-details-content .description > span.badge-cat').text(itemDetails.badge);
                    $('.promo-details-content .order-share-buttons .btn-read-more').attr('href',itemDetails.readmoreurl);
                    $('.promo-details-content .order-share-buttons .btn-goto-app').attr('href',itemDetails.gotourl);
                    $('.promo-details-content .order-share-buttons .promo-readmore-btn').attr('href',itemDetails.readmoreurl);
                    $('.promo-details-content .order-share-buttons .promo-gotoapp-btn').attr('href',itemDetails.gotourl);
                    $('.promo-details-content .order-share-buttons .share-button').html('<fb:share-button href="'+shareLink+'" data-type="box_count"></fb:share-button>');
                    FB.XFBML.parse();
            });
            $('html,body').animate({scrollTop: $("#product-details").offset().top});


    });

      $('.komiks-img').hover( function() {
          $(this).find('.hover-enlarge').animate({top:0});
      }, function() {
          $(this).find('.hover-enlarge').animate({top:300});
      });

     $('.read-profile').on('click',function(e){
                e.preventDefault();
                var buttonEl = $(this).siblings()
                var id = $(this).data('id');
                $(this).parent().parent().siblings('div.person-profile').slideToggle('slow');
                if( $('.glyphicon', buttonEl).hasClass('glyphicon-chevron-right') ){
                  $('#profile-preview-'+id).hide('slow');
                  $('#profile-designation-'+id).show();
                  $('.glyphicon', buttonEl).addClass('glyphicon-chevron-down')
                  $('.glyphicon', buttonEl).removeClass('glyphicon-chevron-right')
                }else{
                  $('#profile-preview-'+id).show('slow');
                  $('#profile-designation-'+id).hide();
                  $('.glyphicon', buttonEl).removeClass('glyphicon-chevron-down')
                  $('.glyphicon', buttonEl).addClass('glyphicon-chevron-right')
                }

     })

      /** END CLICK EVENTS **/

      /** SLIDERS **/


      $("#home-banner").owlCarousel({
        navigation: true, // Show next and prev buttons
        navigationText: ["<i class='glyphicon glyphicon-chevron-left'></i>", "<i class='glyphicon glyphicon-chevron-right'></i>"],
        paginationSpeed: 800,
        singleItem: true,
        autoPlay: 10000
        // "singleItem:true" is a shortcut for:
        // items : 1,
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false
    });
    $("#announcement-banner").owlCarousel({
        navigation: false, // Show next and prev buttons
        navigationText: ["<i class='glyphicon glyphicon-chevron-left'></i>", "<i class='glyphicon glyphicon-chevron-right'></i>"],
        paginationSpeed: 800,
        singleItem: true,
        autoPlay: 10000
        // "singleItem:true" is a shortcut for:
        // items : 1,
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

    });


    $(".cat-product").owlCarousel({
        navigation: true, // Show next and prev buttons
        navigationText: ["<i class='glyphicon glyphicon-chevron-left'></i>", "<i class='glyphicon glyphicon-chevron-right'></i>"],
        paginationSpeed: 800,
        autoPlay: true,
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3],
        itemsTablet: [768,3],
        itemsMobile :[480,2]



    });

    $(".cat-promos").owlCarousel({
        navigation: true, // Show next and prev buttons
        navigationText: ["<i class='glyphicon glyphicon-chevron-left'></i>", "<i class='glyphicon glyphicon-chevron-right'></i>"],
        paginationSpeed: 800,
        autoPlay: true,
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3],
        itemsTablet: [768,3],
        itemsMobile :[520,1]



    });


    /**END SLIDERS **/

    if(pid) {
        setTimeout(function(){
            $('.product-details-btn-'+pid).trigger('click');
        }, 1000);
    }

    if(pcat) {
      console.log(pcat);
        setTimeout(function(){
            $('#'+pcat).trigger('click');
        }, 500);
    }

});
