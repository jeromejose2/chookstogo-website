if(reCaptcha) {
    Recaptcha.create("6Lf8ifMSAAAAAInKgflYhLoywUpnhmR-qitsuFHS", "recaptcha_id", {
      theme: "white",
      callback: Recaptcha.focus_response_field});

    $('input[name=mobile]').keypress(validateNumber);
    $('input[name=firstname], input[name=lastname]').keypress(validateLetters);
}

if(is_forgot) {
    $('#new-password').trigger('click');
}

if(confirmed_user) {
    chooks.thankYouPopUp('Confirmed na ang account mo! Member ka na rin ng super kwelang Ka-Chooks Club!');
}

function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;

    if (event.keyCode == 8 || event.keyCode == 46
     || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 9) {
        return true;
    }
    else if ( key < 48 || key > 57) {
        return false;
    }
    else return true;
}

function validateLetters(event) {
    	var key = event.which;
      console.log(key);
  	return ((key >= 65 && key <= 122) || key == 8 || key == 32 || key == 46 || key == 0);
}

function _ga(param) {

    console.log(param);

    ga('send', {
          'hitType': 'event',
          'eventCategory': param,
          'eventAction': param,
          'eventLabel': param,
          'eventValue': '1'
      });
}

$('.chikoys-tab a').on('click', chooks.chikoyTabChange);
$('.chikoys-tab-responsive').on('change', chooks.chikoySelectChange);
$('.product-tab a').on('click', chooks.productTabChange);
$('.product-select').on('change', chooks.productSelectChange);
$('.promo-tab-ajax li').click(chooks.promoTabChange);
$('.select-promotion').on('change', chooks.promoSelectChange);
$('.promo-tab-select a').on('click', chooks.blogTabChange);
$('.promo-select-select').on('change', chooks.blogSelectChange);
$('.apply-career').on('click', chooks.applicationFormPopup);

$('._hide').hide();