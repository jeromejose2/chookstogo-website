<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

    protected $data = array();

    public function __construct()
    {
        parent::__construct();
        header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');
        $this->load->library('facebook', array(
            'appId'  => FB_APP_ID,
            'secret' => FB_SECRET,
            'fileUpload' => true
        ));
        $this->_fb_auth();
    }

    protected function _fb_auth()
    {
        $this->data['url'] = false;
        $this->data['user_id'] = $this->facebook->getUser();
        $this->data['liked'] = true;
        if($this->data['user_id'] === 0){
            $this->data['url'] = $this->facebook->getLoginUrl(array(
                'scope' => 'user_likes,publish_stream,user_birthday,user_hometown,email',
                'display' => 'page',
                'redirect_uri' => FB_REDIRECT_URI
            ));
        }
        $this->facebook->setExtendedAccessToken();
        $s_request = $this->facebook->getSignedRequest();
        if(isset($s_request['page']['liked']) && !$s_request['page']['liked']){
            $this->data['liked'] = false;
        }
    }

    public function register()
    {
        $now = date('Y-m-d H:i:s');
        $this->output->set_content_type('application/json');
        $data = array(
            'fbid' => $this->facebook->getUser(),
            'first_name' => (string) $this->input->post('first_name'),
            'last_name' => (string) $this->input->post('last_name'),
            'email' => (string) $this->input->post('email'),
            'password' => (string) $this->input->post('email'),
            // 'password' => (string) $this->input->post('password'),
            'gender' => strtoupper((string) $this->input->post('gender')),
            'birthdate' => (string) $this->input->post('birthday'),
            'mobile_num' => (string) $this->input->post('mobile_num'),
            'date_created' => date('Y-m-d H:i:s')
        );
        $this->db->insert('tbl_registrants', $data);
        if($this->db->affected_rows()){
            $entry = array(
                'fbid' => $data['fbid'],
                'entry_date' => $now,
                'entry_code' => unique_code().'-'.$data['fbid']
            );
            $this->db->insert('tbl_entries', $entry);
            if($this->db->affected_rows()){
                $this->output->set_output(json_encode(array('success' => true, 'id' => $data['fbid'])));
            }
        }
        $this->output->set_output(json_encode(array('success' => false)));
    }

    public function fb_share()
    {

    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */