<?php

function time_elapsed_string($ptime, $currentTime = null) {
    if (!$currentTime) {
        $currentTime = time();
    }
    $etime = $currentTime - $ptime;
    
    if ($etime < 1) {
        return '0 seconds';
    }
    
    $a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'minute',
                1                       =>  'second'
                );
    
    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . $str . ($r > 1 ? 's' : '');
        }
    }
}

/**
 * Format date from MySql format to word format
 * @author anthony
 * @param string $date
 * @param string $format
 * @return string
 */
function format_date($date, $format = "F d, Y")
{
    $hour = 0;
    $min = 0;
    $sec = 0;
    $date = explode('-', $date);
    $day = isset($date[2]) ? $date[2] : '00';
    if(strlen($day) > 2){
        $day = explode(' ', $day);
        if(isset($day[1])){
            $time = explode(':', $day[1]);
            $hour = (int) $time[0];
            $min = (int) $time[1];
            $sec = (int) $time[2];
        }
        $day = $day[0];
    }
    $month = isset($date[1]) ? $date[1] : '00';
    $year = isset($date[0]) ? $date[0] : '0000';
    return date($format, mktime($hour, $min, $sec, $month, $day, $year));
}

/**
 * 
 * @author anthony
 * @param string $string
 * @param int $flag
 * @return string
 */
function html($string, $flag = ENT_QUOTES)
{
    // return htmlspecialchars((string) $string, $flag);
    // $string = utf8_encode($string);
    return htmlentities((string) $string);
}

/**
 * 
 * @author anthony
 * @param string $string
 * @return string
 */
function title_case($string)
{
    $string = str_replace('-quote-', "'", $string);
    return ucwords(str_replace('_', ' ', strtolower($string)));
}

/**
 * 
 * @author anthony
 * @param mixed $var
 * @param boolean $vardump
 * @return void
 */
if (!function_exists('debug')) {
    function debug($var, $vardump = false)
    {
        echo '<pre>';
        if (!$vardump) {
            if (empty($var)) {
                if (is_array($var)) {
                    $var = "[[ Empty array ]]";
                } elseif (is_string($var)) {
                    $var = "[[ Empty string ]]";
                } elseif (is_bool($var)) {
                    $var = "[[ Bool: false ]]";
                } elseif (is_null($var)) {
                    $var = "[[ NULL ]]";
                }
            }
            print_r($var);
        } else {
            var_dump($var);
        }
        echo '</pre>';
    }
}


/**
 * 
 * @return string
 */
function select_days($firstText = 'Day')
{
    $string = "<option value=''>{$firstText}</option>";
    foreach(range(1, 31) as $day){
        $string .= "<option value='".str_pad($day, 2, "0", STR_PAD_LEFT)."'>{$day}</option>";
    }
    return $string;
}

# generate pagination links
function pagination($total_rows, $cur_page, $limit, $base_url, $useBootstrap = false)
{
    $ci = & get_instance();
    $ci->load->library('pagination');
    $settings = array(
        "total_rows"  => $total_rows,
        "base_url"    => $base_url,
        "cur_page"    => $cur_page,
        "per_page"    => $limit
    );
    
    $settings['page_query_string'] = TRUE;
    $settings['use_page_numbers'] = TRUE;
    $settings['query_string_segment'] = 'page';

    if ($useBootstrap) {
        $settings['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
        $settings['full_tag_close'] = '</ul></div><!--pagination-->';
         
        $settings['first_link'] = '&laquo; First';
        $settings['first_tag_open'] = '<li class="prev page">';
        $settings['first_tag_close'] = '</li>';
         
        $settings['last_link'] = 'Last &raquo;';
        $settings['last_tag_open'] = '<li class="next page">';
        $settings['last_tag_close'] = '</li>';
         
        $settings['next_link'] = 'Next &rarr;';
        $settings['next_tag_open'] = '<li class="next page">';
        $settings['next_tag_close'] = '</li>';
         
        $settings['prev_link'] = '&larr; Previous';
        $settings['prev_tag_open'] = '<li class="prev page">';
        $settings['prev_tag_close'] = '</li>';
         
        $settings['cur_tag_open'] = '<li class="active"><a href="">';
        $settings['cur_tag_close'] = '</a></li>';
         
        $settings['num_tag_open'] = '<li class="page">';
        $settings['num_tag_close'] = '</li>';
    }
    
    
    $ci->pagination->initialize($settings);
    $pagination_links = $ci->pagination->create_links();
    return $pagination_links;
}


/**
 * 
 * @return string
 */
function select_months($firstText = 'Month')
{
    return "<option value=''>{$firstText}</option>\n
    <option value='01'>Jan</option>\n
    <option value='02'>Feb</option>\n
    <option value='03'>Mar</option>\n
    <option value='04'>Apr</option>\n
    <option value='05'>May</option>\n
    <option value='06'>June</option>\n
    <option value='07'>July</option>\n
    <option value='08'>Aug</option>\n
    <option value='09'>Sept</option>\n
    <option value='10'>Oct</option>\n
    <option value='11'>Nov</option>\n
    <option value='12'>Dec</option>";
}

/**
 * 
 * @return string
 */
function select_province()
{
    return "<option value=''>Select Province</option>" . 
    "<option value='Abra'>Abra</option>" .
    "<option value='AgusandelNorte'>Agusan del Norte</option>" .
    "<option value='AgusandelSur'>Agusan del Sur</option>" .
    "<option value='Aklan'>Aklan</option>" .
    "<option value='Albay'>Albay</option>" .
    "<option value='Antique'>Antique</option>" .
    "<option value='Apayao'>Apayao</option>" .
    "<option value='Aurora'>Aurora</option>" .
    "<option value='Basilan'>Basilan</option>" .
    "<option value='Bataan'>Bataan</option>" .
    "<option value='Batanes'>Batanes</option>" .
    "<option value='Batangas'>Batangas</option>" .
    "<option value='Benguet'>Benguet</option>" .
    "<option value='Biliran'>Biliran</option>" .
    "<option value='Bohol'>Bohol</option>" .
    "<option value='Bukidnon'>Bukidnon</option>" .
    "<option value='Bulacan'>Bulacan</option>" .
    "<option value='Cagayan'>Cagayan</option>" .
    "<option value='CamarinesNorte'>Camarines Norte</option>" .
    "<option value='CamarinesSur'>Camarines Sur</option>" .
    "<option value='Camiguin'>Camiguin</option>" .
    "<option value='Capiz'>Capiz</option>" .
    "<option value='Catanduanes'>Catanduanes</option>" .
    "<option value='Cavite'>Cavite</option>" .
    "<option value='Cebu'>Cebu</option>" .
    "<option value='CompostellaValley'>Compostela Valley</option>" .
    "<option value='Cotabato'>Cotabato</option>" .
    "<option value='DavaoDelNorte'>Davao del Norte</option>" .
    "<option value='DavaoDelSur'>Davao del Sur</option>" .
    "<option value='DavaoOriental'>Davao Oriental</option>" .
    "<option value='DinagatIslands'>Dinagat Islands</option>" .
    "<option value='EasternSamar'>Eastern Samar</option>" .
    "<option value='Guimaras'>Guimaras</option>" .
    "<option value='Ifugao'>Ifugao</option>" .
    "<option value='IlocosNorte'>Ilocos Norte</option>" .
    "<option value='IlocosSur'>Ilocos Sur</option>" .
    "<option value='Iloilo'>Iloilo</option>" .
    "<option value='Isabela'>Isabela</option>" .
    "<option value='Kalinga'>Kalinga</option>" .
    "<option value='LaUnion'>La Union</option>" .
    "<option value='Laguna'>Laguna</option>" .
    "<option value='LanaoDelNorte'>Lanao del Norte</option>" .
    "<option value='LanaoDelSur'>Lanao del Sur</option>" .
    "<option value='Leyte'>Leyte</option>" .
    "<option value='Maguindanao'>Maguindanao</option>" .
    "<option value='Marinduque'>Marinduque</option>" .
    "<option value='Masbate'>Masbate</option>" .
    "<option value='MetroManila'>Metro Manila</option>" .
    "<option value='MisamisOccidental'>Misamis Occidental</option>" .
    "<option value='MisamisOriental'>Misamis Oriental</option>" .
    "<option value='MountainProvince'>Mountain Province</option>" .
    "<option value='NegrosOccidental'>Negros Occidental</option>" .
    "<option value='NegrosOriental'>Negros Oriental</option>" .
    "<option value='NorthernSamar'>Northern Samar</option>" .
    "<option value='NuevaEcija'>Nueva Ecija</option>" .
    "<option value='NuevaVizcaya'>Nueva Vizcaya</option>" .
    "<option value='OccidentalMindoro'>Occidental Mindoro</option>" .
    "<option value='OrientalMindoro'>Oriental Mindoro</option>" .
    "<option value='Palawan'>Palawan</option>" .
    "<option value='Pampanga'>Pampanga</option>" .
    "<option value='Pangasinan'>Pangasinan</option>" .
    "<option value='Quezon'>Quezon</option>" .
    "<option value='Quirino'>Quirino</option>" .
    "<option value='Rizal'>Rizal</option>" .
    "<option value='Roblon'>Romblon</option>" .
    "<option value='Samar'>Samar</option>" .
    "<option value='Saranagani'>Sarangani</option>" .
    "<option value='ShariffKabunsuan'>Shariff Kabunsuan</option>" .
    "<option value='Siquijor'>Siquijor</option>" .
    "<option value='Sorsogon'>Sorsogon</option>" .
    "<option value='SouthCotabato'>South Cotabato</option>" .
    "<option value='SouthernLeyte'>Southern Leyte</option>" .
    "<option value='SultanKudarat'>Sultan Kudarat</option>" .
    "<option value='Sulu'>Sulu</option>" .
    "<option value='SurigaoDelNorte'>Surigao del Norte</option>" .
    "<option value='SurigaoDelSur'>Surigao del Sur</option>" .
    "<option value='Tarlac'>Tarlac</option>" .
    "<option value='TawiTawi'>Tawi-Tawi</option>" .
    "<option value='Zambales'>Zambales</option>" .
    "<option value='ZamboangaDelNorte'>Zamboanga del Norte</option>" .
    "<option value='ZamboangaDelSur'>Zamboanga del Sur</option>" .
    "<option value='ZamboangaSibugay'>Zamboanga Sibugay</option>";
}

/**
 * 
 * @return string
 */
function select_years($start = 1994, $end = 1920, $firstText = 'Year')
{
    $string = "<option value=''>{$firstText}</option>";
    foreach(range((int) $start, (int) $end) as $year){
        $string .= "<option value='{$year}'>{$year}</option>";
    }
    return $string;
}

/**
 * 
 * @return string
 */
function select_countries()
{
    $country_list = array(
        "Afghanistan",
        "Albania",
        "Algeria",
        "Andorra",
        "Angola",
        "Antigua and Barbuda",
        "Argentina",
        "Armenia",
        "Australia",
        "Austria",
        "Azerbaijan",
        "Bahamas",
        "Bahrain",
        "Bangladesh",
        "Barbados",
        "Belarus",
        "Belgium",
        "Belize",
        "Benin",
        "Bhutan",
        "Bolivia",
        "Bosnia and Herzegovina",
        "Botswana",
        "Brazil",
        "Brunei",
        "Bulgaria",
        "Burkina Faso",
        "Burundi",
        "Cambodia",
        "Cameroon",
        "Canada",
        "Cape Verde",
        "Central African Republic",
        "Chad",
        "Chile",
        "China",
        "Colombi",
        "Comoros",
        "Congo (Brazzaville)",
        "Congo",
        "Costa Rica",
        "Cote d'Ivoire",
        "Croatia",
        "Cuba",
        "Cyprus",
        "Czech Republic",
        "Denmark",
        "Djibouti",
        "Dominica",
        "Dominican Republic",
        "East Timor (Timor Timur)",
        "Ecuador",
        "Egypt",
        "El Salvador",
        "Equatorial Guinea",
        "Eritrea",
        "Estonia",
        "Ethiopia",
        "Fiji",
        "Finland",
        "France",
        "Gabon",
        "Gambia, The",
        "Georgia",
        "Germany",
        "Ghana",
        "Greece",
        "Grenada",
        "Guatemala",
        "Guinea",
        "Guinea-Bissau",
        "Guyana",
        "Haiti",
        "Honduras",
        "Hungary",
        "Iceland",
        "India",
        "Indonesia",
        "Iran",
        "Iraq",
        "Ireland",
        "Israel",
        "Italy",
        "Jamaica",
        "Japan",
        "Jordan",
        "Kazakhstan",
        "Kenya",
        "Kiribati",
        "Korea, North",
        "Korea, South",
        "Kuwait",
        "Kyrgyzstan",
        "Laos",
        "Latvia",
        "Lebanon",
        "Lesotho",
        "Liberia",
        "Libya",
        "Liechtenstein",
        "Lithuania",
        "Luxembourg",
        "Macedonia",
        "Madagascar",
        "Malawi",
        "Malaysia",
        "Maldives",
        "Mali",
        "Malta",
        "Marshall Islands",
        "Mauritania",
        "Mauritius",
        "Mexico",
        "Micronesia",
        "Moldova",
        "Monaco",
        "Mongolia",
        "Morocco",
        "Mozambique",
        "Myanmar",
        "Namibia",
        "Nauru",
        "Nepal",
        "Netherlands",
        "New Zealand",
        "Nicaragua",
        "Niger",
        "Nigeria",
        "Norway",
        "Oman",
        "Pakistan",
        "Palau",
        "Panama",
        "Papua New Guinea",
        "Paraguay",
        "Peru",
        "Philippines",
        "Poland",
        "Portugal",
        "Qatar",
        "Romania",
        "Russia",
        "Rwanda",
        "Saint Kitts and Nevis",
        "Saint Lucia",
        "Saint Vincent",
        "Samoa",
        "San Marino",
        "Sao Tome and Principe",
        "Saudi Arabia",
        "Senegal",
        "Serbia and Montenegro",
        "Seychelles",
        "Sierra Leone",
        "Singapore",
        "Slovakia",
        "Slovenia",
        "Solomon Islands",
        "Somalia",
        "South Africa",
        "Spain",
        "Sri Lanka",
        "Sudan",
        "Suriname",
        "Swaziland",
        "Sweden",
        "Switzerland",
        "Syria",
        "Taiwan",
        "Tajikistan",
        "Tanzania",
        "Thailand",
        "Togo",
        "Tonga",
        "Trinidad and Tobago",
        "Tunisia",
        "Turkey",
        "Turkmenistan",
        "Tuvalu",
        "Uganda",
        "Ukraine",
        "United Arab Emirates",
        "United Kingdom",
        "United States",
        "Uruguay",
        "Uzbekistan",
        "Vanuatu",
        "Vatican City",
        "Venezuela",
        "Vietnam",
        "Yemen",
        "Zambia",
        "Zimbabwe"
    );
    return "<option>".implode("</option><option>", $country_list)."</option>";
}

/**
 * 
 * @param string $view
 * @param array $data
 * @param string $layout
 * @return void
 */
function view($view, $data = array(), $layout = 'layout')
{
    $ci = & get_instance();
    $data['content'] = $ci->load->view($view, $data, true);
    $ci->load->view($layout, $data);
}

function unique_code($length = 8)
{
    return strtoupper(substr(md5(uniqid(mt_rand(), true)), 0, $length));
}